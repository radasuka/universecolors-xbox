//-----------------------------------------------------------------------------
// ShatterEffect.fx
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------


float4x4 WorldViewProjection;
float4x4 World : World;
float RotationAmount;
float TranslationAmount;
texture modelTexture;
float time;

// ライティング データ
float3 eyePosition;
float3 lightPosition;
float4 ambientColor;
float4 diffuseColor;
float4 specularColor;
float specularPower;
float4 emissiveColor;


sampler TextureSampler = sampler_state
{
    Texture = <modelTexture>;
    MinFilter = Anisotropic;
    MagFilter = LINEAR;
    MipFilter = LINEAR;
    MaxAnisotropy = 8;
    AddressU  = WRAP;
    AddressV  = WRAP;
};

struct VertexShaderOutput
{
     float4 Position : POSITION;
     float3 WorldNormal : TEXCOORD0;
     float3 WorldPosition : TEXCOORD1;
     float2 texCoords : TEXCOORD2;
     float4 Color : COLOR0;
};

struct PixelShaderInput
{
     float3 WorldNormal : TEXCOORD0;
     float3 WorldPosition : TEXCOORD1;
     float2 TexCoords : TEXCOORD2;
     float4 Color: COLOR0;
};

struct InputVS
{
    float3 Position : POSITION;
    float3 Normal : NORMAL;
    float2 TexCoords : TEXCOORD0;
    float3 TriangleCenter : TEXCOORD1;
    float3 RotationalVelocity : TEXCOORD2;        
};

// YawPitchRoll 行列を作成するためのヘルパー関数
// プロセッサで生成されたランダムな回転値で頂点を回転させるために使用されます。
float4x4 CreateYawPitchRollMatrix(float x, float y, float z)
{
    float4x4 result;
        
    result[0][0] = cos(z)*cos(y) + sin(z)*sin(x)*sin(y);
    result[0][1] = -sin(z)*cos(y) + cos(z)*sin(x)*sin(y);
    result[0][2] = cos(x)*sin(y);
    result[0][3] = 0;
    
    result[1][0] = sin(z)*cos(x);
    result[1][1] = cos(z)*cos(x);
    result[1][2] = -sin(x);
    result[1][3] = 0;
    
    result[2][0] = cos(z)*-sin(y) + sin(z)*sin(x)*cos(y);
    result[2][1] = sin(z)*sin(y) + cos(z)*sin(x)*cos(y);
    result[2][2] = cos(x)*cos(y);
    result[2][3] = 0;
    
    result[3][0] = 0;
    result[3][1] = 0;
    result[3][2] = 0;
    result[3][3] = 1;    

    return result;
}

VertexShaderOutput ShatterVS(InputVS input) 
{
    VertexShaderOutput output = (VertexShaderOutput)0;    
    
    // 粉砕計算
    //
    // 粉砕エフェクトは比較的単純です。まず、プロセッサで生成したランダム値に基づいて 
    // YawPitchRoll 行列を作成します。次に、この回転行列を使用してトライアングルの中心を
    // 軸に回転するように頂点の位置をトランスフォームします。その後、頂点をその法線に沿って、
    // 外側に向かって移動させます。最後に、時間の 2 乗の関数で頂点をその Y 軸に沿って落下させ、
    // 落下エフェクトを生み出します。

    
    // 回転行列を作成します
    input.RotationalVelocity *= RotationAmount;
    float4x4 rotMatrix = CreateYawPitchRollMatrix(input.RotationalVelocity.x, 
                                                  input.RotationalVelocity.y, 
                                                  input.RotationalVelocity.z);
    
    // 頂点のトライアングルの中心点を軸にして頂点を回転させます
    float3 position = input.TriangleCenter + mul(input.Position - input.TriangleCenter, 
                                            rotMatrix);
    
    // その法線に沿って頂点を移動させます
    position += input.Normal * TranslationAmount;    
    
    // 時間の 2 乗の関数で頂点を下向きに移動させ、適切な落下のエフェクトを生み出します。
    position.y -= time*time * 200;                 
    
    // 通常どおり進行します
    
    output.Position = mul(float4(position,1.0),WorldViewProjection);
    
    // ライティングを正確に計算するために、法線も回転させる必要があります。
    output.WorldNormal = mul(mul(input.Normal, rotMatrix), World); 
    float4 worldPosition = mul(float4(position,1.0),World);
    output.WorldPosition = worldPosition / worldPosition.w;
    output.texCoords = input.TexCoords;
    
    // ディフューズ成分を計算します
    float3 directionToLight = normalize(lightPosition - output.WorldPosition);
    float diffuseIntensity = saturate( dot(directionToLight, output.WorldNormal));
    float4 diffuse = diffuseColor * diffuseIntensity;
    
    output.Color = diffuse + ambientColor;
    
    return output;
}

float4 PhongPS(PixelShaderInput input) : COLOR
{
     float3 directionToLight = normalize(lightPosition - input.WorldPosition);
     float3 reflectionVector = normalize(reflect(-directionToLight, input.WorldNormal));
     float3 directionToCamera = normalize(eyePosition - input.WorldPosition);
     
     // スペキュラー成分を計算します
     float4 specular = specularColor *  
                       pow( saturate(dot(reflectionVector, directionToCamera)), 
                       specularPower);
     
     float4 TextureColor   = tex2D(TextureSampler, input.TexCoords);
     
     float4 color = TextureColor * input.Color + specular + emissiveColor;
     color.a = 1.0;                   
          
     return color;
}

technique
{
    pass
    {        
        VertexShader = compile vs_2_0 ShatterVS();
        PixelShader = compile ps_2_0 PhongPS();
    }
}