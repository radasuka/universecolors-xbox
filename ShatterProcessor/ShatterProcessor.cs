#region File Description
//-----------------------------------------------------------------------------
// ShatterProcessor.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
#endregion

namespace ShatterEffectProcessor
{
    [ContentProcessor]
    public class ShatterProcessor : ModelProcessor
    {
        private string triangleCenterChannel = VertexChannelNames.TextureCoordinate(1);
        private string rotationalVelocityChannel = 
            VertexChannelNames.TextureCoordinate(2);
      
        public override ModelContent Process(NodeContent input, 
                                             ContentProcessorContext context)
        {
            // インデックスを解除して、独立したトライアングルにメッシュを分解します。      
            NodeContent processedNode = ProcessMesh(input); 
            return base.Process(processedNode, context);
        }

        /// <summary>
        /// 独立した、インデックス化されていないトライアングルに入力メッシュを分解します。
        /// </summary>
        /// <param name="input">入力 MeshContent ノード。</param>
        /// <returns>インデックスを解除して、分解された MeshContent</returns>
        private MeshContent ProcessMesh(NodeContent input)
        {
            MeshBuilder builder = MeshBuilder.StartMesh("model");
            
            MeshContent mesh = input as MeshContent;
            List<Vector3> normalList = new List<Vector3>();
            List<Vector2> texCoordList = new List<Vector2>();

            if (mesh != null)
            {
                int normalChannel = builder.CreateVertexChannel<Vector3>(
                                               VertexChannelNames.Normal());
                int texChannel = builder.CreateVertexChannel<Vector2>(
                                               VertexChannelNames.TextureCoordinate(0));

                foreach (GeometryContent geometry in mesh.Geometry)
                {
                    IndirectPositionCollection positions = geometry.Vertices.Positions;

                    VertexChannel<Vector3> normals =
                        geometry.Vertices.Channels.Get<Vector3>(
                                                        VertexChannelNames.Normal());
                    
                    VertexChannel<Vector2> texCoords =
                        geometry.Vertices.Channels.Get<Vector2>(
                                            VertexChannelNames.TextureCoordinate(0));

                    // 各頂点の位置をコピーします。
                    // それを行うために、インデックスをスキャンし、インデックス化されている
                    // 位置を取得し、その位置を新しいメッシュに追加します。これは実際には、
                    // メッシュ内で頂点を重複させ、インデックス バッファーの使用による
                    // 圧縮効果を打ち消します。
                    foreach (int i in geometry.Indices)
                    {
                        builder.CreatePosition(positions[i]);

                        // 後でメッシュに追加する法線とテクスチャー座標を保存します。
                        normalList.Add(normals[i]);
                        texCoordList.Add(texCoords[i]);
                    }
                }

                int index = 0;

                foreach (GeometryContent geometry in mesh.Geometry)
                {
                    // マテリアルを新しいメッシュに保存します。
                    builder.SetMaterial(geometry.Material);

                    // ここで、トライアングルを作成します。
                    // それを行うために、0 から geometry.Indices.Count まで連続する
                    // インデックス リストを生成します。これにより、0,1,2,3,4,5,... 
                    // のようなインデックス バッファーが作成されます。
                    for (int i = 0; i < geometry.Indices.Count; i++)
                    {
                        // 現在の頂点の法線を設定します
                        builder.SetVertexChannelData(normalChannel, normalList[index]);
                        // 現在の頂点のテクスチャー座標を設定します
                        builder.SetVertexChannelData(texChannel, texCoordList[index]);
                        builder.AddTriangleVertex(index);
                        index++;
                    }
                }
            }

            MeshContent finalMesh = builder.FinishMesh();
            // 親/子の相対的なトランスフォームを保持するために、ソース メッシュから
            // トランスフォームをコピーします。
            finalMesh.Transform = input.Transform; 

            // ここで、新しい MeshContent を受け取り、すべてのトライアングルの中心を
            // 計算します。中心点はモデルを粉砕する過程でトライアングルを回転させるために
            // 必要です。
            foreach (GeometryContent geometry in finalMesh.Geometry)
            {                   
                Vector3[] triangleCenters = new Vector3[geometry.Indices.Count / 3];
                Vector3[] trianglePoints = new Vector3[2];
                
                IndirectPositionCollection positions = geometry.Vertices.Positions;
                
                for (int i = 0; i < positions.Count; i++)
                {
                    Vector3 position = positions[i];

                    if (i % 3 == 2)
                    {
                        // トライアングルの中心を計算します。
                        triangleCenters[i / 3] = (trianglePoints[0] + trianglePoints[1]
                                                 + position) / 3;
                    }
                    else
                    {
                        trianglePoints[i % 3] = position;
                    }
                }

                // 2 つの新しいチャンネルを MeshContent に追加します。
                // triangleCenterChannel : これは、この頂点が属するトライアングルの
                // 中心を格納するチャンネルです。
                // rotationalVelocityChannel : このチャンネルは、x、y、z の回転角度として
                // ランダムに生成された値を保持します。この情報は、トライアングルがモデルから
                // 飛び散る過程でトライアングルを不規則に回転させるために使用されます。
                geometry.Vertices.Channels.Add<Vector3>(
                    triangleCenterChannel, 
                    new ReplicateTriangleDataToEachVertex <Vector3>(triangleCenters));
                geometry.Vertices.Channels.Add<Vector3>(
                    rotationalVelocityChannel, 
                    new ReplicateTriangleDataToEachVertex <Vector3>(
                    new RandomVectorEnumerable(triangleCenters.Length)));                
            }

            foreach (NodeContent child in input.Children)
            {
                finalMesh.Children.Add(ProcessMesh(child));
            }

            return finalMesh;
        }

        
        
        /// <summary>
        /// 独自の ShatterEffect で BasicEffect を置き換えられるように、
        /// ModelProcessor の ConvertMaterial 関数をオーバーライドします。   
        /// </summary>
        /// <param name="material">入力マテリアル</param>
        /// <param name="context">コンテンツ プロセッサ コンテキスト</param>
        /// <returns></returns>
        protected override MaterialContent ConvertMaterial(MaterialContent material, 
                                                    ContentProcessorContext context)
        {
            EffectMaterialContent effect = new EffectMaterialContent();
            // BasicEffect の代わりに独自の ShatterEffect.fx を使用します。
            effect.Effect = 
                new ExternalReference<EffectContent>("shatterEffect.fx"); 

            foreach (ExternalReference<TextureContent> texture in 
                                                            material.Textures.Values)
            {
                // ソース マテリアル内のテクスチャーをエフェクトに追加します。
                effect.Textures.Add("modelTexture", texture); 
            }
            return base.ConvertMaterial(effect, context);
        }      
    }
}
