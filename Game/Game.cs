#region File Description
//-----------------------------------------------------------------------------
// Game.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using GameDebugTools;
using Microsoft.Xna.Framework;
using UniverseColors.Screens;
using UniverseColors.PlayGameScreens;
#endregion

namespace UniverseColors
{
    /// <summary>
    /// このメインのゲーム クラスはきわめて単純です。すべての興味深い処理は
    /// ScreenManager コンポーネント内で実行されます。
    /// </summary>
    public class MainGame : Game
    {
        #region フィールド

        GraphicsDeviceManager graphics;
        UCScreenManager screenManager;

        // UI 描画の際に使用するアセットを予め読み込んでおくことによって、
        // メニューの移行中に発生し得るフレーム レートの減少を防ぎます。
        static readonly string[] preloadAssets =
        {
            "Screens\\gradient",
        };

        #endregion

        #region 初期化

        /// <summary>
        /// メインのゲーム コンストラクター。
        /// </summary>
        public MainGame()
        {
            Content.RootDirectory = "Content";

            graphics = new GraphicsDeviceManager(this);
            // ウィンドウサイズを設定します。
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            //graphics.IsFullScreen = true;
#if DEBUG
            graphics.IsFullScreen = false;
#endif

#if WINDOWS_PHONE

            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 480;

            // フルスクリーンにします。
            graphics.IsFullScreen = true;

            // Windows Phone の既定ではフレーム レートは 30 fps です。
            TargetElapsedTime = TimeSpan.FromTicks(333333);
#endif

            // ウィンドウタイトルを設定します。
            Window.Title = "UniverseColors";

            // ScreenManager コンポーネントを作成します。
            screenManager = new UCScreenManager(this);
            Components.Add(screenManager);

            // 最初の画面をアクティブ化します。
            screenManager.AddScreen(new LogoScreen(), null);
            //screenManager.AddScreen(new BackgroundScreen(), null);
            //screenManager.AddScreen(new TitleScreen(), null);
            //screenManager.AddScreen(new PlayGameScreen(), PlayerIndex.One);
            //screenManager.AddScreen(new GameClearScreen(new Score()), null);
            //screenManager.AddScreen(new EndingScreen(), null);
            //screenManager.AddScreen(new RankingScreen(new Score()), null);
        }

        protected override void Initialize()
        {
#if DEBUG
            DebugSystem.Initialize(this, "Font\\DebugFont");
            DebugSystem.Instance.FpsCounter.Visible = true;
            DebugSystem.Instance.TimeRuler.Visible = true;
            DebugSystem.Instance.TimeRuler.ShowLog = true;
#endif

            base.Initialize();
        }

        /// <summary>
        /// グラフィック コンテンツを読み込みます。
        /// </summary>
        protected override void LoadContent()
        {
            foreach (string asset in preloadAssets)
            {
                Content.Load<object>(asset);
            }
        }

        #endregion

        #region 更新描画処理

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime"></param>
        protected override void Update(GameTime gameTime)
        {
#if DEBUG
            // タイムルーラーにフレーム開始を伝える
            DebugSystem.Instance.TimeRuler.StartFrame();

            // 更新期間、"Update"の測定開始
            DebugSystem.Instance.TimeRuler.BeginMark("Update", Color.Blue);
#endif

            base.Update(gameTime);

#if DEBUG
            // 更新期間、"Update"の測定終了
            DebugSystem.Instance.TimeRuler.EndMark("Update");
#endif
        }

        /// <summary>
        /// 描画処理
        /// </summary>
        protected override void Draw(GameTime gameTime)
        {
#if DEBUG
            // 描画期間、"Draw"の測定開始
            DebugSystem.Instance.TimeRuler.BeginMark("Draw", Color.Yellow);
#endif
            graphics.GraphicsDevice.Clear(Color.Black);

            // 実際の描画は、ScreenManager コンポーネント内で実行されます。
            base.Draw(gameTime);

#if DEBUG
            // 描画期間、"Draw"の測定終了
            DebugSystem.Instance.TimeRuler.EndMark("Draw");
#endif
        }

        #endregion
    }

    #region Entry Point

    /// <summary>
    /// アプリケーションのメイン エントリ ポイント。
    /// </summary>
    static class Program
    {
        static void Main()
        {
            using (MainGame game = new MainGame())
            {
                game.Run();
            }
        }
    }

    #endregion
}
