#region File Description
//-----------------------------------------------------------------------------
// LoadingScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace UniverseColors
{
    /// <summary>
    /// ローディング画面は、メニュー システムとゲーム自体の間の移行を調整します。
    /// 通常、ある画面は、次の画面がオンに移行すると同時にオフに移行しますが、
    /// データの読み込みに長時間かかる可能性のある大規模な移行では、
    /// ゲームの読み込みを開始する前にメニュー システムを完全に終了する
    /// 必要があります。この操作を実行するには、次の手順に従います。
    /// 
    /// - 既存のすべての画面をオフに移行するように指示します。
    /// - ローディング画面をアクティブ化します。同時にオンに移行します。
    /// - ローディング画面が前の画面の状態を監視します。
    /// - 完全にオフに移行したことを確認すると、
    ///   次の実際の画面をアクティブ化します。
    ///   この場合、データの読み込みに長時間かかる場合があります。この読み込みが
    ///   行われている間は、ローディング画面だけが表示されます。
    /// </summary>
    class LoadingScreen : GameScreen
    {
        #region Fields

        bool loadingIsSlow;
        bool otherScreensAreGone;

        GameScreen[] screensToLoad;

        #endregion

        #region Initialization


        /// <summary>
        /// コンストラクターはプライベートです。したがって、ローディング画面は
        /// 代わりに静的 Load メソッドを介してアクティブ化する必要があります。
        /// </summary>
        private LoadingScreen(ScreenManager screenManager, bool loadingIsSlow,
                              GameScreen[] screensToLoad)
        {
            this.loadingIsSlow = loadingIsSlow;
            this.screensToLoad = screensToLoad;

            TransitionOnTime = TimeSpan.FromSeconds(0.5);
        }


        /// <summary>
        /// ローディング画面をアクティブ化します。
        /// </summary>
        public static void Load(ScreenManager screenManager, bool loadingIsSlow,
                                PlayerIndex? controllingPlayer,
                                params GameScreen[] screensToLoad)
        {
            // 現在のすべての画面をオフに移行するように指示します。
            foreach (GameScreen screen in screenManager.GetScreens())
                screen.ExitScreen();

            // ローディング画面を作成し、アクティブ化します。
            LoadingScreen loadingScreen = new LoadingScreen(screenManager,
                                                            loadingIsSlow,
                                                            screensToLoad);

            screenManager.AddScreen(loadingScreen, controllingPlayer);
        }


        #endregion

        #region Update and Draw


        /// <summary>
        /// ローディング画面を更新します。
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            // 前のすべての画面が完全にオフに移行したら、
            // 実際に読み込みを実行するときです。
            if (otherScreensAreGone)
            {
                ScreenManager.RemoveScreen(this);

                foreach (GameScreen screen in screensToLoad)
                {
                    if (screen != null)
                    {
                        ScreenManager.AddScreen(screen, ControllingPlayer);
                    }
                }

                // 読み込みが完了したら、ResetElapsedTime を使用して、
                // 非常に長いフレームを完了したことと、キャッチアップ
                // しようとする必要がないことを、ゲームのタイミング メカニズムに
                // 指示します。
                ScreenManager.Game.ResetElapsedTime();
            }
        }


        /// <summary>
        /// ローディング画面を描画します。
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            // アクティブな画面を 1 つだけ使用するということは、
            // 前のすべての画面が完全にオフに移行している
            // 必要があることを意味します。Update メソッドは
            // 画面を消去するのには十分ではないので、
            // Draw メソッドでチェックします。
            // 移行が適切に行われているかどうかを確認するには、
            // 読み込みを実行する前に、実際にフレームを
            // 画面なしで描画する必要があります。
            if ((ScreenState == ScreenState.Active) &&
                (ScreenManager.GetScreens().Length == 1))
            {
                otherScreensAreGone = true;
            }

            // ゲームプレイ画面は読み込みにしばらく時間がかかるので、読み込みが
            // 行われている間にローディング メッセージが表示されます。ただし、
            // メニューは非常に速やかに読み込まれるので、
            // ゲームからメニューに戻るほんの 1 秒足らずの間に
            // このメッセージをちらっと見せることはばかげていると思われます。
            // このパラメーターは読み込みにかかる時間を示すので、
            // わざわざメッセージを描画するかどうかがわかります。
            if (loadingIsSlow)
            {
                SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
                SpriteFont font = ScreenManager.Font;

                const string message = "Loading...";

                // ビューポート内のテキストを中央揃えします。
                Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
                Vector2 viewportSize = new Vector2(viewport.Width, viewport.Height);
                Vector2 textSize = font.MeasureString(message);
                Vector2 textPosition = (viewportSize - textSize) / 2;

                Color color = Color.White * TransitionAlpha;

                // テキストを描画します。
                spriteBatch.Begin();
                spriteBatch.DrawString(font, message, textPosition, color);
                spriteBatch.End();
            }
        }


        #endregion
    }
}
