using System;
using UniverseColors;
using Microsoft.Xna.Framework;

namespace UniverseColors
{
    class UCMenuScreen : TextureMenuScreen
    {
        // the number of pixels to pad above and below menu entries for touch input
        const int menuEntryPadding = 10;

        public new UCScreenManager ScreenManager
        {
            get { return (UCScreenManager)base.ScreenManager; }
        }

        public UCMenuScreen(string textureAsset)
            : base(textureAsset)
        {
        }

        protected override void UpdateMenuEntryLocations()
        {
            // 見栄えをよくするために、べき乗曲線を使用して、トランジション中に
            // メニューを固定位置までスライドさせる。
            // (固定位置に近づくと移動がスローダウンする)
            float transitionOffset = (float)Math.Pow(TransitionPosition, 2);

            //Vector2 position = new Vector2(0f, 350f);
            Vector2 position = new Vector2(0f, 480f);

            // すべてのメニュー エントリーの位置を更新する
            for (int i = 0; i < MenuEntries.Count; i++)
            {
                MenuEntry menuEntry = MenuEntries[i];

                // それぞれのエントリーが水平に整列するようにする
                position.X = ScreenManager.GraphicsDevice.Viewport.Width / MenuEntries.Count / 2 * (2 * i + 1) - menuEntry.GetWidth(this) / 2;

                if (ScreenState == ScreenState.TransitionOn)
                    position.X -= transitionOffset * 256;
                else
                    position.X += transitionOffset * 512;

                // エントリーの位置を設定する
                menuEntry.Position = position;
            }
        }
    }
}
