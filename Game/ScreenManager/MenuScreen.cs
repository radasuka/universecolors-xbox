#region File Description
//-----------------------------------------------------------------------------
// MenuScreen.cs
//
// XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Input;
using UniverseColors;
#endregion

namespace UniverseColors
{
    /// <summary>
    /// オプションのメニューを含む画面の基底クラス。ユーザーが
    /// 上下に移動してエントリを選択したり、キャンセルして画面から
    /// バック アウトしたりできます。
    /// </summary>
    abstract class MenuScreen : UCGameScreen
    {
        #region Fields

        List<MenuEntry> menuEntries = new List<MenuEntry>();
        protected int selectedEntry = 0;
        string menuTitle;

        #endregion

        #region Properties


        /// <summary>
        /// メニュー エントリのリストを取得し、派生クラスがメニュー コンテンツを
        /// 追加したり、変更したりできるようにします。
        /// </summary>
        protected IList<MenuEntry> MenuEntries
        {
            get { return menuEntries; }
        }


        #endregion

        #region Initialization


        /// <summary>
        /// コンストラクター。
        /// </summary>
        public MenuScreen(string menuTitle)
        {
            this.menuTitle = menuTitle;

            TransitionOnTime = TimeSpan.FromSeconds(0.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }


        #endregion

        #region Handle Input


        /// <summary>
        /// ユーザー入力に応答し、選択したエントリの変更、
        /// およびメニューの受け入れまたは取り消しを行います。
        /// </summary>
        public override void HandleInput(InputState input)
        {
            // 前のメニュー エントリに移動しますか?
            if (input.IsMenuLeft(ControllingPlayer))
            {
                selectedEntry--;

                if (selectedEntry < 0)
                    selectedEntry = menuEntries.Count - 1;
            }

            // 次のメニュー エントリに移動しますか?
            if (input.IsMenuRight(ControllingPlayer))
            {
                selectedEntry++;

                if (selectedEntry >= menuEntries.Count)
                    selectedEntry = 0;
            }

            // メニューを受け入れますか、またはキャンセルしますか?
            // ControllingPlayer を渡します。これは null
            // (任意のプレイヤーからの入力を受け入れる) 
            // または特定のインデックスです。
            // null 制御プレイヤーを渡した場合、InputState ヘルパーは実際に
            // 入力を行ったプレイヤーを返します。これを OnSelectEntry および
            // OnCancel に渡すことで、トリガーしたプレイヤーを知らせます。
            PlayerIndex playerIndex;

            if (input.IsMenuSelect(ControllingPlayer, out playerIndex))
            {
                OnSelectEntry(selectedEntry, playerIndex);
            }
            else if (input.IsMenuCancel(ControllingPlayer, out playerIndex))
            {
                OnCancel(playerIndex);
            }
        }


        /// <summary>
        /// ユーザーがメニュー エントリを選択した場合のハンドラー。
        /// </summary>
        protected virtual void OnSelectEntry(int entryIndex, PlayerIndex playerIndex)
        {
            menuEntries[entryIndex].OnSelectEntry(playerIndex);
        }


        /// <summary>
        /// ユーザーがメニュー エントリを取り消した場合のハンドラー。
        /// </summary>
        protected virtual void OnCancel(PlayerIndex playerIndex)
        {
            ExitScreen();
        }


        /// <summary>
        /// ヘルパーのオーバーロードは、MenuEntry イベント ハンドラーとしての 
        /// OnCancel を使用しやすくします。
        /// </summary>
        protected void OnCancel(object sender, PlayerIndexEventArgs e)
        {
            OnCancel(e.PlayerIndex);
        }


        #endregion

        #region Update and Draw


        /// <summary>
        /// 画面にメニュー エントリを配置する機会を与えます。既定では、
        /// すべてのメニュー エントリが画面の中央揃えで、垂直方向に
        /// 整列して配置されます。
        /// </summary>
        protected virtual void UpdateMenuEntryLocations()
        {
            // 移行時にメニューを所定の位置にスライドさせ、
            // 出力曲線を使用して、よりおもしろく見えるようにします
            // (これにより、終点に近づくにつれて移動の速度が低下します)。
            float transitionOffset = (float)Math.Pow(TransitionPosition, 2);

            // Y = 175 から開始し、各 X の値はエントリごとに生成されます
            Vector2 position = new Vector2(0f, 175f);

            // 各メニュー エントリの位置を順々に更新します
            for (int i = 0; i < menuEntries.Count; i++)
            {
                MenuEntry menuEntry = menuEntries[i];

                // 各エントリを水平方向の中央揃えで配置します
                position.X = ScreenManager.GraphicsDevice.Viewport.Width / 2 - menuEntry.GetWidth(this) / 2;

                if (ScreenState == ScreenState.TransitionOn)
                    position.X -= transitionOffset * 256;
                else
                    position.X += transitionOffset * 512;

                // エントリの位置を設定します
                menuEntry.Position = position;

                // 次のエントリ用に、このエントリのサイズだけ下に移動します
                position.Y += menuEntry.GetHeight(this);
            }
        }


        /// <summary>
        /// メニューを更新します。
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            // ネストされた各 MenuEntry オブジェクトを更新します。
            for (int i = 0; i < menuEntries.Count; i++)
            {
                bool isSelected = IsActive && (i == selectedEntry);

                menuEntries[i].Update(this, isSelected, gameTime);
            }
        }


        /// <summary>
        /// メニューを描画します。
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            // 描画する前に、エントリが正しい位置にあることを確認します
            UpdateMenuEntryLocations();

            GraphicsDevice graphics = ScreenManager.GraphicsDevice;
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;

            spriteBatch.Begin();

            // 各メニュー エントリを順々に描画します。
            for (int i = 0; i < menuEntries.Count; i++)
            {
                MenuEntry menuEntry = menuEntries[i];

                bool isSelected = IsActive && (i == selectedEntry);

                menuEntry.Draw(this, isSelected, gameTime);
            }

            // 移行時にメニューを所定の位置にスライドさせ、
            // 出力曲線を使用して、よりおもしろく見えるようにします
            // (これにより、終点に近づくにつれて移動の速度が低下します)。
            float transitionOffset = (float)Math.Pow(TransitionPosition, 2);

            // メニューのタイトルを画面の中央揃えで描画します
            Vector2 titlePosition = new Vector2(graphics.Viewport.Width / 2, 80);
            Vector2 titleOrigin = font.MeasureString(menuTitle) / 2;
            Color titleColor = new Color(192, 192, 192) * TransitionAlpha;
            float titleScale = 1.25f;

            titlePosition.Y -= transitionOffset * 100;

            spriteBatch.DrawString(font, menuTitle, titlePosition, titleColor, 0,
                                   titleOrigin, titleScale, SpriteEffects.None, 0);

            spriteBatch.End();
        }


        #endregion
    }
}
