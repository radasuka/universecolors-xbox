#region File Description
//-----------------------------------------------------------------------------
// GameScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using System.IO;
using Microsoft.Xna.Framework.Content;
#endregion

namespace UniverseColors
{
    /// <summary>
    /// 列挙型は画面の移行状態を表します。
    /// </summary>
    public enum ScreenState
    {
        TransitionOn,
        Active,
        TransitionOff,
        Hidden,
    }


    /// <summary>
    /// スクリーン基本クラス。
    /// </summary>
    public abstract class GameScreen
    {
        #region Properties


        /// <summary>
        /// 通常、ある画面が他の画面の上に立ち上がると、
        /// 新しい画面用の領域を確保するために、最初の画面がオフに
        /// 移行します。このプロパティは、画面が単に小さなポップアップ
        /// であるかどうかを示します。
        /// この場合、その下にある画面をオフに移行するかどうか
        /// 悩む必要はありません。
        /// </summary>
        public bool IsPopup
        {
            get { return isPopup; }
            protected set { isPopup = value; }
        }

        bool isPopup = false;


        /// <summary>
        /// 画面を非アクティブ化したときに、
        /// 画面がオンに移行する時間を示します。
        /// </summary>
        public TimeSpan TransitionOnTime
        {
            get { return transitionOnTime; }
            protected set { transitionOnTime = value; }
        }

        TimeSpan transitionOnTime = TimeSpan.Zero;


        /// <summary>
        /// 画面を非アクティブ化したときに、
        /// 画面がオフに移行する時間を示します。
        /// </summary>
        public TimeSpan TransitionOffTime
        {
            get { return transitionOffTime; }
            protected set { transitionOffTime = value; }
        }

        TimeSpan transitionOffTime = TimeSpan.Zero;


        /// <summary>
        /// ゼロ (完全にアクティブ、移行なし) から 1 
        /// (完全にオフに移行して何もない状態) までの
        /// 画面移行の現在の位置を取得します。
        /// </summary>
        public float TransitionPosition
        {
            get { return transitionPosition; }
            protected set { transitionPosition = value; }
        }

        float transitionPosition = 1;


        /// <summary>
        /// 1 (完全にアクティブ、移行なし) から 0 
        /// (完全にオフに移行して何もない状態) までの
        /// 画面移行の現在のアルファ値を取得します。
        /// </summary>
        public float TransitionAlpha
        {
            get { return 1f - TransitionPosition; }
        }


        /// <summary>
        /// 現在の画面の移行状態を取得します。
        /// </summary>
        public ScreenState ScreenState
        {
            get { return screenState; }
            protected set { screenState = value; }
        }

        ScreenState screenState = ScreenState.TransitionOn;


        /// <summary>
        /// 画面がオフに移行する場合、2 つの理由が
        /// 考えられます。その上に表示する他の画面用の領域を
        /// 確保するために一時的に消去されるか、
        /// 永久に消去される可能性があります。
        /// このプロパティは画面が実際に終了するかどうかを示します。
        /// 実際に設定されている場合、移行が完了すると
        /// すぐに画面が自動的に削除されます。
        /// </summary>
        public bool IsExiting
        {
            get { return isExiting; }
            protected internal set { isExiting = value; }
        }

        bool isExiting = false;


        /// <summary>
        /// この画面がアクティブであり、ユーザー入力に
        /// 応答できるかどうかを確認します。
        /// </summary>
        public bool IsActive
        {
            get
            {
                return !otherScreenHasFocus &&
                       (screenState == ScreenState.TransitionOn ||
                        screenState == ScreenState.Active);
            }
        }

        bool otherScreenHasFocus;


        /// <summary>
        /// この画面が属しているマネージャーを取得します。
        /// </summary>
        public ScreenManager ScreenManager
        {
            get { return screenManager; }
            internal set { screenManager = value; }
        }

        ScreenManager screenManager;


        /// <summary>
        /// 現在この画面を制御しているプレイヤーのインデックスを取得するか、
        /// 任意のプレイヤーからの入力を受け入れる場合は null を取得します。
        /// これは、ゲームを特定のプレイヤーの
        /// プロフィールにロックするために使用されます。メイン メニューは
        /// 接続された任意のゲームパッドからの
        /// 入力に応答しますが、このメニューから選択を実行したプレイヤーが
        /// それ以降のすべての画面を制御できるようになります。
        /// このため、制御プレイヤーがメイン メニューに戻るまで
        /// 他のゲームパッドは非アクティブになります。
        /// </summary>
        public PlayerIndex? ControllingPlayer
        {
            get { return controllingPlayer; }
            internal set { controllingPlayer = value; }
        }

        PlayerIndex? controllingPlayer;


        /// <summary>
        /// 画面で処理対象としているジェスチャーを取得します。
        /// ジェスチャー エンジンの精度を高めるには、
        /// 画面ごとに処理対象とするジェスチャーを可能なかぎり限定します。
        /// たとえば、ほとんどのメニュー操作ではタップまたはタップと
        /// 水平方向のドラッグのみが必要です。
        /// これらのジェスチャーは、画面が変わるときに 
        /// ScreenManager によって処理され、
        /// すべてのジェスチャーは、HandleInput メソッドに渡される 
        /// InputState に格納されます。
        /// </summary>
        public GestureType EnabledGestures
        {
            get { return enabledGestures; }
            protected set
            {
                enabledGestures = value;

                // これは、画面が変わるときに画面マネージャーによって
                // 処理されますが、この画面がアクティブなときに
                // ジェスチャーのタイプが変わる場合は、
                // TouchPanel を自分で更新する必要があります。
                if (ScreenState == ScreenState.Active)
                {
                    TouchPanel.EnabledGestures = value;
                }
            }
        }

        GestureType enabledGestures = GestureType.None;


        #endregion

        #region Initialization


        /// <summary>
        /// 画面のグラフィック コンテンツを読み込みます。
        /// </summary>
        public virtual void LoadContent() { }


        /// <summary>
        /// 画面のコンテンツをアンロードします。
        /// </summary>
        public virtual void UnloadContent() { }


        #endregion

        #region Update and Draw


        /// <summary>
        /// 画面で移行位置の更新などのロジックを実行できるようになります。
        /// このメソッドは HandleInput と異なり、画面がアクティブであるか、
        /// 非表示であるか、移行の最中であるかに関係なく呼び出されます。
        /// </summary>
        public virtual void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                      bool coveredByOtherScreen)
        {
            this.otherScreenHasFocus = otherScreenHasFocus;

            if (isExiting)
            {
                // 画面が完全に消去されると、画面はオフに移行します。
                screenState = ScreenState.TransitionOff;

                if (!UpdateTransition(gameTime, transitionOffTime, 1))
                {
                    // 移行が完了すると、画面が削除されます。
                    ScreenManager.RemoveScreen(this);
                }
            }
            else if (coveredByOtherScreen)
            {
                // 画面が他の画面に覆われると、画面はオフに移行します。
                if (UpdateTransition(gameTime, transitionOffTime, 1))
                {
                    // まだ移行が行われています。
                    screenState = ScreenState.TransitionOff;
                }
                else
                {
                    // 移行が完了しました!
                    screenState = ScreenState.Hidden;
                }
            }
            else
            {
                // そうでない場合、画面はオンに移行し、アクティブになります。
                if (UpdateTransition(gameTime, transitionOnTime, -1))
                {
                    // まだ移行が行われています。
                    screenState = ScreenState.TransitionOn;
                }
                else
                {
                    // 移行が完了しました!
                    screenState = ScreenState.Active;
                }
            }
        }


        /// <summary>
        /// 画面の移行位置を更新するためのヘルパー。
        /// </summary>
        bool UpdateTransition(GameTime gameTime, TimeSpan time, int direction)
        {
            // どの程度移動する必要がありますか?
            float transitionDelta;

            if (time == TimeSpan.Zero)
                transitionDelta = 1;
            else
                transitionDelta = (float)(gameTime.ElapsedGameTime.TotalMilliseconds /
                                          time.TotalMilliseconds);

            // 移行位置を更新します。
            transitionPosition += transitionDelta * direction;

            // 移行の最後に到達しましたか?
            if (((direction < 0) && (transitionPosition <= 0)) ||
                ((direction > 0) && (transitionPosition >= 1)))
            {
                transitionPosition = MathHelper.Clamp(transitionPosition, 0, 1);
                return false;
            }

            // そうでない場合、まだ移行が行われています。
            return true;
        }


        /// <summary>
        /// 画面でユーザー入力を処理できるようになります。
        /// このメソッドは Update と異なり、
        /// 画面がアクティブのときにのみ呼び出され、他の何らかの画面が
        /// フォーカスを取得しているときには呼び出されません。
        /// </summary>
        public virtual void HandleInput(InputState input) { }


        /// <summary>
        /// これは、画面が自身を描画するときに呼び出されます。
        /// </summary>
        public virtual void Draw(GameTime gameTime) { }


        #endregion

        #region Public Methods


        /// <summary>
        /// 画面を消去するように指示します。このメソッドは、画面を直ちに
        /// 消去する ScreenManager.RemoveScreen と異なり、
        /// 移行のタイミングを尊重し、
        /// 画面を徐々にオフに移行できるようにします。
        /// </summary>
        public void ExitScreen()
        {
            if (TransitionOffTime == TimeSpan.Zero)
            {
                // 画面の移行時間がゼロに設定されている場合は、
                // すぐに削除されます。
                ScreenManager.RemoveScreen(this);
            }
            else
            {
                // それ以外の場合、オフに移行してから終了するように
                // フラグを設定します。
                isExiting = true;
            }
        }


        #endregion
    }
}
