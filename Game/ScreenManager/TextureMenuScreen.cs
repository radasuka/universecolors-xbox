using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace UniverseColors
{
    class TextureMenuScreen : MenuScreen
    {
        #region フィールドとプロパティ

        // the number of pixels to pad above and below menu entries for touch input
        const int menuEntryPadding = 10;

        /// <summary>
        /// このスクリーンで使うコンテントマネージャーを設定します。
        /// </summary>
        //ContentManager content;

        public string TextureAsset { get; set; }

        /// <summary>
        /// テクスチャを取得または設定します。
        /// </summary>
        public Texture2D Texture { get; set; }

        /// <summary>
        /// テクスチャの領域を取得または設定します。
        /// </summary>
        public Rectangle? TextureBounds { get; set; }

        #endregion

        #region 初期化

        public TextureMenuScreen(string textureAsset)
            : base(string.Empty)
        {
            this.TextureAsset = textureAsset;
        }

        public override void LoadContent()
        {
            if (Content == null)
                Content = new ContentManager(ScreenManager.Game.Services, "Content");

            this.Texture = Content.Load<Texture2D>(this.TextureAsset);

            base.LoadContent();
        }

        #endregion

        /// <summary>
        /// メニューを表示します。
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            // make sure our entries are in the right place before we draw them
            UpdateMenuEntryLocations();

            GraphicsDevice graphics = ScreenManager.GraphicsDevice;
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;

            spriteBatch.Begin();

            // Draw each menu entry in turn.
            for (int i = 0; i < MenuEntries.Count; i++)
            {
                MenuEntry menuEntry = MenuEntries[i];

                bool isSelected = IsActive && (i == selectedEntry);

                menuEntry.Draw(this, false, gameTime);
            }

            // Make the menu slide into place during transitions, using a
            // power curve to make things look more interesting (this makes
            // the movement slow down as it nears the end).
            float transitionOffset = (float)Math.Pow(TransitionPosition, 2);

            // Draw the menu title centered on the screen
            Vector2 titlePosition = new Vector2(graphics.Viewport.Width / 2, 100);
            Vector2 titleOrigin;
            if (TextureBounds.HasValue)
                titleOrigin = new Vector2(TextureBounds.Value.Width / 2, 0);
            else
                titleOrigin = new Vector2(Texture.Width / 2, 0);
            Color titleColor = Color.White * TransitionAlpha;
            float titleScale = 1f;

            titlePosition.Y -= transitionOffset * 100;

            spriteBatch.Draw(Texture, titlePosition, TextureBounds, titleColor, 0,
                titleOrigin, titleScale, SpriteEffects.None, 0);

            spriteBatch.End();
        }
    }
}
