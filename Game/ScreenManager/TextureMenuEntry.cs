using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace UniverseColors
{
    class TextureMenuEntry : MenuEntry
    {
        #region フィールドとプロパティ

        /// <summary>
        /// テクスチャを取得または設定します。
        /// </summary>
        public Texture2D Texture { get; set; }

        /// <summary>
        /// テクスチャの領域を取得または設定します。
        /// </summary>
        public Rectangle? TextureBounds { get; set; }

        #endregion

        #region 初期化

        /// <summary>
        /// コンストラクター
        /// </summary>
        /// <param name="assetName"></param>
        public TextureMenuEntry(Texture2D texture)
            : base(string.Empty)
        {
            this.Texture = texture;
        }

        #endregion

        /// <summary>
        /// メニューで使う画像を描画します。
        /// </summary>
        /// <param name="screen"></param>
        /// <param name="isSelected"></param>
        /// <param name="gameTime"></param>
        public override void Draw(MenuScreen screen, bool isSelected, GameTime gameTime)
        {
            // トランジション中のアルファ値を計算する
            Color color = Color.White;
            color *= screen.TransitionAlpha;

            double time = gameTime.TotalGameTime.TotalSeconds;

            float pulsate = (float)Math.Sin(time * 6) + 1;

            float scale = 1 + pulsate * 0.05f * selectionFade;
            Vector2 origin = Vector2.Zero;

            SpriteBatch spriteBatch = screen.ScreenManager.SpriteBatch;
            spriteBatch.Draw(Texture, Position, this.TextureBounds, color, 0.0f, origin, scale, SpriteEffects.None, 0);
        }

        /// <summary>
        /// このメニュー エントリーが必要とする高さを返します。
        /// </summary>
        public override int GetHeight(MenuScreen screen)
        {
            if (TextureBounds.HasValue)
                return TextureBounds.Value.Height;
            return Texture.Height;
        }

        /// <summary>
        /// このメニュー エントリーが必要とする幅を返します。
        /// </summary>
        public override int GetWidth(MenuScreen screen)
        {
            if (TextureBounds.HasValue)
                return TextureBounds.Value.Width;
            return Texture.Width;
        }
    }
}
