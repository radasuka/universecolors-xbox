#region File Description
//-----------------------------------------------------------------------------
// InputState.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using System.Collections.Generic;
#endregion

namespace UniverseColors
{
    /// <summary>
    /// キーボード、ゲームパッド、およびタッチ入力からの入力を
    /// 読み込むためのヘルパー。このクラスは 
    /// 入力デバイスの現在の状態と以前の状態の両方を追跡し、 
    /// "メニューで上に移動する" や "ゲームをポーズする" などの
    /// 高レベル入力アクションのための
    /// クエリ メソッドを実装します。
    /// </summary>
    public class InputState
    {
        #region Fields

        public const int MaxInputs = 4;

        public readonly KeyboardState[] CurrentKeyboardStates;
        public readonly GamePadState[] CurrentGamePadStates;

        public readonly KeyboardState[] LastKeyboardStates;
        public readonly GamePadState[] LastGamePadStates;

        public readonly bool[] GamePadWasConnected;

        public TouchCollection TouchState;

        public readonly List<GestureSample> Gestures = new List<GestureSample>();

        #endregion

        #region Initialization


        /// <summary>
        /// 新しい入力状態を構築します。
        /// </summary>
        public InputState()
        {
            CurrentKeyboardStates = new KeyboardState[MaxInputs];
            CurrentGamePadStates = new GamePadState[MaxInputs];

            LastKeyboardStates = new KeyboardState[MaxInputs];
            LastGamePadStates = new GamePadState[MaxInputs];

            GamePadWasConnected = new bool[MaxInputs];
        }


        #endregion

        #region Public Methods


        /// <summary>
        /// キーボードとゲームパッドの最新の状態を読み込みます。
        /// </summary>
        public void Update()
        {
            for (int i = 0; i < MaxInputs; i++)
            {
                LastKeyboardStates[i] = CurrentKeyboardStates[i];
                LastGamePadStates[i] = CurrentGamePadStates[i];

                CurrentKeyboardStates[i] = Keyboard.GetState((PlayerIndex)i);
                CurrentGamePadStates[i] = GamePad.GetState((PlayerIndex)i);

                // ゲームパッドが取り外されたかどうかを検出できるように、
                // ゲームパッドが接続されたことがあるかどうかを追跡します。
                if (CurrentGamePadStates[i].IsConnected)
                {
                    GamePadWasConnected[i] = true;
                }
            }

            TouchState = TouchPanel.GetState();

            Gestures.Clear();
            while (TouchPanel.IsGestureAvailable)
            {
                Gestures.Add(TouchPanel.ReadGesture());
            }
        }


        /// <summary>
        /// この更新中にキーが新しく押されたかどうかを確認するためのヘルパー。
        /// controllingPlayer パラメーターは、どのプレイヤーの入力を
        /// 読み込むかを指定します。
        /// このパラメーターが null の場合、任意のプレイヤーからの入力を
        /// 受け入れます。キー押下が検出されると、出力 playerIndex が
        /// キーまたはボタンを押したプレイヤーをレポートします。
        /// </summary>
        public bool IsNewKeyPress(Keys key, PlayerIndex? controllingPlayer,
                                            out PlayerIndex playerIndex)
        {
            if (controllingPlayer.HasValue)
            {
                // 指定したプレイヤーからの入力を読み込みます。
                playerIndex = controllingPlayer.Value;

                int i = (int)playerIndex;

                return (CurrentKeyboardStates[i].IsKeyDown(key) &&
                        LastKeyboardStates[i].IsKeyUp(key));
            }
            else
            {
                // 任意のプレイヤーからの入力を受け入れます。
                return (IsNewKeyPress(key, PlayerIndex.One, out playerIndex) ||
                        IsNewKeyPress(key, PlayerIndex.Two, out playerIndex) ||
                        IsNewKeyPress(key, PlayerIndex.Three, out playerIndex) ||
                        IsNewKeyPress(key, PlayerIndex.Four, out playerIndex));
            }
        }


        /// <summary>
        /// この更新中にボタンが新しく押されたかどうかを確認するためのヘルパー。
        /// controllingPlayer パラメーターは、どのプレイヤーの入力を
        /// 読み込むかを指定します。
        /// このパラメーターが null の場合、任意のプレイヤーからの入力を
        /// 受け入れます。ボタン押下が検出されると、出力 playerIndex が
        /// キーまたはボタンを押したプレイヤーをレポートします。
        /// </summary>
        public bool IsNewButtonPress(Buttons button, PlayerIndex? controllingPlayer,
                                                     out PlayerIndex playerIndex)
        {
            if (controllingPlayer.HasValue)
            {
                // 指定したプレイヤーからの入力を読み込みます。
                playerIndex = controllingPlayer.Value;

                int i = (int)playerIndex;

                return (CurrentGamePadStates[i].IsButtonDown(button) &&
                        LastGamePadStates[i].IsButtonUp(button));
            }
            else
            {
                // 任意のプレイヤーからの入力を受け入れます。
                return (IsNewButtonPress(button, PlayerIndex.One, out playerIndex) ||
                        IsNewButtonPress(button, PlayerIndex.Two, out playerIndex) ||
                        IsNewButtonPress(button, PlayerIndex.Three, out playerIndex) ||
                        IsNewButtonPress(button, PlayerIndex.Four, out playerIndex));
            }
        }


        /// <summary>
        /// "メニュー選択" 入力アクションを確認します。
        /// controllingPlayer パラメーターは、どのプレイヤーの入力を
        /// 読み込むかを指定します。
        /// このパラメーターが null の場合、任意のプレイヤーからの入力を
        /// 受け入れます。アクションが検出されると、出力 playerIndex が
        /// キーまたはボタンを押したプレイヤーをレポートします。
        /// </summary>
        public bool IsMenuSelect(PlayerIndex? controllingPlayer,
                                 out PlayerIndex playerIndex)
        {
            return IsNewKeyPress(Keys.Space, controllingPlayer, out playerIndex) ||
                   IsNewKeyPress(Keys.Enter, controllingPlayer, out playerIndex) ||
                   IsNewButtonPress(Buttons.A, controllingPlayer, out playerIndex) ||
                   IsNewButtonPress(Buttons.Start, controllingPlayer, out playerIndex);
        }


        /// <summary>
        /// "メニュー取り消し" 入力アクションを確認します。
        /// controllingPlayer パラメーターは、どのプレイヤーの入力を
        /// 読み込むかを指定します。
        /// このパラメーターが null の場合、任意のプレイヤーからの入力を
        /// 受け入れます。アクションが検出されると、出力 playerIndex が
        /// キーまたはボタンを押したプレイヤーをレポートします。
        /// </summary>
        public bool IsMenuCancel(PlayerIndex? controllingPlayer,
                                 out PlayerIndex playerIndex)
        {
            return IsNewKeyPress(Keys.Escape, controllingPlayer, out playerIndex) ||
                   IsNewButtonPress(Buttons.B, controllingPlayer, out playerIndex) ||
                   IsNewButtonPress(Buttons.Back, controllingPlayer, out playerIndex);
        }


        /// <summary>
        /// "メニュー上" 入力アクションを確認します。
        /// controllingPlayer パラメーターは、どのプレイヤーの入力を読み込むかを
        /// 指定します。このパラメーターが null の場合、任意のプレイヤーからの
        /// 入力を受け入れます。
        /// </summary>
        public bool IsMenuUp(PlayerIndex? controllingPlayer)
        {
            PlayerIndex playerIndex;

            return IsNewKeyPress(Keys.Up, controllingPlayer, out playerIndex) ||
                   IsNewButtonPress(Buttons.DPadUp, controllingPlayer, out playerIndex) ||
                   IsNewButtonPress(Buttons.LeftThumbstickUp, controllingPlayer, out playerIndex);
        }


        /// <summary>
        /// "メニュー下" 入力アクションを確認します。
        /// controllingPlayer パラメーターは、どのプレイヤーの入力を読み込むかを
        /// 指定します。このパラメーターが null の場合、任意のプレイヤーからの
        /// 入力を受け入れます。
        /// </summary>
        public bool IsMenuDown(PlayerIndex? controllingPlayer)
        {
            PlayerIndex playerIndex;

            return IsNewKeyPress(Keys.Down, controllingPlayer, out playerIndex) ||
                   IsNewButtonPress(Buttons.DPadDown, controllingPlayer, out playerIndex) ||
                   IsNewButtonPress(Buttons.LeftThumbstickDown, controllingPlayer, out playerIndex);
        }


        /// <summary>
        /// "メニュー左" 入力アクションを確認します。
        /// controllingPlayer パラメーターは、どのプレイヤーの入力を読み込むかを
        /// 指定します。このパラメーターが null の場合、任意のプレイヤーからの
        /// 入力を受け入れます。
        /// </summary>
        public bool IsMenuLeft(PlayerIndex? controllingPlayer)
        {
            PlayerIndex playerIndex;

            return IsNewKeyPress(Keys.Left, controllingPlayer, out playerIndex) ||
                   IsNewButtonPress(Buttons.DPadLeft, controllingPlayer, out playerIndex) ||
                   IsNewButtonPress(Buttons.LeftThumbstickLeft, controllingPlayer, out playerIndex);
        }


        /// <summary>
        /// "メニュー右" 入力アクションを確認します。
        /// controllingPlayer パラメーターは、どのプレイヤーの入力を読み込むかを
        /// 指定します。このパラメーターが null の場合、任意のプレイヤーからの
        /// 入力を受け入れます。
        /// </summary>
        public bool IsMenuRight(PlayerIndex? controllingPlayer)
        {
            PlayerIndex playerIndex;

            return IsNewKeyPress(Keys.Right, controllingPlayer, out playerIndex) ||
                   IsNewButtonPress(Buttons.DPadRight, controllingPlayer, out playerIndex) ||
                   IsNewButtonPress(Buttons.LeftThumbstickRight, controllingPlayer, out playerIndex);
        }


        /// <summary>
        /// "ゲームのポーズ" 入力アクションを確認します。
        /// controllingPlayer パラメーターは、どのプレイヤーの入力を読み込むかを
        /// 指定します。このパラメーターが null の場合、任意のプレイヤーからの
        /// 入力を受け入れます。
        /// </summary>
        public bool IsPauseGame(PlayerIndex? controllingPlayer)
        {
            PlayerIndex playerIndex;

            return IsNewKeyPress(Keys.Escape, controllingPlayer, out playerIndex) ||
                   IsNewButtonPress(Buttons.Back, controllingPlayer, out playerIndex) ||
                   IsNewButtonPress(Buttons.Start, controllingPlayer, out playerIndex);
        }


        #endregion
    }
}
