#region File Description
//-----------------------------------------------------------------------------
// MenuEntry.cs
//
// XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace UniverseColors
{
    /// <summary>
    /// ヘルパー クラスは MenuScreen 内の単一エントリを表します。既定では、
    /// エントリのテキスト文字列を描画しますが、メニュー エントリを
    /// さまざまな方法で表示するようにカスタマイズできます。
    /// メニュー エントリを選択したときに発生するイベントも提供します。
    /// </summary>
    class MenuEntry
    {
        #region Fields

        /// <summary>
        /// このエントリに対してレンダリングされるテキスト。
        /// </summary>
        string text;

        /// <summary>
        /// エントリ上のフェーディング選択効果を追跡します。
        /// </summary>
        /// <remarks>
        /// 選択が解除された場合、エントリは選択効果から移行します。
        /// </remarks>
        protected float selectionFade;

        /// <summary>
        /// エントリが描画される位置。これは、Update の各フレームで
        /// MenuScreen によって設定されます。
        /// </summary>
        Vector2 position;

        #endregion

        #region Properties


        /// <summary>
        /// このメニュー エントリのテキストを取得または設定します。
        /// </summary>
        public string Text
        {
            get { return text; }
            set { text = value; }
        }


        /// <summary>
        /// このメニュー エントリを描画する位置を取得または設定します。
        /// </summary>
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }


        #endregion

        #region Events


        /// <summary>
        /// メニュー エントリが選択された場合に発生するイベント。
        /// </summary>
        public event EventHandler<PlayerIndexEventArgs> Selected;


        /// <summary>
        /// Selected イベントを発生させるメソッド。
        /// </summary>
        protected internal virtual void OnSelectEntry(PlayerIndex playerIndex)
        {
            if (Selected != null)
                Selected(this, new PlayerIndexEventArgs(playerIndex));
        }


        #endregion

        #region Initialization


        /// <summary>
        /// 指定されたテキストを持つ新しいメニュー エントリを構築します。
        /// </summary>
        public MenuEntry(string text)
        {
            this.text = text;
        }


        #endregion

        #region Update and Draw


        /// <summary>
        /// メニュー エントリを更新します。
        /// </summary>
        public virtual void Update(MenuScreen screen, bool isSelected, GameTime gameTime)
        {
            // メニューの選択が変更されると、エントリは新しい状態に直ちに
            // 移動するのではなく、選択した外観と選択を解除した外観の間で徐々に
            // フェード イン/フェード アウトします。
            float fadeSpeed = (float)gameTime.ElapsedGameTime.TotalSeconds * 4;

            if (isSelected)
                selectionFade = Math.Min(selectionFade + fadeSpeed, 1);
            else
                selectionFade = Math.Max(selectionFade - fadeSpeed, 0);
        }


        /// <summary>
        /// メニュー エントリを描画します。このメソッドは、
        /// オーバーライドにより外観をカスタマイズできます。
        /// </summary>
        public virtual void Draw(MenuScreen screen, bool isSelected, GameTime gameTime)
        {
            // 選択したエントリを黄色で描画し、それ以外の場合は白で描画します。
            Color color = isSelected ? Color.Yellow : Color.White;

            // 選択したメニュー エントリのサイズを鼓動させます。
            double time = gameTime.TotalGameTime.TotalSeconds;

            float pulsate = (float)Math.Sin(time * 6) + 1;

            float scale = 1 + pulsate * 0.05f * selectionFade;

            // 移行時にテキストをフェード アウトするアルファ値を変更します。
            color *= screen.TransitionAlpha;

            // テキストを各行の中央揃えで描画します。
            ScreenManager screenManager = screen.ScreenManager;
            SpriteBatch spriteBatch = screenManager.SpriteBatch;
            SpriteFont font = screenManager.Font;

            Vector2 origin = new Vector2(0, font.LineSpacing / 2);

            spriteBatch.DrawString(font, text, position, color, 0,
                                   origin, scale, SpriteEffects.None, 0);
        }


        /// <summary>
        /// このメニュー エントリに必要なスペースの大きさを照会します。
        /// </summary>
        public virtual int GetHeight(MenuScreen screen)
        {
            return screen.ScreenManager.Font.LineSpacing;
        }


        /// <summary>
        /// エントリの幅をクエリし、画面上でのセンタリングに使用します。
        /// </summary>
        public virtual int GetWidth(MenuScreen screen)
        {
            return (int)screen.ScreenManager.Font.MeasureString(Text).X;
        }


        #endregion
    }
}
