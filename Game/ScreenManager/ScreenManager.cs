#region File Description
//-----------------------------------------------------------------------------
// ScreenManager.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
#endregion

namespace UniverseColors
{
    /// <summary>
    /// スクリーンマネージャーは 1 つまたは複数の GameScreen インスタンスを管理する
    /// コンポーネントです。画面のスタックを維持し、Update および
    /// Draw メソッドを適切なタイミングで呼び出し、最上位のアクティブな
    /// 画面に入力を自動的にルーティングします。
    /// </summary>
    public class ScreenManager : DrawableGameComponent
    {
        #region Fields

        List<GameScreen> screens = new List<GameScreen>();
        List<GameScreen> screensToUpdate = new List<GameScreen>();

        InputState input = new InputState();

        SpriteBatch spriteBatch;
        SpriteFont font;
        Texture2D blankTexture;

        bool isInitialized;

        bool traceEnabled;

        #endregion

        #region Properties


        /// <summary>
        /// すべての画面で共有される既定の SpriteBatch。これにより、
        /// 各画面で独自のローカル インスタンスを作成するかどうか
        /// 気を遣う必要がなくなります。
        /// </summary>
        public SpriteBatch SpriteBatch
        {
            get { return spriteBatch; }
        }


        /// <summary>
        /// すべての画面で共有される既定のフォント。これにより、
        /// 各画面で独自のローカル コピーを読み込むかどうか
        /// 気を遣う必要がなくなります。
        /// </summary>
        public SpriteFont Font
        {
            get { return font; }
        }


        /// <summary>
        /// true の場合、マネージャーは更新されるたびにすべての画面の
        /// リストを出力します。これは、すべてが適切な時点で
        /// 追加/削除されていることを確認するのに便利です。
        /// </summary>
        public bool TraceEnabled
        {
            get { return traceEnabled; }
            set { traceEnabled = value; }
        }


        #endregion

        #region Initialization


        /// <summary>
        /// 新しい画面マネージャー コンポーネントを構築します。
        /// </summary>
        public ScreenManager(Game game)
            : base(game)
        {
            // EnabledGestures を照会するにはそれらを設定する必要がありますが、
            // このゲームでそれらを読み取ることは想定していません。
            TouchPanel.EnabledGestures = GestureType.None;
        }


        /// <summary>
        /// 画面マネージャー コンポーネントを初期化します。
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();

            isInitialized = true;
        }


        /// <summary>
        /// グラフィック コンテンツを読み込みます。
        /// </summary>
        protected override void LoadContent()
        {
            // 画面マネージャーに属するコンテンツを読み込みます。
            ContentManager content = Game.Content;

            spriteBatch = new SpriteBatch(GraphicsDevice);
            font = content.Load<SpriteFont>("Font\\menufont");
            blankTexture = content.Load<Texture2D>("Screens\\blank");

            // 各画面がそれぞれのコンテンツを読み込むように指示します。
            foreach (GameScreen screen in screens)
            {
                screen.LoadContent();
            }
        }


        /// <summary>
        /// グラフィック コンテンツをアンロードします。
        /// </summary>
        protected override void UnloadContent()
        {
            // 各画面がそれぞれのコンテンツをアンロードするように指示します。
            foreach (GameScreen screen in screens)
            {
                screen.UnloadContent();
            }
        }


        #endregion

        #region Update and Draw


        /// <summary>
        /// 各画面でロジックを実行できるようにします。
        /// </summary>
        public override void Update(GameTime gameTime)
        {
            // キーボードとゲームパッドを読み込みます。
            input.Update();

            // ある画面の更新の過程で別の画面を追加または削除する場合の
            // 混乱を防ぐため、マスター画面リストのコピーを作成します。
            screensToUpdate.Clear();

            foreach (GameScreen screen in screens)
                screensToUpdate.Add(screen);

            bool otherScreenHasFocus = !Game.IsActive;
            bool coveredByOtherScreen = false;

            // 更新を待機する画面が存在する限りループします。
            while (screensToUpdate.Count > 0)
            {
                // 最上位の画面を待機リストからポップ オフします。
                GameScreen screen = screensToUpdate[screensToUpdate.Count - 1];

                screensToUpdate.RemoveAt(screensToUpdate.Count - 1);

                // 画面を更新します。
                screen.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

                if (screen.ScreenState == ScreenState.TransitionOn ||
                    screen.ScreenState == ScreenState.Active)
                {
                    // この画面が最初に表示されたアクティブな画面である場合、
                    // 入力を処理する機会が提供されます。
                    if (!otherScreenHasFocus)
                    {
                        screen.HandleInput(input);

                        otherScreenHasFocus = true;
                    }

                    // この画面がアクティブな非ポップアップ画面の場合、
                    // 以降の画面がこの画面に覆われることを通知します。
                    if (!screen.IsPopup)
                        coveredByOtherScreen = true;
                }
            }

            // デバッグ トレースを出力しますか?
            if (traceEnabled)
                TraceScreens();
        }


        /// <summary>
        /// すべての画面のリストをデバッグ用に出力します。
        /// </summary>
        void TraceScreens()
        {
            List<string> screenNames = new List<string>();

            foreach (GameScreen screen in screens)
                screenNames.Add(screen.GetType().Name);

            Debug.WriteLine(string.Join(", ", screenNames.ToArray()));
        }


        /// <summary>
        /// 各画面を描画するように指示します。
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            foreach (GameScreen screen in screens)
            {
                if (screen.ScreenState == ScreenState.Hidden)
                    continue;

                screen.Draw(gameTime);
            }
        }


        #endregion

        #region Public Methods


        /// <summary>
        /// 画面マネージャーに新しい画面を追加します。
        /// </summary>
        public void AddScreen(GameScreen screen, PlayerIndex? controllingPlayer)
        {
            screen.ControllingPlayer = controllingPlayer;
            screen.ScreenManager = this;
            screen.IsExiting = false;

            // グラフィック デバイスを備えている場合、画面がコンテンツを
            // 読み込むように指示します。
            if (isInitialized)
            {
                screen.LoadContent();
            }

            screens.Add(screen);

            // この画面が処理対象とするジェスチャーに応答するように 
            // TouchPanel を更新します
            TouchPanel.EnabledGestures = screen.EnabledGestures;
        }


        /// <summary>
        /// 画面マネージャーから画面を削除します。通常は、
        /// 画面を直ちに削除するのではなく、徐々にオフに移行できるように、
        /// このメソッドを直接呼び出す代わりに GameScreen.ExitScreen を
        /// 使用します。
        /// </summary>
        public void RemoveScreen(GameScreen screen)
        {
            // グラフィック デバイスを備えている場合、画面がコンテンツを
            // アンロードするように指示します。
            if (isInitialized)
            {
                screen.UnloadContent();
            }

            screens.Remove(screen);
            screensToUpdate.Remove(screen);

            // マネージャー内にまだ画面がある場合、その画面が処理対象としている
            // ジェスチャーに応答するように TouchPanel を更新します。
            if (screens.Count > 0)
            {
                TouchPanel.EnabledGestures = screens[screens.Count - 1].EnabledGestures;
            }
        }


        /// <summary>
        /// すべての画面を保持する配列を公開します。画面は AddScreen と 
        /// RemoveScreen メソッドを使用して追加または削除されただけなので、
        /// 実際のマスター リストではなくコピーを返します。
        /// </summary>
        public GameScreen[] GetScreens()
        {
            return screens.ToArray();
        }


        /// <summary>
        /// ヘルパーは、画面をフェード インしたり、フェード アウトしたり、
        /// ポップアップの背後にある背景を暗くしたりするのに使用する半透明な
        /// 黒の全画面スプライトを描画します。
        /// </summary>
        public void FadeBackBufferToBlack(float alpha)
        {
            Viewport viewport = GraphicsDevice.Viewport;

            spriteBatch.Begin();

            spriteBatch.Draw(blankTexture,
                             new Rectangle(0, 0, viewport.Width, viewport.Height),
                             Color.Black * alpha);

            spriteBatch.End();
        }


        #endregion
    }
}
