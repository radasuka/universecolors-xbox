#region File Description
//-----------------------------------------------------------------------------
// PlayerIndexEventArgs.cs
//
// XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
#endregion

namespace UniverseColors
{
    /// <summary>
    /// イベントをトリガーしたプレイヤーのインデックスを含む
    /// カスタム イベント引数。この引数は MenuEntry.Selected 
    /// イベントによって使用されます。
    /// </summary>
    class PlayerIndexEventArgs : EventArgs
    {
        /// <summary>
        /// コンストラクター。
        /// </summary>
        public PlayerIndexEventArgs(PlayerIndex playerIndex)
        {
            this.playerIndex = playerIndex;
        }


        /// <summary>
        /// このイベントをトリガーしたプレイヤーのインデックスを取得します。
        /// </summary>
        public PlayerIndex PlayerIndex
        {
            get { return playerIndex; }
        }

        PlayerIndex playerIndex;
    }
}
