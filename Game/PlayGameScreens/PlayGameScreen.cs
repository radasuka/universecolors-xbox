#region File Description
//-----------------------------------------------------------------------------
// PlayGameScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using ステートメント
using System;
using System.Collections.Generic;
using GameDebugTools;
using GameLibrary;
using GameLibrary.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using UniverseColors.PlayGameScreens.BackGrounds;
using UniverseColors.PlayGameScreens.Bullets;
using UniverseColors.PlayGameScreens.Enemies;
using UniverseColors.PlayGameScreens.Particles;
using UniverseColors.PlayGameScreens.Players;
using UniverseColors.PlayGameScreens.PlayGameState;
using UniverseColors.Screens;
#endregion

namespace UniverseColors.PlayGameScreens
{
    /// <summary>
    /// ゲーム本体のスクリーン。
    /// </summary>
    public class PlayGameScreen : UCGameScreen
    {

        #region 定数フィールド

        // プレイヤーの領域外判定用定数
        private const float playerRightLimit = 2950.0f;
        private const float playerLeftLimit = -2950.0f;
        private const float playerUpLimit = 2500.0f;
        private const float playerDownLimit = -200.0f;
        // 自機弾の領域外判定用定数
        internal const float playerBulletRightLimit = 5000.0f;
        internal const float playerBulletLeftLimit = -5000.0f;
        internal const float playerBulletUpLimit = 7000.0f;
        internal const float playerBulletDownLimit = -5000.0f;
        internal const float playerBulletDeepLimit = -100000.0f;
        internal const float playerBulletBeforeLimit = 5500.0f;
        // 敵機が出現する距離
        internal const float enemyActiveBorderline = 100000.0f;
        // 敵機をロックオン出来る距離
        internal const float lockOnIsEnemy = 85000.0f;
        // 敵機の領域外判定用定数
        private const float enemyRightLimit = 5000.0f;
        private const float enemyLeftLimit = -5000.0f;
        private const float enemyUpLimit = 10000.0f;
        private const float enemyDownLimit = -1000.0f;
        // 泡パーティクルエミッターの位置
        private readonly Vector2 bubblePosition = new Vector2(400, 240);
        // 雪パーティクルエミッターの位置
        private readonly Vector2 snowPosition = new Vector2(400, 0);
        // 敵破壊エフェクト再生時間
        internal const double enemyShatterTimeLimit = 0.6;
        // 弾破壊エフェクト再生時間
        internal const double bulletShatterTimeLimit = 0.5;
        // ラストステージ
        internal const int lastStageNo = 3;

        #endregion

        #region 形状データ

        /// <summary>
        /// 自機モデルのリストを取得します。
        /// </summary>
        public List<Model> PlayerShapes { get; private set; }
        /// <summary>
        /// 自機のテクスチャ
        /// </summary>
        private Texture2D[] playerTexture = new Texture2D[3];
        /// <summary>
        /// 敵機モデルのリストを設定します。
        /// </summary>
        public List<Model> EnemyShapes { get; private set; }
        /// <summary>
        /// 背景モデルのリストを設定します。
        /// </summary>
        public List<Model> BackGroundShapes { get; private set; }
        /// <summary>
        /// 円柱モデルのリストを設定します。
        /// </summary>
        public List<Model> CylinderShapes { get; private set; }
        /// <summary>
        /// 弾モデルのリストを設定します。
        /// </summary>
        public List<Model> BulletShapes { get; private set; }
        /// <summary>
        /// ステージ番号のテクスチャ
        /// </summary>
        public List<Texture2D> StageNumberShape { get; private set; }
        /// <summary>
        /// バウンディングスフィア(デバッグ用)
        /// </summary>
        public RenderBoundingSphere RenderBoundingSphere2 { get; private set; }

        #endregion

        #region フィールドとプロパティ

        // ゲームプレイ画面の経過時間
        private TimeSpan screenTime = TimeSpan.Zero;

        // スクリーンの状態が変化してからの経過時間
        public TimeSpan StateTime { get; set; }
        // 自機弾制御用時間
        public TimeSpan rapidFireTime { get; set; }
        // 現在のステージ番号
        public int stageNo = 0;
        //public int stageNo = 3;
        // コンティニュー選択用フラグ
        public bool continueSelect = true;

        /// <summary>
        /// このスクリーンの状態を取得します。
        /// </summary>
        public IScreenState<PlayGameScreen> State { get; private set; }

        /// <summary>
        /// スプライト描画オブジェクトを設定します。
        /// </summary>
        private SpriteBatch spriteBatch;

        /// <summary>
        /// カメラ
        /// </summary>
        public ChaseCamera camera { get; private set; }
        /// <summary>
        /// カメラ
        /// </summary>
        public ChaseCamera playerCamera { get; private set; }
        /// <summary>
        /// カメラのスプリング
        /// </summary>
        public bool cameraSpringEnabled = true;

        /// <summary>
        /// プレイヤー
        /// </summary>
        public Player Player { get; private set; }
        /// <summary>
        /// 敵機配列
        /// </summary>
        public EnemyManager Enemies { get; private set; }
        /// <summary>
        /// ボス
        /// </summary>
        public LinkedListNode<Enemy> Boss { get; private set; }
        /// <summary>
        /// 背景
        /// </summary>
        public List<BackGround> BackGrounds { get; private set; }
        /// <summary>
        /// 円柱
        /// </summary>
        public Cylinder Cylinder { get; private set; }
        /// <summary>
        /// 弾
        /// </summary>
        public BulletManager Bullets { get; private set; }
        /// <summary>
        /// ロックオン
        /// </summary>
        public LockOnManager LockOns { get; private set; }
        /// <summary>
        /// ステージ番号画像
        /// </summary>
        public List<SpriteItem> StageNumberLogo { get; private set; }
        /// <summary>
        /// 残機アイコン
        /// </summary>
        private SpriteItem[] hitPointIcons = new SpriteItem[3];

        /// <summary>
        /// ステージクリア
        /// </summary>
        public SpriteItem StageClear { get; private set; }
        /// ゲームオーバー
        /// </summary>
        public SpriteItem GameOver { get; private set; }
        /// <summary>
        /// コンテニュー
        /// </summary>
        public SpriteItem ContinueText { get; private set; }
        /// <summary>
        /// Yes(コンテニュー)
        /// </summary>
        public SpriteItem YesText { get; private set; }
        /// <summary>
        /// No(コンテニュー)
        /// </summary>
        public SpriteItem NoText { get; private set; }
        /// <summary>
        /// Yes選択(コンテニュー)
        /// </summary>
        public SpriteItem YesTextLight { get; private set; }
        /// <summary>
        /// No選択(コンテニュー)
        /// </summary>
        public SpriteItem NoTextLight { get; private set; }
        /// <summary>
        /// スコア
        /// </summary>
        public Score scoreNumber { get; set; }

        /// <summary>
        /// ポーズ時のアルファ値
        /// </summary>
        private float pauseAlpha;

        /// <summary>
        /// オーディオを作成します。
        /// </summary>
        private AudioEngine audioEngine;
        private WaveBank waveBank;
        private AudioCategory musicCategory;
        public SoundBank SoundBank { get; set; }
        public Cue BGM, Enter, PlayerDamage, EnemyDestory, BulletSound;

#if DEBUG
        private XYZAxisDraw XYZAxis;
        private SpriteFont debugFont;
#endif

        #endregion

        #region パーティクル

        /// <summary>
        /// 爆発パーティクルシステム
        /// </summary>
        public ParticleSystem Explosion { set; get; }
        /// <summary>
        /// 煙パーティクルシステム
        /// </summary>
        public ParticleSystem Smoke { set; get; }
        /// <summary>
        /// 泡パーティクルシステム
        /// </summary>
        public ParticleEmitter BubbleEmitter { set; get; }
        public ParticleSystem BubbleEmitterSystem { set; get; }
        /// <summary>
        /// 雪パーティクルシステム
        /// </summary>
        public ParticleEmitter SnowEmitter { set; get; }
        public ParticleSystem SnowEmitterSystem { set; get; }
        /// <summary>
        /// オーラパーティクルシステム
        /// </summary>
        public ParticleEmitter AuraEmitter { set; get; }
        public ParticleSystem AuraEmitterSystem { set; get; }

        #endregion

        #region 初期化

        /// <summary>
        /// コンストラクター
        /// </summary>
        public PlayGameScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            // 追尾カメラを作成します。
            camera = new ChaseCamera();
            playerCamera = new ChaseCamera();

            // カメラのオフセットを設定します。
            playerCamera.LookAtOffset = new Vector3(0.0f, 0.0f, -8500.0f);
            playerCamera.DesiredPositionOffset = new Vector3(0.0f, 1100.0f, 9700.0f * 2);

            // カメラのクリップ面を設定します。
            playerCamera.NearPlaneDistance = 10.0f;
            playerCamera.FarPlaneDistance = 200000.0f;

            // カメラの画角を設定します。
            playerCamera.FieldOfView = (MathHelper.Pi / 8);

            playerCamera.desiredPositionY = 1500.0f;
            playerCamera.lookAtY = 1500.0f;

            // 各オブジェクトのインスタンスを作成します。
            Player = new Players.Player();
            EnemyShapes = new List<Model>();
            BackGroundShapes = new List<Model>();
            CylinderShapes = new List<Model>();
            BulletShapes = new List<Model>();
            StageNumberShape = new List<Texture2D>();

            // 敵機配列
            Enemies = new EnemyManager();
            // 弾配列
            Bullets = new BulletManager(50);
            // ロックオン配列
            LockOns = new LockOnManager();
            // 背景
            BackGrounds = new List<BackGround>();

            // スコアを初期化します。
            scoreNumber = new Score();
            scoreNumber.CurrentScore = 0;

            // 時間を初期化
            StateTime = TimeSpan.Zero;
            rapidFireTime = TimeSpan.Zero;
        }

        /// <summary>
        /// ゲームのグラフィック コンテンツを読み込みます。
        /// </summary>
        public override void LoadContent()
        {
            if (this.Content == null)
                this.Content = new ContentManager(ScreenManager.Game.Services, "Content\\PlayGames");

            // リスト登録用
            Model _model;

            spriteBatch = ScreenManager.SpriteBatch;

            // カメラの縦横比を設定します。これは、グラフィック デバイスを初期化する 
            // base.Initalize() の後に行う必要があります。
            playerCamera.AspectRatio = (float)this.ScreenManager.Game.GraphicsDevice.Viewport.Width /
                this.ScreenManager.Game.GraphicsDevice.Viewport.Height;

            // 自機
            PlayerShapes = LoadPlayerContent();
            // 自機のテクスチャ
            playerTexture[0] = this.Content.Load<Texture2D>("Player\\dtred");
            playerTexture[1] = this.Content.Load<Texture2D>("Player\\dtblue");
            playerTexture[2] = this.Content.Load<Texture2D>("Player\\dtgreen");

            // 水面
            _model = this.Content.Load<Model>("BackGrounds\\Models\\WaterSurface");
            BackGroundShapes.Add(_model);
            // 氷面
            _model = this.Content.Load<Model>("BackGrounds\\Models\\IceSurface");
            BackGroundShapes.Add(_model);
            // 雪原
            _model = this.Content.Load<Model>("BackGrounds\\terrain");
            BackGroundShapes.Add(_model);
            // 円柱
            CylinderShapes = LoadCylinder();

            // 弾
            BulletShapes.Add(this.Content.Load<Model>("Bullets\\ammo_red"));     // 赤
            BulletShapes.Add(this.Content.Load<Model>("Bullets\\ammo_blue"));    // 蒼
            BulletShapes.Add(this.Content.Load<Model>("Bullets\\ammo_green"));   // 緑

            // 敵機
            EnemyShapes.Add(this.Content.Load<Model>("Enemy\\en_red"));      // 赤
            EnemyShapes.Add(this.Content.Load<Model>("Enemy\\en_blue"));     // 蒼
            EnemyShapes.Add(this.Content.Load<Model>("Enemy\\en_green"));    // 緑
            EnemyShapes.Add(this.Content.Load<Model>("Enemy\\Boss\\boss"));  // ボス

            // ステージ番号
            for (int index = 0; index <= lastStageNo; ++index)
                StageNumberShape.Add(this.Content.Load<Texture2D>("Label\\Stage" + (index + 1)));

            SpriteItem stageNumber;
            StageNumberLogo = new List<SpriteItem>();
            for (int index = 0; index < StageNumberShape.Count; ++index)
            {
                stageNumber = new SpriteItem();
                stageNumber.Shape = new SpriteShape();
                stageNumber.Shape.Texture = StageNumberShape[index];
                stageNumber.Shape.Origin.X = stageNumber.Shape.Texture.Width / 2;
                stageNumber.Shape.Origin.Y = stageNumber.Shape.Texture.Height / 2;
                stageNumber.Position.X = ScreenManager.GraphicsDevice.Viewport.Width / 2;
                stageNumber.Position.Y = ScreenManager.GraphicsDevice.Viewport.Height / 2;
                stageNumber.Color.A = 0;
                StageNumberLogo.Add(stageNumber);
            }

            // ステージクリア
            StageClear = new SpriteItem();
            StageClear.Position.X = this.ScreenManager.GraphicsDevice.Viewport.Width / 2;
            StageClear.Position.Y = this.ScreenManager.GraphicsDevice.Viewport.Height / 2;
            StageClear.Color.A = 0;
            StageClear.Shape = new SpriteShape();
            StageClear.Shape.Texture = this.Content.Load<Texture2D>("Label\\StageClear");
            StageClear.Shape.Origin.X = StageClear.Shape.Texture.Width / 2;
            StageClear.Shape.Origin.Y = StageClear.Shape.Texture.Height / 2;

            // ゲームオーバー
            GameOver = new SpriteItem();
            GameOver.Position.X = ScreenManager.GraphicsDevice.Viewport.Width / 2;
            GameOver.Position.Y = ScreenManager.GraphicsDevice.Viewport.Height / 2 - 100;
            GameOver.Color.A = 0;
            GameOver.Shape = new SpriteShape();
            GameOver.Shape.Texture = this.Content.Load<Texture2D>("..\\Screens\\GameOverScreen\\GameOver");
            GameOver.Shape.Origin.X = GameOver.Shape.Texture.Width / 2;
            GameOver.Shape.Origin.Y = GameOver.Shape.Texture.Height / 2;

            // コンテニュー
            ContinueText = new SpriteItem();
            ContinueText.Position.X = this.ScreenManager.GraphicsDevice.Viewport.Width / 2;
            ContinueText.Position.Y = this.ScreenManager.GraphicsDevice.Viewport.Height / 2;
            ContinueText.Color.A = 0;
            ContinueText.Shape = new SpriteShape();
            ContinueText.Shape.Texture = this.Content.Load<Texture2D>("..\\Screens\\GameOverScreen\\continue");
            ContinueText.Shape.Origin.X = ContinueText.Shape.Texture.Width / 2;
            ContinueText.Shape.Origin.Y = ContinueText.Shape.Texture.Height / 2;

            // Yes
            YesText = new SpriteItem();
            YesText.Position.X = ContinueText.Position.X - 100;
            YesText.Position.Y = ContinueText.Position.Y + 100;
            YesText.Color.A = 255;
            YesText.Shape = new SpriteShape();
            YesText.Shape.Texture = this.Content.Load<Texture2D>("..\\Screens\\GameOverScreen\\yes");
            YesText.Shape.Origin.X = YesText.Shape.Texture.Width / 2;
            YesText.Shape.Origin.Y = YesText.Shape.Texture.Height / 2;

            // Yes選択
            YesTextLight = new SpriteItem();
            YesTextLight.Position.X = ContinueText.Position.X - 100;
            YesTextLight.Position.Y = ContinueText.Position.Y + 100;
            YesTextLight.Color.A = 0;
            YesTextLight.Shape = new SpriteShape();
            YesTextLight.Shape.Texture = this.Content.Load<Texture2D>("..\\Screens\\GameOverScreen\\YES_light");
            YesTextLight.Shape.Origin.X = YesTextLight.Shape.Texture.Width / 2;
            YesTextLight.Shape.Origin.Y = YesTextLight.Shape.Texture.Height / 2;

            // No
            NoText = new SpriteItem();
            NoText.Position.X = ContinueText.Position.X + 100;
            NoText.Position.Y = ContinueText.Position.Y + 100;
            NoText.Color.A = 0;
            NoText.Shape = new SpriteShape();
            NoText.Shape.Texture = this.Content.Load<Texture2D>("..\\Screens\\GameOverScreen\\no");
            NoText.Shape.Origin.X = NoText.Shape.Texture.Width / 2;
            NoText.Shape.Origin.Y = NoText.Shape.Texture.Height / 2;

            // No選択
            NoTextLight = new SpriteItem();
            NoTextLight.Position.X = ContinueText.Position.X + 100;
            NoTextLight.Position.Y = ContinueText.Position.Y + 100;
            NoTextLight.Color.A = 255;
            NoTextLight.Shape = new SpriteShape();
            NoTextLight.Shape.Texture = this.Content.Load<Texture2D>("..\\Screens\\GameOverScreen\\No_light");
            NoTextLight.Shape.Origin.X = NoTextLight.Shape.Texture.Width / 2;
            NoTextLight.Shape.Origin.Y = NoTextLight.Shape.Texture.Height / 2;

            // 残機アイコン
            for (int index = 0; index < hitPointIcons.Length; ++index)
            {
                hitPointIcons[index] = new SpriteItem();
                hitPointIcons[index].Position = new Vector2(1060.0f + index * 50.0f, 50.0f);
                hitPointIcons[index].Shape = new SpriteShape();
                hitPointIcons[index].Shape.Texture = this.Content.Load<Texture2D>("Player\\HP\\hp");
                hitPointIcons[index].Shape.Origin.X = hitPointIcons[index].Shape.Texture.Width / 2;
                hitPointIcons[index].Shape.Origin.Y = hitPointIcons[index].Shape.Texture.Height / 2;
            }

            // ロックオンマーカー
            SpriteSheet lockOnSheet;
            lockOnSheet = this.Content.Load<SpriteSheet>("LockOn");
            lockOnSheet.Origin.X = lockOnSheet.SourceRectangle(0).Width / 2;
            lockOnSheet.Origin.Y = lockOnSheet.SourceRectangle(0).Height / 2;
            // ロックオンマネージャーに登録
            LockOn lockOn;
            for (int index = 0; index < LockOnManager.MaximumLockOns; ++index)
            {
                lockOn = new LockOn();
                lockOn.Shape = lockOnSheet;
                lockOn.Position = Vector2.Zero;
                lockOn.Scale = new Vector2(0.5f);
                LockOns.ReservedList.AddLast(lockOn);
            }

            // パーティクル システムを作成
            // 爆発
            Explosion = new ParticleSystem(this.ScreenManager.Game, "PlayGames\\Particle\\ExplosionSettings") { DrawOrder = ParticleSystem.AdditiveDrawOrder };
            this.ScreenManager.Game.Components.Add(Explosion);
            Smoke = new ParticleSystem(this.ScreenManager.Game, "PlayGames\\Particle\\ExplosionSmokeSettings") { DrawOrder = ParticleSystem.AlphaBlendDrawOrder };
            this.ScreenManager.Game.Components.Add(Smoke);
            // 泡
            BubbleEmitterSystem = new ParticleSystem(this.ScreenManager.Game, "PlayGames\\Particle\\Bubble") { DrawOrder = ParticleSystem.AlphaBlendDrawOrder };
            BubbleEmitterSystem.Initialize();
            BubbleEmitter = new ParticleEmitter(BubbleEmitterSystem, 15, new Vector2(1280, 240));
            // 雪
            SnowEmitterSystem = new ParticleSystem(this.ScreenManager.Game, "PlayGames\\Particle\\SnowSetteings") { DrawOrder = ParticleSystem.AlphaBlendDrawOrder };
            SnowEmitterSystem.Initialize();
            SnowEmitter = new ParticleEmitter(SnowEmitterSystem, 30, new Vector2(1280, 0));
            // オーラ
            AuraEmitterSystem = new ParticleSystem(this.ScreenManager.Game, "PlayGames\\Particle\\AuraSettings") { DrawOrder = ParticleSystem.AlphaBlendDrawOrder };
            AuraEmitterSystem.Initialize();
            AuraEmitter = new ParticleEmitter(AuraEmitterSystem, 2, Vector2.Zero);

            // オーディオオブジェクトを初期化します。
            audioEngine = new AudioEngine("Content\\PlayGames\\Sounds\\PlayGameScreenSounds.xgs");
            SoundBank = new SoundBank(audioEngine, "Content\\PlayGames\\Sounds\\Sound Bank.xsb");
            waveBank = new WaveBank(audioEngine, "Content\\PlayGames\\Sounds\\Wave Bank.xwb");

            // カテゴリーを取得します。
            musicCategory = audioEngine.GetCategory("Music");
            musicCategory.SetVolume(1.0f);
            // 音楽を再生します。
            BGM = SoundBank.GetCue("stage1");

#if DEBUG
            // テスト用球体
            this.RenderBoundingSphere2 = new RenderBoundingSphere();
            _model = this.Content.Load<Model>("Debug\\testsphere");
            this.RenderBoundingSphere2.Shape = _model;
            // Font
            this.debugFont = this.Content.Load<SpriteFont>("..\\Font\\DebugFont");
#endif
            // 初期化処理
            Reset();
            SetStartState();

#if DEBUG
            // デバックコマンドの登録
            DebugSystem.Instance.DebugCommandUI.RegisterCommand(
                 "N",              // コマンド名
                 "next stage",     // コマンド説明
                 NextStage         // コマンド実行用デリゲート
                 );
            DebugSystem.Instance.DebugCommandUI.RegisterCommand(
                 "B",              // コマンド名
                 "boss.Value stage",     // コマンド説明
                 BossStage         // コマンド実行用デリゲート
                 );

            this.XYZAxis = new XYZAxisDraw(this.ScreenManager.GraphicsDevice);
#endif
            // ガベージコレクションを実行
            GC.Collect();

            // 読み込みが完了したら、ResetElapsedTime() を使用して、非常に長い
            // フレームを完了したことと、キャッチアップしようとする必要がないことを、
            // ゲームのタイミング メカニズムに指示します。
            ScreenManager.Game.ResetElapsedTime();
        }

#if DEBUG
        #region デバックコマンド

        /// <summary>
        /// 次のステージに進めます。
        /// </summary>
        /// <param name="host">ホストインスタンス</param>
        /// <param name="command">コマンド名</param>
        /// <param name="arguments">引数</param>
        private void NextStage(IDebugCommandHost host, string command,
                                            IList<string> arguments)
        {
            SetStageClearState();
        }

        /// <summary>
        /// ボスステージに進めます。
        /// </summary>
        /// <param name="host">ホストインスタンス</param>
        /// <param name="command">コマンド名</param>
        /// <param name="arguments">引数</param>
        private void BossStage(IDebugCommandHost host, string command,
                                            IList<string> arguments)
        {
            SetStageClearState();
            stageNo = PlayGameScreen.lastStageNo;
        }

        #endregion
#endif

        // 自機のリソースを読み込みます。
        private List<Model> LoadPlayerContent()
        {
            List<Model> list = new List<Model>();
            list.Add(this.Content.Load<Model>("Player\\Dolphin_S"));
            list.Add(this.Content.Load<Model>("Player\\Dolphin_A"));
            list.Add(this.Content.Load<Model>("Player\\Dolphin_D"));

            return list;
        }

        // 円柱のリソースを読み込みます。
        private List<Model> LoadCylinder()
        {
            List<Model> list = new List<Model>();
            list.Add(this.Content.Load<Model>("BackGrounds\\Models\\red"));
            list.Add(this.Content.Load<Model>("BackGrounds\\Models\\blue"));
            list.Add(this.Content.Load<Model>("BackGrounds\\Models\\green"));

            return list;
        }

        /// <summary>
        /// 画面のコンテンツを破棄します。
        /// </summary>
        public override void UnloadContent()
        {
            this.Content.Unload();

#if DEBUG
            DebugSystem.Instance.DebugCommandUI.UnregisterCommand("N");
            DebugSystem.Instance.DebugCommandUI.UnregisterCommand("B");
#endif

            base.UnloadContent();
        }

        /// <summary>
        /// ゲーム画面を初期化します。
        /// </summary>
        public void Reset()
        {
            if (BGM.IsPlaying)
                BGM.Stop(AudioStopOptions.Immediate);
            BGM = SoundBank.GetCue("stage" + (stageNo + 1));
            BGM.Play();

            // 自機を初期化します。
            ResetPlayer();

            // 敵機を追加します。
            LoadEnemies();
            // ラストステージの場合ボスと取り出します。
            if (stageNo == lastStageNo &&
                Enemies.ReservedList.First.Value.ElementType == ElementType.Boss)
            {
                Boss = Enemies.ReservedList.First;
                Enemies.ActiveEnemy(Boss);
            }

            // ロックオンマーカーを初期化します。
            if (LockOns.ActiveList.Count > 0)
            {
                LinkedListNode<LockOn> lockOn = LockOns.ActiveList.First, removeLockOn;
                while (lockOn != null)
                {
                    removeLockOn = lockOn;
                    lockOn = lockOn.Next;
                    LockOns.Clear(removeLockOn);
                }
            }

            // 背景をリセットします。
            ResetBackGrounds();

            // カメラをリセットします。
            UpdateCameraChaseTarget();
            camera.Reset();
            playerCamera.Reset();
        }

        /// <summary>
        /// 背景をリセットします。
        /// </summary>
        private void ResetBackGrounds()
        {
            // 背景を追加します。
            BackGrounds.Clear();
            BackGround backGround;

            // 水面
            backGround = new BackGround();
            backGround.Shape = BackGroundShapes[0];
            backGround.Position = Vector3.Zero;
            backGround.Alpha = 0.7f;
            BackGrounds.Add(backGround);
            // 氷面
            backGround = new BackGround();
            backGround.Shape = BackGroundShapes[1];
            backGround.Position = new Vector3(0, -150000, 0);
            backGround.Alpha = 1;
            BackGrounds.Add(backGround);
            // 雪原
            backGround = new BackGround();
            backGround.Shape = BackGroundShapes[2];
            backGround.Position = Vector3.Zero;
            backGround.Alpha = 0.7f;
            BackGrounds.Add(backGround);
            // 円柱
            Cylinder = new Cylinder();
            Cylinder.Shapes = CylinderShapes;
            Cylinder.Position = Vector3.Zero;
            Cylinder.SetNomalState(this);
        }

        /// <summary>
        /// 自機を開始状態にリセットします。
        /// </summary>
        private void ResetPlayer()
        {
            //Player = new Player();
            Player.Acceleration = Vector3.Zero;
            Player.Velocity = Vector3.Zero;
            Player.Position = new Vector3(0, -3000, 0);
            Player.SetRedState(this);
            Player.HitPoint = 3;
            Player.Shapes = PlayerShapes;
            Player.EmissiveColor = new Vector3(0.1f, 0.1f, 0.4f);
            Player.Alpha = 1;
            Player.SetStandState(this);
        }

        /// <summary>
        /// 敵機データをCSVファイルから読み込みます。
        /// </summary>
        private void LoadEnemies()
        {
            Enemy _enemy;

            // 敵機配列をクリアします。
            Enemies.Clear();

            // ボスステージ
            if (stageNo == PlayGameScreen.lastStageNo)
            {
                _enemy = new Enemy();
                _enemy.Acceleration = Vector3.Zero;
                _enemy.Position = new Vector3(0, 30000, -50000.0f);
                _enemy.SetElement(EnemyShapes[(int)ElementType.Boss], ElementType.Boss);
                _enemy.HitPoint = 24;
                _enemy.SetBossState(this);
                _enemy.Element = BossEnemy.GetInstance(this);
                EnemyShapes[(int)_enemy.ElementType].Meshes[0].BoundingSphere.Transform(Matrix.CreateScale(40));
                Enemies.EnemyAdd(_enemy,
                                 _enemy.Position.X,
                                 _enemy.Position.Y,
                                 _enemy.Position.Z,
                                 Vector3.Zero,
                                 _enemy.Acceleration.X,
                                 _enemy.Acceleration.Y,
                                 _enemy.Acceleration.Z,
                                 EnemyShapes[(int)_enemy.ElementType],
                                 _enemy.HitPoint,
                                 _enemy.ElementType);

                for (int index = 0; index < 30; ++index)
                {
                    _enemy = new Enemy();
                    _enemy.Acceleration = Vector3.Zero;
                    _enemy.Position = new Vector3(0, 30000, -50000.0f);
                    _enemy.HitPoint = 6;
                    _enemy.SetBossState(this);
                    _enemy.BoundingSphere.Transform(Matrix.CreateScale(10));
                    _enemy.SetElement(EnemyShapes[index % 3], (ElementType)(index % 3));
                    switch (_enemy.ElementType)
                    {
                        case ElementType.Red:
                            _enemy.Element = RedEnemyState.GetInstance(this);
                            break;
                        case ElementType.Blue:
                            _enemy.Element = BlueEnemyState.GetInstance(this);
                            break;
                        case ElementType.Green:
                            _enemy.Element = GreenEnemyState.GetInstance(this);
                            break;
                    }
                    Enemies.EnemyAdd(_enemy,
                                     _enemy.Position.X,
                                     _enemy.Position.Y,
                                     _enemy.Position.Z,
                                     Vector3.Zero,
                                     _enemy.Acceleration.X,
                                     _enemy.Acceleration.Y,
                                     _enemy.Acceleration.Z,
                                     EnemyShapes[(int)_enemy.ElementType],
                                     _enemy.HitPoint,
                                     _enemy.ElementType);
                }
            }
            else
            {
                Table enemyTable = this.Content.Load<Table>("Stage" + (stageNo + 1));
                for (int Row = 0; Row < enemyTable.RowCount; ++Row)
                {
                    _enemy = new Enemy();
                    _enemy.Acceleration.X = float.Parse(enemyTable.Rows[Row][0]);
                    _enemy.Acceleration.Y = float.Parse(enemyTable.Rows[Row][1]);
                    _enemy.Acceleration.Z = float.Parse(enemyTable.Rows[Row][2]);
                    _enemy.Position.X = float.Parse(enemyTable.Rows[Row][3]);
                    _enemy.Position.Y = float.Parse(enemyTable.Rows[Row][4]);
                    _enemy.Position.Z = float.Parse(enemyTable.Rows[Row][5]);
                    _enemy.HitPoint = int.Parse(enemyTable.Rows[Row][6]);
                    _enemy.SetElement(EnemyShapes[int.Parse(enemyTable.Rows[Row][7])], (ElementType)int.Parse(enemyTable.Rows[Row][7]));

                    _enemy.SetDefaultState(this);
                    switch (_enemy.ElementType)
                    {
                        case ElementType.Red:
                            _enemy.Element = RedEnemyState.GetInstance(this);
                            break;
                        case ElementType.Blue:
                            _enemy.Element = BlueEnemyState.GetInstance(this);
                            break;
                        case ElementType.Green:
                            _enemy.Element = GreenEnemyState.GetInstance(this);
                            break;
                    }
                    Enemies.EnemyAdd(_enemy,
                                     _enemy.Position.X,
                                     _enemy.Position.Y,
                                     _enemy.Position.Z,
                                     Vector3.Zero,
                                     _enemy.Acceleration.X,
                                     _enemy.Acceleration.Y,
                                     _enemy.Acceleration.Z,
                                     EnemyShapes[(int)_enemy.ElementType],
                                     _enemy.HitPoint,
                                     _enemy.ElementType);
                }
            }
        }

        #endregion

        #region フレーム更新処理

        /// <summary>
        /// ゲームプレイ画面を更新します。
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            // オーディオエンジンを更新します。
            audioEngine.Update();

            // ポーズ画面のアルファ値を設定します。
            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);

            if (IsActive)
            {
                screenTime += gameTime.ElapsedGameTime;
                StateTime += gameTime.ElapsedGameTime;
                rapidFireTime += gameTime.ElapsedGameTime;

                // 弾リストの更新
                Bullets.Update(gameTime);
                // パーティクルの更新
                Explosion.Update(gameTime);
                Smoke.Update(gameTime);
                if (Player.ElementType == ElementType.Blue)
                {
                    BubbleEmitterSystem.Update(gameTime);
                    BubbleEmitter.Update(gameTime, bubblePosition);
                }
                else if (Player.ElementType == ElementType.Green)
                {
                    SnowEmitterSystem.Update(gameTime);
                    SnowEmitter.Update(gameTime, snowPosition);
                }
                // 背景更新
                Cylinder.Position.Z = camera.Position.Z;
                Cylinder.Position.X = camera.Position.X;
                Cylinder.Update(Player, gameTime);
                for (int index = 0; index < BackGrounds.Count; ++index)
                    BackGrounds[index].Update(Player, gameTime);

                // ゲーム状態によって更新処理を変更する。
                State.Update(gameTime, this);
            }
            if (ScreenState == UniverseColors.ScreenState.Hidden)
            {
                if (BGM.IsPlaying)
                    BGM.Stop(AudioStopOptions.Immediate);
            }
        }

        /// <summary>
        /// プレイヤーの領域外判定
        /// </summary>
        public void PlayStageIsPlayerOutOfBounds()
        {
            if (Player.Position.X >= playerRightLimit)
            {
                Player.Acceleration.X = 0;
                Player.Velocity.X = 0;
            }
            else if (Player.Position.X <= playerLeftLimit)
            {
                Player.Acceleration.X = 0;
                Player.Velocity.X = 0;
            }

            if (Player.Position.Y >= playerUpLimit)
            {
                Player.Acceleration.Y = 0;
                Player.Velocity.Y = 0;
            }
            else if (Player.Position.Y <= playerDownLimit)
            {
                Player.Acceleration.Y = 0;
                Player.Velocity.Y = 0;
            }
        }

        /// <summary>
        /// 敵機配列と自機弾との当たり判定を行います。
        /// </summary>
        /// <param name="bullet">判定を行なう弾</param>
        /// <returns>ヒットした敵(いない場合null)</returns>
        public LinkedListNode<Enemy> CollisionDetectByBullets(Bullet bullet)
        {
            LinkedListNode<Enemy> enemyNode = Enemies.LockOnList.First;
            while (enemyNode != null)
            {
                Enemy enemy = enemyNode.Value;

                // 速い弾の場合、すり抜けてしまうことがあるため直前の位置座標を考慮して衝突判定する
                float time;
                Vector3 sourcePos, targetPos;
                if (CollisionDetect.ParticleCollisionDetect(
                    enemy.BoundingSphere.Radius,
                    bullet.BoundingSphere.Radius,
                    ref enemy.PreviousPosition,
                    ref enemy.Position,
                    ref bullet.PreviousPosition,
                    ref bullet.Position,
                    out time, out sourcePos, out targetPos))
                    return enemyNode;

                enemyNode = enemyNode.Next;
            }
            return null;
        }
        /// <summary>
        /// 敵機とアイテムのあたり判定を行います。
        /// </summary>
        /// <param name="Enemy">敵機</param>
        /// <param name="Item">ターゲットアイテム</param>
        /// <returns></returns>
        public bool CollisionDetectByEnemies(LinkedListNode<Enemy> Enemy, BasicItem Item)
        {
            float time;
            Vector3 sourcePos, targetPos;
            if (CollisionDetect.ParticleCollisionDetect(
                Enemy.Value.BoundingSphere.Radius,
                Item.BoundingSphere.Radius,
                ref Enemy.Value.PreviousPosition,
                ref Enemy.Value.Position,
                ref Item.PreviousPosition,
                ref Item.Position,
                out time, out sourcePos, out targetPos))
                return true;

            return false;
        }

        /// <summary>
        /// ワールド座標をスクリーン座標に変換します。
        /// </summary>
        /// <returns>スクリーン座標</returns>
        public Vector2 GetScreenPosition(BasicItem item)
        {
            Matrix screenMatrix = Matrix.Identity;
            screenMatrix.M11 = this.ScreenManager.GraphicsDevice.Viewport.Width / 2;
            screenMatrix.M22 = -this.ScreenManager.GraphicsDevice.Viewport.Height / 2;
            screenMatrix.M41 = this.ScreenManager.GraphicsDevice.Viewport.Width / 2;
            screenMatrix.M42 = this.ScreenManager.GraphicsDevice.Viewport.Height / 2;

            Vector4 vector4 = Vector4.Zero;
            vector4 = Vector4.Transform(Vector4.UnitW, item.World * playerCamera.View * playerCamera.Projection * screenMatrix);

            Vector2 screenPosition = Vector2.Zero;
            screenPosition.X = vector4.X / vector4.W;
            screenPosition.Y = vector4.Y / vector4.W;

            return screenPosition;
        }

        /// <summary>
        /// カメラが追尾するべき位置情報を更新します。
        /// </summary>
        public void UpdateCameraChaseTarget()
        {
            camera.ChasePosition = Player.Position;
            playerCamera.ChasePosition = Player.Position;
            playerCamera.ChaseDirection = Vector3.Forward;
            playerCamera.Up = Vector3.Up;
        }

        #endregion

        #region 入力処理

        /// <summary>
        /// ゲームがプレイヤーの入力に応答できるようにします。Update メソッドと異なり、
        /// ゲームプレイ画面がアクティブな場合にのみ呼び出されます。
        /// </summary>
        public override void HandleInput(InputState input)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            // Pause画面の場合
            if (input.IsPauseGame(ControllingPlayer))
            {
                ScreenManager.AddScreen(new PauseMenuScreen(), ControllingPlayer);
                return;
            }
            // それ以外の場合、プレイヤーの位置を移動します。
            // ゲームの状態に応じて操作を切り替えます。
            State.HandleInput(input, this);

#if DEBUG
            PlayerIndex playerIndex;
            KeyboardState keyState = Keyboard.GetState();

            // ゲームの終了処理
            if (input.IsNewButtonPress(Buttons.Back, PlayerIndex.One, out playerIndex) ||
                input.IsNewKeyPress(Keys.Escape, PlayerIndex.One, out playerIndex))
                this.ScreenManager.Game.Exit();

            // デバッグコマンドUIが表示されている間はキー入力は無視する
            if (DebugSystem.Instance.DebugCommandUI.Focused)
                keyState = new KeyboardState();

            // Aボタン、AキーでFPSカウンターの表示、非表示
            if (input.IsNewButtonPress(Buttons.A, PlayerIndex.One, out playerIndex) ||
                            input.IsNewKeyPress(Keys.A, PlayerIndex.One, out playerIndex))
                DebugSystem.Instance.FpsCounter.Visible = !DebugSystem.Instance.FpsCounter.Visible;

            // Bボタン、BキーでTimeRulerの表示、非表示
            if (input.IsNewButtonPress(Buttons.B, PlayerIndex.One, out playerIndex) ||
                            input.IsNewKeyPress(Keys.S, PlayerIndex.One, out playerIndex))
                DebugSystem.Instance.TimeRuler.Visible = !DebugSystem.Instance.TimeRuler.Visible;

            // Yボタン、XキーでTimeRulerログの表示、非表示
            if (input.IsNewButtonPress(Buttons.Y, PlayerIndex.One, out playerIndex) ||
                            input.IsNewKeyPress(Keys.D, PlayerIndex.One, out playerIndex))
            {
                DebugSystem.Instance.TimeRuler.Visible = true;
                DebugSystem.Instance.TimeRuler.ShowLog = !DebugSystem.Instance.TimeRuler.ShowLog;
            }
            // Gキーで無敵モード
            if (input.IsNewKeyPress(Keys.G, PlayerIndex.One, out playerIndex))
                Player.Invincible = !Player.Invincible;
#endif
        }

        #endregion

        #region 描画処理

        /// <summary>
        /// ゲームプレイ画面を描画します。
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice device = ScreenManager.GraphicsDevice;

            // 画面のクリア色
            device.Clear(ClearOptions.Target, Color.DarkBlue, 0, 0);

            // 2Dの設定をリセットします
            device.BlendState = BlendState.Opaque;
            device.DepthStencilState = DepthStencilState.Default;
            device.SamplerStates[0] = SamplerState.LinearWrap;

            // 雪原を描画します。
            if (Player.ElementType == ElementType.Green)
                BackGrounds[2].Draw(gameTime, playerCamera.View, playerCamera.Projection, Player);

            // 自機弾配列の描画
            LinkedListNode<Bullet> playerbullet = Bullets.ActiveList.First;
            while (playerbullet != null)
            {
                playerbullet.Value.Draw(gameTime, playerCamera.Position, playerCamera.View, playerCamera.Projection);
                playerbullet = playerbullet.Next;
            }
            playerbullet = Bullets.DestroyList.First;
            while (playerbullet != null)
            {
                playerbullet.Value.Draw(gameTime, playerCamera.Position, playerCamera.View, playerCamera.Projection);
                playerbullet = playerbullet.Next;
            }

            // 円柱を描画します。
            Cylinder.Draw(gameTime, playerCamera.View, playerCamera.Projection, Player);

            // アルファブレンディングあり
            device.BlendState = BlendState.AlphaBlend;

            // 泡を描画します。
            if (Player.ElementType == ElementType.Blue)
                BubbleEmitterSystem.Draw(gameTime);
            // 雪を描画します。
            if (Player.ElementType == ElementType.Green)
                SnowEmitterSystem.Draw(gameTime);

            // 2Dの設定をリセットします
            device.BlendState = BlendState.AlphaBlend;
            device.DepthStencilState = DepthStencilState.Default;
            device.SamplerStates[0] = SamplerState.LinearWrap;

            // 自機を描画します。
            DrawPlayer(device, Player.Shape);
            // 自機のオーラを描画します。
            AuraEmitterSystem.Draw(gameTime);

            // 2Dの設定をリセットします
            device.BlendState = BlendState.AlphaBlend;
            device.DepthStencilState = DepthStencilState.Default;
            device.SamplerStates[0] = SamplerState.LinearWrap;

            // 裏面も描画
            device.RasterizerState = RasterizerState.CullNone;

            // 水面を描画します。
            if (Player.ElementType == ElementType.Red)
                BackGrounds[0].Draw(gameTime, playerCamera.View, playerCamera.Projection, Player);
            // 氷面を描画します。
            if (Player.ElementType == ElementType.Red)
                BackGrounds[1].Draw(gameTime, playerCamera.View, playerCamera.Projection, Player);

            // 表面のみ描画
            device.RasterizerState = RasterizerState.CullCounterClockwise;

            // 敵機の描画
            LinkedListNode<Enemy> enemy;
            // アクティブリスト
            enemy = Enemies.ActiveList.First;
            while (enemy != null)
            {
                enemy.Value.Draw(gameTime, playerCamera.Position, playerCamera.View, playerCamera.Projection);
                enemy = enemy.Next;
            }
            // ロックオンリスト
            enemy = Enemies.LockOnList.First;
            while (enemy != null)
            {
                enemy.Value.Draw(gameTime, playerCamera.Position, playerCamera.View, playerCamera.Projection);
                enemy = enemy.Next;
            }
            // 破壊中の敵機配列を描画します。
            enemy = Enemies.DestroyList.First;
            while (enemy != null)
            {
                enemy.Value.Draw(gameTime, playerCamera.Position, playerCamera.View, playerCamera.Projection);
                enemy = enemy.Next;
            }

            // ロックオンマーカー配列の描画
            LinkedListNode<LockOn> lockOn = LockOns.ActiveList.First;
            while (lockOn != null)
            {
                SpriteItemDraw(lockOn.Value, gameTime);
                lockOn = lockOn.Next;
            }

            // アルファブレンディングなし
            device.BlendState = BlendState.Opaque;

            // 残機アイコンの描画
            for (int index = 0; index < Player.HitPoint; ++index)
                SpriteItemDraw(hitPointIcons[index], gameTime);

            // ステージ番号ラベルを描画します。
            if (this.stageNo <= lastStageNo)
                SpriteItemDraw(StageNumberLogo[stageNo], gameTime);

            // ゲームオーバーを描画します。
            SpriteItemDraw(GameOver, gameTime);
            SpriteItemDraw(ContinueText, gameTime);
            if (continueSelect)
            {
                SpriteItemDraw(NoText, gameTime);
                SpriteItemDraw(YesTextLight, gameTime);
            }
            else if (!continueSelect)
            {
                SpriteItemDraw(YesText, gameTime);
                SpriteItemDraw(NoTextLight, gameTime);
            }

            // ステージクリアラベルを描画します。
            SpriteItemDraw(StageClear, gameTime);

            // 2Dの設定をリセットします
            device.BlendState = BlendState.AlphaBlend;
            device.DepthStencilState = DepthStencilState.Default;
            device.SamplerStates[0] = SamplerState.LinearWrap;

#if DEBUG   // バウンディングスフィアの描画

            RasterizerState rasterizerState = new RasterizerState();
            RasterizerState rasterizerState2 = new RasterizerState();
            // ワイヤーフレームで描画
            rasterizerState.FillMode = FillMode.WireFrame;
            device.RasterizerState = rasterizerState;

            RenderBoundingSphere2.Shape.Draw(Matrix.CreateScale(Player.BoundingSphere.Radius) *
                Matrix.CreateTranslation(Player.BoundingSphere.Center), playerCamera.View, playerCamera.Projection);

            enemy = Enemies.ActiveList.First;
            while (enemy != null)
            {
                RenderBoundingSphere2.Shape.Draw(Matrix.CreateScale(enemy.Value.BoundingSphere.Radius) *
                Matrix.CreateTranslation(enemy.Value.BoundingSphere.Center), playerCamera.View, playerCamera.Projection);
                enemy = enemy.Next;
            }
            enemy = Enemies.LockOnList.First;
            while (enemy != null)
            {
                RenderBoundingSphere2.Shape.Draw(Matrix.CreateScale(enemy.Value.BoundingSphere.Radius) *
                Matrix.CreateTranslation(enemy.Value.BoundingSphere.Center), playerCamera.View, playerCamera.Projection);
                enemy = enemy.Next;
            }

            if (this.stageNo == PlayGameScreen.lastStageNo)
            {
                RenderBoundingSphere2.Shape.Draw(Matrix.CreateScale(Boss.Value.BoundingSphere.Radius) *
                    Matrix.CreateTranslation(Boss.Value.BoundingSphere.Center), playerCamera.View, playerCamera.Projection);
            }

            rasterizerState2.FillMode = FillMode.Solid;
            device.RasterizerState = rasterizerState2;

            XYZAxis.Draw(this.ScreenManager.GraphicsDevice, this.playerCamera.View, this.playerCamera.Projection);

            DebugStringDraw(gameTime);

#endif

            // ゲームがオンまたはオフに移行する場合、表示画面を黒にフェード アウトします。
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);
                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }

        /// <summary>
        /// スキンアニメーションを描画します。
        /// </summary>
        /// <param name="model"></param>
        public void DrawPlayer(GraphicsDevice device, Model model)
        {
            Matrix[] bones = Player.animationplayer.GetSkinTransforms();

            ModelMesh mesh = model.Meshes[0];
            SkinnedEffect effect = mesh.Effects[0] as SkinnedEffect;

            // スキニングされたメッシュをレンダリングします。
            effect.SetBoneTransforms(bones);
            effect.View = playerCamera.View;
            effect.Projection = playerCamera.Projection;

            effect.Texture = playerTexture[(int)Player.ElementType];

            effect.EnableDefaultLighting();
            effect.EmissiveColor = Player.EmissiveColor;
            effect.Alpha = Player.Alpha;
            effect.SpecularColor = new Vector3(0.25f);
            effect.SpecularPower = 16;

            mesh.Draw();
        }

        /// <summary>
        /// SpriteItem を描画します。
        /// </summary>
        /// <param name="spriteitem">描画する SpriteItem</param>
        /// <param name="gameTime">前回のフレームからの経過時間</param>
        private void SpriteItemDraw(SpriteItem spriteitem, GameTime gameTime)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied);
            spriteitem.Draw(spriteBatch, gameTime);
            spriteBatch.End();
        }

#if DEBUG
        private void DebugStringDraw(GameTime gameTime)
        {
            float pos1X = 25;
            float pos1Y = 50;
            float pos2X = 1000;
            spriteBatch.Begin();
            if (this.stageNo == lastStageNo)
            {
                spriteBatch.DrawString(this.debugFont, "BOSS HP:" + Boss.Value.HitPoint, new Vector2(pos1X, pos1Y), Color.White);
                spriteBatch.DrawString(this.debugFont, "BOSS Acel:" + Boss.Value.Acceleration, new Vector2(pos1X, pos1Y + 25), Color.White);
                spriteBatch.DrawString(this.debugFont, "BOSS Velocity:" + Boss.Value.Velocity, new Vector2(pos1X, pos1Y + 50), Color.White);
                spriteBatch.DrawString(this.debugFont, "BOSS Position:" + Boss.Value.Position, new Vector2(pos1X, pos1Y + 75), Color.White);
                spriteBatch.DrawString(this.debugFont, "BOSS AttackTime:" + Boss.Value.AttackTime, new Vector2(pos1X, pos1Y + 200), Color.White);
                spriteBatch.DrawString(this.debugFont, "BOSS StateTime:" + Boss.Value.StateTime, new Vector2(pos1X, pos1Y + 225), Color.White);
                spriteBatch.DrawString(this.debugFont, "BOSS DamageTime:" + Boss.Value.DamageTime, new Vector2(pos1X, pos1Y + 250), Color.White);
                spriteBatch.DrawString(this.debugFont, "boss.Value.Position - Player.Position:" + (Boss.Value.Position - Player.Position).Length(), new Vector2(pos1X, pos1Y + 275), Color.White);
            }
            spriteBatch.DrawString(this.debugFont, "Player Acel:" + Player.Acceleration, new Vector2(pos1X, pos1Y + 100), Color.White);
            spriteBatch.DrawString(this.debugFont, "Player Velocity:" + Player.Velocity, new Vector2(pos1X, pos1Y + 125), Color.White);
            spriteBatch.DrawString(this.debugFont, "Player Position:" + Player.Position, new Vector2(pos1X, pos1Y + 150), Color.White);
            spriteBatch.DrawString(this.debugFont, "Bullets.ActiveList:" + Bullets.ActiveList.Count, new Vector2(pos1X, pos1Y + 175), Color.White);
            spriteBatch.DrawString(this.debugFont, "Player Invincible:" + Player.Invincible, new Vector2(pos1X, pos1Y + 300), Color.White);
            spriteBatch.DrawString(this.debugFont, "Enemies.ActiveList:" + Enemies.ActiveList.Count, new Vector2(pos2X, pos1Y + 50), Color.White);
            spriteBatch.DrawString(this.debugFont, "Enemies.LockOnList:" + Enemies.LockOnList.Count, new Vector2(pos2X, pos1Y + 75), Color.White);
            spriteBatch.DrawString(this.debugFont, "Enemies.DestroyList:" + Enemies.DestroyList.Count, new Vector2(pos2X, pos1Y + 100), Color.White);
            spriteBatch.DrawString(this.debugFont, "Enemies.RemovedList:" + Enemies.RemovedList.Count, new Vector2(pos2X, pos1Y + 125), Color.White);
            spriteBatch.DrawString(this.debugFont, "Enemies.ReservedList:" + Enemies.ReservedList.Count, new Vector2(pos2X, pos1Y + 150), Color.White);
            spriteBatch.DrawString(this.debugFont, "Cylinder.Alpha:" + Cylinder.Alpha, new Vector2(pos1X, pos1Y + 375), Color.White);
            spriteBatch.DrawString(this.debugFont, "Cylinder.Position:" + Cylinder.Position, new Vector2(pos1X, pos1Y + 350), Color.White);
            spriteBatch.DrawString(this.debugFont, "State:" + this.State, new Vector2(pos1X, pos1Y + 325), Color.White);
            spriteBatch.DrawString(this.debugFont, "StateTime:" + this.StateTime, new Vector2(pos1X, pos1Y + 400), Color.White);
            spriteBatch.End();
        }
#endif

        #endregion

        #region パブリック メソッド

        /// <summary>
        /// 状態をStartに設定します。
        /// </summary>
        public void SetStartState()
        {
            State = StartState.Instance;
            StateTime = TimeSpan.Zero;
        }
        /// <summary>
        /// 状態をPlayingに設定します。
        /// </summary>
        public void SetPlayingState()
        {
            State = PlayingState.Instance;
            StateTime = TimeSpan.Zero;
        }
        /// <summary>
        /// 状態をBossPlaytingに設定します。
        /// </summary>
        public void SetBossPlayingState()
        {
            State = BossPlaytingState.Instance;
            StateTime = TimeSpan.Zero;
        }
        /// <summary>
        /// 状態をGameOverに設定します。
        /// </summary>
        public void SetGameOverState()
        {
            State = GameOverState.Instance;
            StateTime = TimeSpan.Zero;
        }
        /// <summary>
        /// 状態をStageClearに設定します。
        /// </summary>
        public void SetStageClearState()
        {
            State = StageClearState.Instance;
            StateTime = TimeSpan.Zero;
        }

        #endregion
    }
}