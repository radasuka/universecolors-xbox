#region File Description
//-----------------------------------------------------------------------------
// Particle.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace UniverseColors.PlayGameScreens.Particles
{
    /// <summary>
    /// パーティクルとはエフェクトを構成する小さな粒子です。
    /// </summary>
    public class Particle
    {
        // Position (位置)、Velocity (速度)、および Acceleration (加速度) は、
        // それぞれの名前が示すとおりの内容を表します。
        public Vector2 Position;
        public Vector2 Velocity;
        public Vector2 Acceleration;

        // このパーティクルの存続期間
        private float lifetime;
        public float Lifetime
        {
            get { return lifetime; }
            set { lifetime = value; }
        }

        // 初期化処理が呼び出されてからの経過時間
        private float timeSinceStart;
        public float TimeSinceStart
        {
            get { return timeSinceStart; }
            set { timeSinceStart = value; }
        }

        // このパーティクルのスケール
        private float scale;
        public float Scale
        {
            get { return scale; }
            set { scale = value; }
        }

        // 回転 (ラジアン単位)
        private float rotation;
        public float Rotation
        {
            get { return rotation; }
            set { rotation = value; }
        }

        // 回転速度
        private float rotationSpeed;
        public float RotationSpeed
        {
            get { return rotationSpeed; }
            set { rotationSpeed = value; }
        }

        // パーティクルがまだ存在しているかどうか。TimeSinceStart の値が
        // Lifetime を超えると、パーティクルの描画 (更新) を行いません。
        public bool Active
        {
            get { return TimeSinceStart < Lifetime; }
        }


        // ParticleSystem によって初期化処理が呼び出されて、パーティクルを設定し、
        // パーティクルを使用できるように準備します。
        public void Initialize(Vector2 position, Vector2 velocity, Vector2 acceleration,
            float lifetime, float scale, float rotationSpeed)
        {
            // 要求された値に設定します
            this.Position = position;
            this.Velocity = velocity;
            this.Acceleration = acceleration;
            this.Lifetime = lifetime;
            this.Scale = scale;
            this.RotationSpeed = rotationSpeed;

            // TimeSinceStart をリセットします。パーティクルを再利用するために、
            // これを行う必要があります。
            this.TimeSinceStart = 0.0f;

            // 0 〜 360 度の間のランダムな値に回転を設定します。
            this.Rotation = ParticleHelpers.RandomBetween(0, MathHelper.TwoPi);
        }

        // フレームごとに ParticleSystem によって更新処理が呼び出されます。
        // ここでは、パーティクルの位置などが更新されます。
        public void Update(float dt)
        {
            Velocity += Acceleration * dt;
            Position += Velocity * dt;

            Rotation += RotationSpeed * dt;

            TimeSinceStart += dt;
        }
    }
}
