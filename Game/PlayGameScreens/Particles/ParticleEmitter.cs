#region File Description
//-----------------------------------------------------------------------------
// ParticleEmitter.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
#endregion

namespace UniverseColors.PlayGameScreens.Particles
{
    /// <summary>
    /// パーティクルを後方に従えて移動するオブジェクトの
    /// ヘルパーです。このエミッターの実装によって、関係する 2 つの問題が
    /// 解決されます。
    /// 
    /// オブジェクトがきわめて低速なパーティクルの作成を望む場合、つまり作成が
    /// フレームごとに 1 回未満の場合、新しいパーティクルを作成する更新と
    /// 作成しない更新を追跡することは困難です。
    /// 
    /// オブジェクトが高速で動き、フレームごとに多数のパーティクルを
    /// 作成している場合、これらのパーティクルがすべて束ねられると
    /// 見栄えが悪くなります。
    /// 前後のフレームのオブジェクトの位置の間に、パーティクルが広がっていれば、
    /// 見栄えが良くなります。これは、ロケットのように高速で移動する
    /// オブジェクトの後方にトレールが残る場合などに特に重要です。
    /// 
    /// このエミッター クラスは、移動するオブジェクトを追跡し、
    /// 前フレームの位置を記憶するので、オブジェクトの速度を計算できます。
    /// ゲームの更新速度の高低によらず、指定した任意の周期で
    /// パーティクルを作成する際に、正確な位置を
    /// 割り出します。
    /// </summary>
    public class ParticleEmitter
    {
        #region Fields

        ParticleSystem particleSystem;
        float timeBetweenParticles;
        Vector2 position;
        float timeLeftOver;

        #endregion

        /// <summary>
        /// エミッターの位置を取得します。位置を変更するには、新しい値を
        /// Update に渡します。
        /// </summary>
        public Vector2 Position
        {
            get { return position; }
        }

        /// <summary>
        /// 新しいパーティクル エミッター オブジェクトを構築します。
        /// </summary>
        public ParticleEmitter(ParticleSystem particleSystem, float particlesPerSecond, Vector2 initialPosition)
        {
            this.particleSystem = particleSystem;

            timeBetweenParticles = 1.0f / particlesPerSecond;

            position = initialPosition;
        }


        /// <summary>
        /// エミッターを更新して、適切な数のパーティクルを適切な
        /// 位置に作成します。
        /// </summary>
        public void Update(GameTime gameTime, Vector2 newPosition)
        {
            if (gameTime == null)
                throw new ArgumentNullException("gameTime");

            // 前回の更新から経過した時間を割り出します。
            float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (elapsedTime > 0)
            {
                // 移動の速度を割り出します。
                Vector2 velocity = (newPosition - position) / elapsedTime;

                // 前回の更新中に使用しなかった期間があれば、
                // その時間を現在の経過時間に加えます。
                float timeToSpend = timeLeftOver + elapsedTime;

                // 時間間隔のカウンター。
                float currentTime = -timeLeftOver;

                // 時間間隔が十分な場合のみ、パーティクルを作成します。
                while (timeToSpend > timeBetweenParticles)
                {
                    currentTime += timeBetweenParticles;
                    timeToSpend -= timeBetweenParticles;

                    // このパーティクルに最適な位置を割り出します。
                    // オブジェクトの速度、
                    // パーティクルの作成周期、ゲームの更新速度とは無関係に、
                    // パーティクルが空間に均等に生成されます。
                    float mu = currentTime / elapsedTime;

                    Vector2 particlePosition = Vector2.Lerp(position, newPosition, mu);

                    Vector2 where = Vector2.Zero;
                    where.X = ParticleHelpers.RandomBetween(0, 1280);
                    where.Y = ParticleHelpers.RandomBetween(0, 720);

                    // パーティクルを作成します。
                    particleSystem.AddParticles(where, velocity);
                }

                // 使用されなかった時間を格納しておき、次の更新で
                // 使用できるようにします。
                timeLeftOver = timeToSpend;
            }

            position = newPosition;
        }

        /// <summary>
        /// エミッターを更新して、適切な数のパーティクルを適切な
        /// 位置に作成します。
        /// </summary>
        public void AuraUpdate(GameTime gameTime, Vector2 newPosition)
        {
            if (gameTime == null)
                throw new ArgumentNullException("gameTime");

            // 前回の更新から経過した時間を割り出します。
            float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (elapsedTime > 0)
            {
                // 移動の速度を割り出します。
                Vector2 velocity = (newPosition - position) / elapsedTime;

                // 前回の更新中に使用しなかった期間があれば、
                // その時間を現在の経過時間に加えます。
                float timeToSpend = timeLeftOver + elapsedTime;

                // 時間間隔のカウンター。
                float currentTime = -timeLeftOver;

                // 時間間隔が十分な場合のみ、パーティクルを作成します。
                while (timeToSpend > timeBetweenParticles)
                {
                    currentTime += timeBetweenParticles;
                    timeToSpend -= timeBetweenParticles;

                    // このパーティクルに最適な位置を割り出します。
                    // オブジェクトの速度、
                    // パーティクルの作成周期、ゲームの更新速度とは無関係に、
                    // パーティクルが空間に均等に生成されます。
                    float mu = currentTime / elapsedTime;

                    Vector2 particlePosition = Vector2.Lerp(position, newPosition, mu);

                    Vector2 leftFillet = Vector2.Zero;
                    Vector2 rightFillet = Vector2.Zero;
                    Vector2 dorsalFin = Vector2.Zero;

                    leftFillet.X = newPosition.X - 60;
                    leftFillet.Y = newPosition.Y;

                    rightFillet.X = newPosition.X + 60;
                    rightFillet.Y = newPosition.Y;

                    dorsalFin.X = newPosition.X;
                    dorsalFin.Y = newPosition.Y - 20;

                    // パーティクルを作成します。
                    particleSystem.AddParticles(leftFillet, velocity);
                    particleSystem.AddParticles(rightFillet, velocity);
                    particleSystem.AddParticles(dorsalFin, velocity);
                }

                // 使用されなかった時間を格納しておき、次の更新で
                // 使用できるようにします。
                timeLeftOver = timeToSpend;
            }

            position = newPosition;
        }
    }
}
