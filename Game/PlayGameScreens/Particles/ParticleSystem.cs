#region File Description
//-----------------------------------------------------------------------------
// SmokePlumeParticleSystem.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ParticlesSettings;
#endregion

namespace UniverseColors.PlayGameScreens.Particles
{
    public class ParticleSystem : DrawableGameComponent
    {
        // これら 2 つの値は、パーティクル システムが描画される順序を制御します。
        // 通常、加算ブレンディングを使用するパーティクルは、
        // 通常のアルファ ブレンディングを使用するパーティクルの上に描画されます。
        // そのため、ParticleSystems はDrawOrder を InitializeConstants の適切な
        // 値に設定します。ただし、より高度な効果を得るための他の値を
        // 使用することもできます。
        public const int AlphaBlendDrawOrder = 100;
        public const int AdditiveDrawOrder = 200;

        private SpriteBatch spriteBatch;

        // このパーティクル システムが使用するテクスチャー。
        private Texture2D texture;

        // テクスチャー描画時の原点。これはテクスチャーの中央に
        // なります。
        private Vector2 origin;

        // このシステムが使用するパーティクルの配列。これらは、
        // 使用可能な数を超えるパーティクルを作成しようとした場合に、
        // AddParticles の呼び出しによって割り当てのみが
        // 行われるように再利用されます。
        private List<Particle> particles;

        // フリーのパーティクルのキューによって、現在エフェクトで
        // 使用されていないパーティクルが追跡されます。
        // 新しいエフェクトが要求されると、パーティクルが
        // キューから取得されます。終了したパーティクルは、このキューに
        // 入れられます。
        private Queue<Particle> freeParticles;

        // このパーティクル システムに使用する設定
        private ParticleSystemSettings settings;

        // ファイルからの設定の読み込みに使用するアセット名。
        private string settingsAssetName;

        // パーティクルのレンダリング時に使用する BlendState。
        private BlendState blendState;

        /// <summary>
        /// 新しいエフェクトに使用可能なパーティクルの数を返します。
        /// </summary>
        public int FreeParticleCount
        {
            get { return freeParticles.Count; }
        }

        /// <summary>
        /// 新しい ParticleSystem を構築します。
        /// </summary>
        /// <param name="game">このパーティクル システムのホスト。</param>
        /// <param name="settingsAssetName">読み込む設定ファイルの名前 
        /// システムでパーティクルを作成および更新するときに使用されます。
        /// </param>
        public ParticleSystem(Game game, string settingsAssetName)
            : this(game, settingsAssetName, 10)
        { }


        /// <summary>
        /// 新しい ParticleSystem を構築します。
        /// </summary>
        /// <param name="game">このパーティクル システムのホスト。</param>
        /// <param name="settingsAssetName">読み込む設定ファイルの名前 
        /// システムでパーティクルを作成および更新するときに使用されます。
        /// </param>
        /// <param name="initialParticleCount">このシステムが使用することを
        /// 想定しているパーティクル数の初期値。システムは必要に応じて
        /// 拡大しますが、この値をできる限り正確に設定すれば、
        /// 割り当て回数を減らすことができます。</param>
        public ParticleSystem(Game game, string settingsAssetName, int initialParticleCount)
            : base(game)
        {
            this.settingsAssetName = settingsAssetName;

            // 初期カウントを使用してパーティクルのリストとキューを作成し、
            // その数だけパーティクルを作成します。
            // 妥当な値を選択すれば、システムはこれ以降さらに
            // オブジェクトを割り当てません。ただし、AddParticles メソッドは
            // 必要に応じてより多くのパーティクルを割り当てます。
            particles = new List<Particle>(initialParticleCount);
            freeParticles = new Queue<Particle>(initialParticleCount);
            for (int i = 0; i < initialParticleCount; i++)
            {
                particles.Add(new Particle());
                freeParticles.Enqueue(particles[i]);
            }
        }

        /// <summary>
        /// 基底クラス LoadContent をオーバーライドしてテクスチャーを
        /// 読み込みます。
        /// 読み込んだら、原点を計算します。
        /// </summary>
        protected override void LoadContent()
        {
            // 設定を読み込みます
            settings = Game.Content.Load<ParticleSystemSettings>(settingsAssetName);

            // テクスチャーを読み込みます
            texture = Game.Content.Load<Texture2D>(settings.TextureFilename);

            // ... 中心を計算します。これは描画呼び出しで使用されます。
            // 常にこの点の周りで回転およびスケーリングします。
            origin.X = texture.Width / 2;
            origin.Y = texture.Height / 2;

            // パーティクルを描画する SpriteBatch を作成します
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // 設定されている値を使用して、ブレンド ステートを作成します
            blendState = new BlendState
            {
                AlphaSourceBlend = settings.SourceBlend,
                ColorSourceBlend = settings.SourceBlend,
                AlphaDestinationBlend = settings.DestinationBlend,
                ColorDestinationBlend = settings.DestinationBlend
            };

            base.LoadContent();
        }

        /// <summary>
        /// PickRandomDirection は、パーティクルが移動する方向を決定するために、
        /// AddParticle によって使用されます。 
        /// </summary>
        private Vector2 PickRandomDirection()
        {
            float angle = ParticleHelpers.RandomBetween(settings.MinDirectionAngle, settings.MaxDirectionAngle);

            // 設定の角度は度単位なので、ラジアン単位に変換する必要があります。
            angle = MathHelper.ToRadians(angle);

            return new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
        }

        /// <summary>
        /// AddParticles は、画面上の任意の場所にエフェクトを追加します。
        /// freeParticles キューに 十分なパーティクルが存在しない場合、
        /// あるだけのパーティクルを 使用します。これは、十分な数の
        /// パーティクルが使用できない場合、AddParticles を呼び出しても
        /// 効果がないことを意味します。
        /// </summary>
        /// <param name="where">パーティクル エフェクトを作成する場所</param>
        /// <param name="velocity">すべてのパーティクルに適用する基本速度。
        /// これは、パーティクル システムの設定で指定されている 
        /// EmitterVelocitySensitivity によって重み付けされます。</param>
        public void AddParticles(Vector2 where, Vector2 velocity)
        {
            // このエフェクトに使用するパーティクルの数は、設定で指定された 
            // 2 つの定数の間のランダムな値になります。
            int numParticles =
                ParticleHelpers.Random.Next(settings.MinNumParticles, settings.MaxNumParticles);

            // 可能であれば、その数だけパーティクルを作成します。
            for (int i = 0; i < numParticles; i++)
            {
                // フリーのパーティクルがなくなると、さらに 10 個の
                // パーティクルを割り当てて、処理を続行します。
                if (freeParticles.Count == 0)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        Particle newParticle = new Particle();
                        particles.Add(newParticle);
                        freeParticles.Enqueue(newParticle);
                    }
                }

                // freeParticles キューからパーティクルを取得し、初期化します。
                Particle p = freeParticles.Dequeue();
                InitializeParticle(p, where, velocity);
            }
        }

        /// <summary>
        /// InitializeParticle は、パーティクルのプロパティをランダムに
        /// 設定し、続いて初期化の処理を呼び出します。
        /// これはサブクラスによってオーバーライド可能で、 
        /// パーティクルの作成方法を変更できます。たとえば、 
        /// SmokePlumeParticleSystem は、この関数をオーバーライドして、
        /// すべてのパーティクルを右方向へ加速し、風をシミュレートします。
        /// </summary>
        /// <param name="p">初期化するパーティクル</param>
        /// <param name="where">パーティクルを配置する画面上の位置
        /// </param>
        /// <param name="velocity">パーティクルに設定する基本速度</param>
        private void InitializeParticle(Particle p, Vector2 where, Vector2 velocity)
        {
            // このパーティクル システムに与える影響の
            // 大きさに基づいて入力速度を調整します。
            velocity *= settings.EmitterVelocitySensitivity;

            // ランダム値に基づいて速度を調整します
            Vector2 direction = PickRandomDirection();
            float speed = ParticleHelpers.RandomBetween(settings.MinInitialSpeed, settings.MaxInitialSpeed);
            velocity += direction * speed;

            // パーティクル用にランダム値を選択します
            float lifetime =
                ParticleHelpers.RandomBetween(settings.MinLifetime, settings.MaxLifetime);
            float scale =
                ParticleHelpers.RandomBetween(settings.MinSize, settings.MaxSize);
            float rotationSpeed = 
                ParticleHelpers.RandomBetween(settings.MinRotationSpeed, settings.MaxRotationSpeed);

            // 設定の角度は度単位なので、ラジアン単位に変換する必要があります。
            rotationSpeed = MathHelper.ToRadians(rotationSpeed);

            // AccelerationMode に基づいて加速度を計算します
            Vector2 acceleration = Vector2.Zero;
            switch (settings.AccelerationMode)
            {
                case AccelerationMode.Scalar:
                    // 方向と MinAcceleration/MaxAcceleration 値を使用して 
                    // 加速度をランダムに取得します
                    float accelerationScale = ParticleHelpers.RandomBetween(
                        settings.MinAccelerationScale, settings.MaxAccelerationScale);
                    acceleration = direction * accelerationScale;
                    break;
                case AccelerationMode.EndVelocity:
                    // 設定されている終了速度に基づいて加速度を計算します。
                    // 使用する計算式は vt = v0 + (a0 * t) です。(これは、
                    // 加速度が一定の場合に適用される基本的な運動方程式の 1 つ
                    // であり、基本的に以下のことを示しています。
                    // 時間 t での速度 = 速度の初期値 + 加速度 * t)
                    // 存続時間に t を代入して a0 について解きます。v0 は
                    // 速度、vt は速度 * settings.EndVelocity です。
                    acceleration = (velocity * (settings.EndVelocity - 1)) / lifetime;
                    break;
                case AccelerationMode.Vector:
                    acceleration = new Vector2(
                        ParticleHelpers.RandomBetween(settings.MinAccelerationVector.X, settings.MaxAccelerationVector.X),
                        ParticleHelpers.RandomBetween(settings.MinAccelerationVector.Y, settings.MaxAccelerationVector.Y));
                    break;
                default:
                    break;
            }

            // 次に、これらのランダム値で初期化します。初期化ではこれらの値が
            // 保持され、アクティブとしてマークされます。
            p.Initialize(
                where, 
                velocity, 
                acceleration, 
                lifetime, 
                scale, 
                rotationSpeed);
        }

        /// <summary>
        /// DrawableGameComponent からオーバーライドされます。Update によって
        /// すべてのアクティブなパーティクルが更新されます。
        /// </summary>
        public override void Update(GameTime gameTime)
        {
            // dt を計算します。これは、前回のフレームからの変化です。
            // パーティクルの更新で、この値が使用されます。
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // すべてのパーティクルを処理します。
            foreach (Particle p in particles)
            {

                if (p.Active)
                {
                    // ... アクティブな場合、更新します。
                    p.Acceleration += settings.Gravity * dt;
                    p.Update(dt);
                    // 更新で終了したら、フリーのパーティクルのキューに
                    // 入れます。
                    if (!p.Active)
                    {
                        freeParticles.Enqueue(p);
                    }
                }   
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// DrawableGameComponent からオーバーライドされます。Draw は 
        /// ParticleSampleGame のスプライト バッチを使用して、アクティブな
        /// パーティクルをすべてレンダリングします。
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            // initializeConstants で指定されている spriteBlendMode を使用して、
            // スプライト バッチを開始します。
			spriteBatch.Begin(SpriteSortMode.Deferred, blendState);

            foreach (Particle p in particles)
            {
                // アクティブではないパーティクルをスキップします。
                if (!p.Active)
                    continue;

                // 正規化された存続期間は 0 〜 1 の値であり、パーティクルが
                // どの程度存続しているかを表します。
                // 0 は開始直後、.5 は期間の半分、1.0 は期間が終了するときを
                // 意味します。
                // この値はアルファとスケールの計算に使用されて、 
                // パーティクルが突然現れたり消えたりすることが回避されます。
                float normalizedLifetime = p.TimeSinceStart / p.Lifetime;

                // パーティクルをフェードインおよびフェードアウトしたいので、
                // アルファが
                // (normalizedLifetime) * (1-normalizedLifetime) となるように
                // 計算します。このようにすると、
                // normalizedLifetime が 0 または 1 の場合、アルファは 0 になり
                // ます。最大値となるのは
                // normalizedLifetime = .5 の時で、次の値になります。
                // (normalizedLifetime) * (1-normalizedLifetime)
                // (.5)                 * (1-.5)
                // .25
                // ここではアルファの最大値を .25 ではなく 1 としたいので、 
                // 式全体に 4 を乗算します。
                float alpha = 4 * normalizedLifetime * (1 - normalizedLifetime);
                Color color = Color.White * alpha;

                // 存続期間が経過するほどパーティクルを大きくします。75% の
                // サイズで開始し、終了時に 100% になるように増加させます。
                float scale = p.Scale * (.75f + .25f * normalizedLifetime);

                spriteBatch.Draw(texture, p.Position, null, color,
                    p.Rotation, origin, scale, SpriteEffects.None, 0.0f);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
