#region File Description
//-----------------------------------------------------------------------------
// ChaseCamera.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using ステートメント
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
#endregion

namespace UniverseColors.PlayGameScreens
{
    public class ChaseCamera
    {
        #region 追尾されるオブジェクトのプロパティー (フレーム毎に更新されます)

        public float desiredPositionY;
        public float lookAtY;

        /// <summary>
        /// 追尾されるオブジェクトの位置(Position of object being chased.)
        /// </summary>
        public Vector3 ChasePosition
        {
            get { return chasePosition; }
            set { chasePosition = value; }
        }
        private Vector3 chasePosition;

        /// <summary>
        /// 追尾されるオブジェクトが向いている方向(Direction the chased object is facing.)
        /// </summary>
        public Vector3 ChaseDirection
        {
            get { return chaseDirection; }
            set { chaseDirection = value; }
        }
        private Vector3 chaseDirection;

        /// <summary>
        /// 追尾されるオブジェクトの上方ベクトル(Chased object's Up vector.)
        /// </summary>
        public Vector3 Up
        {
            get { return up; }
            set { up = value; }
        }
        private Vector3 up = Vector3.Up;

        #endregion

        #region カメラの目的位置 (カメラの作成時および視点を変更する際に更新されます)

        /// <summary>
        /// 追尾されるオブジェクトの座標系におけるカメラの目的位置。
        /// (Desired camera position in the chased object's coordinate system.)
        /// </summary>
        public Vector3 DesiredPositionOffset
        {
            get { return desiredPositionOffset; }
            set { desiredPositionOffset = value; }
        }
        private Vector3 desiredPositionOffset = new Vector3(0, 2.0f, 0.0f);

        /// <summary>
        /// ワールド空間におけるカメラの目的位置。
        /// (Desired camera position in world space.)
        /// </summary>
        public Vector3 DesiredPosition
        {
            get
            {
                // このフレームで Update が呼び出されなかった場合でも、正しい値を取得します。
                // (Ensure correct value even if update has not been called this frame)
                UpdateWorldPositions();

                return desiredPosition;
            }
        }
        private Vector3 desiredPosition;

        /// <summary>
        /// 追尾されるオブジェクトの座標系でのルックアット (見ている) 位置。
        /// (Look at point in the chased object's coordinate system.)
        /// </summary>
        public Vector3 LookAtOffset
        {
            get { return lookAtOffset; }
            set { lookAtOffset = value; }
        }
        private Vector3 lookAtOffset = new Vector3(0, 2.8f, 20.0f);

        /// <summary>
        /// ワールド空間でのルックアット位置。
        /// (Look at point in world space.)
        /// </summary>
        public Vector3 LookAt
        {
            get
            {
                // このフレームで Update が呼び出されなかった場合でも、正しい値を取得します。(Ensure correct value even if update has not been called this frame)
                UpdateWorldPositions();

                return lookAt;
            }
        }
        private Vector3 lookAt;

        #endregion

        #region カメラの物理 (カメラの作成時に設定されます)

        /// <summary>
        /// スプリングの力に対するカメラの位置の影響を制御する物理係数。スプリングの
        /// 剛性が高いほど、追尾されるオブジェクトの近くにとどまります。
        /// </summary>
        public float Stiffness
        {
            get { return stiffness; }
            set { stiffness = value; }
        }
        private float stiffness = 10000.0f;

        /// <summary>
        /// スプリングの内部摩擦を概算する物理係数。適度な減衰は、スプリングの無限の
        /// 振動を防ぎます。
        /// </summary>
        public float Damping
        {
            get { return damping; }
            set { damping = value; }
        }
        private float damping = 1000.0f;

        /// <summary>
        /// カメラ本体の質量。重くなると、スプリングの剛性を高くし、減衰を低く
        /// する必要があります。
        /// </summary>
        public float Mass
        {
            get { return mass; }
            set { mass = value; }
        }
        private float mass = 50.0f;

        #endregion

        #region 現在のカメラのプロパティー (カメラの物理コントロールによって更新されます)

        /// <summary>
        /// ワールド空間におけるカメラの位置。
        /// </summary>
        public Vector3 Position
        {
            get { return position; }
            set { position = value; }
        }
        private Vector3 position;

        /// <summary>
        /// カメラの速度。
        /// </summary>
        public Vector3 Velocity
        {
            get { return velocity; }
        }
        private Vector3 velocity;

        #endregion


        #region 視点に関するプロパティー

        /// <summary>
        /// パースペクティブ縦横比。デフォルト値は、アプリケーションによってオーバーライドされます。
        /// </summary>
        public float AspectRatio
        {
            get { return aspectRatio; }
            set { aspectRatio = value; }
        }
        private float aspectRatio = 4.0f / 3.0f;

        /// <summary>
        /// パースペクティブ視野。
        /// </summary>
        public float FieldOfView
        {
            get { return fieldOfView; }
            set { fieldOfView = value; }
        }
        private float fieldOfView = MathHelper.ToRadians(45.0f);

        /// <summary>
        /// 前方クリップ面までの距離。
        /// </summary>
        public float NearPlaneDistance
        {
            get { return nearPlaneDistance; }
            set { nearPlaneDistance = value; }
        }
        private float nearPlaneDistance = 1.0f;

        /// <summary>
        /// 後方クリップ面までの距離。
        /// </summary>
        public float FarPlaneDistance
        {
            get { return farPlaneDistance; }
            set { farPlaneDistance = value; }
        }
        private float farPlaneDistance = 100000.0f;

        #endregion

        #region 行列に関するプロパティー

        /// <summary>
        /// ビュー トランスフォーム行列。
        /// (View transform matrix.)
        /// </summary>
        public Matrix View
        {
            get { return view; }
        }
        private Matrix view;

        /// <summary>
        /// 射影トランスフォーム行列。
        /// (Projecton transform matrix.)
        /// </summary>
        public Matrix Projection
        {
            get { return projection; }
        }
        private Matrix projection;

        #endregion


        #region メソッド

        /// <summary>
        /// 最新のオブジェクト空間値をワールド空間に反映させます。ワールド空間値を
        /// 公開して返すか、非公開でアクセスする前に呼び出します。
        /// </summary>
        private void UpdateWorldPositions()
        {
            // オブジェクト空間からワールド空間に変換する行列を構築します。
            Matrix transform = Matrix.Identity;
            transform.Forward = ChaseDirection;
            transform.Up = Up;
            transform.Right = Vector3.Cross(Up, ChaseDirection);

            // ワールド空間における目的のカメラ プロパティを計算します。
            desiredPosition.Z = ChasePosition.Z +
                Vector3.TransformNormal(DesiredPositionOffset, transform).Z;
            lookAt.Z = ChasePosition.Z +
                Vector3.TransformNormal(LookAtOffset, transform).Z;

            desiredPosition.Y = desiredPositionY;
            lookAt.Y = lookAtY;
        }

        /// <summary>
        /// カメラのビュー行列と射影行列を更新します。
        /// </summary>
        private void UpdateMatrices()
        {
            view = Matrix.CreateLookAt(this.Position, this.LookAt, this.Up);
            projection = Matrix.CreatePerspectiveFieldOfView(FieldOfView,
                AspectRatio, NearPlaneDistance, FarPlaneDistance);
        }

        /// <summary>
        /// カメラを目的位置に置き、移動を停止させます。追尾されるオブジェクトが、
        /// 最初に作成されたとき、または瞬間移動した後に役立ちます。追尾される
        /// オブジェクトの位置に大きな変更があった後に、このメソッドを呼び出さないと、
        /// カメラが急にワールドを横切る結果になります。
        /// </summary>
        public void Reset()
        {
            UpdateWorldPositions();

            // 動きを停止します。
            velocity = Vector3.Zero;

            // カメラの位置を目的位置に強制的に設定します。
            position = desiredPosition;

            UpdateMatrices();
        }

        /// <summary>
        /// 現在の位置から、追尾されるオブジェクトの背後にある目的位置オフセットに向けて、
        /// カメラを移動させます。カメラの動きは、カメラと目的位置を結ぶスプリングに
        /// よって制御されます。
        /// </summary>
        public void Update(GameTime gameTime)
        {
            if (gameTime == null)
                throw new ArgumentNullException("gameTime");

            UpdateWorldPositions();

            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // スプリングの力を計算します。
            Vector3 stretch = position - desiredPosition;
            Vector3 force = -stiffness * stretch - damping * velocity;

            // 加速を適用します。
            Vector3 acceleration = force / mass;
            velocity += acceleration * elapsed;

            // 速度を適用します。
            position += velocity * elapsed;

            UpdateMatrices();
        }

        #endregion
    }
}
