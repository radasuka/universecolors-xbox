﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UniverseColors.PlayGameScreens.Bullets;

namespace UniverseColors.PlayGameScreens
{
    /// <summary>
    /// 複数の弾を管理するクラス
    /// </summary>
    public class BulletManager
    {

        #region フィールド

        /// <summary>
        /// 予約リスト
        /// </summary>
        public LinkedList<Bullet> ReservedList { get; set; }
        /// <summary>
        /// アクティブ リスト
        /// </summary>
        public LinkedList<Bullet> ActiveList { get; set; }
        /// <summary>
        /// 破壊中 リスト
        /// </summary>
        public LinkedList<Bullet> DestroyList { get; set; }

        #endregion

        /// <summary>
        /// コンストラクター
        /// </summary>
        /// <param name="capacity">同時に表示する弾の最大数</param>
        public BulletManager(int capacity)
        {
            ReservedList = new LinkedList<Bullet>();
            ActiveList = new LinkedList<Bullet>();
            DestroyList = new LinkedList<Bullet>();

            for (int index = 0; index < capacity; ++index)
                ReservedList.AddLast(new Bullet());
        }

        /// <summary>
        /// フレームの更新処理を実行します。
        /// </summary>
        public void Update(GameTime gameTime)
        {
            // アクティブな自機弾を更新します。
            LinkedListNode<Bullet> node = this.ActiveList.First, removeNode = null;
            while (node != null)
            {
                // bulletを使った処理
                node.Value.Update(gameTime);

                // 自機弾の生存時間を超えたら削除します。
                if (node.Value.LifeTime >= PlayerBullet.Duration)
                {
                    removeNode = node;
                    node = node.Next;
                    this.ActiveListRemove(removeNode);
                    continue;
                }

                node = node.Next;
            }
            node = this.DestroyList.First;
            removeNode = null;
            while (node != null)
            {
                // bulletを使った処理
                node.Value.Update(gameTime);
                node = node.Next;
            }
        }

      /// <summary>
      /// 自機の弾を発射します。
      /// </summary>
      /// <param name="position">弾の発射位置(プレイヤーの現在位置)</param>
      /// <param name="velocity">プレイヤーの速度</param>
      /// <param name="model">弾のモデル</param>
      /// <param name="elementType">プレイヤーの属性</param>
      /// <param name="lockOns"></param>
      /// <param name="screen"></param>
        public void Shoot(Vector3 position, Vector3 velocity, Model model, ElementType elementType, LinkedList<LockOn> lockOns, PlayGameScreen screen)
        {
            if (lockOns != null)
            {
                var lockOn = lockOns.First;
                while (lockOn != null)
                {
                    // 予約リストから取り出す。
                    LinkedListNode<Bullet> bullet = ReservedList.First;
                    if (bullet != null)
                    {
                        ReservedList.Remove(bullet);
                        // 弾の設定
                        bullet.Value.Target = lockOn.Value.Target;
                        bullet.Value.Acceleration = Vector3.Zero;
                        bullet.Value.Velocity = velocity + Vector3.Normalize(bullet.Value.Target.Position - position) * Bullet.Speed;
                        bullet.Value.Position = position;
                        bullet.Value.ElementType = elementType;
                        bullet.Value.Shape = model;
                        bullet.Value.LifeTime = TimeSpan.Zero;
                        bullet.Value.SetNomalState(screen);
                        switch (bullet.Value.ElementType)
                        {
                            case ElementType.Red:
                                bullet.Value.Element = RedBulletState.GetInstance(screen);
                                break;
                            case ElementType.Blue:
                                bullet.Value.Element = BlueBulletState.GetInstance(screen);
                                break;
                            case ElementType.Green:
                                bullet.Value.Element = GreenBulletState.GetInstance(screen);
                                break;
                        }
                        // アクティブ リストに追加する
                        ActiveList.AddFirst(bullet);
                    }
                    lockOn = lockOn.Next;
                }
            }
            else
            {
                // 予約リストから取り出す。
                var bullet = ReservedList.First;
                if (bullet != null)
                {
                    ReservedList.Remove(bullet);
                    // 弾の設定
                    bullet.Value.Acceleration = Vector3.Zero;
                    bullet.Value.Velocity = Vector3.Forward * Bullet.Speed;
                    bullet.Value.Position = position;
                    bullet.Value.Target = null;
                    bullet.Value.ElementType = elementType;
                    bullet.Value.Shape = model;
                    bullet.Value.LifeTime = TimeSpan.Zero;
                    bullet.Value.SetNomalState(screen);
                    switch (bullet.Value.ElementType)
                    {
                        case ElementType.Red:
                            bullet.Value.Element = RedBulletState.GetInstance(screen);
                            break;
                        case ElementType.Blue:
                            bullet.Value.Element = BlueBulletState.GetInstance(screen);
                            break;
                        case ElementType.Green:
                            bullet.Value.Element = GreenBulletState.GetInstance(screen);
                            break;
                    }
                    // アクティブ リストに追加する
                    ActiveList.AddFirst(bullet);
                }
            }
        }

        /// <summary>
        /// アクティブ リストから破壊中リストに移動します。
        /// </summary>
        /// <param name="item">アクティブ リストから削除する弾</param>
        public void ActiveListRemove(LinkedListNode<Bullet> item)
        {
            ActiveList.Remove(item);
            DestroyList.AddFirst(item);
        }
        /// <summary>
        /// 破壊中リストから予約リストに移動します。
        /// </summary>
        /// <param name="item"></param>
        public void DestroyListRemove(LinkedListNode<Bullet> item)
        {
            DestroyList.Remove(item);
            ReservedList.AddFirst(item);
        }
    }
}
