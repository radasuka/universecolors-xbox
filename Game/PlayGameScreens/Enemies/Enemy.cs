﻿#region File Description
//-----------------------------------------------------------------------------
// Enemy.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

using System;
using GameLibrary.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UniverseColors.PlayGameScreens.Enemies
{
    /// <summary>
    /// 敵基底クラス
    /// </summary>
    public class Enemy : BasicItem
    {
        #region 定数フィールド

        /// <summary>
        /// 敵弾の加速度。
        /// </summary>
        public static readonly Vector3 BulletAceleration = new Vector3(0, 0, 25000.0f);
        /// <summary>
        /// 敵の最高速度
        /// </summary>
        protected const float maximumSpeed = 20000.0f;

        #endregion

        #region フィールドとプロパティ

        Matrix[] transforms;

        /// <summary>
        /// モデルデータ
        /// </summary>
        public override Model Shape
        {
            get { return base.Shape; }
            set
            {
                base.Shape = value;
                this.transforms = new Matrix[value.Bones.Count];
                this.InitializeBoundingSphere();
            }
        }

        /// <summary>
        /// 敵機の状態
        /// </summary>
        public ItemState<Enemy> State { get; private set; }

        /// <summary>
        /// 敵機の属性
        /// </summary>
        public ItemState<Enemy> Element { get; set; }

        public ElementType ElementType { get; private set; }

        /// <summary>
        /// ヒットポイント
        /// </summary>
        public int HitPoint { get; set; }
        /// <summary>
        /// 破壊エフェクトの経過時間
        /// </summary>
        public TimeSpan ShatterTime = TimeSpan.Zero;
        /// <summary>
        /// ボスの攻撃制御用時間
        /// </summary>
        public TimeSpan AttackTime = TimeSpan.Zero;
        /// <summary>
        /// ボスの状態制御用時間
        /// </summary>
        public TimeSpan StateTime = TimeSpan.Zero;
        /// <summary>
        /// 被弾エフェクト時間
        /// </summary>
        public TimeSpan DamageTime = TimeSpan.Zero;
        /// <summary>
        /// 点滅時間
        /// </summary>
        public TimeSpan FlashingTime = TimeSpan.FromSeconds(3f);
        /// <summary>
        /// 放射光
        /// </summary>
        public Vector4 EmissiveColor = Vector4.Zero;
        /// <summary>
        /// アルファ値
        /// </summary>
        public float Alpha { get; set; }
        /// <summary>
        /// 物質の色
        /// </summary>
        public Vector4 DiffuseColor = Vector4.One;

        #endregion

        /// <summary>
        /// 敵機更新処理。
        /// </summary>
        /// <param name="gameTime">前回のフレームからの経過時間</param>
        public override void Update(GameTime gameTime)
        {
            StateTime += gameTime.ElapsedGameTime;

            base.Update(gameTime);

            // 敵機が最高速度を超えないように設定します。
            if (this.ElementType == PlayGameScreens.ElementType.Boss)
                if (Velocity.Length() >= maximumSpeed)
                    Velocity.Z = -maximumSpeed;

            // 属性に応じて更新処理を変更します。
            this.Element.Update(gameTime, this);
        }

        /// <summary>
        /// 敵機を描画します。
        /// </summary>
        /// <param name="cameraPosition"></param>
        /// <param name="view"></param>
        /// <param name="projection"></param>
        public virtual void Draw(GameTime gameTime, Vector3 cameraPosition, Matrix view, Matrix projection)
        {
            Shape.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in Shape.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    SetupEffect(transforms, mesh, part, cameraPosition, view, projection);
                }
                mesh.Draw();
            }
        }

        // シェーダーが必要とするエフェクト パラメーターを設定します。
        protected void SetupEffect(Matrix[] transforms, ModelMesh mesh,
                                ModelMeshPart part, Vector3 cameraPosition, Matrix view, Matrix projection)
        {
            Effect effect = part.Effect;
            effect.Parameters["TranslationAmount"].SetValue(25.0f * (float)ShatterTime.TotalSeconds);
            effect.Parameters["RotationAmount"].SetValue(MathHelper.Pi * 3 * (float)ShatterTime.TotalSeconds);
            effect.Parameters["time"].SetValue((float)ShatterTime.TotalSeconds);
            effect.Parameters["WorldViewProjection"].SetValue(transforms[mesh.ParentBone.Index] * World * view * projection);
            effect.Parameters["World"].SetValue(transforms[mesh.ParentBone.Index] * World);
            effect.Parameters["eyePosition"].SetValue(cameraPosition);
            effect.Parameters["lightPosition"].SetValue(Vector3.UnitY);
            effect.Parameters["ambientColor"].SetValue(Color.DarkGray.ToVector4());
            effect.Parameters["diffuseColor"].SetValue(DiffuseColor);
            effect.Parameters["specularColor"].SetValue(Color.White.ToVector4());
            effect.Parameters["specularPower"].SetValue(25);
            effect.Parameters["emissiveColor"].SetValue(EmissiveColor);
        }

        /// <summary>
        /// ステートを通常に設定します。
        /// </summary>
        public void SetDefaultState(PlayGameScreen screen)
        {
            State = NomalState.GetInstance(screen);
        }
        /// <summary>
        /// ステートを破壊中に設定します。
        /// </summary>
        public void SetDestroyState(PlayGameScreen screen)
        {
            State = DestroyState.GetInstance(screen);
        }
        /// <summary>
        /// ステートをBoss戦時に設定します。
        /// </summary>
        /// <param name="screen"></param>
        public void SetBossState(PlayGameScreen screen)
        {
            State = BossState.GetInstance(screen);
        }
        /// <summary>
        /// ステートを被弾時に設定
        /// </summary>
        /// <param name="screen"></param>
        public void SetDamageState(PlayGameScreen screen)
        {
            State = DamageState.GetInstance(screen);
        }

        /// <summary>
        /// ElementType を設定
        /// </summary>
        /// <param name="shape"></param>
        /// <param name="element"></param>
        public void SetElement(Model shape, ElementType element)
        {
            this.ElementType = element;
            this.Shape = shape;
        }
    }
}
