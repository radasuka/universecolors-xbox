﻿using Microsoft.Xna.Framework;

namespace UniverseColors.PlayGameScreens.Enemies
{
    /// <summary>
    /// 青属性の敵
    /// </summary>
    public class BlueEnemyState : ItemState<Enemy>
    {
        private static BlueEnemyState _instance;

        // プライベートコンストラクタ
        private BlueEnemyState(PlayGameScreen screen) : base(screen) { }

        /// <summary>
        /// BlueEnemyState 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns></returns>
        public static BlueEnemyState GetInstance(PlayGameScreen screen)
        {
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            _instance = new BlueEnemyState(screen);

            return _instance;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="item"></param>
        public override void Update(GameTime gameTime, Enemy item)
        {
            item.State.Update(gameTime, item);
        }
    }
}
