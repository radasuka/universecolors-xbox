﻿using GameLibrary;
using Microsoft.Xna.Framework;
using System;

namespace UniverseColors.PlayGameScreens.Enemies
{
    /// <summary>
    /// ボス戦時の雑魚敵
    /// </summary>
    public class BossState : ItemState<Enemy>
    {
        private static BossState _instance;

        // プライベートコンストラクタ
        private BossState(PlayGameScreen screen) : base(screen) { }

        /// <summary>
        /// BossState 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns></returns>
        public static BossState GetInstance(PlayGameScreen screen)
        {
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            _instance = new BossState(screen);

            return _instance;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="item"></param>
        public override void Update(GameTime gameTime, Enemy item)
        {
            if (item.ElementType != ElementType.Boss)
            {
                Vector3 distance;
                distance = Parent.Player.Position - item.Position;

                item.Acceleration.X = Vector3.Normalize(distance).X * 160000.0f;
                item.Acceleration.Z = Vector3.Normalize(distance).Z * 35000.0f;
                item.Acceleration.Y = Vector3.Normalize(distance).Y * 48000.0f;

            }
        }
    }
}
