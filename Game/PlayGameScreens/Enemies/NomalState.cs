﻿using Microsoft.Xna.Framework;
using GameLibrary;

namespace UniverseColors.PlayGameScreens.Enemies
{
    /// <summary>
    /// デフォルト状態の敵の更新
    /// </summary>
    public class NomalState : ItemState<Enemy>
    {
        private static NomalState _instance;

        // プライベートコンストラクタ
        private NomalState(PlayGameScreen screen) : base(screen) { }

        /// <summary>
        /// NomalState 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns></returns>
        public static NomalState GetInstance(PlayGameScreen screen)
        {
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            _instance = new NomalState(screen);

            return _instance;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="enemy"></param>
        /// <param name="screen"></param>
        public override void Update(GameTime gameTime, Enemy enemy)
        {
            Vector3 distance;
            distance = Parent.Player.Position - enemy.Position;
            // 自機が敵機に近づいたら突進してくる。
            if (distance.Length() <= 20000.0f)
                enemy.Acceleration = Vector3.Normalize(distance) * 32000.0f;
        }
    }
}

