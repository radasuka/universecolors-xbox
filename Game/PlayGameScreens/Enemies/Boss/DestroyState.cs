﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace UniverseColors.PlayGameScreens.Enemies.Boss
{
    class DestroyState : ItemState<Boss>
    {
        public DestroyState(PlayGameScreen screen)
            : base(screen) { }

        public override void Update(GameTime gameTime, Boss item)
        {
            item.ShatterTime += gameTime.ElapsedGameTime;
        }
    }
}
