﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace UniverseColors.PlayGameScreens.Enemies.Boss
{
    public class Boss : Enemy
    {
        public enum AttackState
        {
            LeftAttack,
            RightAttack,
            UpAttack,
            MainGun,
        }

        ItemState<Enemy> state = null;
        public ItemState<Enemy> State { get { return state; } }

        /// <summary>
        /// ボスの攻撃制御用時間
        /// </summary>
        public TimeSpan AttackTime = TimeSpan.Zero;
        /// <summary>
        /// ボスの状態制御用時間
        /// </summary>
        public TimeSpan StateTime = TimeSpan.Zero;
        /// <summary>
        /// 放射光
        /// </summary>
        public Vector4 EmissiveColor = Vector4.Zero;

        public override void Update(GameTime gameTime)
        {
            AttackTime += gameTime.ElapsedGameTime;
            StateTime += gameTime.ElapsedGameTime;

            base.Update(gameTime);
        }

        //public void SetStartState(PlayGameScreen screen)
        //{
        //    state = new StartState(screen);
        //}
        //public void SetAttackState(PlayGameScreen screen)
        //{
        //    state = new AttackState(screen);
        //}
        ///// <summary>
        ///// ステートを破壊中に設定します。
        ///// </summary>
        //public void SetDeathState(PlayGameScreen screen)
        //{
        //    state = new DestroyState(screen);
        //}

    }
}
