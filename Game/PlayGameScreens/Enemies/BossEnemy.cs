﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System;

namespace UniverseColors.PlayGameScreens.Enemies
{
    /// <summary>
    /// ボス特有の更新処理
    /// </summary>
    public class BossEnemy : ItemState<Enemy>
    {
        private static BossEnemy _instance;
        // 攻撃色
        private ElementType attackType = ElementType.Red;

        // プライベートコンストラクタ
        private BossEnemy(PlayGameScreen screen) : base(screen) { }

        /// <summary>
        /// BossEnemy 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns>インスタンス</returns>
        public static BossEnemy GetInstance(PlayGameScreen screen)
        {
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            _instance = new BossEnemy(screen);

            return _instance;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime">前回のフレームからの経過時間</param>
        /// <param name="item"></param>
        public override void Update(GameTime gameTime, Enemy item)
        {
            item.FlashingTime -= gameTime.ElapsedGameTime;

            // 右回転
            item.Yaw += 0.01f;

            // 状態によって更新を変えます。
            item.State.Update(gameTime, item);

            #region ボス攻撃

            // ボスの攻撃パターン
            LinkedListNode<Enemy> reservedEnemy = Parent.Enemies.ReservedList.First, activeEnemy = null;
            if (reservedEnemy != null)
            {
                Random rand = new Random();

                // 攻撃前に色点滅させる
                if (item.FlashingTime <= TimeSpan.Zero)
                {
                    float dColor = 0f;
                    switch (attackType)
                    {
                        case ElementType.Red:
                            dColor = (float)Math.Sin(item.FlashingTime.TotalSeconds * 8) / 2.0f + 0.5f;
                            item.DiffuseColor = new Vector4(1.0f, dColor, dColor, 1.0f);
                            break;

                        case ElementType.Blue:
                            dColor = (float)Math.Sin(item.FlashingTime.TotalSeconds * 8) / 2.0f + 0.5f;
                            item.DiffuseColor = new Vector4(dColor, dColor, 1.0f, 1.0f);
                            break;

                        case ElementType.Green:
                            dColor = (float)Math.Sin(item.FlashingTime.TotalSeconds * 8) / 2.0f + 0.5f;
                            item.DiffuseColor = new Vector4(dColor, 1.0f, dColor, 1.0f);
                            break;
                    }
                    item.Alpha = (float)Math.Sin(item.AttackTime.TotalSeconds);

                    item.AttackTime += gameTime.ElapsedGameTime;

                    // 攻撃
                    if (item.AttackTime >= TimeSpan.FromSeconds(5f))
                    {
                        for (int index = 0; index < 5; ++index)
                        {
                            activeEnemy = reservedEnemy;
                            reservedEnemy = reservedEnemy.Next;
                            activeEnemy.Value.SetElement(Parent.EnemyShapes[(int)attackType], attackType);
                            activeEnemy.Value.Acceleration.Z = item.Acceleration.Z + 500.0f;
                            activeEnemy.Value.Position = item.Position;
                            activeEnemy.Value.Position.Z = item.Position.Z + index * 5000.0f;
                            Parent.Enemies.ActiveEnemy(activeEnemy);
                        }
                        item.AttackTime = TimeSpan.Zero;
                        item.FlashingTime = TimeSpan.FromSeconds(3.5f);
                        item.DiffuseColor = Color.White.ToVector4();
                        attackType = (ElementType)rand.Next(0, 3);
                    }
                }
            }
            #endregion
        }
    }
}
