﻿using Microsoft.Xna.Framework;
using GameLibrary;

namespace UniverseColors.PlayGameScreens.Enemies
{
    /// <summary>
    /// 破壊中の敵
    /// </summary>
    public class DestroyState : ItemState<Enemy>
    {
        private static DestroyState _instance;

        // プライベートコンストラクタ
        private DestroyState(PlayGameScreen screen) : base(screen) { }

        /// <summary>
        /// DestroyState 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns>インスタンス</returns>
        public static DestroyState GetInstance(PlayGameScreen screen)
        {
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            _instance = new DestroyState(screen);

            return _instance;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime">前回のフレームからの経過時間</param>
        /// <param name="enemy"></param>
        public override void Update(GameTime gameTime, Enemy enemy)
        {
            enemy.ShatterTime += gameTime.ElapsedGameTime;
        }
    }
}
