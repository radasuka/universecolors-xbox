﻿using System;
using Microsoft.Xna.Framework;

namespace UniverseColors.PlayGameScreens.Enemies
{
    /// <summary>
    /// ボス被弾状態
    /// </summary>
    public class DamageState : ItemState<Enemy>
    {
        private static DamageState _instance;

        // プライベートコンストラクタ
        private DamageState(PlayGameScreen screen) : base(screen) { }

        /// <summary>
        /// DamageState 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns>インスタンス</returns>
        public static DamageState GetInstance(PlayGameScreen screen)
        {
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            _instance = new DamageState(screen);

            return _instance;
        }

        /// <summary>
        /// フレーム更新処理
        /// </summary>
        /// <param name="gameTime">前回のフレームからの経過時間</param>
        /// <param name="item"></param>
        public override void Update(GameTime gameTime, Enemy item)
        {
            item.DamageTime += gameTime.ElapsedGameTime;

            // 発光
            item.EmissiveColor.X = (float)Math.Sin(item.DamageTime.TotalSeconds * 8) / 2f + 0.5f;
            item.EmissiveColor.Y = (float)Math.Sin(item.DamageTime.TotalSeconds * 8) / 2f + 0.5f;
            item.EmissiveColor.Z = (float)Math.Sin(item.DamageTime.TotalSeconds * 8) / 2f + 0.5f;

            // ボスを左右に揺らす
            item.Position.X += (float)Math.Sin(item.StateTime.TotalSeconds * 15) * 50;

            if (item.DamageTime > TimeSpan.FromSeconds(1.5f))
            {
                item.SetBossState(this.Parent);
                item.StateTime = TimeSpan.Zero;
                item.DamageTime = TimeSpan.Zero;
                item.EmissiveColor = Vector4.Zero;
                item.Alpha = 1;
                item.Position.X = 0;
            }
        }
    }
}
