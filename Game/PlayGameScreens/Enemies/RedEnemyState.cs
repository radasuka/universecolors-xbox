﻿using Microsoft.Xna.Framework;

namespace UniverseColors.PlayGameScreens.Enemies
{
    /// <summary>
    /// 赤属性の敵
    /// </summary>
    public class RedEnemyState : ItemState<Enemy>
    {
        private static RedEnemyState _instance;

        // プライベートコンストラクタ
        private RedEnemyState(PlayGameScreen screen) : base(screen) { }

        /// <summary>
        /// RedEnemyState 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns></returns>
        public static RedEnemyState GetInstance(PlayGameScreen screen)
        {
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            _instance = new RedEnemyState(screen);

            return _instance;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="item"></param>
        public override void Update(GameTime gameTime, Enemy item)
        {
            // 状態によって更新を変えます。
            item.State.Update(gameTime, item);
        }
    }
}
