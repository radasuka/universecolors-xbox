﻿using Microsoft.Xna.Framework;

namespace UniverseColors.PlayGameScreens.Enemies
{
    /// <summary>
    /// 緑属性の敵
    /// </summary>
    public class GreenEnemyState : ItemState<Enemy>
    {
        private static GreenEnemyState _instance;

        // プライベートコンストラクタ
        private GreenEnemyState(PlayGameScreen screen) : base(screen) { }

        /// <summary>
        /// GreenEnemyState 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns></returns>
        public static GreenEnemyState GetInstance(PlayGameScreen screen)
        {
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            _instance = new GreenEnemyState(screen);

            return _instance;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="item"></param>
        public override void Update(GameTime gameTime, Enemy item)
        {
            item.State.Update(gameTime, item);
        }
    }
}
