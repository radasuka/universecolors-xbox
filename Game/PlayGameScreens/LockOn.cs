﻿using GameLibrary.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UniverseColors.PlayGameScreens.Enemies;

namespace UniverseColors.PlayGameScreens
{
    public class LockOn : SpriteItem
    {
        #region 列挙体

        /// <summary>
        /// 敵機との相性を表す列挙体
        /// </summary>
        public enum CompatibilityType
        {
            Strong,
            Even,
            Weak
        }

        #endregion

        #region プロパティ

        /// <summary>
        /// スプライトシェイプ
        /// </summary>
        public new SpriteSheet Shape { get; set; }

        CompatibilityType compatibility = CompatibilityType.Strong; 
        /// <summary>
        /// ターゲットとの相性
        /// </summary>
        public CompatibilityType Compatibility
        {
            get { return compatibility; }
            set { compatibility = value; }
        }
        /// <summary>
        /// ターゲット
        /// </summary>
        public Enemy Target { get; set; }

        #endregion

        /// <summary>
        /// このスクリーン アイテムを描画する必要がある場合に呼び出されます。
        /// </summary>
        /// <param name="spriteBatch">スプライト描画に使用する<see cref="SpriteBatch"/></param>
        /// <param name="gameTime">前回<c>Update</c>が呼び出されてからの経過時間</param>
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Draw(Shape.Texture,
                             Position,
                             Shape.SourceRectangle((int)Compatibility),
                             Color,
                             Rotation,
                             Shape.Origin,
                             Scale, SpriteEffects, 0);
        }
    }
}
