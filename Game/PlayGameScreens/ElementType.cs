﻿namespace UniverseColors.PlayGameScreens
{
    /// <summary>
    /// 赤、青、緑の属性を表す列挙体。
    /// </summary>
    public enum ElementType
    {
        Red = 0,
        Blue = 1,
        Green = 2,
        Boss = 3,
    }
}