﻿using System;
using GameLibrary.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UniverseColors.PlayGameScreens
{
    public class Score : SpriteItem
    {
        #region 定数フィールド

        /// <summary>
        /// 表示桁数
        /// </summary>
        private const int digitNumber = 8;

        #endregion

        #region フィールドとプロパティ

        /// <summary>
        /// スプライトシェイプ
        /// </summary>
        public new SpriteSheet Shape { get; set; }
        /// <summary>
        /// 描画基準位置
        /// </summary>
        public Vector2 BasicPosition { get; set; }
        /// <summary>
        /// 現在スコア
        /// </summary>
        public int CurrentScore { get; set;}
        /// <summary>
        /// ランキングで使用
        /// </summary>
        public bool IsNew = false;

        #endregion

        /// <summary>
        /// スコアの描画処理。
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="gameTime"></param>
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Begin();

            for (int index = 1; index < digitNumber; ++index)
            {
                // 数字の描画位置を計算します。
                int surplus, number;
                surplus = (int)(CurrentScore % Math.Pow(10, index));
                number = (int)(surplus / Math.Pow(10, index - 1));

                Position.X = BasicPosition.X + (index * -Shape.SourceRectangle(0).Width);
                Position.Y = BasicPosition.Y;

                spriteBatch.Draw(Shape.Texture,
                                 Position,
                                 Shape.SourceRectangle(number),
                                 Color,
                                 Rotation,
                                 Shape.Origin,
                                 Scale, SpriteEffects, 0);
            }
            spriteBatch.End();
        }
    }
}
