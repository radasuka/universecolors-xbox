﻿using Microsoft.Xna.Framework;

namespace UniverseColors.PlayGameScreens.Bullets
{
    public class BlueBulletState : ItemState<Bullet>
    {
        private static BlueBulletState _instance;

        // プライベートコンストラクタ
        private BlueBulletState(PlayGameScreen screen)
            : base(screen) { }

        /// <summary>
        /// BlueBulletState 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns></returns>
        public static BlueBulletState GetInstance(PlayGameScreen screen)
        {
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            _instance = new BlueBulletState(screen);

            return _instance;
        }

        public override void Update(GameTime gameTime, Bullet item)
        {
            item.State.Update(gameTime, item);
        }
    }
}
