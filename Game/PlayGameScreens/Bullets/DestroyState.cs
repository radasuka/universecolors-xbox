﻿using GameLibrary;
using Microsoft.Xna.Framework;

namespace UniverseColors.PlayGameScreens.Bullets
{
    public class DestroyState : ItemState<Bullet>
    {
        private static DestroyState _instance;

        // プライベートコンストラクタ
        private DestroyState(PlayGameScreen screen)
            : base(screen) { }

        /// <summary>
        /// DestroyState 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns></returns>
        public static DestroyState GetInstance(PlayGameScreen screen)
        {
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            _instance = new DestroyState(screen);

            return _instance;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="item"></param>
        public override void Update(GameTime gameTime, Bullet item)
        {
            item.ShatterTime += gameTime.ElapsedGameTime;
        }
    }
}
