﻿using GameLibrary;
using Microsoft.Xna.Framework;
using System;

namespace UniverseColors.PlayGameScreens.Bullets
{
    public class NomalState : ItemState<Bullet>
    {
        private static NomalState _instance;

        // プライベートコンストラクタ
        private NomalState(PlayGameScreen screen)
            : base(screen) { }

        /// <summary>
        /// NomalState 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns></returns>
        public static NomalState GetInstance(PlayGameScreen screen)
        {
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            _instance = new NomalState(screen);

            return _instance;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="bullet"></param>
        public override void Update(GameTime gameTime, Bullet item)
        {
            if (item.Target != null)
            {
                Vector3 distance;
                // ターゲットまでの距離を算出
                distance = item.Target.Position - item.Position;
                // 目標に向かって誘導する
                item.Velocity = Vector3.Normalize(distance) * 105000.0f;
            }
        }
    }
}
