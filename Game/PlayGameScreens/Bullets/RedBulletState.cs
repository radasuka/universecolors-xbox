﻿using Microsoft.Xna.Framework;

namespace UniverseColors.PlayGameScreens.Bullets
{
    public class RedBulletState : ItemState<Bullet>
    {
        private static RedBulletState _instance;

        // プライベートコンストラクタ
        private RedBulletState(PlayGameScreen screen)
            : base(screen) { }

        /// <summary>
        /// RedBulletState 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns></returns>
        public static RedBulletState GetInstance(PlayGameScreen screen)
        {
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            _instance = new RedBulletState(screen);

            return _instance;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="item"></param>
        public override void Update(GameTime gameTime, Bullet item)
        {
            item.State.Update(gameTime, item);
        }
    }
}
