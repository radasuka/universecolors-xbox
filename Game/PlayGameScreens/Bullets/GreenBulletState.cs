﻿using Microsoft.Xna.Framework;

namespace UniverseColors.PlayGameScreens.Bullets
{
    public class GreenBulletState : ItemState<Bullet>
    {
        private static GreenBulletState _instance;

        // プライベートコンストラクタ
        private GreenBulletState(PlayGameScreen screen)
            : base(screen) { }

        /// <summary>
        /// GreenBulletState 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns></returns>
        public static GreenBulletState GetInstance(PlayGameScreen screen)
        {
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            _instance = new GreenBulletState(screen);

            return _instance;
        }

        public override void Update(GameTime gameTime, Bullet item)
        {
            item.State.Update(gameTime, item);
        }
    }
}
