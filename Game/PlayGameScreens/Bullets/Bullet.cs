﻿using System;
using GameLibrary;
using GameLibrary.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UniverseColors.PlayGameScreens.Bullets
{
    public class Bullet : BasicItem
    {

        #region フィールド

        // 弾の発射速度
        public const float Speed = 150000.0f;

        private Matrix[] transforms;

        /// <summary>
        /// モデルデータ
        /// </summary>
        public override Model Shape
        {
            get { return base.Shape; }
            set
            {
                base.Shape = value;
                this.transforms = new Matrix[value.Bones.Count];
                this.InitializeBoundingSphere();
            }
        }

        /// <summary>
        /// ターゲット
        /// </summary>
        public BasicItem Target { get; set; }
        /// <summary>
        /// 弾の属性
        /// </summary>
        public ElementType ElementType { get; set; }

        public ItemState<Bullet> Element { get; set; }

        /// <summary>
        /// 弾の状態
        /// </summary>
        public ItemState<Bullet> State { get; private set; }

        /// <summary>
        /// 弾の生存時間
        /// </summary>
        public TimeSpan LifeTime = TimeSpan.Zero;
        /// <summary>
        /// 破壊エフェクトの経過時間
        /// </summary>
        public TimeSpan ShatterTime = TimeSpan.Zero;

        #endregion

        /// <summary>
        /// 弾の更新処理。
        /// </summary>
        /// <param name="gameTime">前回のフレームからの経過時間</param>
        public override void Update(GameTime gameTime)
        {
            LifeTime += gameTime.ElapsedGameTime;

            //PreviousPosition = Position;
            base.Update(gameTime);

            // 状態に応じて更新処理を変更します。
            Element.Update(gameTime, this);
        }

        /// <summary>
        /// 弾を描画します。
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="cameraPosition"></param>
        /// <param name="view"></param>
        /// <param name="projection"></param>
        public void Draw(GameTime gameTime, Vector3 cameraPosition, Matrix view, Matrix projection)
        {
            Shape.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in Shape.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    SetupEffect(transforms, mesh, part, cameraPosition, view, projection);
                }
                mesh.Draw();
            }
        }

        // シェーダーが必要とするエフェクト パラメーターを設定します。
        private void SetupEffect(Matrix[] transforms, ModelMesh mesh,
                                ModelMeshPart part, Vector3 cameraPosition, Matrix view, Matrix projection)
        {
            Effect effect = part.Effect;
            effect.Parameters["TranslationAmount"].SetValue(5.0f * (float)ShatterTime.TotalSeconds);
            effect.Parameters["RotationAmount"].SetValue(MathHelper.Pi * 3 * (float)ShatterTime.TotalSeconds);
            effect.Parameters["time"].SetValue((float)ShatterTime.TotalSeconds);
            effect.Parameters["WorldViewProjection"].SetValue(
                transforms[mesh.ParentBone.Index] * World * view * projection);
            effect.Parameters["World"].SetValue(transforms[mesh.ParentBone.Index] * World);
            effect.Parameters["eyePosition"].SetValue(cameraPosition);
            effect.Parameters["lightPosition"].SetValue(Vector3.UnitY);
            effect.Parameters["ambientColor"].SetValue(Color.DarkGray.ToVector4());
            effect.Parameters["diffuseColor"].SetValue(Color.White.ToVector4());
            effect.Parameters["specularColor"].SetValue(Color.White.ToVector4());
            effect.Parameters["specularPower"].SetValue(25);
        }

        /// <summary>
        /// ステートをNomalに設定します。
        /// </summary>
        public void SetNomalState(PlayGameScreen screen)
        {
            State = NomalState.GetInstance(screen);
        }
        /// <summary>
        /// ステートをDestroyに設定します。
        /// </summary>
        public void SetDestroyState(PlayGameScreen screen)
        {
            Acceleration = Vector3.Zero;
            Velocity = Vector3.Zero;
            State = DestroyState.GetInstance(screen);
        }
    }
}
