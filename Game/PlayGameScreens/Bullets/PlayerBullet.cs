﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UniverseColors.PlayGameScreens.Bullets
{
    public class PlayerBullet : Bullet
    {
        #region 定数フィールド

        /// <summary>
        /// 弾の生存期間を取得します。
        /// </summary>
        public static readonly TimeSpan Duration = TimeSpan.FromSeconds(1.5f);

        #endregion

        /// <summary>
        /// 自機弾を更新します。
        /// </summary>
        /// <param name="gameTime">前回のフレームからの経過時間</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (Target != null)
            {
                Vector3 distance;
                // ターゲットまでの距離を算出
                distance = this.Target.Position - this.Position;
                // 目標に向かって誘導する
                this.Velocity = Vector3.Normalize(distance) * Speed;
            }
        }
    }
}
