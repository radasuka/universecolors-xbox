﻿using Microsoft.Xna.Framework;

namespace UniverseColors.PlayGameScreens.Players
{
    public class PlayerBlueState : ItemState<Player>
    {
        private static PlayerBlueState _instance;

        // プライベートコンストラクタ
        private PlayerBlueState(PlayGameScreen screen) : base(screen) { }

        /// <summary>
        /// PlayerBlueState 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns></returns>
        public static PlayerBlueState GetInstance(PlayGameScreen screen)
        {
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            _instance = new PlayerBlueState(screen);

            return _instance;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="item"></param>
        public override void Update(GameTime gameTime, Player item)
        {
            item.MotionState.Update(gameTime, item);
        }
    }
}
