using System;
using System.Collections.Generic;
using GameLibrary;
using GameLibrary.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SkinnedModel;

namespace UniverseColors.PlayGameScreens.Players
{
    public class Player : BasicItem
    {
        #region 定数フィールド

        /// <summary>
        /// 最小高度を表します。
        /// </summary>
        public const float MinimumAltitude = 350.0f;
        /// <summary>
        /// プレイヤーの最高速度
        /// </summary>
        private const float maximumSpeed = 20000.0f;
        /// <summary>
        /// 自機が発射する弾の加速度
        /// </summary>
        public static readonly Vector3 BulletAceleration = new Vector3(0, 0, 90000.0f);
        /// <summary>
        /// 自機が同時に発射できる弾の最大数
        /// </summary>
        public const int MaxBullet = 6;

        /// <summary>
        /// アタックモーション時のタイムアウト
        /// </summary>
        public static readonly TimeSpan AttackTimeout = TimeSpan.FromSeconds(1.0);
        /// <summary>
        /// ダメージモーション時のタイムアウト
        /// </summary>
        public static readonly TimeSpan DamageTimeout = TimeSpan.FromSeconds(0.8);

        #endregion

        #region フィールドとプロパティ

        /// <summary>
        /// モデルデータ
        /// </summary>
        public override Model Shape
        {
            get { return base.Shape; }
            set
            {
                // カスタム スキニング情報を検索します。
                this.skinningdata = value.Tag as SkinningData;

                if (this.skinningdata == null)
                    throw new InvalidOperationException
                        ("このモデルにはSkinningDataタグは含まれていません。");

                // アニメーション プレイヤーを作成し、アニメーション クリップのデコードを開始します。
                this.animationplayer = new AnimationPlayer(this.skinningdata);

                AnimationClip clip = this.skinningdata.AnimationClips["Take 001"];

                this.animationplayer.StartClip(clip);

                base.Shape = value;
                this.InitializeBoundingSphere();
            }
        }

        /// <summary>
        /// 自機モデルのリストを取得します。
        /// </summary>
        public List<Model> Shapes { get; set; }

        /// <summary>
        /// プレイヤーの属性
        /// </summary>
        public ItemState<Player> Element { get; private set; }
        public ElementType ElementType { get; private set; }

        /// <summary>
        /// プレイヤーの状態
        /// </summary>
        public ItemState<Player> MotionState { get; private set; }

        /// <summary>
        /// アニメーション処理
        /// </summary>
        public AnimationPlayer animationplayer { get; set; }
        public SkinningData skinningdata { get; set; }
        /// <summary>
        /// 放射光
        /// </summary>
        public Vector3 EmissiveColor { get; set; }
        /// <summary>
        /// アルファ値
        /// </summary>
        public float Alpha { get; set; }

        /// <summary>
        /// プレイヤーの残機
        /// </summary>
        public int HitPoint { get; set; }
        /// <summary>
        /// プレイヤーの状態管理用時間
        /// </summary>
        public TimeSpan StateTime = TimeSpan.Zero;

        // 無敵フラグ(DEBUG)
        public bool Invincible = false;

        #endregion

        /// <summary>
        /// プレイヤーの更新処理をします。
        /// </summary>
        /// <param name="gameTime">前回のフレームからの経過時間</param>
        public override void Update(GameTime gameTime)
        {
            StateTime += gameTime.ElapsedGameTime;

            // 直前の位置座標を設定
            //PreviousPosition = Position;
            base.Update(gameTime);

            // プレイヤーが最高速度を超えないように設定します。
            if (Velocity.Length() >= maximumSpeed)
                Velocity.Z = -maximumSpeed;

            // プレイヤーの状態に応じて更新処理を変更します。
            this.Element.Update(gameTime, this);

            // アニメーションを更新します
            this.animationplayer.Update(gameTime.ElapsedGameTime, true, World);

#if DEBUG
            if (this.Invincible)
                if (this.HitPoint < 3)
                    this.HitPoint = 3;
#endif
        }

        /// <summary>
        /// ステートをStandに設定します。
        /// </summary>
        public void SetStandState(PlayGameScreen screen)
        {
            MotionState = StandState.GetInstance(screen);
            Shape = Shapes[0];
        }
        /// <summary>
        /// ステートをAttackに設定します。
        /// </summary>
        public void SetAttackState(PlayGameScreen screen)
        {
            MotionState = AttackState.GetInstance(screen);
            Shape = Shapes[1];
        }
        /// <summary>
        /// ステートをDamageに設定します。
        /// </summary>
        public void SetDamageState(PlayGameScreen screen)
        {
            MotionState = DamageState.GetInstance(screen);
            Shape = Shapes[2];
        }

        /// <summary>
        /// 属性を赤に設定します。
        /// </summary>
        /// <param name="screen"></param>
        public void SetRedState(PlayGameScreen screen)
        {
            Element = PlayerRedState.GetInstance(screen);
            ElementType = ElementType.Red;
        }
        /// <summary>
        /// 属性を青に設定します。
        /// </summary>
        /// <param name="screen"></param>
        public void SetBlueState(PlayGameScreen screen)
        {
            Element = PlayerBlueState.GetInstance(screen);
            ElementType = ElementType.Blue;
        }
        /// <summary>
        /// 属性を緑に設定します。
        /// </summary>
        /// <param name="screen"></param>
        public void SetGreenState(PlayGameScreen screen)
        {
            Element = PlayerGreenState.GetInstance(screen);
            ElementType = ElementType.Green;
        }
    }
}