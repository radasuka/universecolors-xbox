﻿using System;
using GameLibrary;
using Microsoft.Xna.Framework;

namespace UniverseColors.PlayGameScreens.Players
{
    public class AttackState : ItemState<Player>
    {
        private static AttackState _instance;

        //プライベート コンストラクター
        private AttackState(PlayGameScreen screen) : base(screen) { }

        /// <summary>
        /// AttackState 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns></returns>
        public static AttackState GetInstance(PlayGameScreen screen)
        {
            // 単一インスタンスが存在する場合は、それを返します。
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            // ステート作成
            _instance = new AttackState(screen);

            return _instance;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime">前回のフレームからの経過時間</param>
        /// <param name="item"></param>
        public override void Update(GameTime gameTime, Player item)
        {
            // アタックモーションが終わったらStandに遷移する
            if (item.StateTime > Player.AttackTimeout)
            {
                item.SetStandState(this.Parent);
                item.StateTime = TimeSpan.Zero;
            }
        }
    }
}
