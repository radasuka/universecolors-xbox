﻿using Microsoft.Xna.Framework;

namespace UniverseColors.PlayGameScreens.Players
{
    public class PlayerGreenState: ItemState<Player>
    {
        private static PlayerGreenState _instance;

        // プライベートコンストラクタ
        private PlayerGreenState(PlayGameScreen screen) : base(screen) { }

        /// <summary>
        /// PlayerGreenState 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns></returns>
        public static PlayerGreenState GetInstance(PlayGameScreen screen)
        {
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            _instance = new PlayerGreenState(screen);

            return _instance;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="item"></param>
        public override void Update(GameTime gameTime, Player item)
        {
            item.MotionState.Update(gameTime, item);
        }
    }
}
