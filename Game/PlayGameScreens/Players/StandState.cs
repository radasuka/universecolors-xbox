﻿using GameLibrary;
using Microsoft.Xna.Framework;

namespace UniverseColors.PlayGameScreens.Players
{
    public class StandState : ItemState<Player>
    {
        private static StandState _instance;

        //プライベート コンストラクター
        private StandState(PlayGameScreen screen) : base(screen) { }

        /// <summary>
        /// StandState 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns></returns>
        public static StandState GetInstance(PlayGameScreen screen)
        {
            // 単一インスタンスが存在する場合は、それを返します。
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            // ステート作成
            _instance = new StandState(screen);

            return _instance;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime">前回のフレームからの経過時間</param>
        /// <param name="player"></param>
        public override void Update(GameTime gameTime, Player item)
        {
        }
    }
}
