﻿using System;
using GameLibrary;
using Microsoft.Xna.Framework;

namespace UniverseColors.PlayGameScreens.Players
{
    /// <summary>
    /// 被弾時
    /// </summary>
    public class DamageState : ItemState<Player>
    {
        private static DamageState _instance;

        //プライベート コンストラクター
        private DamageState(PlayGameScreen screen) : base(screen){ }

        /// <summary>
        /// DamageState 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns>インスタンス</returns>
        public static DamageState GetInstance(PlayGameScreen screen)
        {
            // 単一インスタンスが存在する場合は、それを返します。
            // 1 つのゲームに対して 2 つのステートを作成しないようにします。
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            // ステート作成
            _instance = new DamageState(screen);

            return _instance;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime">前回のフレームからの経過時間</param>
        public override void Update(GameTime gameTime, Player item)
        {
            // プレイヤーを発光させます。
            var _emissiveColor = Vector3.Zero;
            _emissiveColor.X += (float)Math.Sin(item.StateTime.TotalSeconds);
            _emissiveColor.Y += (float)Math.Sin(item.StateTime.TotalSeconds);
            _emissiveColor.Z += (float)Math.Sin(item.StateTime.TotalSeconds);
            item.EmissiveColor = _emissiveColor;
            item.Alpha = (float)Math.Sin(item.StateTime.TotalSeconds);

            // カメラを振動させます。
            Vector3 cameraPosition = Parent.playerCamera.Position;
            cameraPosition.X += (float)Math.Sin(item.StateTime.TotalSeconds * 15) * 50;
            this.Parent.playerCamera.Position = cameraPosition;

            // ダメージモーションが終わったらStandに遷移する
            if (item.StateTime >= Player.DamageTimeout)
            {
                item.SetStandState(this.Parent);
                item.StateTime = TimeSpan.Zero;
                item.EmissiveColor = new Vector3(0.1f, 0.1f, 0.4f);
                item.Alpha = 1;
            }
        }
    }
}
