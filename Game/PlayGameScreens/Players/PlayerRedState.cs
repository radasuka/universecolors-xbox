﻿using Microsoft.Xna.Framework;

namespace UniverseColors.PlayGameScreens.Players
{
    public class PlayerRedState : ItemState<Player>
    {
        private static PlayerRedState _instance;

        // プライベートコンストラクタ
        private PlayerRedState(PlayGameScreen screen) : base(screen) { }

        /// <summary>
        /// PlayerGreenState 取得
        /// </summary>
        /// <param name="screen"></param>
        /// <returns></returns>
        public static PlayerRedState GetInstance(PlayGameScreen screen)
        {
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            _instance = new PlayerRedState(screen);

            return _instance;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="item"></param>
        public override void Update(GameTime gameTime, Player item)
        {
            item.MotionState.Update(gameTime, item);
        }
    }
}
