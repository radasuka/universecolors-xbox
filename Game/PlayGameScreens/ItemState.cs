﻿using GameLibrary;
using GameLibrary.Graphics;
using Microsoft.Xna.Framework;

namespace UniverseColors.PlayGameScreens
{
    public abstract class ItemState<T> : IState<T>
    {
        public PlayGameScreen Parent { get; protected set; }
        // コンストラクター
        public ItemState(PlayGameScreen screen)
        {
            Parent = screen;
        }

        public abstract void Update(GameTime gameTime, T item);
    }
}
