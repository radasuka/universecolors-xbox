﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UniverseColors.PlayGameScreens.BackGrounds
{
    public class ChangeState : BackGroundState
    {
        private static ChangeState _instance;

        private ChangeState(PlayGameScreen screen)
            : base(screen) { }

        public static ChangeState GetInstance(PlayGameScreen screen)
        {
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            _instance = new ChangeState(screen);

            return _instance;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime">前回のフレームからの経過時間</param>
        /// <param name="item">Cylinder</param>
        public override void Update(GameTime gameTime, Cylinder item)
        {
            item.Position.Z = Parent.Player.Position.Z;
            if (item.StateTime > TimeSpan.FromSeconds(2))
                item.SetNomalState(this.Parent);
        }

        /// <summary>
        /// 描画処理
        /// </summary>
        /// <param name="gameTime">前回のフレームからの経過時間</param>
        /// <param name="item">Cylinder</param>
        /// <param name="view">view</param>
        /// <param name="projection">projection</param>
        public override void Draw(GameTime gameTime, Cylinder item, Matrix view, Matrix projection)
        {
            for (int index = 0; index < item.Shapes.Count; ++index)
            {
                if (index <= (int)this.Parent.Player.ElementType)
                {
                    // 切替時に描画しない背景は省く
                    if (index == (((int)this.Parent.Player.ElementType + 1) % 3))
                        continue;

                    Matrix[] transforms = new Matrix[item.Shapes[index].Bones.Count];
                    item.Shapes[index].CopyAbsoluteBoneTransformsTo(transforms);

                    var pos = Vector3.Zero;
                    pos.Y = index * -27600.0f;
                    if (index == 2)
                        pos.Y = index * -37300.0f;

                    foreach (ModelMesh mesh in item.Shapes[index].Meshes)
                    {
                        foreach (BasicEffect effect in mesh.Effects)
                        {
                            effect.EnableDefaultLighting();
                            effect.World = transforms[mesh.ParentBone.Index] * item.World * Matrix.CreateTranslation(pos);
                            effect.DiffuseColor = Color.White.ToVector3();
                            // 追尾カメラの行列を使用します。
                            effect.View = view;
                            effect.Projection = projection;
                        }
                        mesh.Draw();
                    }
                }
                else if (index >= (int)this.Parent.Player.ElementType)
                {
                    Matrix[] transforms = new Matrix[item.Shapes[index].Bones.Count];
                    item.Shapes[index].CopyAbsoluteBoneTransformsTo(transforms);

                    var pos = Vector3.Zero;
                    pos.Y = index * -27600.0f;
                    if (index == 2)
                        pos.Y = index * -37300.0f;

                    foreach (ModelMesh mesh in item.Shapes[index].Meshes)
                    {
                        foreach (BasicEffect effect in mesh.Effects)
                        {
                            effect.EnableDefaultLighting();
                            effect.World = transforms[mesh.ParentBone.Index] * item.World * Matrix.CreateTranslation(pos);
                            effect.DiffuseColor = Color.White.ToVector3();
                            // 追尾カメラの行列を使用します。
                            effect.View = view;
                            effect.Projection = projection;
                        }
                        mesh.Draw();
                    }
                }
            }
        }
    }
}
