﻿#region File Description
//-----------------------------------------------------------------------------
// BackGround.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

using System;
using GameLibrary.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.ObjectModel;
using UniverseColors.PlayGameScreens.Players;

namespace UniverseColors.PlayGameScreens.BackGrounds
{
    public class BackGround : BasicItem
    {
        protected Matrix[] transforms;

        public Matrix[] Transforms
        {
            get { return transforms; }
        }

        /// <summary>
        /// モデルデータ
        /// </summary>
        public override Model Shape
        {
            get { return base.Shape; }
            set
            {
                base.Shape = value;
                this.transforms = new Matrix[value.Bones.Count];
            }
        }

        /// <summary>
        /// マテリアルのアルファ値
        /// </summary>
        public float Alpha { get; set; }

        /// <summary>
        /// 背景の更新処理。
        /// </summary>
        /// <param name="gametime">前回のフレームからの経過時間</param>
        public virtual void Update(Player player,GameTime gametime)
        {
            base.Update(gametime);

            switch (player.ElementType)
            {
                case ElementType.Red:
                    if (Position.Y <= 0)
                    {
                        Position.Y = 0;
                        Velocity.Y = 0f;
                    }
                    break;
                case ElementType.Blue:
                    if (Position.Y >= 25000.0f)
                    {
                        Position.Y = 25000.0f;
                        Velocity.Y = 0;
                    }
                    break;
                case ElementType.Green:
                    if (Position.Y >= 75000.0f)
                    {
                        Position.Y = 75000.0f;
                        Velocity.Y = 0;
                    }
                    break;
            }
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="view"></param>
        /// <param name="projection"></param>
        public virtual void Draw(GameTime gameTime, Matrix view, Matrix projection, Player player)
        {
            Shape.CopyAbsoluteBoneTransformsTo(transforms);
            foreach (ModelMesh mesh in Shape.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.World = transforms[mesh.ParentBone.Index] * World;
                    effect.DiffuseColor = Color.White.ToVector3();
                    // 追尾カメラの行列を使用します。
                    effect.View = view;
                    effect.Projection = projection;
                }
                mesh.Draw();
            }
        }

    }
}
