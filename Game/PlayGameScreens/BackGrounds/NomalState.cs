﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UniverseColors.PlayGameScreens.BackGrounds
{
    public class NomalState : BackGroundState
    {
        private static NomalState _instance;

        // プライベートコンストラクタ
        private NomalState(PlayGameScreen screen)
            : base(screen) { }

        /// <summary>
        /// インスタンスを取得
        /// </summary>
        /// <param name="screen">PlayGameScreen</param>
        /// <returns>インスタンス</returns>
        public static NomalState GetInstance(PlayGameScreen screen)
        {
            if (_instance != null)
            {
                _instance.Parent = screen;
                return _instance;
            }

            _instance = new NomalState(screen);

            return _instance;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime">前回のフレームからの経過時間</param>
        /// <param name="item">Cylinder</param>
        public override void Update(GameTime gameTime, Cylinder item) 
        {
            item.Position.Z = Parent.Player.Position.Z;
        }

        /// <summary>
        /// 描画処理
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="item">Cylinder</param>
        /// <param name="view">view</param>
        /// <param name="projection">projection</param>
        public override void Draw(GameTime gameTime, Cylinder item, Matrix view, Matrix projection)
        {
            for (int index = 0; index < item.Shapes.Count; ++index)
            {
                if (index != (int)this.Parent.Player.ElementType)
                    continue;
                //if (index == (((int)this.Parent.Player.ElementType + 1) % 3))
                //    continue;
                // 属性が固定の時
                //if (index == (int)this.Parent.Player.ElementType)
                //{
                    Matrix[] transforms = new Matrix[item.Shapes[index].Bones.Count];
                    item.Shapes[index].CopyAbsoluteBoneTransformsTo(transforms);

                    Vector3 pos = Vector3.Zero;
                    pos.Y = index * -27600;
                    if (index == 2)
                        pos.Y = index * -37300;

                    foreach (ModelMesh mesh in item.Shapes[index].Meshes)
                    {
                        foreach (BasicEffect effect in mesh.Effects)
                        {
                            effect.EnableDefaultLighting();
                            effect.World = transforms[mesh.ParentBone.Index] * item.World * Matrix.CreateTranslation(pos);
                            effect.DiffuseColor = Color.White.ToVector3();
                            // 追尾カメラの行列を使用します。
                            effect.View = view;
                            effect.Projection = projection;
                        }
                        mesh.Draw();
                    }
                //}
            }
        }
    }
}
