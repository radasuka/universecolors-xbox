﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UniverseColors.PlayGameScreens.Players;

namespace UniverseColors.PlayGameScreens.BackGrounds
{
    public class Cylinder : BackGround
    {
        /// <summary>
        /// モデルのリストを取得します。
        /// </summary>
        public List<Model> Shapes { get; set; }
        /// <summary>
        /// 現在の状態
        /// </summary>
        public BackGroundState State { get; private set; }
        /// <summary>
        /// 背景の状態管理用時間
        /// </summary>
        public TimeSpan StateTime = TimeSpan.Zero;


        /// <summary>
        /// 背景の更新処理。
        /// </summary>
        /// <param name="gametime">前回のフレームからの経過時間</param>
        public override void Update(Player player, GameTime gametime)
        {
            base.Update(player, gametime);
            StateTime += gametime.ElapsedGameTime;
            State.Update(gametime, this);
        }

        /// <summary>
        /// 描画処理
        /// </summary>
        /// <param name="gameTime">前回のフレームからの経過時間</param>
        /// <param name="view"></param>
        /// <param name="projection"></param>
        public override void Draw(GameTime gameTime, Matrix view, Matrix projection, Player player)
        {
            State.Draw(gameTime, this, view, projection);
        }

        /// <summary>
        /// 状態を移行中に設定します。
        /// </summary>
        /// <param name="screen"></param>
        public void SetChangeState(PlayGameScreen screen)
        {
            State = ChangeState.GetInstance(screen);
            StateTime = TimeSpan.Zero;
        }
        /// <summary>
        /// 状態を通常にします。
        /// </summary>
        /// <param name="screen"></param>
        public void SetNomalState(PlayGameScreen screen)
        {
            State = NomalState.GetInstance(screen);
            StateTime = TimeSpan.Zero;
        }
    }
}
