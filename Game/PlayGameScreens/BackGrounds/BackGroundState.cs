﻿using Microsoft.Xna.Framework;

namespace UniverseColors.PlayGameScreens.BackGrounds
{
    public abstract class BackGroundState : ItemState<Cylinder>
    {
        public BackGroundState(PlayGameScreen screen)
            : base(screen) { }

        public virtual void Draw(GameTime gameTime, Cylinder item, Matrix view, Matrix projection)
        {
        }
    }
}
