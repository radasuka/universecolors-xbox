﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UniverseColors.PlayGameScreens.Enemies;

namespace UniverseColors.PlayGameScreens
{
    /// <summary>
    /// 複数の敵を管理するクラス
    /// </summary>
    public class EnemyManager
    {
        #region フィールド

        /// <summary>
        /// 予約リスト(マップに登場する前の状態)
        /// </summary>
        public LinkedList<Enemy> ReservedList { get; set; }
        /// <summary>
        /// アクティブ リスト(プレイヤーと接近してアクティブになった状態)
        /// </summary>
        public LinkedList<Enemy> ActiveList { get; set; }
        /// <summary>
        /// ロックオン リスト(アクティブになっており、ロックオンされた状態)
        /// </summary>
        public LinkedList<Enemy> LockOnList { get; set; }
        /// <summary>
        /// デストロイリスト(破壊中の状態)
        /// </summary>
        public LinkedList<Enemy> DestroyList { get; set; }
        /// <summary>
        /// リムーブ リスト(削除済みの状態)
        /// </summary>
        public LinkedList<Enemy> RemovedList { get; set; }

        #endregion

        /// <summary>
        /// コントラクター
        /// </summary>
        public EnemyManager()
        {
            // 各リストを作成
            ReservedList = new LinkedList<Enemy>();
            ActiveList = new LinkedList<Enemy>();
            LockOnList = new LinkedList<Enemy>();
            DestroyList = new LinkedList<Enemy>();
            RemovedList = new LinkedList<Enemy>();
        }

        public void Update(GameTime gameTime)
        {
            LinkedListNode<Enemy> node = this.ActiveList.First;
            while (node != null)
            {
                node.Value.Update(gameTime);
                node = node.Next;
            }

            node = this.LockOnList.First;
            while (node != null)
            {
                node.Value.Update(gameTime);
                node = node.Next;
            }

            node = this.DestroyList.First;
            while (node != null)
            {
                node.Value.Update(gameTime);
                node = node.Next;
            }
        }

        /// <summary>
        /// 予約リストに敵機情報を設定します。
        /// </summary>
        /// <param name="position">敵機の出現位置</param>
        /// <param name="velocity">敵機の速度</param>
        /// <param name="acceleration">敵機の加速度</param>
        /// <param name="model">敵機のモデル</param>
        /// <param name="hitpoint">敵機のHP</param>
        /// <param name="elementType">敵機の属性</param>
        public void EnemyAdd(Enemy enemy,
                             float positionX, float positionY, float positionZ,
                             Vector3 velocity,
                             float accelerationX, float accelerationY, float accelerationZ,
                             Model model, int hitpoint, ElementType elementType)
        {
            enemy.Acceleration.X = accelerationX;
            enemy.Acceleration.Y = accelerationY;
            enemy.Acceleration.Z = accelerationZ;
            enemy.Velocity = velocity;
            enemy.Position.X = positionX;
            enemy.Position.Y = positionY;
            enemy.Position.Z = positionZ;
            enemy.Alpha = 1.0f;
            enemy.HitPoint = hitpoint;
            enemy.SetElement(model, elementType);
            ReservedList.AddLast(enemy);
        }

        /// <summary>
        /// ロックオンリストから予約リストに移動させます。
        /// </summary>
        /// <param name="item"></param>
        public void AddReservedListFromLockOnList(LinkedListNode<Enemy> item)
        {
            LockOnList.Remove(item);
            ReservedList.AddLast(item);
        }
        /// <summary>
        /// アクティブリストから予約リストに移動させます。
        /// </summary>
        /// <param name="item"></param>
        public void AddReservedListFromActiveList(LinkedListNode<Enemy> item)
        {
            ActiveList.Remove(item);
            ReservedList.AddLast(item);
        }

        /// <summary>
        /// 敵機をアクティブにします。
        /// </summary>
        public void ActiveEnemy(LinkedListNode<Enemy> item)
        {
            ReservedList.Remove(item);
            ActiveList.AddLast(item);
        }

        /// <summary>
        /// 敵機をアクティブリストからロックオンリストに移動させます。
        /// </summary>
        /// <remarks>このメソッドを呼び出すとアクティブ リストが変更されます。</remarks>
        /// <param name="item"></param>
        public void AddLockOnList(LinkedListNode<Enemy> item)
        {
            ActiveList.Remove(item);
            LockOnList.AddLast(item);
        }

        /// <summary>
        /// 敵機をアクティブリストから削除済みリストに移動させます。
        /// </summary>
        /// <param name="item"></param>
        public void RemoveActiveList(LinkedListNode<Enemy> item)
        {
            ActiveList.Remove(item);
            RemovedList.AddLast(item);
        }

        /// <summary>
        /// 敵機をアクティブリストから破壊中リストに移動させています。
        /// </summary>
        /// <param name="item"></param>
        public void AddDestroyListByActiveList(LinkedListNode<Enemy> item)
        {
            ActiveList.Remove(item);
            DestroyList.AddLast(item);
        }

        /// <summary>
        /// 敵機のロックオンリストから破壊中リストに移動します。
        /// </summary>
        /// <param name="item"></param>
        public void AddDestroyList(LinkedListNode<Enemy> item)
        {
            LockOnList.Remove(item);
            DestroyList.AddLast(item);
        }

        /// <summary>
        /// 敵機の破壊中リストから削除済みリストに移動します。
        /// </summary>
        /// <param name="item"></param>
        public void RemoveDestroyList(LinkedListNode<Enemy> item)
        {
            DestroyList.Remove(item);
            RemovedList.AddLast(item);
        }

        /// <summary>
        /// 各リストをクリアします。
        /// </summary>
        public void Clear()
        {
            ReservedList.Clear();
            ActiveList.Clear();
            LockOnList.Clear();
            DestroyList.Clear();
            RemovedList.Clear();
        }
    }
}
