﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using UniverseColors.PlayGameScreens.Bullets;
using UniverseColors.Screens;

namespace UniverseColors.PlayGameScreens.PlayGameState
{
    class StageClearState : IScreenState<PlayGameScreen>
    {
        private static StageClearState _instance;
        /// <summary>
        /// StageClearStateの単一インスタンスを取得します。
        /// </summary>
        public static StageClearState Instance
        {
            get { return _instance ?? (_instance = new StageClearState()); }
        }

        // プライベートコンストラクター
        private StageClearState() { }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="screen"></param>
        public void Update(GameTime gameTime, PlayGameScreen screen)
        {
            // プレイヤーを更新します。
            screen.Player.Update(gameTime);
            // プレイヤーのオーラパーティクルを更新します。
            screen.AuraEmitter.AuraUpdate(gameTime, screen.GetScreenPosition(screen.Player));
            screen.AuraEmitterSystem.Update(gameTime);

            // アクティブな自機弾を更新します。
            LinkedListNode<Bullet> playerBullet = screen.Bullets.ActiveList.First, playerRemoveBullet = null;
            while (playerBullet != null)
            {
                // 自機弾が領域外に行ったらアクティブ リストから削除します。
                if (playerBullet.Value.Position.X < PlayGameScreen.playerBulletLeftLimit || playerBullet.Value.Position.X > PlayGameScreen.playerBulletRightLimit ||
                    playerBullet.Value.Position.Y < PlayGameScreen.playerBulletDownLimit || playerBullet.Value.Position.Y > PlayGameScreen.playerBulletUpLimit ||
                    playerBullet.Value.Position.Z < PlayGameScreen.playerBulletDeepLimit + screen.Player.Position.Z ||
                    playerBullet.Value.Position.Z > PlayGameScreen.playerBulletBeforeLimit + screen.Player.Position.Z)
                {
                    playerBullet.Value.Target = null;
                    playerRemoveBullet = playerBullet;
                    playerBullet = playerBullet.Next;
                    screen.Bullets.ActiveListRemove(playerRemoveBullet);
                    continue;
                }
                playerBullet = playerBullet.Next;
            }
            // 破壊中の弾を更新
            playerBullet = screen.Bullets.DestroyList.First;
            while (playerBullet != null)
            {
                if (playerBullet.Value.ShatterTime > TimeSpan.FromSeconds(PlayGameScreen.bulletShatterTimeLimit))
                {
                    playerBullet.Value.ShatterTime = TimeSpan.Zero;
                    playerRemoveBullet = playerBullet;
                    playerBullet = playerBullet.Next;
                    screen.Bullets.DestroyListRemove(playerRemoveBullet);
                    continue;
                }
                playerBullet = playerBullet.Next;
            }

            // フェードインフェードアウト
            screen.StageClear.Color.A = (byte)(Math.Sin(screen.StateTime.TotalSeconds) * 0xFF);
            if (screen.StateTime.TotalSeconds > Math.PI)
            {
                ++screen.stageNo;
                screen.StageClear.Color.A = 0;
                screen.StateTime = TimeSpan.Zero;

                if (screen.stageNo > PlayGameScreen.lastStageNo)
                {
                    if (gameTime.TotalGameTime > new TimeSpan(0, 0, 6))
                        LoadingScreen.Load(screen.ScreenManager, false, screen.ControllingPlayer, new GameClearScreen(screen.scoreNumber));
                }
                else
                {
                    screen.SetStartState();
                    screen.Reset();
                }
                // 音楽の再生中であれば、音楽を停止します。
                if (screen.BGM.IsPlaying)
                    screen.BGM.Stop(AudioStopOptions.Immediate);
            }
        }

        /// <summary>
        /// 入力処理
        /// </summary>
        /// <param name="input"></param>
        /// <param name="screen"></param>
        public void HandleInput(InputState input, PlayGameScreen screen)
        {
        }
    }
}
