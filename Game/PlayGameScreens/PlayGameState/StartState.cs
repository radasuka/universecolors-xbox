﻿using System;
using Microsoft.Xna.Framework;
using UniverseColors.PlayGameScreens.Players;
using UniverseColors.PlayGameScreens.Enemies;
using System.Collections.Generic;

namespace UniverseColors.PlayGameScreens.PlayGameState
{
    public class StartState : IScreenState<PlayGameScreen>
    {
        private static StartState _instance;
        /// <summary>
        /// StartStateの単一インスタンスを取得します。
        /// </summary>
        public static StartState Instance
        {
            get { return _instance ?? (_instance = new StartState()); }
        }

        // プライベートコンストラクター
        private StartState() { }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="screen"></param>
        public void Update(GameTime gameTime, PlayGameScreen screen)
        {
            // プレイヤーのオーラパーティクルを更新します。
            screen.AuraEmitter.AuraUpdate(gameTime, screen.GetScreenPosition(screen.Player));
            screen.AuraEmitterSystem.Update(gameTime);
            switch (screen.stageNo)
            {
                default:
                    // 自機の登場演出
                    if (screen.Player.Position.Y > 0)
                        screen.Player.Pitch -= 0.02f;
                    else
                        screen.Player.Pitch = 0.4f;

                    if (screen.Player.Position.Y < 0)
                        screen.Player.Velocity.Y = 2700.0f * (float)screen.StateTime.TotalSeconds;
                    screen.Player.Acceleration = (Vector3.Normalize(screen.BackGrounds[0].Position - screen.Player.Position) * 2500.0f) * (float)screen.StateTime.TotalSeconds;
                    screen.Player.Update(gameTime);

                    // フェードイン・フェードアウト
                    screen.StageNumberLogo[screen.stageNo].Color.A = (byte)(Math.Sin(screen.StateTime.TotalSeconds) * 0xFF);
                    if (screen.StateTime.TotalSeconds > Math.PI)
                    {
                        screen.Player.Pitch = 0;
                        screen.Player.Acceleration = new Vector3(0, 0, -2500);
                        screen.Player.Velocity = Vector3.Zero;
                        screen.Player.Position = new Vector3(0, Player.MinimumAltitude, 0);

                        screen.StageNumberLogo[screen.stageNo].Color.A = 0;
                        screen.SetPlayingState();
                    }
                    break;

                case PlayGameScreen.lastStageNo:
                    // 自機の登場演出
                    if (screen.Player.Position.Y > 0)
                        screen.Player.Pitch -= 0.02f;
                    else
                        screen.Player.Pitch = 0.4f;

                    if (screen.Player.Position.Y < 0)
                        screen.Player.Velocity.Y = 3000.0f;
                    screen.Player.Acceleration = Vector3.Normalize(screen.BackGrounds[0].Position - screen.Player.Position) * 3000;
                    screen.Player.Update(gameTime);

                    // ボス登場演出
                    if (screen.Boss.Value.Position.Y <= 5500)
                    {
                        screen.Boss.Value.Acceleration.Y = 0;
                        screen.Boss.Value.Velocity = Vector3.Zero;
                    }
                    else
                        screen.Boss.Value.Acceleration.Y = Vector3.Normalize(screen.BackGrounds[0].Position - screen.Boss.Value.Position).Y * 14000;
                    screen.Boss.Value.Update(gameTime);

                    // フェードイン・フェードアウト
                    screen.StageNumberLogo[screen.stageNo].Color.A = (byte)(Math.Sin(screen.StateTime.TotalSeconds) * 0xFF);

                    if (screen.StateTime.TotalSeconds > Math.PI)
                    {
                        screen.Player.Pitch = 0;
                        screen.Player.Acceleration = new Vector3(0, 0, -2500);
                        screen.Player.Velocity = Vector3.Zero;
                        screen.Player.Position = new Vector3(0, Player.MinimumAltitude, 0);

                        screen.Boss.Value.Velocity = Vector3.Zero;
                        screen.Boss.Value.Acceleration = new Vector3(0, 0, -2500);
                        screen.Boss.Value.Position = new Vector3(0, 5500, -50000.0f);

                        screen.StageNumberLogo[screen.stageNo].Color.A = 0;
                        screen.SetBossPlayingState();
                    }
                    break;
            }
        }

        /// <summary>
        /// 入力処理
        /// </summary>
        /// <param name="input"></param>
        /// <param name="screen"></param>
        public void HandleInput(InputState input, PlayGameScreen screen) {}
    }
}
