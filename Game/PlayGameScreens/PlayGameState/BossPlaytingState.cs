﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using UniverseColors.PlayGameScreens.Bullets;
using UniverseColors.PlayGameScreens.Enemies;
using GameDebugTools;

namespace UniverseColors.PlayGameScreens.PlayGameState
{
    public class BossPlaytingState : IScreenState<PlayGameScreen>
    {
        private static BossPlaytingState _instance;
        /// <summary>
        /// BossPlaytingStateの単一インスタンスを取得します。
        /// </summary>
        public static BossPlaytingState Instance
        {
            get { return _instance ?? (_instance = new BossPlaytingState()); }
        }

        // プライベートコンストラクター
        private BossPlaytingState() { }

        #region 更新処理

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="screen"></param>
        public void Update(GameTime gameTime, PlayGameScreen screen)
        {
#if DEBUG
            DebugSystem.Instance.TimeRuler.BeginMark("BossPlayingState", Color.AliceBlue);
#endif
            // プレイヤーを更新します。
            screen.Player.Update(gameTime);
            // プレイヤーの領域外判定をします。
            screen.PlayStageIsPlayerOutOfBounds();
            // プレイヤーのオーラパーティクルを更新します。
            screen.AuraEmitter.AuraUpdate(gameTime, screen.GetScreenPosition(screen.Player));
            screen.AuraEmitterSystem.Update(gameTime);

            #region 敵機関連

            // アクティブな敵機を更新します。
            LinkedListNode<Enemy> enemy = screen.Enemies.ActiveList.First, removedEnemy = null;
            while (enemy != null)
            {
                if ((screen.Player.Position.Z - enemy.Value.Position.Z) <= PlayGameScreen.lockOnIsEnemy && screen.LockOns.ReservedList.Count > 0)
                {
                    // 敵機と自機との相性を判定します。
                    LockOn.CompatibilityType compatibilityType;
                    if (enemy.Value.ElementType == screen.Player.ElementType)
                        compatibilityType = LockOn.CompatibilityType.Strong;
                    else if (enemy.Value.ElementType == (ElementType)(((int)screen.Player.ElementType + 1) % 3))
                        compatibilityType = LockOn.CompatibilityType.Even;
                    else
                        compatibilityType = LockOn.CompatibilityType.Weak;

                    // 敵機をロックオンします。
                    screen.LockOns.LockOnEnemy(enemy.Value, screen.GetScreenPosition(enemy.Value), compatibilityType);
                    removedEnemy = enemy;
                    enemy = enemy.Next;
                    screen.Enemies.AddLockOnList(removedEnemy);
                    continue;
                }
                // 敵機が領域外に行ったらアクティブ リストから削除します。
                // 敵機のスクリーン座標を取得します。
                Vector2 screenPositionIsEnemy = screen.GetScreenPosition(enemy.Value);
                if (screenPositionIsEnemy.X < 0 || screenPositionIsEnemy.X > screen.ScreenManager.GraphicsDevice.Viewport.Width ||
                    screenPositionIsEnemy.Y < 0 || screenPositionIsEnemy.Y > screen.ScreenManager.GraphicsDevice.Viewport.Height ||
                    screen.playerCamera.Position.Z <= enemy.Value.Position.Z)
                {
                    removedEnemy = enemy;
                    enemy = enemy.Next;
                    if (screen.Boss.Value.HitPoint > 0)
                    {
                        removedEnemy.Value.Position = screen.Boss.Value.Position;
                        removedEnemy.Value.Velocity = Vector3.Zero;
                        removedEnemy.Value.Acceleration = Vector3.Zero;

                        screen.Enemies.AddReservedListFromActiveList(removedEnemy);
                    }
                    else
                        screen.Enemies.RemoveActiveList(removedEnemy);
                    continue;
                }
                // 敵機と自機との衝突処理。
                Vector3 distance = screen.Player.Position - enemy.Value.Position;
                if (distance.Length() <= 1000.0f)
                {
                    // TODO:無敵
                    if (screen.Player.MotionState.GetType() != typeof(Players.DamageState))
                    {
                        --screen.Player.HitPoint;
                        screen.PlayerDamage = screen.SoundBank.GetCue("pl_crush_01");
                        screen.PlayerDamage.Play();

                        // プレイヤーのステートを変更します。
                        screen.Player.SetDamageState(screen);
                        screen.Player.StateTime = TimeSpan.Zero;
                    }

                    removedEnemy = enemy;
                    removedEnemy.Value.SetDestroyState(screen);
                    enemy = enemy.Next;
                    screen.Enemies.AddDestroyList(removedEnemy);
                    continue;
                }
                if (enemy.Value.ElementType != ElementType.Boss)
                    enemy.Value.Update(gameTime);
                enemy = enemy.Next;
            }

            // ロックオンした敵機を更新します。
            enemy = screen.Enemies.LockOnList.First;
            removedEnemy = null;
            LinkedListNode<LockOn> activeLockOn = screen.LockOns.ActiveList.First, removeActiveLockOn = null;
            while (enemy != null)
            {
                if (enemy.Value.ElementType != ElementType.Boss)
                    enemy.Value.Update(gameTime);
                // 敵機が領域外に行ったらロックオン リストから削除します。
                // 敵のスクリーン座標を取得
                Vector2 screenPositionIsEnemy = screen.GetScreenPosition(enemy.Value);
                if (screenPositionIsEnemy.X < -100 || screenPositionIsEnemy.X > screen.ScreenManager.GraphicsDevice.Viewport.Width + 100 ||
                    screenPositionIsEnemy.Y < -100 || screenPositionIsEnemy.Y > screen.ScreenManager.GraphicsDevice.Viewport.Height + 100 ||
                    screen.playerCamera.Position.Z <= enemy.Value.Position.Z)
                {
                    removedEnemy = enemy;
                    enemy = enemy.Next;
                    if (screen.Boss.Value.HitPoint > 0)
                    {
                        removedEnemy.Value.Position = screen.Boss.Value.Position;
                        //removedEnemy.Value.Position.Y -= 5000.0f;
                        //removedEnemy.Value.Velocity = new Vector3(-1000, 3000, 0);
                        removedEnemy.Value.Velocity = Vector3.Zero;
                        removedEnemy.Value.Acceleration = Vector3.Zero;

                        screen.Enemies.AddReservedListFromLockOnList(removedEnemy);
                    }
                    else
                    {
                        removedEnemy.Value.SetDestroyState(screen);
                        screen.Enemies.AddDestroyList(removedEnemy);
                    }

                    removeActiveLockOn = activeLockOn;
                    activeLockOn = activeLockOn.Next;
                    screen.LockOns.ActiveListRemove(removeActiveLockOn);
                    screen.LockOns.ReservedListAdd(removeActiveLockOn);
                    continue;
                }
                // 敵機と自機との衝突処理。
                if (screen.CollisionDetectByEnemies(enemy, screen.Player))
                {
                    if (screen.Player.MotionState.GetType() != typeof(Players.DamageState))
                    {
                        --screen.Player.HitPoint;
                        screen.PlayerDamage = screen.SoundBank.GetCue("pl_crush_01");
                        screen.PlayerDamage.Play();

                        // プレイヤーのステートを変更します。
                        screen.Player.SetDamageState(screen);
                        screen.Player.StateTime = TimeSpan.Zero;
                    }

                    removedEnemy = enemy;
                    enemy = enemy.Next;
                    // ボスのHPが0以上なら予約リストに戻す。
                    if (screen.Boss.Value.HitPoint > 0)
                    {
                        removedEnemy.Value.Position = screen.Boss.Value.Position;
                        removedEnemy.Value.Velocity = Vector3.Zero;
                        removedEnemy.Value.Acceleration = Vector3.Zero;

                        screen.Enemies.AddReservedListFromLockOnList(removedEnemy);
                    }
                    else
                    {
                        removedEnemy.Value.SetDestroyState(screen);
                        screen.Enemies.AddDestroyList(removedEnemy);
                    }

                    removeActiveLockOn = activeLockOn;
                    activeLockOn = activeLockOn.Next;
                    screen.LockOns.ActiveListRemove(removeActiveLockOn);
                    screen.LockOns.ReservedListAdd(removeActiveLockOn);
                    continue;
                }
                // 敵機との相性を更新します。
                LockOn.CompatibilityType compatibilityType;
                if (enemy.Value.ElementType == screen.Player.ElementType)
                    compatibilityType = LockOn.CompatibilityType.Strong;
                else if (enemy.Value.ElementType == (ElementType)(((int)screen.Player.ElementType + 1) % 3))
                    compatibilityType = LockOn.CompatibilityType.Even;
                else
                    compatibilityType = LockOn.CompatibilityType.Weak;

                activeLockOn.Value.Compatibility = compatibilityType;

                // ロックオンマーカーの位置を更新します。
                activeLockOn.Value.Position = screen.GetScreenPosition(enemy.Value);
                activeLockOn = activeLockOn.Next;

                enemy = enemy.Next;
            }
            // 破壊中の敵機を更新します。
            LinkedListNode<Enemy> removeisActiveEnemy = null;
            enemy = screen.Enemies.DestroyList.First;
            while (enemy != null)
            {
                if (enemy.Value.ShatterTime > TimeSpan.FromSeconds(PlayGameScreen.enemyShatterTimeLimit))
                {
                    enemy.Value.ShatterTime = TimeSpan.Zero;
                    removeisActiveEnemy = enemy;
                    enemy = enemy.Next;
                    screen.Enemies.RemoveDestroyList(removeisActiveEnemy);
                    continue;
                }
                enemy.Value.Update(gameTime);
                enemy = enemy.Next;
            }
            // ボスを更新します。
            if (screen.Boss.Value != null)
                screen.Boss.Value.Update(gameTime);

            #endregion

            #region 弾関連

            // アクティブな自機弾を更新します。
            LinkedListNode<Bullet> playerBullet = screen.Bullets.ActiveList.First, playerRemoveBullet = null;
            while (playerBullet != null)
            {
                // 敵機の被弾処理をします。
                LinkedListNode<Enemy> removeHitEnemy = null;
                if ((enemy = screen.CollisionDetectByBullets(playerBullet.Value)) != null)
                {
                    // 敵機と自機との相性にしたがって敵機にダメージを与えます。
                    if (playerBullet.Value.ElementType == enemy.Value.ElementType)
                        enemy.Value.HitPoint -= 6;
                    else if (playerBullet.Value.ElementType == (ElementType)(((int)enemy.Value.ElementType + 1) % 3))
                        enemy.Value.HitPoint -= 3;
                    else
                        enemy.Value.HitPoint -= 2;

                    // 自機弾のステートを破壊中に設定します。
                    playerBullet.Value.SetDestroyState(screen);

                    if (enemy.Value.ElementType == ElementType.Boss)
                    {
                        enemy.Value.SetDamageState(screen);
                    }

                    // 敵機のHPが0以下なら敵機を破壊します。
                    if (enemy.Value.HitPoint <= 0)
                    {
                        // ロックオンマーカーの削除
                        LinkedListNode<LockOn> removeHitLockOn = null;
                        activeLockOn = screen.LockOns.ActiveList.First;
                        while (activeLockOn != null)
                        {
                            if (activeLockOn.Value.Target == enemy.Value)
                            {
                                removeHitLockOn = activeLockOn;
                                activeLockOn = activeLockOn.Next;
                                screen.LockOns.ActiveListRemove(removeHitLockOn);
                                screen.LockOns.ReservedListAdd(removeHitLockOn);
                                continue;
                            }
                            activeLockOn = activeLockOn.Next;
                        }

                        // スコアを加算
                        if (playerBullet.Value.ElementType == enemy.Value.ElementType)
                            screen.scoreNumber.CurrentScore += 300;
                        else if (playerBullet.Value.ElementType == (ElementType)(((int)enemy.Value.ElementType + 1) % 3))
                            screen.scoreNumber.CurrentScore += 200;
                        else
                            screen.scoreNumber.CurrentScore += 100;

                        // 敵機撃破時の演出
                        screen.EnemyDestory = screen.SoundBank.GetCue("en_crush_02");
                        screen.EnemyDestory.Play();

                        if (enemy.Value.ElementType == ElementType.Boss)
                        {
                            screen.Explosion.AddParticles(screen.GetScreenPosition(enemy.Value), Vector2.Zero);
                            screen.Smoke.AddParticles(screen.GetScreenPosition(enemy.Value), Vector2.Zero);
                        }
                        else
                        {
                            screen.Explosion.AddParticles(screen.GetScreenPosition(enemy.Value), Vector2.Zero);
                            screen.Smoke.AddParticles(screen.GetScreenPosition(enemy.Value), Vector2.Zero);
                        }

                        // 敵機リストからの削除
                        removeHitEnemy = enemy;
                        enemy = enemy.Next;
                        if (screen.Boss.Value.HitPoint > 0)
                        {
                            removeHitEnemy.Value.Position = screen.Boss.Value.Position;
                            removeHitEnemy.Value.Velocity = Vector3.Zero;
                            removeHitEnemy.Value.Acceleration = Vector3.Zero;
                            screen.Enemies.AddReservedListFromLockOnList(removeHitEnemy);
                        }
                        else
                        {
                            removeHitEnemy.Value.SetDestroyState(screen);
                            screen.Enemies.AddDestroyList(removeHitEnemy);
                        }
                    }

                    // 自機弾リストからの削除
                    playerBullet.Value.Target = null;
                    playerRemoveBullet = playerBullet;
                    playerBullet = playerBullet.Next;
                    screen.Bullets.ActiveListRemove(playerRemoveBullet);
                    continue;
                }

                // 自機弾が領域外に行ったらアクティブ リストから削除します。
                if (playerBullet.Value.Position.X < PlayGameScreen.playerBulletLeftLimit || playerBullet.Value.Position.X > PlayGameScreen.playerBulletRightLimit ||
                    playerBullet.Value.Position.Y < PlayGameScreen.playerBulletDownLimit || playerBullet.Value.Position.Y > PlayGameScreen.playerBulletUpLimit ||
                    playerBullet.Value.Position.Z < PlayGameScreen.playerBulletDeepLimit + screen.Player.Position.Z ||
                    playerBullet.Value.Position.Z > PlayGameScreen.playerBulletBeforeLimit + screen.Player.Position.Z)
                {
                    playerRemoveBullet = playerBullet;
                    playerBullet = playerBullet.Next;
                    screen.Bullets.ActiveListRemove(playerRemoveBullet);
                    continue;
                }
                // bulletを使った処理
                playerBullet = playerBullet.Next;
            }
            // 破壊中弾の更新
            playerBullet = screen.Bullets.DestroyList.First;
            playerRemoveBullet = null;
            while (playerBullet != null)
            {
                if (playerBullet.Value.ShatterTime > TimeSpan.FromSeconds(PlayGameScreen.bulletShatterTimeLimit))
                {
                    playerBullet.Value.ShatterTime = TimeSpan.Zero;
                    playerRemoveBullet = playerBullet;
                    playerBullet = playerBullet.Next;
                    screen.Bullets.DestroyListRemove(playerRemoveBullet);
                    continue;
                }
                playerBullet = playerBullet.Next;
            }

            #endregion

            // 勝敗判定
            // プレイヤーのライフが0以下になったときゲームオーバー
            if (screen.Player.HitPoint <= 0)
            {
                // 音楽の再生中であれば、音楽を停止します。
                if (screen.BGM.IsPlaying)
                    screen.BGM.Stop(AudioStopOptions.Immediate);
                screen.BGM = screen.SoundBank.GetCue("over_01");
                screen.BGM.Play();

                screen.SetGameOverState();
            }
            // ボスのHPが0以下ならゲームクリア
            else if (screen.Boss.Value.HitPoint <= 0 && screen.Enemies.DestroyList.Count <= 0)
            {
                // 音楽の再生中であれば、音楽を停止します。
                if (screen.BGM.IsPlaying)
                    screen.BGM.Stop(AudioStopOptions.Immediate);
                screen.BGM = screen.SoundBank.GetCue("stageclear");
                screen.BGM.Play();

                screen.SetStageClearState();
            }

            // 新しいターゲットを追尾するようにカメラを更新します。
            screen.UpdateCameraChaseTarget();

            // スプリングが有効になっているかを判定し、カメラの追尾方法を選択します。
            // スプリングが無効になっている場合、Reset メソッドでカメラの位置が
            // 常にオブジェクトから一定の間隔に固定されます。
            if (screen.cameraSpringEnabled)
            {
                screen.camera.Update(gameTime);
                screen.playerCamera.Update(gameTime);
            }
            else
            {
                screen.camera.Reset();
                screen.playerCamera.Reset();
            }
#if DEBUG
            DebugSystem.Instance.TimeRuler.EndMark("BossPlayingState");
#endif
        }

        #endregion

        /// <summary>
        /// 入力処理
        /// </summary>
        /// <param name="input"></param>
        /// <param name="screen"></param>
        public void HandleInput(InputState input, PlayGameScreen screen)
        {
            PlayerIndex playerIndex;
            // アクティブなプロフィールの入力を取得します。
            PlayerIndex controllingPlayer = screen.ControllingPlayer.Value;
            KeyboardState keyboardState = input.CurrentKeyboardStates[(int)controllingPlayer];
            GamePadState gamePadState = input.CurrentGamePadStates[(int)controllingPlayer];


            // Xボタンを押すと自機の色が変わります。(赤→青→緑)
            if (input.IsNewButtonPress(Buttons.X, controllingPlayer, out playerIndex) ||
                input.IsNewKeyPress(Keys.X, controllingPlayer, out playerIndex))
            {
                if (screen.Player.ElementType == ElementType.Red)
                {
                    screen.Player.SetBlueState(screen);
                    screen.Cylinder.SetChangeState(screen);
                    for (int index = 0; index < screen.BackGrounds.Count; ++index)
                        screen.BackGrounds[index].Velocity.Y = 70000.0f;
                    screen.Cylinder.Velocity.Y = 70000.0f;
                }
                else if (screen.Player.ElementType == ElementType.Blue)
                {
                    screen.Player.SetGreenState(screen);
                    screen.Cylinder.SetChangeState(screen);
                    for (int index = 0; index < screen.BackGrounds.Count; ++index)
                        screen.BackGrounds[index].Velocity.Y = 70000.0f;
                    screen.Cylinder.Velocity.Y = 70000.0f;
                }
                else if (screen.Player.ElementType == ElementType.Green)
                {
                    screen.Player.SetRedState(screen);
                    screen.Cylinder.SetChangeState(screen);
                    for (int index = 0; index < screen.BackGrounds.Count; ++index)
                        screen.BackGrounds[index].Velocity.Y = -140000.0f;
                    screen.Cylinder.Velocity.Y = -140000.0f;
                }
            }

            // 右トリガー(Xbox)、Zキー(Windows)を押すと自機が弾を発射します。
            if (input.IsNewButtonPress(Buttons.RightTrigger, controllingPlayer, out playerIndex) ||
                input.IsNewKeyPress(Keys.Z, controllingPlayer, out playerIndex) &&
                screen.rapidFireTime > TimeSpan.FromSeconds(2f) /*&&
                Player.State == StandState*/)
            {
                // 自機弾発射音
                screen.BulletSound = screen.SoundBank.GetCue("bullet_01");
                screen.BulletSound.Play();
                // プレイヤーのステートを変更します。
                screen.Player.SetAttackState(screen);
                screen.Player.StateTime = TimeSpan.Zero;

                var lockOnEnemy = screen.Enemies.LockOnList.First;
                if (lockOnEnemy != null)
                {
                    screen.rapidFireTime = TimeSpan.Zero;
                    screen.Bullets.Shoot(screen.Player.Position,
                                         screen.Player.Velocity,
                                         screen.BulletShapes[(int)screen.Player.ElementType],
                                         screen.Player.ElementType,
                                         screen.LockOns.ActiveList,
                                         screen);
                }
                else
                {
                    screen.rapidFireTime = TimeSpan.Zero;
                    screen.Bullets.Shoot(screen.Player.Position,
                                         screen.Player.Velocity,
                                         screen.BulletShapes[(int)screen.Player.ElementType],
                                         screen.Player.ElementType,
                                         null,
                                         screen);
                }
            }
        }
    }
}