﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using UniverseColors.PlayGameScreens.Bullets;
using UniverseColors.Screens;
using UniverseColors.PlayGameScreens.Enemies;

namespace UniverseColors.PlayGameScreens.PlayGameState
{
    public class GameOverState : IScreenState<PlayGameScreen>
    {
        private static GameOverState _instance;
        /// <summary>
        /// GameOverStateの単一インスタンスを取得します。
        /// </summary>
        public static GameOverState Instance
        {
            get { return _instance ?? (_instance = new GameOverState()); }
        }

        // プライベートコンストラクター
        private GameOverState() { }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="screen"></param>
        public void Update(GameTime gameTime, PlayGameScreen screen)
        {
            // 破壊中の敵機を更新します。
            LinkedListNode<Enemy> removeisActiveEnemy = null;
            LinkedListNode<Enemy> enemy = screen.Enemies.DestroyList.First;
            while (enemy != null)
            {
                if (enemy.Value.ShatterTime > TimeSpan.FromSeconds(PlayGameScreen.enemyShatterTimeLimit))
                {
                    enemy.Value.ShatterTime = TimeSpan.Zero;
                    removeisActiveEnemy = enemy;
                    enemy = enemy.Next;
                    screen.Enemies.RemoveDestroyList(removeisActiveEnemy);
                    continue;
                }
                enemy.Value.Update(gameTime);
                enemy = enemy.Next;
            }

            // アクティブな自機弾を更新します。
            LinkedListNode<Bullet> playerBullet = screen.Bullets.ActiveList.First, playerRemoveBullet = null;
            while (playerBullet != null)
            {
                // 自機弾が領域外に行ったらアクティブ リストから削除します。
                if (playerBullet.Value.Position.X < PlayGameScreen.playerBulletLeftLimit || playerBullet.Value.Position.X > PlayGameScreen.playerBulletRightLimit ||
                    playerBullet.Value.Position.Y < PlayGameScreen.playerBulletDownLimit || playerBullet.Value.Position.Y > PlayGameScreen.playerBulletUpLimit ||
                    playerBullet.Value.Position.Z < PlayGameScreen.playerBulletDeepLimit + screen.Player.Position.Z ||
                    playerBullet.Value.Position.Z > PlayGameScreen.playerBulletBeforeLimit + screen.Player.Position.Z)
                {
                    playerBullet.Value.Target = null;
                    playerRemoveBullet = playerBullet;
                    playerBullet = playerBullet.Next;
                    screen.Bullets.ActiveListRemove(playerRemoveBullet); ;
                    continue;
                }
                playerBullet = playerBullet.Next;
            }
            // 破壊中弾の更新
            playerBullet = screen.Bullets.DestroyList.First;
            playerRemoveBullet = null;
            while (playerBullet != null)
            {
                if (playerBullet.Value.ShatterTime > TimeSpan.FromSeconds(PlayGameScreen.bulletShatterTimeLimit))
                {
                    playerBullet.Value.ShatterTime = TimeSpan.Zero;
                    playerRemoveBullet = playerBullet;
                    playerBullet = playerBullet.Next;
                    screen.Bullets.DestroyListRemove(playerRemoveBullet);
                    continue;
                }
                playerBullet = playerBullet.Next;
            }

            // フェードイン・フェードアウト
            if (screen.GameOver.Color.A < 255 && screen.ContinueText.Color.A < 255 &&
                screen.YesTextLight.Color.A < 255 && screen.NoText.Color.A < 255)
            {
                screen.GameOver.Color.A = (byte)(Math.Sin(screen.StateTime.TotalSeconds) * 0xFF);
                screen.ContinueText.Color.A = (byte)(Math.Sin(screen.StateTime.TotalSeconds) * 0xFF);
                screen.YesTextLight.Color.A = (byte)(Math.Sin(screen.StateTime.TotalSeconds) * 0xFF);
                screen.NoText.Color.A = (byte)(Math.Sin(screen.StateTime.TotalSeconds) * 0xFF);
            }
            if (screen.StateTime.TotalSeconds > 1.6)
            {
                screen.GameOver.Color.A = 255;
                screen.ContinueText.Color.A = 255;
                screen.YesText.Color.A = 255;
                screen.YesTextLight.Color.A = 255;
                screen.NoText.Color.A = 255;
                screen.NoTextLight.Color.A = 255;
            }
        }

        /// <summary>
        /// 入力処理
        /// </summary>
        /// <param name="input"></param>
        /// <param name="screen"></param>
        public void HandleInput(InputState input, PlayGameScreen screen)
        {
            PlayerIndex playerIndex;
            // アクティブなプロフィールの入力を取得します。
            PlayerIndex controllingPlayer = screen.ControllingPlayer.Value;
            KeyboardState keyboardState = input.CurrentKeyboardStates[(int)controllingPlayer];
            GamePadState gamePadState = input.CurrentGamePadStates[(int)controllingPlayer];

            // 左右キーで選択します。
            if (input.IsNewKeyPress(Keys.Left, null, out playerIndex) ||
                input.IsNewButtonPress(Buttons.LeftThumbstickLeft, null, out playerIndex))
            {
                if (screen.continueSelect)
                    screen.continueSelect = false;
                else
                    screen.continueSelect = true;
            }
            else if (input.IsNewKeyPress(Keys.Right, null, out playerIndex) ||
                input.IsNewButtonPress(Buttons.LeftThumbstickRight, null, out playerIndex))
            {
                if (!screen.continueSelect)
                    screen.continueSelect = true;
                else
                    screen.continueSelect = false;
            }
            // Enterキーで決定します。
            if (input.IsNewKeyPress(Keys.Enter, null, out playerIndex) ||
                input.IsNewButtonPress(Buttons.A, null, out playerIndex))
            {
                screen.GameOver.Color.A = 0;
                screen.ContinueText.Color.A = 0;
                screen.YesText.Color.A = 0;
                screen.YesTextLight.Color.A = 0;
                screen.NoText.Color.A = 0;
                screen.NoTextLight.Color.A = 0;

                if (screen.continueSelect)
                {
                    // 決定音再生
                    screen.Enter = screen.SoundBank.GetCue("enter_01");
                    screen.Enter.Play();
                    screen.BGM.Stop(AudioStopOptions.Immediate);

                    screen.scoreNumber.CurrentScore = 0;
                    screen.Reset();
                    screen.SetStartState();
                }
                else
                {
                    // 決定音再生
                    screen.Enter = screen.SoundBank.GetCue("enter_01");
                    screen.Enter.Play();
                    screen.BGM.Stop(AudioStopOptions.Immediate);

                    screen.ScreenManager.RemoveScreen(screen);
                    LoadingScreen.Load(screen.ScreenManager, false, screen.ControllingPlayer, new BackgroundScreen(), new MainMenuScreen());
                }
            }
        }
    }
}
