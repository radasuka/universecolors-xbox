﻿using System;
using System.Collections.Generic;
using GameDebugTools;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using UniverseColors.PlayGameScreens.Bullets;
using UniverseColors.PlayGameScreens.Enemies;

namespace UniverseColors.PlayGameScreens.PlayGameState
{
    public class PlayingState : IScreenState<PlayGameScreen>
    {
        private static PlayingState _instance;
        /// <summary>
        /// PlayingStateの単一インスタンスを取得します。
        /// </summary>
        public static PlayingState Instance
        {
            get { return _instance ?? (_instance = new PlayingState()); }
        }

        // プライベートコンストラクター
        private PlayingState() { }

        #region 更新処理

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gamTime"></param>
        /// <param name="screen"></param>
        public void Update(GameTime gameTime, PlayGameScreen screen)
        {
#if DEBUG
            DebugSystem.Instance.TimeRuler.BeginMark("PlayGameScreen.Update", Color.CadetBlue);
#endif
            // プレイヤーを更新します。
            screen.Player.Update(gameTime);
            // プレイヤーの領域外判定をします。
            screen.PlayStageIsPlayerOutOfBounds();
            // プレイヤーのオーラパーティクルを更新します。
            screen.AuraEmitterSystem.Update(gameTime);
            screen.AuraEmitter.AuraUpdate(gameTime, screen.GetScreenPosition(screen.Player));

            #region 敵機関連

            // 敵機が自機に近づいたらアクティブにします。
            LinkedListNode<Enemy> reservedEnemy = screen.Enemies.ReservedList.First, activeEnemy = null;
            while (reservedEnemy != null)
            {
                if ((screen.Player.Position.Z - reservedEnemy.Value.Position.Z) <= PlayGameScreen.enemyActiveBorderline)
                {
                    activeEnemy = reservedEnemy;
                    reservedEnemy = reservedEnemy.Next;
                    screen.Enemies.ActiveEnemy(activeEnemy);
                    continue;
                }
                reservedEnemy = reservedEnemy.Next;
            }

            // アクティブな敵機を更新します。
            LinkedListNode<Enemy> enemy = screen.Enemies.ActiveList.First, removedEnemy = null;
            while (enemy != null)
            {
                enemy.Value.Update(gameTime);

                // 敵機がカメラより後ろに行ったらアクティブ リストから削除します。
                if (screen.playerCamera.Position.Z <= enemy.Value.Position.Z)
                {
                    removedEnemy = enemy;
                    enemy = enemy.Next;
                    screen.Enemies.RemoveActiveList(removedEnemy);
                    continue;
                }
                // 敵機がロックオン出来る距離になったらロックオンします。
                if ((screen.Player.Position.Z - enemy.Value.Position.Z) <= PlayGameScreen.lockOnIsEnemy &&
                   screen.LockOns.ReservedList.Count > 0)
                {
                    // 敵機と自機との相性を判定します。
                    LockOn.CompatibilityType compatibilityType;
                    if (enemy.Value.ElementType == screen.Player.ElementType)
                        compatibilityType = LockOn.CompatibilityType.Strong;
                    else if (enemy.Value.ElementType == (ElementType)(((int)screen.Player.ElementType + 1) % 3))
                        compatibilityType = LockOn.CompatibilityType.Even;
                    else
                        compatibilityType = LockOn.CompatibilityType.Weak;

                    // 敵機をロックオンします。
                    screen.LockOns.LockOnEnemy(enemy.Value, screen.GetScreenPosition(enemy.Value), compatibilityType);
                    removedEnemy = enemy;
                    enemy = enemy.Next;
                    screen.Enemies.AddLockOnList(removedEnemy);
                    continue;
                }
                // 敵機と自機との衝突処理。
                if (screen.CollisionDetectByEnemies(enemy, screen.Player))
                {
                    if (screen.Player.MotionState.GetType() != typeof(Players.DamageState))
                    {
                        --screen.Player.HitPoint;
                        screen.PlayerDamage = screen.SoundBank.GetCue("pl_crush_01");
                        screen.PlayerDamage.Play();
                        // プレイヤーのステートを変更します。
                        screen.Player.SetDamageState(screen);
                        screen.Player.StateTime = TimeSpan.Zero;
                    }

                    removedEnemy = enemy;
                    enemy = enemy.Next;
                    removedEnemy.Value.SetDestroyState(screen);
                    screen.Enemies.AddDestroyListByActiveList(removedEnemy);
                    continue;
                }
                enemy = enemy.Next;
            }

            // ロックオンした敵機を更新します。
            enemy = screen.Enemies.LockOnList.First;
            removedEnemy = null;
            LinkedListNode<LockOn> activeLockOn = screen.LockOns.ActiveList.First, removeActiveLockOn = null;
            while (enemy != null)
            {
                // 敵機がカメラ外に出たらアクティブ リストから削除します。
                if (screen.playerCamera.Position.Z <= enemy.Value.Position.Z)
                {
                    removedEnemy = enemy;
                    enemy = enemy.Next;
                    removedEnemy.Value.SetDestroyState(screen);
                    screen.Enemies.AddDestroyList(removedEnemy);

                    removeActiveLockOn = activeLockOn;
                    activeLockOn = activeLockOn.Next;
                    screen.LockOns.ActiveListRemove(removeActiveLockOn);
                    screen.LockOns.ReservedListAdd(removeActiveLockOn);
                    continue;
                }
                // 敵機と自機との衝突処理。
                if (screen.CollisionDetectByEnemies(enemy, screen.Player))
                {
                    if (screen.Player.MotionState.GetType() != typeof(Players.DamageState))
                    {
                        --screen.Player.HitPoint;
                        screen.PlayerDamage = screen.SoundBank.GetCue("pl_crush_01");
                        screen.PlayerDamage.Play();
                        // プレイヤーのステートを変更します。
                        screen.Player.SetDamageState(screen);
                        screen.Player.StateTime = TimeSpan.Zero;
                    }

                    removedEnemy = enemy;
                    enemy = enemy.Next;
                    removedEnemy.Value.SetDestroyState(screen);
                    screen.Enemies.AddDestroyList(removedEnemy);

                    removeActiveLockOn = activeLockOn;
                    activeLockOn = activeLockOn.Next;
                    screen.LockOns.ActiveListRemove(removeActiveLockOn);
                    screen.LockOns.ReservedListAdd(removeActiveLockOn);
                    continue;
                }
                // 敵機との相性を更新します。
                LockOn.CompatibilityType compatibilityType;
                if (enemy.Value.ElementType == screen.Player.ElementType)
                    compatibilityType = LockOn.CompatibilityType.Strong;
                else if (enemy.Value.ElementType == (ElementType)(((int)screen.Player.ElementType + 1) % 3))
                    compatibilityType = LockOn.CompatibilityType.Even;
                else
                    compatibilityType = LockOn.CompatibilityType.Weak;

                activeLockOn.Value.Compatibility = compatibilityType;

                // ロックオンマーカーの位置を更新します。
                activeLockOn.Value.Position = screen.GetScreenPosition(enemy.Value);
                activeLockOn = activeLockOn.Next;

                enemy.Value.Update(gameTime);
                enemy = enemy.Next;
            }

            // 破壊中の敵機を更新します。
            LinkedListNode<Enemy> removeisActiveEnemy = null;
            enemy = screen.Enemies.DestroyList.First;
            while (enemy != null)
            {
                if (enemy.Value.ShatterTime > TimeSpan.FromSeconds(PlayGameScreen.enemyShatterTimeLimit))
                {
                    enemy.Value.ShatterTime = TimeSpan.Zero;
                    removeisActiveEnemy = enemy;
                    enemy = enemy.Next;
                    screen.Enemies.RemoveDestroyList(removeisActiveEnemy);
                    continue;
                }
                enemy.Value.Update(gameTime);
                enemy = enemy.Next;
            }

            #endregion

            #region 弾関連

            // アクティブな自機弾を更新します。
            LinkedListNode<Bullet> PlayerBullet = screen.Bullets.ActiveList.First, PlayerRemoveBullet = null;
            while (PlayerBullet != null)
            {
                // 敵機の被弾処理をします。
                LinkedListNode<Enemy> removeHitEnemy = null;
                if ((enemy = screen.CollisionDetectByBullets(PlayerBullet.Value)) != null)
                {
                    // 敵機と自機との相性にしたがって敵機にダメージを与えます。
                    if (PlayerBullet.Value.ElementType == enemy.Value.ElementType)
                        enemy.Value.HitPoint -= 6;
                    else if (PlayerBullet.Value.ElementType == (ElementType)(((int)enemy.Value.ElementType + 1) % 3))
                        enemy.Value.HitPoint -= 3;
                    else
                        enemy.Value.HitPoint -= 2;

                    // 自機弾のステートを破壊中に設定します。
                    PlayerBullet.Value.SetDestroyState(screen);

                    // 敵機のHPが0以下なら敵機を破壊します。
                    if (enemy.Value.HitPoint <= 0)
                    {
                        // ロックオンマーカーの削除
                        LinkedListNode<LockOn> removeHitLockOn = null;
                        activeLockOn = screen.LockOns.ActiveList.First;
                        while (activeLockOn != null)
                        {
                            if (activeLockOn.Value.Target == enemy.Value)
                            {
                                removeHitLockOn = activeLockOn;
                                activeLockOn = activeLockOn.Next;
                                screen.LockOns.ActiveListRemove(removeHitLockOn);
                                screen.LockOns.ReservedListAdd(removeHitLockOn);
                                continue;
                            }
                            activeLockOn = activeLockOn.Next;
                        }

                        // 敵機撃破時の演出
                        // 敵機との相性によって獲得するスコアを変更します。
                        if (PlayerBullet.Value.ElementType == enemy.Value.ElementType)
                            screen.scoreNumber.CurrentScore += 300;
                        else if (PlayerBullet.Value.ElementType == (ElementType)(((int)enemy.Value.ElementType + 1) % 3))
                            screen.scoreNumber.CurrentScore += 200;
                        else
                            screen.scoreNumber.CurrentScore += 100;

                        screen.EnemyDestory = screen.SoundBank.GetCue("en_crush_02");
                        screen.EnemyDestory.Play();
                        screen.Explosion.AddParticles(screen.GetScreenPosition(enemy.Value), Vector2.Zero);
                        screen.Smoke.AddParticles(screen.GetScreenPosition(enemy.Value), Vector2.Zero);

                        // 敵機リストからの削除
                        removeHitEnemy = enemy;
                        removeHitEnemy.Value.SetDestroyState(screen);
                        enemy = enemy.Next;
                        screen.Enemies.AddDestroyList(removeHitEnemy);
                    }

                    // 自機弾リストからの削除
                    PlayerBullet.Value.Target = null;
                    PlayerRemoveBullet = PlayerBullet;
                    PlayerBullet = PlayerBullet.Next;
                    screen.Bullets.ActiveListRemove(PlayerRemoveBullet);
                    continue;
                }

                // 自機弾が領域外に行ったらアクティブ リストから削除します。
                if (PlayerBullet.Value.Position.X < PlayGameScreen.playerBulletLeftLimit || PlayerBullet.Value.Position.X > PlayGameScreen.playerBulletRightLimit ||
                    PlayerBullet.Value.Position.Y < PlayGameScreen.playerBulletDownLimit || PlayerBullet.Value.Position.Y > PlayGameScreen.playerBulletUpLimit ||
                    PlayerBullet.Value.Position.Z < PlayGameScreen.playerBulletDeepLimit + screen.Player.Position.Z ||
                    PlayerBullet.Value.Position.Z > PlayGameScreen.playerBulletBeforeLimit + screen.Player.Position.Z)
                {
                    PlayerBullet.Value.Target = null;
                    PlayerRemoveBullet = PlayerBullet;
                    PlayerBullet = PlayerBullet.Next;
                    screen.Bullets.ActiveListRemove(PlayerRemoveBullet);
                    continue;
                }

                // bulletを使った処理
                PlayerBullet = PlayerBullet.Next;
            }

            // 破壊中弾の更新
            PlayerBullet = screen.Bullets.DestroyList.First;
            PlayerRemoveBullet = null;
            while (PlayerBullet != null)
            {
                if (PlayerBullet.Value.ShatterTime > TimeSpan.FromSeconds(PlayGameScreen.bulletShatterTimeLimit))
                {
                    PlayerBullet.Value.ShatterTime = TimeSpan.Zero;
                    PlayerRemoveBullet = PlayerBullet;
                    PlayerBullet = PlayerBullet.Next;
                    screen.Bullets.DestroyListRemove(PlayerRemoveBullet);
                    continue;
                }
                PlayerBullet = PlayerBullet.Next;
            }

            #endregion

            // 勝敗判定
            // プレイヤーのライフが0以下になったときゲームオーバー
            if (screen.Player.HitPoint <= 0)
            {
                // 音楽の再生中であれば、音楽を停止します。
                if (screen.BGM.IsPlaying)
                    screen.BGM.Stop(AudioStopOptions.Immediate);
                screen.BGM = screen.SoundBank.GetCue("over_01");
                screen.BGM.Play();

                screen.SetGameOverState();
            }
            // 敵機の配列数が0になったらステージクリア
            else if (screen.Enemies.ReservedList.Count == 0 &&
                     screen.Enemies.ActiveList.Count == 0 &&
                     screen.Enemies.LockOnList.Count == 0 &&
                     screen.Enemies.DestroyList.Count == 0)
            {
                // 音楽の再生中であれば、音楽を停止します。
                if (screen.BGM.IsPlaying)
                    screen.BGM.Stop(AudioStopOptions.Immediate);
                screen.BGM = screen.SoundBank.GetCue("stageclear");
                screen.BGM.Play();

                screen.SetStageClearState();
            }

            // 新しいターゲットを追尾するようにカメラを更新します。
            screen.UpdateCameraChaseTarget();

            // スプリングが有効になっているかを判定し、カメラの追尾方法を選択します。
            // スプリングが無効になっている場合、Reset メソッドでカメラの位置が
            // 常にオブジェクトから一定の間隔に固定されます。
            if (screen.cameraSpringEnabled)
            {
                screen.camera.Update(gameTime);
                screen.playerCamera.Update(gameTime);
            }
            else
            {
                screen.camera.Reset();
                screen.playerCamera.Reset();
            }
#if DEBUG
            DebugSystem.Instance.TimeRuler.EndMark("PlayGameScreen.Update");
#endif
        }

        #endregion

        /// <summary>
        /// 入力処理
        /// </summary>
        /// <param name="input"></param>
        /// <param name="screen"></param>
        public void HandleInput(InputState input, PlayGameScreen screen)
        {
            PlayerIndex playerIndex;
            // アクティブなプロフィールの入力を取得します。
            PlayerIndex controllingPlayer = screen.ControllingPlayer.Value;
            KeyboardState keyboardState = input.CurrentKeyboardStates[(int)controllingPlayer];
            GamePadState gamePadState = input.CurrentGamePadStates[(int)controllingPlayer];


            // Xボタンを押すと自機の色が変わります。(赤→青→緑)
            if (input.IsNewButtonPress(Buttons.X, controllingPlayer, out playerIndex) ||
                input.IsNewKeyPress(Keys.X, controllingPlayer, out playerIndex))
            {
                if (screen.Player.ElementType == ElementType.Red)
                {
                    screen.Player.SetBlueState(screen);
                    screen.Cylinder.SetChangeState(screen);
                    for (int index = 0; index < screen.BackGrounds.Count; ++index)
                        screen.BackGrounds[index].Velocity.Y = 70000.0f;
                    screen.Cylinder.Velocity.Y = 70000.0f;
                }
                else if (screen.Player.ElementType == ElementType.Blue)
                {
                    screen.Player.SetGreenState(screen);
                    screen.Cylinder.SetChangeState(screen);
                    for (int index = 0; index < screen.BackGrounds.Count; ++index)
                        screen.BackGrounds[index].Velocity.Y = 70000.0f;
                    screen.Cylinder.Velocity.Y = 70000.0f;
                }
                else if (screen.Player.ElementType == ElementType.Green)
                {
                    screen.Player.SetRedState(screen);
                    screen.Cylinder.SetChangeState(screen);
                    for (int index = 0; index < screen.BackGrounds.Count; ++index)
                        screen.BackGrounds[index].Velocity.Y = -140000.0f;
                    screen.Cylinder.Velocity.Y = -140000.0f;
                }
            }

            // 右トリガーを押すと自機が弾を発射します。
            if (input.IsNewButtonPress(Buttons.RightTrigger, controllingPlayer, out playerIndex) ||
                input.IsNewKeyPress(Keys.Z, controllingPlayer, out playerIndex) &&
                screen.rapidFireTime > TimeSpan.FromSeconds(1.5) /*&&
                Player.State == StandState*/)
            {
                // 自機弾発射音
                screen.BulletSound = screen.SoundBank.GetCue("bullet_01");
                screen.BulletSound.Play();
                // プレイヤーのステートを変更します。
                screen.Player.SetAttackState(screen);
                screen.Player.StateTime = TimeSpan.Zero;

                LinkedListNode<Enemy> lockOnEnemy = screen.Enemies.LockOnList.First;
                if (lockOnEnemy != null)
                {
                    screen.rapidFireTime = TimeSpan.Zero;
                    screen.Bullets.Shoot(screen.Player.Position,
                                         screen.Player.Velocity,
                                         screen.BulletShapes[(int)screen.Player.ElementType],
                                         screen.Player.ElementType,
                                         screen.LockOns.ActiveList,
                                         screen);
                }
                else
                {
                    screen.rapidFireTime = TimeSpan.Zero;
                    screen.Bullets.Shoot(screen.Player.Position,
                                         screen.Player.Velocity,
                                         screen.BulletShapes[(int)screen.Player.ElementType],
                                         screen.Player.ElementType,
                                         null,
                                         screen);
                }
            }
        }
    }
}

