﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using UniverseColors.PlayGameScreens.Enemies;

namespace UniverseColors.PlayGameScreens
{
    public class LockOnManager
    {

        /// <summary>
        /// ロックオン出来る最大数
        /// </summary>
        public const int MaximumLockOns = 6;

        #region フィールド

        /// <summary>
        /// 予約リスト
        /// </summary>
        public LinkedList<LockOn> ReservedList { get; set; }
        /// <summary>
        /// アクティブ リスト
        /// </summary>
        public LinkedList<LockOn> ActiveList { get; set; }

        #endregion

        /// <summary>
        /// コンストラクター
        /// </summary>
        public LockOnManager()
        {
            ReservedList = new LinkedList<LockOn>();
            ActiveList = new LinkedList<LockOn>();
        }

        /// <summary>
        /// 敵機をロックオンします。
        /// </summary>
        /// <param name="target">ロックオンするターゲット</param>
        /// <param name="position">ロックオンする位置</param>
        /// <param name="compatibilityType">敵機との相性</param>
        public void LockOnEnemy(Enemy target, Vector2 position, LockOn.CompatibilityType compatibilityType)
        {
            // 予約リストから取り出す。
            LinkedListNode<LockOn> lockOn = ReservedList.First;
            ReservedList.Remove(lockOn);
            // ロックオンを設定します。
            lockOn.Value.Target = target;
            lockOn.Value.Position = position;
            lockOn.Value.Compatibility = compatibilityType;
            // アクティブリストに追加します。
            ActiveList.AddLast(lockOn);
        }

        /// <summary>
        /// アクティブ リストから削除します。
        /// </summary>
        /// <param name="item"></param>
        public void ActiveListRemove(LinkedListNode<LockOn> item)
        {
            ActiveList.Remove(item);
        }

        /// <summary>
        /// 予約リストの先頭に追加します。
        /// </summary>
        /// <param name="item"></param>
        public void ReservedListAdd(LinkedListNode<LockOn> item)
        {
            ReservedList.AddFirst(item);
        }

        /// <summary>
        /// ロックオンマネージャーをクリアします。
        /// </summary>
        /// <param name="item"></param>
        public void Clear(LinkedListNode<LockOn> item)
        {
            ActiveListRemove(item);
            ReservedListAdd(item);
        }

    }
}
