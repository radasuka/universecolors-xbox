using UniverseColors;
using Microsoft.Xna.Framework.Content;

namespace UniverseColors
{
    public class UCGameScreen : GameScreen
    {
        public new UCScreenManager ScreenManager
        {
            get { return (UCScreenManager)base.ScreenManager; }
        }

        public ContentManager Content { get; set; }

    }
}
