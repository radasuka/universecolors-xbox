#region File Description
//-----------------------------------------------------------------------------
// BackgroundScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using ステートメント
using System;
using UniverseColors;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace UniverseColors.Screens
{
    /// <summary>
    /// 背景画面は他のすべてのメニュー画面の背後にあります。
    /// その上にある画面で行われる移行のタイプに関係なく、所定の位置に
    /// 固定されたままになる背景イメージを描画します。
    /// </summary>
    class BackgroundScreen : UCGameScreen
    {
        #region フィールド

        private Texture2D backgroundTexture;

        #endregion

        #region 初期化

        /// <summary>
        /// コンストラクター。
        /// </summary>
        public BackgroundScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(0.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }


        /// <summary>
        /// この画面のグラフィック コンテンツを読み込みます。バックグラウンド 
        /// テクスチャーはかなり大きいので、独自のローカル ContentManager を使用して
        /// 読み込みます。これにより、メニューからゲーム自体に切り替える前に
        /// アンロードすることができます。これに対して、Game クラスによって
        /// 提供される共有 ContentManager を使用する場合、コンテンツは永久に
        /// 読み込まれたままになります。
        /// </summary>
        public override void LoadContent()
        {
            if (this.Content == null)
                this.Content = new ContentManager(ScreenManager.Game.Services, "Content");

            backgroundTexture = this.Content.Load<Texture2D>("Screens\\TitleScreen\\title");
        }


        /// <summary>
        /// この画面のグラフィック コンテンツをアンロードします。
        /// </summary>
        public override void UnloadContent()
        {
            this.Content.Unload();
        }


        #endregion

        #region 更新描画処理


        /// <summary>
        /// 背景画面を更新します。
        /// このオーバーロードは、オフへ移行しようとする基本 Update メソッドが 
        /// coveredByOtherScreen パラメーターを変更しないよう、強制的に false に設定します。
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);
        }


        /// <summary>
        /// 背景画面を描画します。
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
            Rectangle fullscreen = new Rectangle(0, 0, viewport.Width, viewport.Height);

            spriteBatch.Begin();

            spriteBatch.Draw(backgroundTexture, fullscreen,
                             new Color(TransitionAlpha, TransitionAlpha, TransitionAlpha));

            spriteBatch.End();
        }


        #endregion
    }
}
