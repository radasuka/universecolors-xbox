#region File Description
//-----------------------------------------------------------------------------
// MainMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using ステートメント
using System;
using UniverseColors;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using UniverseColors.PlayGameScreens;
using GameLibrary;
#endregion

namespace UniverseColors.Screens
{
    class MainMenuScreen : UCMenuScreen
    {
        #region フィールド

        /// <summary>
        /// メニューエントリー
        /// </summary>
        TextureMenuEntry startGameMenuEntry;
        TextureMenuEntry exitMenuEntry;

        /// <summary>
        /// オーディオ
        /// </summary>
        private AudioManager audioManager;
        private Cue titleBGM;
        private Cue enter;

        #endregion

        #region 初期化

        /// <summary>
        /// コンストラクター
        /// </summary>
        public MainMenuScreen()
            : base("Screens\\TitleScreen\\title_01")
        {
        }

        /// <summary>
        /// メインメニューで使うコンテンツを読み込みます。
        /// </summary>
        public override void LoadContent()
        {
            if (this.Content== null)
                this.Content = new ContentManager(ScreenManager.Game.Services, "Content");

            // メニュー エントリを作成します。
            startGameMenuEntry = new TextureMenuEntry(this.Content.Load<Texture2D>("Screens\\TitleScreen\\START"));
            exitMenuEntry = new TextureMenuEntry(this.Content.Load<Texture2D>("Screens\\TitleScreen\\EXIT"));

            // メニューのイベント ハンドラーを登録します。
            startGameMenuEntry.Selected += StartGameMenuEntrySelected;
            exitMenuEntry.Selected += OnCancel;

            // エントリをメニューに追加します。
            MenuEntries.Add(startGameMenuEntry);
            MenuEntries.Add(exitMenuEntry);

            // カテゴリーを取得します。
            AudioManager.Instance.SetCategory("Music");
            // 音楽を再生します。
            titleBGM = AudioManager.Instance.GetCue("title_01");
            titleBGM.Play();

            base.LoadContent();

            GC.Collect();
        }

        /// <summary>
        /// コンテンツを破棄します。
        /// </summary>
        public override void UnloadContent()
        {
            this.Content.Unload();

            base.UnloadContent();
        }

        #endregion

        #region イベント

        /// <summary>
        /// [Play Game (ゲームを始める)] メニュー エントリが選択された場合の
        /// イベント ハンドラー。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void StartGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            foreach (GameScreen screen in ScreenManager.GetScreens())
                screen.ExitScreen();

            enter = AudioManager.Instance.GetCue("enter_01");
            enter.Play();
            // 音楽の再生中であれば、音楽を停止します。
            AudioManager.Instance.Stop(titleBGM);

            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex, new PlayGameScreen());
        }

        /// <summary>
        /// [Exit] メニュー エントリが選択された場合の
        /// イベント ハンドラー。
        /// </summary>
        /// <param name="playerIndex"></param>
        protected override void OnCancel(PlayerIndex playerIndex)
        {
            enter = AudioManager.Instance.GetCue("enter_01");
            enter.Play();

            // ゲームを終了します。
            ScreenManager.Game.Exit();
        }

        #endregion
    }
}
