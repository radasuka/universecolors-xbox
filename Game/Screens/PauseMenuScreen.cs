#region File Description
//-----------------------------------------------------------------------------
// PauseMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using ステートメント
using UniverseColors;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace UniverseColors.Screens
{
    /// <summary>
    /// ポーズ メニューはメイン ゲームの上に立ち上がり、このメニューから
    /// プレイヤーはゲームの再開または終了を選択できます。
    /// </summary>
    class PauseMenuScreen : UCMenuScreen
    {
        #region フィールド

        /// <summary>
        /// メニュー エントリ
        /// </summary>
        TextureMenuEntry gameReturn;

        /// <summary>
        /// メニュー エントリ
        /// </summary>
        TextureMenuEntry titleReturn;

        #endregion

        #region 初期化

        /// <summary>
        /// コンストラクタ−
        /// </summary>
        public PauseMenuScreen()
            : base("Screens\\PauseMenuScreen\\Pause")
        {
        }

        /// <summary>
        /// ポーズ画面で使うグラフィック コンテンツを読み込みます。
        /// </summary>
        public override void LoadContent()
        {
            if (Content == null)
                Content = new ContentManager(ScreenManager.Game.Services, "Content");

            // メニュー エントリを作成します。
            gameReturn = new TextureMenuEntry(Content.Load<Texture2D>("Screens\\PauseMenuScreen\\return to game_on"));
            gameReturn.Position = Vector2.Zero;
            titleReturn = new TextureMenuEntry(Content.Load<Texture2D>("Screens\\PauseMenuScreen\\go to title_on"));
            titleReturn.Position = new Vector2(300, 250);

            // メニューのイベント ハンドラーを登録します。
            gameReturn.Selected += OnCancel;
            titleReturn.Selected += ReturnGameMenuEntrySelected;

            // エントリをメニューに追加します。
            MenuEntries.Add(gameReturn);
            MenuEntries.Add(titleReturn);

            base.LoadContent();
        }

        #endregion

        #region 更新処理と描画処理

        /// <summary>
        /// [Return Game (ゲームに戻る)] メニュー エントリが選択された場合の
        /// イベント ハンドラー。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReturnGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            foreach (GameScreen screen in ScreenManager.GetScreens())
            {
                if (screen.GetType() == typeof(PlayGameScreens.PlayGameScreen))
                {
                    var playGameScreen = screen as PlayGameScreens.PlayGameScreen;
                    if (playGameScreen == null)
                        continue;
                    if (playGameScreen.BGM.IsPlaying)
                        playGameScreen.BGM.Stop(Microsoft.Xna.Framework.Audio.AudioStopOptions.Immediate);
                }
                screen.ExitScreen();
            }

            ScreenManager.AddScreen(new BackgroundScreen(), null);
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex, new BackgroundScreen(), new MainMenuScreen());
        }

        /// <summary>
        /// [Exit] メニュー エントリが選択された場合の
        /// イベント ハンドラー。
        /// </summary>
        /// <param name="playerIndex"></param>
        protected override void OnCancel(PlayerIndex playerIndex)
        {
            ExitScreen();
        }

        #endregion

    }
}
