﻿#region File Description
//-----------------------------------------------------------------------------
// LogoScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using ステートメント
using System;
using GameLibrary.Graphics;
using UniverseColors;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
#endregion

namespace UniverseColors.Screens
{
    class LogoScreen : UCGameScreen
    {
        #region フィールド

        /// <summary>
        /// ロゴ
        /// </summary>
        SpriteItem[] logo = new SpriteItem[2];
        /// <summary>
        /// ロゴの中心座標
        /// </summary>
        Vector2[] origin = new Vector2[2];
        /// <summary>
        /// ロゴ画面の時間
        /// </summary>
        double elapsed;
        /// <summary>
        /// ロゴ配列の番号
        /// </summary>
        int logoindex;

        #endregion

        #region 初期化

        /// <summary>
        /// ゲームのグラフィック コンテンツを読み込みます。
        /// </summary>
        public override void LoadContent()
        {
            if (Content == null)
                Content = new ContentManager(ScreenManager.Game.Services, "Content");

            // ロゴ
            for (int index = 0; index < logo.Length; ++index)
            {
                logo[index] = new SpriteItem();
                logo[index].Shape = new SpriteShape();
            }
            logo[0].Shape.Texture = Content.Load<Texture2D>("Screens\\LogoScreen\\academy_symbol"); // ヒューマンロゴ
            logo[1].Shape.Texture = Content.Load<Texture2D>("Screens\\LogoScreen\\teamlogo");       // チームロゴ
            for (int index = 0; index < logo.Length; ++index)
            {
                logo[index].Position.X = ScreenManager.GraphicsDevice.Viewport.Width / 2;
                logo[index].Position.Y = ScreenManager.GraphicsDevice.Viewport.Height / 2;
                logo[index].Shape.Origin.X = logo[index].Shape.Texture.Width / 2;
                logo[index].Shape.Origin.Y = logo[index].Shape.Texture.Height / 2;
                logo[index].Color.A = 0;
            }

            // フェード時間の設定
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            // ガベージコレクションを実行
            GC.Collect();
        }

        /// <summary>
        /// 画面のコンテンツを破棄します。
        /// </summary>
        public override void UnloadContent()
        {
            Content.Unload();
            base.UnloadContent();
        }

        #endregion

        #region 更新描画処理

        /// <summary>
        /// ロゴ画面を更新します。
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            switch (ScreenState)
            {
                case ScreenState.TransitionOn:
                case ScreenState.TransitionOff:
                case ScreenState.Hidden:
                    break;

                case ScreenState.Active:
                    elapsed += gameTime.ElapsedGameTime.TotalSeconds;

                    // フェード
                    logo[logoindex].Color.A = (byte)(Math.Sin(elapsed) * 0xFF);

                    if (elapsed > Math.PI)
                    {
                        logo[logoindex].Color.A = 0;
                        elapsed = 0.0;
                        ++logoindex;
                        if (logoindex >= logo.Length)
                        {
                            logoindex = logo.Length - 1;
                            LoadingScreen.Load(this.ScreenManager, false, null, new BackgroundScreen(), new MainMenuScreen());
                        }
                    }
                    break;
            }
        }

#if DEBUG
        public override void HandleInput(InputState input)
        {
            PlayerIndex playerIndex;
            if(input.IsNewKeyPress(Keys.Enter, PlayerIndex.One, out playerIndex))
                LoadingScreen.Load(this.ScreenManager, false, null, new BackgroundScreen(), new MainMenuScreen());
        }
#endif

        /// <summary>
        /// ロゴ画面を描画します。
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            // 画面のクリア色
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target, Color.White, 0, 0);

            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied);
            
            logo[logoindex].Draw(spriteBatch, gameTime);

            spriteBatch.End();

            // ゲームがオンまたはオフに移行する場合、表示画面を黒にフェード アウトします。
            if (TransitionPosition > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, 0 / 2);
                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }
           #endregion
    }
}
