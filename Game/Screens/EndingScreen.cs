﻿#region Using ステートメント
using System;
using UniverseColors;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using GameLibrary.Graphics;
using System.Collections.Generic;
using GameLibrary;
#endregion

namespace UniverseColors.Screens
{
    class EndingScreen : UCGameScreen
    {
        #region フィールド

        /// <summary>
        /// 制作チーム画像
        /// </summary>
        private Texture2D memberTexture;

        /// <summary>
        /// 制作者の名前ロゴの描画位置
        /// </summary>
        private Vector2 memberPosition;
        /// <summary>
        /// 制作者の名前ロゴの中心座標
        /// </summary>
        private Vector2 memberOrigin;

        private Rectangle memberRect = new Rectangle(0, 0, 800, 64);

        /// <summary>
        /// ゲームクリアのカラー
        /// </summary>
        private Color color;
        /// <summary>
        /// この画面がアクティブになってからの経過時間
        /// </summary>
        private float elapsedTime;

        /// <summary>
        /// カウンター
        /// </summary>
        private int endingIndex;

        /// <summary>
        /// オーディオを作成します。
        /// </summary>
        private AudioEngine engine;
        private SoundBank soundBank;
        private WaveBank waveBank;
        private AudioCategory musicCategory;
        private Cue bgm;

        #endregion

        #region 初期化

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public EndingScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(1.0);
        }

        /// <summary>
        /// この画面で使うグラフィック コンテンツを読み込みます。
        /// </summary>
        public override void LoadContent()
        {
            if (Content == null)
                Content = new ContentManager(ScreenManager.Game.Services, "Content\\Screens");

            memberTexture = this.Content.Load<Texture2D>("EndingScreen\\name");

            memberPosition.X = ScreenManager.GraphicsDevice.Viewport.Width / 2;
            memberPosition.Y = ScreenManager.GraphicsDevice.Viewport.Height / 2;

            memberOrigin.X = memberTexture.Width / 2;
            memberOrigin.Y = memberRect.Height / 2;

            // ゲームクリアのカラーを設定します。
            color = new Color(1.0f, 1.0f, 1.0f, 0.0f);

            // オーディオオブジェクトを初期化します。
            engine = new AudioEngine("Content\\Sounds\\UniverseColorsSound.xgs");
            soundBank = new SoundBank(engine, "Content\\Sounds\\BGM.xsb");
            waveBank = new WaveBank(engine, "Content\\Sounds\\Wave Bank.xwb");

            // カテゴリーを取得します。
            musicCategory = engine.GetCategory("Music");
            musicCategory.SetVolume(1.0f);
            // 音楽を再生します。
            bgm = soundBank.GetCue("roll_01");
            bgm.Play();

            base.LoadContent();
        }

        /// <summary>
        /// 画面のコンテンツをアンロードします。
        /// </summary>
        public override void UnloadContent()
        {
            this.Content.Unload();
            base.UnloadContent();
        }

        #endregion

        /// <summary>
        /// エンディング画面の更新処理。
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="otherScreenHasFocus"></param>
        /// <param name="coveredByOtherScreen"></param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            switch (ScreenState)
            {
                case ScreenState.TransitionOn:
                case ScreenState.TransitionOff:
                case ScreenState.Hidden:
                    break;

                case ScreenState.Active:
                    elapsedTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
                    // オーディオエンジンを更新します。
                    engine.Update();

                    color.A = (byte)(Math.Sin(elapsedTime) * 0xFF);
                    if (elapsedTime > Math.PI)
                    {
                        ++endingIndex;
                        color.A = 0;
                        elapsedTime = 0;
                        switch (endingIndex)
                        {
                            case 4:
                                memberRect.Height = 112;
                                memberRect.Y = 64 * endingIndex;
                                memberOrigin.Y = memberRect.Height / 2;
                                break;

                            case 5:
                                memberRect.Height = 64;
                                memberRect.Y = 64 * endingIndex + 48;
                                memberOrigin.Y = memberRect.Height / 2;
                                break;

                            case 6:
                            case 7:
                                memberRect.Y = 64 * endingIndex + 48;
                                memberOrigin.Y = memberRect.Height / 2;
                                break;

                            case 8:
                                memberRect.Height = 160;
                                memberRect.Y = 64 * endingIndex + 48;
                                memberOrigin.Y = memberRect.Height / 2;
                                break;

                            case 9:
                                memberRect.Y = 64 * endingIndex + 144;
                                memberOrigin.Y = memberRect.Height / 2;
                                break;

                            case 10:
                                memberRect.Height = 64;
                                memberRect.Y = 64 * endingIndex + 240;
                                memberOrigin.Y = memberRect.Height / 2;
                                break;

                            default:
                                memberRect.Height = 64;
                                memberRect.Y = 64 * endingIndex;
                                memberOrigin.Y = memberRect.Height / 2;
                                break;
                        }

                        if (endingIndex >= 11)
                        {
                            // 音楽の再生中であれば、音楽を停止します。
                            if (bgm.IsPlaying)
                                bgm.Stop(AudioStopOptions.Immediate);

                            LoadingScreen.Load(this.ScreenManager, false, this.ControllingPlayer, new BackgroundScreen(), new MainMenuScreen());
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// エンディング画面を描画します。
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target,
                                                Color.White, 0, 0);

            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

            spriteBatch.Begin();

            spriteBatch.Draw(memberTexture, memberPosition, memberRect, color, 0.0f, memberOrigin, 1.0f, SpriteEffects.None, 0);

            spriteBatch.End();

            // ゲームがオンまたはオフに移行する場合、表示画面を黒にフェード アウトします。
            if (TransitionPosition > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, 0 / 2);
                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }
    }
}
