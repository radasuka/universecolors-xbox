﻿#region Using ステートメント
using System;
using System.Collections.Generic;
using GameLibrary;
using GameLibrary.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using UniverseColors.PlayGameScreens;
using Microsoft.Xna.Framework.Audio;
#endregion

namespace UniverseColors.Screens
{
    class RankingScreen : UCGameScreen
    {
        // スコア描画数
        const int DrawNum = 10;

        #region フィールド
        /// <summary>
        /// 背景
        /// </summary>
        SpriteItem backGround;
        /// <summary>
        /// ランキング
        /// </summary>
        SpriteItem rankingLabel;
        /// <summary>
        /// 今回のスコア
        /// </summary>
        Score score;
        /// <summary>
        /// 過去のスコア
        /// </summary>
        List<Score> scores;
        /// <summary>
        /// セーブデータ
        /// </summary>
        SaveData saveData;
        /// <summary>
        /// セーブ
        /// </summary>
        Saver saver;
        /// <summary>
        /// ロード
        /// </summary>
        Loader loader;

        // オーディオ
        Cue BGM;

        /// <summary>
        /// この画面がアクティブになってからの経過時間
        /// </summary>
        TimeSpan elapsedTime;
        #endregion

        #region 初期化
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="score"></param>
        public RankingScreen(Score score)
        {
            this.score = score;
            scores = new List<Score>();

            loader = new Loader();
            saver = new Saver();

            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(1.0);
        }

        /// <summary>
        /// グラフィックコンテンツを読み込みます。
        /// </summary>
        public override void LoadContent()
        {
            if (Content == null)
                Content = new ContentManager(ScreenManager.Game.Services, "Content");

            // 背景
            backGround = new SpriteItem();
            backGround.Position = Vector2.Zero;
            backGround.Shape = new SpriteShape();
            backGround.Shape.Texture = this.Content.Load<Texture2D>("Screens\\EndingScreen\\Planet");
            // ランキング
            rankingLabel = new SpriteItem();
            rankingLabel.Shape = new SpriteShape();
            rankingLabel.Shape.Texture = this.Content.Load<Texture2D>("Screens\\ranking");
            rankingLabel.Shape.Origin = new Vector2(rankingLabel.Shape.Texture.Width / 2, rankingLabel.Shape.Texture.Height / 2);
            rankingLabel.Scale = new Vector2(0.8f);

            // スコア
            LoadScore();

            // カテゴリー取得
            AudioManager.Instance.SetCategory("Music");
            // BGM再生
            BGM = AudioManager.Instance.GetCue("rank");
            BGM.Play();

            // ガベージコレクションを実行
            GC.Collect();
        }

        // スコア読み込み
        private void LoadScore()
        {
            // スコアの数字
            SpriteSheet scoreSheet;
            scoreSheet = this.Content.Load<SpriteSheet>("PlayGames\\Score");
            scoreSheet.Origin.X = scoreSheet.SourceRectangle(0).Width / 2;
            scoreSheet.Origin.Y = scoreSheet.SourceRectangle(0).Height / 2;
            score.Shape = scoreSheet;
            score.IsNew = true;

            // 今回のスコアを追加
            scores.Add(score);

            // セーブデータロード
            loader.Initialize(ScreenManager.Game.Window.Title, saveData, PlayerIndex.One);
            loader.Update();
            saveData = loader.Data;

            // セーブデータがない場合作成
            if (saveData == null)
            {
                saveData = new SaveData()
                {
                    Score = new List<int>()
                };
            }

            // ロードしたスコアを追加
            foreach (var currentScore in saveData.Score)
            {
                Score item = new Score();
                item.CurrentScore = currentScore;
                item.Shape = scoreSheet;
                item.IsNew = false;
                scores.Add(item);
            }

            // スコアが描画数以下の場合ダミー追加
            if (scores.Count < DrawNum)
            {
                for (int index = scores.Count; index < DrawNum; ++index)
                {
                    Score item = new Score();
                    item.CurrentScore = 0;
                    item.Shape = scoreSheet;
                    item.IsNew = false;
                    scores.Add(item);
                }
            }

            // スコアのソート(降順)
            scores.Sort((a, b) => b.CurrentScore - a.CurrentScore);
        }
        #endregion

        /// <summary>
        /// 画面のコンテンツをアンロードします。
        /// </summary>
        public override void UnloadContent()
        {
            this.Content.Unload();
            base.UnloadContent();
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="otherScreenHasFocus"></param>
        /// <param name="coveredByOtherScreen"></param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            switch (ScreenState)
            {
                case ScreenState.TransitionOn:
                case ScreenState.TransitionOff:
                case ScreenState.Hidden:
                    break;

                case ScreenState.Active:

                    elapsedTime += gameTime.ElapsedGameTime;
                    // オーディオエンジン更新
                    AudioManager.Instance.Update();

                    // 今回のスコアを点滅
                    for (int index = 0; index < scores.Count; ++index)
                    {
                        if (scores[index].IsNew)
                        {
                            scores[index].Color.A = (byte)(Math.Sin(elapsedTime.TotalSeconds) * 0xFF);
                        }
                    }

                    if (elapsedTime.TotalSeconds >= MathHelper.Pi * 2)
                    {
                        // セーブ
                        saveData.Score.Add(this.score.CurrentScore);
                        saveData.Score.Sort((a, b) => b - a);
                        saver.Initialize(ScreenManager.Game.Window.Title, saveData, PlayerIndex.One);
                        saver.Update();

                        // BGM再生中なら停止
                        AudioManager.Instance.Stop(BGM);
                        LoadingScreen.Load(this.ScreenManager, false, this.ControllingPlayer, new EndingScreen());
                    }
                    break;
            }
        }

#if DEBUG
        public override void HandleInput(InputState input)
        {
            PlayerIndex playerIndex;
            if (input.IsNewKeyPress(Keys.Enter, PlayerIndex.One, out playerIndex))
            {
                saveData.Score.Add(this.score.CurrentScore);
                saveData.Score.Sort((a, b) => b - a);

                saver.Initialize(ScreenManager.Game.Window.Title, saveData, PlayerIndex.One);
                saver.Update();
                if (BGM.IsPlaying)
                    BGM.Stop(AudioStopOptions.Immediate);
                LoadingScreen.Load(this.ScreenManager, false, this.ControllingPlayer, new EndingScreen());
            }
        }
#endif

        /// <summary>
        /// 描画処理
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target,
                                   Color.White, 0, 0);

            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied);

            // 背景
            backGround.Draw(spriteBatch, gameTime);
            // ランキング
            UpdateRankingLocation();
            rankingLabel.Draw(spriteBatch, gameTime);

            spriteBatch.End();

            // スコア
            UpdateScoreLocations();
            for (int index = 0; index < DrawNum; ++index)
                scores[index].Draw(spriteBatch, gameTime);

            // ゲームがオンまたはオフに移行する場合、表示画面を黒にフェード アウトします。
            if (TransitionPosition > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, 0 / 2);
                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }

        private void UpdateScoreLocations()
        {
            // 見栄えをよくするために、べき乗曲線を使用して、トランジション中に
            // スコアを固定位置までスライドさせる。
            // (固定位置に近づくと移動がスローダウンする)
            float transitionOffset = (float)Math.Pow(TransitionPosition, 2);

            Vector2 position = Vector2.Zero;

            // 全てのスコア位置を更新する
            for (int index = 0; index < scores.Count; ++index)
            {
                Score score = scores[index];

                position.Y = 150.0f + score.Shape.Origin.Y + (score.Shape.SourceRectangle(0).Height * index);
                position.X = (ScreenManager.GraphicsDevice.Viewport.Width / 2) + (score.Shape.SourceRectangle(0).Width * 4);

                if (ScreenState == UniverseColors.ScreenState.TransitionOn)
                    position.X -= transitionOffset * 256;
                else
                    position.X += transitionOffset * 512;

                score.BasicPosition = position;
            }
        }

        private void UpdateRankingLocation()
        {
            float transitionOffset = (float)Math.Pow(TransitionPosition, 2);

            Vector2 position = Vector2.Zero;

            position.Y = 100.0f;
            position.X = (ScreenManager.GraphicsDevice.Viewport.Width / 2);

            if (ScreenState == UniverseColors.ScreenState.TransitionOn)
                position.X -= transitionOffset * 256;
            else
                position.X += transitionOffset * 512;

            rankingLabel.Position = position;
        }
    }
}
