﻿#region File Description
//-----------------------------------------------------------------------------
// GameClearScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using ステートメント
using System;
using GameLibrary.Graphics;
using UniverseColors;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using UniverseColors.PlayGameScreens;
using Microsoft.Xna.Framework.Audio;
using GameLibrary;
#endregion

namespace UniverseColors.Screens
{
    class GameClearScreen : UCGameScreen
    {
        #region フィールド

        /// <summary>
        /// ゲームクリア
        /// </summary>
        private SpriteItem gameClearLogo = new SpriteItem();
        /// <summary>
        /// スコアロゴ
        /// </summary>
        private SpriteItem scoreLogo = new SpriteItem();
        /// <summary>
        /// スコア
        /// </summary>
        private Score score;
        /// <summary>
        /// この画面がアクティブになってからの経過時間
        /// </summary>
        private float elapsedTime;
        /// <summary>
        /// BGM
        /// </summary>
        private Cue BGM;

        #endregion

        #region 初期化

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="game"></param>
        public GameClearScreen(Score score)
        {
            this.score = score;
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(1.0);
        }

        /// <summary>
        /// ゲームクリア画面のグラフィックコンテンツを読み込みます。
        /// </summary>
        public override void LoadContent()
        {
            if (Content == null)
                Content = new ContentManager(ScreenManager.Game.Services, "Content");

            // ゲームクリア
            gameClearLogo.Position.X = this.ScreenManager.GraphicsDevice.Viewport.Width / 2;
            gameClearLogo.Position.Y = this.ScreenManager.GraphicsDevice.Viewport.Height / 2;
            gameClearLogo.Shape = new SpriteShape();
            gameClearLogo.Shape.Texture = Content.Load<Texture2D>("Screens\\GameClearScreen\\GameClear");
            gameClearLogo.Shape.Origin.X = gameClearLogo.Shape.Texture.Width / 2;
            gameClearLogo.Shape.Origin.Y = gameClearLogo.Shape.Texture.Height / 2;
            gameClearLogo.Color.A = 0;

            // スコアロゴ
            scoreLogo.Position.X = 540;
            scoreLogo.Position.Y = ScreenManager.GraphicsDevice.Viewport.Height / 2 + 150;
            scoreLogo.Shape = new SpriteShape();
            scoreLogo.Shape.Texture = Content.Load<Texture2D>("Screens\\GameClearScreen\\ScoreLogo");
            scoreLogo.Shape.Origin.X = scoreLogo.Shape.Texture.Width / 2;
            scoreLogo.Shape.Origin.Y = scoreLogo.Shape.Texture.Height / 2;
            scoreLogo.Color.A = 0;

            // スコアの数字
            SpriteSheet scoreSheet;
            scoreSheet = this.Content.Load<SpriteSheet>("PlayGames\\Score");
            scoreSheet.Origin.X = scoreSheet.SourceRectangle(0).Width / 2;
            scoreSheet.Origin.Y = scoreSheet.SourceRectangle(0).Height / 2;
            score.Shape = scoreSheet;
            score.BasicPosition = new Vector2(900.0f, ScreenManager.GraphicsDevice.Viewport.Height / 2 + 150);
            score.Color.A = 0;

            // カテゴリーを取得します。
            AudioManager.Instance.SetCategory("Music");
            //// 音楽を再生します。
            BGM = AudioManager.Instance.GetCue("gameClear");
            BGM.Play();

            base.LoadContent();
        }

        /// <summary>
        /// 画面のコンテンツをアンロードします。
        /// </summary>
        public override void UnloadContent()
        {
            this.Content.Unload();
            base.UnloadContent();
        }


        #endregion

        #region 更新処理

        /// <summary>
        /// ゲームクリア画面の更新処理。
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="otherScreenHasFocus"></param>
        /// <param name="coveredByOtherScreen"></param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            switch (ScreenState)
            {
                case ScreenState.TransitionOn:
                case ScreenState.TransitionOff:
                case ScreenState.Hidden:
                    break;

                case ScreenState.Active:
                    elapsedTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

                    // オーディオエンジンを更新します。
                    AudioManager.Instance.Update();

                    // フェードイン
                    gameClearLogo.Color.A = (byte)(Math.Sin(elapsedTime) * 0xFF);
                    score.Color.A = (byte)(Math.Sin(elapsedTime) * 0xFF);
                    scoreLogo.Color.A = (byte)(Math.Sin(elapsedTime) * 0xFF);
                    if (elapsedTime >= MathHelper.PiOver2)
                    {
                        gameClearLogo.Color.A = 255;
                        score.Color.A = 255;
                        scoreLogo.Color.A = 255;
                        if (elapsedTime > 7.0f)
                        {
                            // 音楽の再生中であれば、音楽を停止します。
                            AudioManager.Instance.Stop(BGM);
                            LoadingScreen.Load(this.ScreenManager, false, this.ControllingPlayer, new RankingScreen(this.score));
                        }
                    }
                    break;
            }
        }

        #endregion

        /// <summary>
        /// ゲームクリア画面を描画します。
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target,
                                               Color.White, 0, 0);

            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

            spriteBatch.Begin();

            gameClearLogo.Draw(spriteBatch, gameTime);
            scoreLogo.Draw(spriteBatch, gameTime);

            spriteBatch.End();

            score.Draw(spriteBatch, gameTime);

            // ゲームがオンまたはオフに移行する場合、表示画面を黒にフェード アウトします。
            if (TransitionPosition > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, 0 / 2);
                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }
    }
}
