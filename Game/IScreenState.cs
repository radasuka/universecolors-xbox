﻿using GameLibrary;

namespace UniverseColors
{
    /// <summary>
    /// スクリーン特有の更新を含んだインターフェイスです。
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IScreenState<T> : IState<T>
    {
        void HandleInput(InputState input, T screen);
    }
}
