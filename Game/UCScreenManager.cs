using GameLibrary;
using UniverseColors;

namespace UniverseColors
{
    public class UCScreenManager : ScreenManager
    {
        public new MainGame Game
        {
            get { return (MainGame)base.Game; }
        }

        public UCScreenManager(MainGame game)
            : base(game)
        {
        }
    }
}
