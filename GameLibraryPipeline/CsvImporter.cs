using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework.Content.Pipeline;

namespace GameLibrary.Content.Pipeline
{
    /// <summary>
    /// このクラスは XNA Framework コンテンツ パイプラインによってインスタンス化され、
    /// ディスク上にある任意のファイルを<see cref="TableContent"/>型にインポートします。
    /// </summary>
    [ContentImporter(".csv", DisplayName = "CSV : Game Library")]
    public class CsvImporter : ContentImporter<TableContent>
    {
        private static IEnumerable<string> GetLines(string filename)
        {
            using (StreamReader reader = new StreamReader(filename))
            {
                string lineString;
                while (true)
                {
                    lineString = reader.ReadLine();
                    if (lineString != null)
                        yield return lineString;
                    else
                        yield break;
                }
            }
        }

        /// <summary>
        /// ゲーム アセットのインポート時にフレームワークによって呼び出されます。
        /// </summary>
        /// <param name="filename">ゲーム アセット ファイルの名前。</param>
        /// <param name="context">ゲーム アセットをインポートするための情報 (ロガー インターフェイスなど) が含まれます。</param>
        /// <returns>結果のゲーム アセット。</returns>
        public override TableContent Import(string filename, ContentImporterContext context)
        {
            TableContent retVal = new TableContent();

            retVal.Name = Path.GetFileNameWithoutExtension(filename);
            retVal.Identity = new ContentIdentity(filename);

            foreach (string lineString in GetLines(filename))
            {
                string[] tokens = lineString.Split(',');

                List<string> columns = new List<string>();
                foreach (string token in tokens)
                    columns.Add(token);

                retVal.Rows.Add(columns);
            }

            return retVal;
        }
    }
}