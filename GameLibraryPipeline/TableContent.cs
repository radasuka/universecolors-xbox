﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Content.Pipeline;

namespace GameLibrary.Content.Pipeline
{
    /// <summary>
    /// 表形式を維持するためのプロパティを提供します。
    /// </summary>
    [ContentSerializerRuntimeType("GameLibrary.Table, GameLibrary")]
    public class TableContent : ContentItem
    {
        /// <summary>
        /// このテーブルに属する行のコレクションを取得します。
        /// </summary>
        public List<List<string>> Rows = new List<List<string>>();
    }
}