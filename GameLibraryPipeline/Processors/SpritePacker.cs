#region File Description
//-----------------------------------------------------------------------------
// SpritePacker.cs
//
// Microsoft Game Technology Group
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
#endregion

namespace GameLibrary.Content.Pipeline.Processors
{
    /// <summary>
    /// 多くの小さなスプライトを 1 枚の大きなシートに配置するためのヘルパー。
    /// </summary>
    public static class SpritePacker
    {
        /// <summary>
        /// スプライトのリストを単一の大きなテクスチャーにパックして、
        /// それぞれが格納された場所を記録します。
        /// </summary>
        public static BitmapContent PackSprites(IList<BitmapContent> sourceSprites,
                                                ICollection<Rectangle> outputSprites,
                                                ContentProcessorContext context)
        {
            if (sourceSprites.Count == 0)
                throw new InvalidContentException("There are no sprites to arrange");

            // 配置する必要があるすべてのスプライトのリストを構築します。
            List<ArrangedSprite> sprites = new List<ArrangedSprite>();

            for (int i = 0; i < sourceSprites.Count; i++)
            {
                ArrangedSprite sprite = new ArrangedSprite();

                // スプライトをサイズ変更したり、回転したりする場合に、
                // フィルタリングの問題を回避するために、
                // 各スプライトの周囲に単一のピクセル パディングを取り込みます。
                sprite.Width = sourceSprites[i].Width + 2;
                sprite.Height = sourceSprites[i].Height + 2;

                sprite.Index = i;

                sprites.Add(sprite);
            }

            // 最大のスプライトが最初に配置されるように並べ替えます。
            sprites.Sort(CompareSpriteSizes);

            // 出力ビットマップの大きさを割り出します。
            int outputWidth = GuessOutputWidth(sprites);
            int outputHeight = 0;
            int totalSpriteSize = 0;

            // スプライトの位置を 1 つずつ選択します。
            for (int i = 0; i < sprites.Count; i++)
            {
                PositionSprite(sprites, i, outputWidth);

                outputHeight = Math.Max(outputHeight, sprites[i].Y + sprites[i].Height);

                totalSpriteSize += sprites[i].Width * sprites[i].Height;
            }

            // スプライトをもう一度インデックス順に並べ替えます。
            sprites.Sort(CompareSpriteIndices);

            context.Logger.LogImportantMessage(
                "Packed {0} sprites into a {1}x{2} sheet, {3}% efficiency",
                sprites.Count, outputWidth, outputHeight,
                totalSpriteSize * 100 / outputWidth / outputHeight);

            return CopySpritesToOutput(sprites, sourceSprites, outputSprites,
                                       outputWidth, outputHeight);
        }


        /// <summary>
        /// 配置が完了したら、各スプライトのビットマップ データを単一の大きな
        /// 出力ビットマップ内の選択した位置にコピーします。
        /// </summary>
        static BitmapContent CopySpritesToOutput(List<ArrangedSprite> sprites,
                                                 IList<BitmapContent> sourceSprites,
                                                 ICollection<Rectangle> outputSprites,
                                                 int width, int height)
        {
            BitmapContent output = new PixelBitmapContent<Color>(width, height);

            foreach (ArrangedSprite sprite in sprites)
            {
                BitmapContent source = sourceSprites[sprite.Index];

                int x = sprite.X;
                int y = sprite.Y;

                int w = source.Width;
                int h = source.Height;

                // 主要なスプライト データを出力シートにコピーします。
                BitmapContent.Copy(source, new Rectangle(0, 0, w, h),
                                   output, new Rectangle(x + 1, y + 1, w, h));

                // スプライトの各エッジから境界ストリップをコピーして、
                // スプライトをサイズ変更したり、回転したりする場合に、
                // フィルタリングの問題を回避するために、
                // 1 つのピクセル パディング領域を作成します。
                BitmapContent.Copy(source, new Rectangle(0, 0, 1, h),
                                   output, new Rectangle(x, y + 1, 1, h));

                BitmapContent.Copy(source, new Rectangle(w - 1, 0, 1, h),
                                   output, new Rectangle(x + w + 1, y + 1, 1, h));

                BitmapContent.Copy(source, new Rectangle(0, 0, w, 1),
                                   output, new Rectangle(x + 1, y, w, 1));

                BitmapContent.Copy(source, new Rectangle(0, h - 1, w, 1),
                                   output, new Rectangle(x + 1, y + h + 1, w, 1));

                // スプライトの各コーナーから単一のピクセルをコピーして、
                // 1 つのピクセル パディング領域のコーナーに埋め込みます。
                BitmapContent.Copy(source, new Rectangle(0, 0, 1, 1),
                                   output, new Rectangle(x, y, 1, 1));

                BitmapContent.Copy(source, new Rectangle(w - 1, 0, 1, 1),
                                   output, new Rectangle(x + w + 1, y, 1, 1));

                BitmapContent.Copy(source, new Rectangle(0, h - 1, 1, 1),
                                   output, new Rectangle(x, y + h + 1, 1, 1));

                BitmapContent.Copy(source, new Rectangle(w - 1, h - 1, 1, 1),
                                   output, new Rectangle(x + w + 1, y + h + 1, 1, 1));

                // このスプライトを配置した場所を忘れないでください。
                outputSprites.Add(new Rectangle(x + 1, y + 1, w, h));
            }

            return output;
        }


        /// <summary>
        /// スプライトが配置されている間、内部ヘルパー クラスがスプライトを
        /// 追跡します。
        /// </summary>
        class ArrangedSprite
        {
            public int Index;

            public int X;
            public int Y;

            public int Width;
            public int Height;
        }


        /// <summary>
        /// 単一のスプライトを配置する場所を割り出します。
        /// </summary>
        static void PositionSprite(List<ArrangedSprite> sprites,
                                   int index, int outputWidth)
        {
            int x = 0;
            int y = 0;

            while (true)
            {
                // この位置は自由に利用できますか?
                int intersects = FindIntersectingSprite(sprites, index, x, y);

                if (intersects < 0)
                {
                    sprites[index].X = x;
                    sprites[index].Y = y;

                    return;
                }

                // 衝突した既存のスプライトを飛び越します。
                x = sprites[intersects].X + sprites[intersects].Width;

                // 部屋から飛び出して右側に移動するには、
                // 代わりに次の行を試行します。
                if (x + sprites[index].Width > outputWidth)
                {
                    x = 0;
                    y++;
                }
            }
        }


        /// <summary>
        /// 提案されたスプライトの位置が既に配置されたスプライトと
        /// 衝突するかどうかチェックします。
        /// </summary>
        static int FindIntersectingSprite(List<ArrangedSprite> sprites,
                                          int index, int x, int y)
        {
            int w = sprites[index].Width;
            int h = sprites[index].Height;

            for (int i = 0; i < index; i++)
            {
                if (sprites[i].X >= x + w)
                    continue;

                if (sprites[i].X + sprites[i].Width <= x)
                    continue;

                if (sprites[i].Y >= y + h)
                    continue;

                if (sprites[i].Y + sprites[i].Height <= y)
                    continue;

                return i;
            }

            return -1;
        }


        /// <summary>
        /// スプライトをサイズによって並べ替えるための比較関数。
        /// </summary>
        static int CompareSpriteSizes(ArrangedSprite a, ArrangedSprite b)
        {
            int aSize = a.Height * 1024 + a.Width;
            int bSize = b.Height * 1024 + b.Width;

            return bSize.CompareTo(aSize);
        }


        /// <summary>
        /// スプライトを元のインデックスによって並べ替えるための比較関数。
        /// </summary>
        static int CompareSpriteIndices(ArrangedSprite a, ArrangedSprite b)
        {
            return a.Index.CompareTo(b.Index);
        }


        /// <summary>
        /// 何がスプライトのリストの適切な出力幅であるかを試行錯誤により
        /// 推測します。
        /// </summary>
        static int GuessOutputWidth(List<ArrangedSprite> sprites)
        {
            // すべてのスプライトの幅をテンポラリ リストに集めます。
            List<int> widths = new List<int>();

            foreach (ArrangedSprite sprite in sprites)
            {
                widths.Add(sprite.Width);
            }

            // 幅を昇順に並べ替えます。
            widths.Sort();

            // 最大幅と最小幅を取得します。
            int maxWidth = widths[widths.Count - 1];
            int medianWidth = widths[widths.Count / 2];

            // 中央値の大きさのスプライトの NxN グリッドを試行錯誤により
            // 推測します。
            int width = medianWidth * (int)Math.Round(Math.Sqrt(sprites.Count));

            // 最大のスプライトより小さなスプライトを選択しないように
            // してください。
            return Math.Max(width, maxWidth);
        }
    }        
}
