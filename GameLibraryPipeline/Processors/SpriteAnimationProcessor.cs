﻿using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework.Content.Pipeline;

namespace GameLibrary.Content.Pipeline.Processors
{
    /// <summary>
    /// カスタム コンテンツ プロセッサは、個々のスプライト シート ファイル名の配列
    /// (通常、XML ファイルからインポートされます)を取得し、これらをすべてメモリーに
    /// 読み込み、生成されたスプライト アニメーション オブジェクトを返します。
    /// </summary>
    [ContentProcessor(DisplayName = "スプライト アニメーション : Game Library")]
    public class SpriteAnimationProcessor : ContentProcessor<string[], SpriteAnimationContent>
    {
        public override SpriteAnimationContent Process(string[] input, ContentProcessorContext context)
        {
            SpriteAnimationContent spriteAnimation = new SpriteAnimationContent();
            List<SpriteSheetContent> sourceSheets = new List<SpriteSheetContent>();

            // 各入力アニメーション クリップ名に対してループ処理を行います。
            foreach (string inputFilename in input)
            {
                // このアニメーション クリップの名前を保存します。
                string clipName = Path.GetFileNameWithoutExtension(inputFilename);

                spriteAnimation.ClipNames.Add(clipName, sourceSheets.Count);

                // スプライト シートをメモリーに読み込みます。
                ExternalReference<string[]> spriteSheetReference =
                    new ExternalReference<string[]>(inputFilename);

                SpriteSheetContent spriteSheet =
                    context.BuildAndLoadAsset<string[], SpriteSheetContent>(
                        spriteSheetReference, "SpriteSheetProcessor");

                spriteAnimation.AnimationClips.Add(spriteSheet);
            }

            return spriteAnimation;
        }
    }
}