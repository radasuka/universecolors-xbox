﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using System.ComponentModel;

namespace UniverseColors.Content.Pipeline
{
    /// <summary>
    /// スカイドーム メッシュを作成するためのカスタム コンテンツ プロセッサ。処理しようとする
    /// 空のテクスチャーが与えられると、このプロセッサは MeshBuilder クラスを使用して
    /// スカイドーム ジオメトリをプログラムによって生成します。円柱を作成し、
    /// テクスチャーが周囲に空のイメージをマッピングし、空をレンダリングするために
    /// 使用するカスタム エフェクトを割り当てます。
    /// </summary>
    [ContentProcessor(DisplayName = "スカイ : Universe Colors")]
    public class SkyTubeProcessor : ContentProcessor<Texture2DContent, SkyTubeContent>
    {
        private float cylinderRadius = 100;
        /// <summary>
        /// 円柱の半径を取得または設定します。
        /// </summary>
        [DefaultValue(100)]
        [DisplayName("円柱の半径")]
        public float CylinderRadius
        {
            get { return cylinderRadius; }
            set { cylinderRadius = value; }
        }

        private int cylinderSegments = 32;
        /// <summary>
        /// 円柱の分割数を取得または設定します。
        /// </summary>
        [DefaultValue(32)]
        [DisplayName("円柱の分割数")]
        public int CylinderSegments
        {
            get { return cylinderSegments; }
            set { cylinderSegments = value; }
        }

        private float top = 100;
        /// <summary>
        /// 円柱の天面のY座標を取得または設定します。
        /// </summary>
        [DefaultValue(100)]
        [DisplayName("円柱の天面のY座標")]
        public float Top
        {
            get { return top; }
            set { top = value; }
        }

        private float bottom = -100;
        /// <summary>
        /// 円柱の底面のY座標を取得または設定します。
        /// </summary>
        [DefaultValue(-100)]
        [DisplayName("円柱の底面のY座標")]
        public float Bottom
        {
            get { return bottom; }
            set { bottom = value; }
        }

        const float texCoordTop = 0.1f;
        const float texCoordBottom = 0.9f;

        /// <summary>
        /// 処理しようとする空のテクスチャーのスカイドーム ジオメトリを生成します。
        /// </summary>
        public override SkyTubeContent Process(Texture2DContent input, ContentProcessorContext context)
        {
            MeshBuilder builder = MeshBuilder.StartMesh("sky");

            // 円柱の上面と底面の周囲に 2 つの頂点のリングを作成します。
            List<int> topVertices = new List<int>();
            List<int> bottomVertices = new List<int>();

            for (int i = 0; i < CylinderSegments; i++)
            {
                float angle = MathHelper.TwoPi * i / CylinderSegments;

                float x = (float)Math.Cos(angle) * CylinderRadius;
                float z = (float)Math.Sin(angle) * CylinderRadius;

                topVertices.Add(builder.CreatePosition(x, CylinderRadius, z));
                bottomVertices.Add(builder.CreatePosition(x, Bottom, z));
            }

            // 上面と底面を閉じるために使用する 2 つの中心頂点を作成します。
            int topCenterVertex = builder.CreatePosition(0, CylinderRadius * 2, 0);
            int bottomCenterVertex = builder.CreatePosition(0, -Bottom, 0);

            // テクスチャー座標を保持するための頂点チャンネルを作成します。
            int texCoordId = builder.CreateVertexChannel<Vector2>(
                                            VertexChannelNames.TextureCoordinate(0));

            builder.SetMaterial(new BasicMaterialContent());

            // スカイドームを構成する個々のトライアングルを作成します。
            for (int i = 0; i < CylinderSegments; i++)
            {
                int j = (i + 1) % CylinderSegments;

                // この円柱のセグメントのテクスチャー座標を計算します。
                float u1 = (float)i / (float)CylinderSegments;
                float u2 = (float)(i + 1) / (float)CylinderSegments;

                // 2 つのトライアングルが円柱の一つの側面にクワッドを形成します。
                AddVertex(builder, topVertices[i], texCoordId, u1, texCoordTop);
                AddVertex(builder, topVertices[j], texCoordId, u2, texCoordTop);
                AddVertex(builder, bottomVertices[i], texCoordId, u1, texCoordBottom);

                AddVertex(builder, topVertices[j], texCoordId, u2, texCoordTop);
                AddVertex(builder, bottomVertices[j], texCoordId, u2, texCoordBottom);
                AddVertex(builder, bottomVertices[i], texCoordId, u1, texCoordBottom);

                // このセグメントの上にある上面を満たすために内側に広がるトライアングル。
                AddVertex(builder, topCenterVertex, texCoordId, u1, 0);
                AddVertex(builder, topVertices[j], texCoordId, u2, texCoordTop);
                AddVertex(builder, topVertices[i], texCoordId, u1, texCoordTop);

                // このセグメントの下にある底面を満たすために内側に広がるトライアングル。
                AddVertex(builder, bottomCenterVertex, texCoordId, u1, 1);
                AddVertex(builder, bottomVertices[i], texCoordId, u1, texCoordBottom);
                AddVertex(builder, bottomVertices[j], texCoordId, u2, texCoordBottom);
            }

            // 出力オブジェクトを作成します。
            SkyTubeContent sky = new SkyTubeContent();

            // 生成したばかりのメッシュを変換できるように、ModelProcessor にチェーンします。
            MeshContent skyMesh = builder.FinishMesh();

            sky.Model = context.Convert<MeshContent, ModelContent>(skyMesh,
                                                                   "ModelProcessor");

            // 空のテクスチャーを変換できるように、TextureProcessor に
            // チェーンします。通常、空のイメージでは見栄えが
            // あまりよくない DXT 圧縮が適用されるので、ここでは、
            // 既定の ModelTextureProcessor を使用しません。

            // 注意: これは、既定の TextureProcessor によって、そのテクスチャーを
            // 処理するカスタム ModelProcessor を作成し、空のテクスチャーを
            // マテリアルの Textures 辞書に追加することで、実現することもできます。
            // 簡潔にするため、代わりに次の方法を使用します。
            sky.Texture = context.Convert<TextureContent, TextureContent>(input,
                                                                    "TextureProcessor");

            return sky;
        }

        /// <summary>
        /// 関連付けられたテクスチャー座標値とともに新しいトライアングル
        /// 頂点を MeshBuilder に追加するためのヘルパー。
        /// </summary>
        static void AddVertex(MeshBuilder builder, int vertex, int texCoordId, float u, float v)
        {
            builder.SetVertexChannelData(texCoordId, new Vector2(u, v));

            builder.AddTriangleVertex(vertex);
        }
    }
}
