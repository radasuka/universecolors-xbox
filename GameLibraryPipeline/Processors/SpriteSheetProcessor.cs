#region File Description
//-----------------------------------------------------------------------------
// SpriteSheetProcessor.cs
//
// Microsoft Game Technology Group
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System.IO;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
#endregion

namespace GameLibrary.Content.Pipeline.Processors
{
    /// <summary>
    /// カスタム コンテンツ プロセッサは、個々のスプライト ファイル名の配列 
    /// (通常、XML ファイルからインポートされます) を取得し、
    /// これらをすべてメモリーに読み込み、単一の大きなテクスチャーに配置して、
    /// 生成されたスプライト シート オブジェクトを返します。
    /// </summary>
    [ContentProcessor(DisplayName = "スプライト シート : Game Library")]
    public class SpriteSheetProcessor : ContentProcessor<string[], SpriteSheetContent>
    {
        /// <summary>
        /// スプライト ファイル名の配列をスプライト シート オブジェクトに変換します。
        /// </summary>
        public override SpriteSheetContent Process(string[] input,
                                                   ContentProcessorContext context)
        {
            SpriteSheetContent spriteSheet = new SpriteSheetContent();
            List<BitmapContent> sourceSprites = new List<BitmapContent>();

            // 各入力スプライト ファイル名に対してループ処理を行います。
            foreach (string inputFilename in input)
            {
                // このスプライトの名前を保存します。
                string spriteName = Path.GetFileNameWithoutExtension(inputFilename);

                spriteSheet.SpriteNames.Add(spriteName, sourceSprites.Count);

                // スプライト テクスチャーをメモリーに読み込みます。
                ExternalReference<TextureContent> textureReference =
                                new ExternalReference<TextureContent>(inputFilename);

                TextureContent texture =
                    context.BuildAndLoadAsset<TextureContent,
                                              TextureContent>(textureReference, "TextureProcessor");

                sourceSprites.Add(texture.Faces[0][0]);
            }

            // すべてのスプライトを単一の大きなテクスチャーにパックします。
            BitmapContent packedSprites = SpritePacker.PackSprites(sourceSprites,
                                                spriteSheet.SpriteRectangles, context);

            spriteSheet.Texture.Mipmaps.Add(packedSprites);

            return spriteSheet;
        }
    }
}
