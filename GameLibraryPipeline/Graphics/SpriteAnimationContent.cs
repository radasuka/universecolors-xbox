﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace GameLibrary.Content.Pipeline
{
    /// <summary>
    /// <see cref="SpriteAnimationProcessor"/>からの出力データを保持するために使用する
    /// ビルド時のタイプ。
    /// これは XNB フォーマットにシリアル化され、実行時に、<see cref="ContentManager"/>
    /// がデータを<see cref="SpriteAnimationShape"/>オブジェクトに読み込みます。
    /// </summary>
    [ContentSerializerRuntimeType("GameLibrary.Graphics.SpriteAnimationShape, GameLibrary")]
    public class SpriteAnimationContent
    {
        // アニメーション クリップのリスト。
        public List<SpriteSheetContent> AnimationClips = new List<SpriteSheetContent>();

        // 名前でアニメーション クリップを検索できるように、元のスプライト シート
        // ファイル名を保存します。
        public Dictionary<string, int> ClipNames = new Dictionary<string, int>();
    }
}