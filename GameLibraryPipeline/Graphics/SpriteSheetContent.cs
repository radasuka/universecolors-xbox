#region File Description
//-----------------------------------------------------------------------------
// SpriteSheetContent.cs
//
// Microsoft Game Technology Group
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
#endregion

namespace GameLibrary.Content.Pipeline
{
    /// <summary>
    /// SpriteSheetProcessor からの出力データを保持するために使用する
    /// ビルド時のタイプ。
    /// これは XNB フォーマットにシリアル化され、実行時に、ContentManager が
    /// データを SpriteSheet オブジェクトに読み込みます。
    /// </summary>
    [ContentSerializerRuntimeType("GameLibrary.Graphics.SpriteSheet, GameLibrary")]
    public class SpriteSheetContent
    {
        // 単一のテクスチャーには、多くの別個のスプライト イメージが
        // 含まれています。
        public Texture2DContent Texture = new Texture2DContent();

        // 各スプライトがテクスチャー内に配置された場所を忘れないでください。
        public List<Rectangle> SpriteRectangles = new List<Rectangle>();

        // 名前でスプライトを検索できるように、元のスプライト ファイル名を
        // 保存します。
        public Dictionary<string, int> SpriteNames = new Dictionary<string, int>();
    }
}
