﻿using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;

namespace UniverseColors.Content.Pipeline
{
    public class SkyTubeContent
    {
        public ModelContent Model;
        public TextureContent Texture;
    }
}
