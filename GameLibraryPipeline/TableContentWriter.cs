using System.Collections.Generic;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

namespace GameLibrary.Content.Pipeline
{
    /// <summary>
    /// このクラスは XNA Framework コンテンツ パイプラインによってインスタンス化され、
    /// 指定された<see cref="TableContent"/>型を xnb 形式のバイナリ ファイルに書き出します。
    /// </summary>
    [ContentTypeWriter]
    public class TableContentWriter : ContentTypeWriter<TableContent>
    {
        /// <summary>
        /// <see cref="TableContent"/>オブジェクトをバイナリ フォーマットにコンパイルします。
        /// </summary>
        /// <param name="output">値をシリアル化するコンテンツ ライタ。</param>
        /// <param name="value">書き込む値。</param>
        protected override void Write(ContentWriter output, TableContent value)
        {
            output.WriteObject<List<List<string>>>(value.Rows);
        }

        /// <summary>
        /// <see cref="TableContent"/>のランタイム ローダーのアセンブリ修飾名を取得します。
        /// </summary>
        /// <param name="targetPlatform">プラットフォームの名前。</param>
        /// <returns>ランタイム ローダーの名前。</returns>
        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "GameLibrary.TableReader, GameLibrary";
        }
    }
}
