#region File Description
//-----------------------------------------------------------------------------
// Layout.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements

using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GameDebugTools
{
    /// <summary>
    /// レイアウトのための整列。
    /// </summary>
    [Flags]
    public enum Alignment
    {
        None = 0,

        // 水平方向のレイアウト。
        Left = 1,
        Right = 2,
        HorizontalCenter = 4,

        // 垂直方向のレイアウト。
        Top = 8,
        Bottom = 16,
        VerticalCenter = 32,

        // 組み合わせ。
        TopLeft = Top | Left,
        TopRight = Top | Right,
        TopCenter = Top | HorizontalCenter,

        BottomLeft = Bottom | Left,
        BottomRight = Bottom | Right,
        BottomCenter = Bottom | HorizontalCenter,

        CenterLeft = VerticalCenter | Left,
        CenterRight = VerticalCenter | Right,
        Center = VerticalCenter | HorizontalCenter
    }

    /// <summary>
    /// タイトル セーフ領域をサポートするレイアウト クラス。
    /// </summary>
    /// <remarks>
    /// マルチプラットフォームのゲームを開発するときには、さまざまな解像度を
    /// サポートする必要があります。また、Xbox 360 ゲームでは
    /// タイトル セーフ領域をサポートする必要があります。
    /// 
    /// この構造体は、指定された矩形に、指定された整列と余白を配置します。
    /// これは、セーフ領域を持つレイアウト領域 (クライアント領域) に基づいて
    /// 行われます。
    /// 
    /// 余白はクライアント領域のサイズに対する割合で示します。
    /// 
    /// 例
    /// 
    /// Place( region, 0.1f, 0.2f, Aligment.TopLeft );
    /// 
    /// region を、クライアント領域の左側から 10%、
    /// クライアント領域の上部から 20% の位置に配置します。
    /// 
    /// 
    /// Place( region, 0.3f, 0.4f, Aligment.BottomRight );
    /// 
    /// region を、クライアント領域の右側から 30%、
    /// クライアント領域の下部から 40% の位置に配置します。
    /// 
    /// 
    /// クライアント領域とセーフ領域は、個別に指定できます。
    /// これは、画面分割型のゲームで便利です。画面分割はクライアント領域に
    /// 基づいて行われ、同時にセーフ領域が考慮されます。
    /// 
    /// </remarks>
    public struct Layout
    {
        #region Fields

        /// <summary>
        /// クライアント領域を取得または設定します。
        /// </summary>
        public Rectangle ClientArea;

        /// <summary>
        /// セーフ領域を取得または設定します。
        /// </summary>
        public Rectangle SafeArea;

        #endregion

        #region Initialization

        /// <summary>
        /// クライアント領域とセーフ領域の両方を指定することで、
        /// レイアウト オブジェクトを構築します。
        /// </summary>
        /// <param name="client">クライアント領域。</param>
        /// <param name="safeArea">セーフ領域。</param>
        public Layout(Rectangle clientArea, Rectangle safeArea)
        {
            ClientArea = clientArea;
            SafeArea = safeArea;
        }

        /// <summary>
        /// クライアント領域を指定することで、レイアウト オブジェクトを
        /// 構築します。
        /// セーフ領域はクライアント領域と同じサイズになります。
        /// </summary>
        /// <param name="client">クライアント領域。</param>
        public Layout(Rectangle clientArea)
            : this(clientArea, clientArea)
        {
        }

        /// <summary>
        /// ビューポートを指定することで、レイアウト オブジェクトを構築します。
        /// セーフ領域は Viewport.TitleSafeArea と同じになります。
        /// </summary>
        public Layout(Viewport viewport)
        {
            ClientArea = new Rectangle((int)viewport.X, (int)viewport.Y,
                                        (int)viewport.Width, (int)viewport.Height);
            SafeArea = viewport.TitleSafeArea;
        }

        #endregion

        /// <summary>
        /// 指定された領域をレイアウト設定します。
        /// </summary>
        public Vector2 Place(Vector2 size, float horizontalMargin,
                                            float verticalMargine, Alignment alignment)
        {
            Rectangle rc = new Rectangle(0, 0, (int)size.X, (int)size.Y);
            rc = Place(rc, horizontalMargin, verticalMargine, alignment);
            return new Vector2(rc.X, rc.Y);
        }

        /// <summary>
        /// 指定された領域をレイアウト設定します。
        /// </summary>
        /// <param name="region">配置する矩形。</param>
        /// <returns>配置された矩形。</returns>
        public Rectangle Place(Rectangle region, float horizontalMargin,
                                            float verticalMargine, Alignment alignment)
        {
            // 水平方向のレイアウト。
            if ((alignment & Alignment.Left) != 0)
            {
                region.X = ClientArea.X + (int)(ClientArea.Width * horizontalMargin);
            }
            else if ((alignment & Alignment.Right) != 0)
            {
                region.X = ClientArea.X +
                            (int)(ClientArea.Width * (1.0f - horizontalMargin)) -
                            region.Width;
            }
            else if ((alignment & Alignment.HorizontalCenter) != 0)
            {
                region.X = ClientArea.X + (ClientArea.Width - region.Width) / 2 +
                            (int)(horizontalMargin * ClientArea.Width);
            }
            else
            {
                // レイアウト設定しません。
            }

            // 垂直方向のレイアウト。
            if ((alignment & Alignment.Top) != 0)
            {
                region.Y = ClientArea.Y + (int)(ClientArea.Height * verticalMargine);
            }
            else if ((alignment & Alignment.Bottom) != 0)
            {
                region.Y = ClientArea.Y +
                            (int)(ClientArea.Height * (1.0f - verticalMargine)) -
                            region.Height;
            }
            else if ((alignment & Alignment.VerticalCenter) != 0)
            {
                region.Y = ClientArea.Y + (ClientArea.Height - region.Height) / 2 +
                            (int)(verticalMargine * ClientArea.Height);
            }
            else
            {
                // レイアウト設定しません。
            }

            // レイアウト領域がセーフ領域内にあるようにします。
            if (region.Left < SafeArea.Left)
                region.X = SafeArea.Left;

            if (region.Right > SafeArea.Right)
                region.X = SafeArea.Right - region.Width;

            if (region.Top < SafeArea.Top)
                region.Y = SafeArea.Top;

            if (region.Bottom > SafeArea.Bottom)
                region.Y = SafeArea.Bottom - region.Height;

            return region;
        }

    }
}
