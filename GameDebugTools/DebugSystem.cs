#region File Description
//-----------------------------------------------------------------------------
// DebugSystem.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

/*
 * GameDebugTools を開始するには、メイン ゲーム クラスに移動し、
 Initialize メソッドをオーバーライドして、
 * 次のコード行を追加します。
 * 
 * GameDebugTools.DebugSystem.Initialize(this, "MyFont");
 * 
 * ここで、"MyFont" はコンテンツ プロジェクト内の SpriteFont の名前です。このメソッドは、すべてのデバッグ ツールを初期化し、必要なコンポーネントをゲームに
 * 追加します。ゲームの計測を開始するには、Update メソッドの先頭に次のコード行
 * を追加します。
 *
 * GameDebugTools.DebugSystem.Instance.TimeRuler.StartFrame()
 * 
 * 上記のコードを配置したら、TimeRuler の BeginMark および EndMark 呼び出しでコードを囲むことによって、ゲーム全体にマーカーを追加できます。
 * 次に例を示します。
 * 
 * GameDebugTools.DebugSystem.Instance.TimeRuler.BeginMark("SomeCode", Color.Blue);
 * // ここにコードを挿入します。
 * GameDebugTools.DebugSystem.Instance.TimeRuler.EndMark("SomeCode");
 * 
 * 次に、TimeRuler の Visible プロパティを true に設定することで、これらの結果を表示できます。これにより、最適化のためのゲームのプロファイリングに使用できる
 * 視覚表示が得られます。
 *
 * GameDebugTools には FpsCounter および DebugCommandUI も付属しており、実行時に
 * コマンドを入力したりさまざまな表示を切り替えたりできるほか、独自のコマンドを登録して、再起動せずにゲームを変更できます。
 */

using Microsoft.Xna.Framework;

namespace GameDebugTools
{
    /// <summary>
    /// DebugSystem は、さまざまな GameDebug 要素の作成を効率化する
    /// ヘルパー クラスです。ゲームでは使用する要素のみを自由に追加できますが、
    /// DebugSystem を使用すると、Initialize メソッドを呼び出すことで、ゲームは
    /// すべてのコンポーネントをすばやく作成および追加できます。
    /// </summary>
    public class DebugSystem
    {
        private static DebugSystem singletonInstance;

        /// <summary>
        /// デバッグ システムの単一インスタンスを取得します。Initialize を
        /// 呼び出して、インスタンスを作成する必要があります。
        /// </summary>
        public static DebugSystem Instance
        {
            get { return singletonInstance; }
        }

        /// <summary>
        /// システムの DebugManager を取得します。
        /// </summary>
        public DebugManager DebugManager { get; private set; }

        /// <summary>
        /// システムの DebugCommandUI を取得します。
        /// </summary>
        public DebugCommandUI DebugCommandUI { get; private set; }

        /// <summary>
        /// システムの FpsCounter を取得します。
        /// </summary>
        public FpsCounter FpsCounter { get; private set; }

        /// <summary>
        /// システムの TimeRuler を取得します。
        /// </summary>
        public TimeRuler TimeRuler { get; private set; }

#if !WINDOWS_PHONE
        /// <summary>
        /// システムの RemoteDebugCommand を取得します。
        /// </summary>
        public RemoteDebugCommand RemoteDebugCommand { get; private set; }
#endif

        /// <summary>
        /// DebugSystem を初期化し、すべてのコンポーネントをゲームの
        /// Components コレクションに追加します。
        /// </summary>
        /// <param name="game">DebugSystem を使用するゲーム。</param>
        /// <param name="debugFont">DebugSystem が使用するフォント。</param>
        /// <returns>ゲームで使用する DebugSystem。</returns>
        public static DebugSystem Initialize(Game game, string debugFont)
        {
            // 単一インスタンスが存在する場合は、それを返します。
            // 1 つのゲームに対して 2 つのシステムを作成しないようにします。
            if (singletonInstance != null)
                return singletonInstance;

            // システムを作成します。
            singletonInstance = new DebugSystem();

            // すべてのシステム コンポーネントを作成します。
            singletonInstance.DebugManager = new DebugManager(game, debugFont);
            game.Components.Add(singletonInstance.DebugManager);

            singletonInstance.DebugCommandUI = new DebugCommandUI(game);
            game.Components.Add(singletonInstance.DebugCommandUI);

            singletonInstance.FpsCounter = new FpsCounter(game);
            game.Components.Add(singletonInstance.FpsCounter);

            singletonInstance.TimeRuler = new TimeRuler(game);
            game.Components.Add(singletonInstance.TimeRuler);

#if !WINDOWS_PHONE
            singletonInstance.RemoteDebugCommand = new RemoteDebugCommand(game);
            game.Components.Add(singletonInstance.RemoteDebugCommand);
#endif

            return singletonInstance;
        }

        // プライベート コンストラクター。ゲームは Initialize を使用する
        // 必要があります。
        private DebugSystem() { }
    }
}
