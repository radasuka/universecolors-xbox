#region File Description
//-----------------------------------------------------------------------------
// IDebugCommandHost.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements

using System.Collections.Generic;

#endregion

namespace GameDebugTools
{
    /// <summary>
    /// デバッグ コマンドのメッセージの種類。
    /// </summary>
    public enum DebugCommandMessage
    {
        Standard = 1,
        Error = 2,
        Warning = 3
    }

    /// <summary>
    /// デバッグ コマンドの実行デリゲート。
    /// </summary>
    /// <param name="host">コマンドを実行するホスト。</param>
    /// <param name="command">コマンド名。</param>
    /// <param name="arguments">コマンド引数。</param>
    public delegate void DebugCommandExecute(IDebugCommandHost host, string command,
                                                            IList<string> arguments);

    /// <summary>
    /// デバッグ コマンド実行機能のインターフェイス。
    /// </summary>
    public interface IDebugCommandExecutioner
    {
        /// <summary>
        /// 実行コマンド。
        /// </summary>
        void ExecuteCommand(string command);
    }

    /// <summary>
    /// デバッグ コマンドのメッセージ リスナーのインターフェイス。
    /// </summary>
    public interface IDebugEchoListner
    {
        /// <summary>
        /// 出力メッセージ。
        /// </summary>
        /// <param name="messageType">メッセージの種類。</param>
        /// <param name="text">メッセージ テキスト。</param>
        void Echo(DebugCommandMessage messageType, string text);
    }

    /// <summary>
    /// デバッグ コマンド ホストのインターフェイス。
    /// </summary>
    public interface IDebugCommandHost : IDebugEchoListner, IDebugCommandExecutioner
    {
        /// <summary>
        /// 新しいコマンドを登録します。
        /// </summary>
        /// <param name="command">コマンド名。</param>
        /// <param name="description">コマンドの説明。</param>
        /// <param name="callback">実行デリゲート。</param>
        void RegisterCommand(string command, string description,
                                                        DebugCommandExecute callback);

        /// <summary>
        /// コマンドを登録解除します。
        /// </summary>
        /// <param name="command">コマンド名。</param>
        void UnregisterCommand(string command);

        /// <summary>
        /// 標準メッセージを出力します。
        /// </summary>
        /// <param name="text"></param>
        void Echo(string text);

        /// <summary>
        /// 警告メッセージを出力します。
        /// </summary>
        /// <param name="text"></param>
        void EchoWarning(string text);

        /// <summary>
        /// エラー メッセージを出力します。
        /// </summary>
        /// <param name="text"></param>
        void EchoError(string text);

        /// <summary>
        /// メッセージ リスナーを登録します。
        /// </summary>
        /// <param name="listner"></param>
        void RegisterEchoListner(IDebugEchoListner listner);

        /// <summary>
        /// メッセージ リスナーを登録解除します。
        /// </summary>
        /// <param name="listner"></param>
        void UnregisterEchoListner(IDebugEchoListner listner);

        /// <summary>
        /// コマンド実行機能を追加します。
        /// </summary>
        void PushExecutioner(IDebugCommandExecutioner executioner);

        /// <summary>
        /// コマンド実行機能を削除します。
        /// </summary>
        void PopExecutioner();
    }

}
