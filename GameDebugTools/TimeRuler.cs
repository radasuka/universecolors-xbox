#region File Description
//-----------------------------------------------------------------------------
// TimeRuler.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GameDebugTools
{
    /// <summary>
    /// リアルタイムの CPU 測定ツール
    /// </summary>
    /// <remarks>
    /// このツールを使用することで、ボトルネックを視覚的に発見し、CPU ジョブを
    /// どれだけ追加できるかを知ることができます。
    /// これはリアルタイムのプロファイリングなので、ゲーム内の誤作動を
    /// 発見することもできます。
    /// 
    /// TimeRuler は以下の機能を提供します。
    ///  * 最大 8 個のバー (構成可能)。
    ///  * マーカーごとのカラー変更。
    ///  * マーカー ログ。
    ///  * TRACE 定数を取り除くと、BeginMark メソッドおよび EndMark メソッドの
    /// 呼び出しを生成しない。
    ///  * 最大 32 回 (構成可能) の入れ子にした BeginMark メソッドの呼び出しを
    /// サポート。
    ///  * マルチスレッド セーフ。
    ///  * フレームの継続時間に基づいた表示フレームの自動的な変更。
    ///  
    /// 使用方法
    /// TimerRuler インスタンスを Game.Components に追加し、
    /// timerRuler.StartFrame をGame.Update メソッドの先頭で呼び出します。
    /// 
    /// 次に、測定したいコードを BeginMark および EndMark で囲みます。
    /// 
    /// timeRuler.BeginMark( "Update", Color.Blue );
    /// // 測定したい処理
    /// timerRuler.EndMark( "Update" );
    /// 
    /// また、マーカーのバー インデックスを指定できます (既定値は 0)。
    /// 
    /// timeRuler.BeginMark( 1, "Update", Color.Blue );
    /// 
    /// すべてのプロファイリング メソッドには CondionalAttribute に "TRACE" が
    /// 設定されています。
    /// "TRACE" 定数を指定しなかった場合は、BeginMark メソッドおよび
    /// EndMark メソッドを呼び出しません。
    /// したがって、ゲームのリリース時には "TRACE" 定数の削除を忘れないで
    /// ください。
    /// 
    /// </remarks>
    public class TimeRuler : DrawableGameComponent
    {
        #region Constants

        /// <summary>
        /// バーの最大数。
        /// </summary>
        const int MaxBars = 8;

        /// <summary>
        /// 各バーのサンプル最大数。
        /// </summary>
        const int MaxSamples = 256;

        /// <summary>
        /// 各バーの入れ子呼び出しの最大数。
        /// </summary>
        const int MaxNestCall = 32;

        /// <summary>
        /// 最大表示フレーム数。
        /// </summary>
        const int MaxSampleFrames = 4;

        /// <summary>
        /// ログのスナップ ショット採取期間 (フレーム数単位)。
        /// </summary>
        const int LogSnapDuration = 120;

        /// <summary>
        /// バーの高さ (ピクセル単位)。
        /// </summary>
        const int BarHeight = 8;

        /// <summary>
        /// バーのパディング (ピクセル単位)。
        /// </summary>
        const int BarPadding = 2;

        /// <summary>
        /// 表示フレーム自動調整の遅延フレーム数。
        /// </summary>
        const int AutoAdjustDelay = 30;

        #endregion

        #region Properties

        /// <summary>
        /// ログ表示の有無を取得または設定します。
        /// </summary>
        public bool ShowLog { get; set; }

        /// <summary>
        /// ターゲット サンプル フレームを取得または設定します。
        /// </summary>
        public int TargetSampleFrames { get; set; }

        /// <summary>
        /// TimeRuler のレンダリング位置を取得または設定します。
        /// </summary>
        public Vector2 Position { get { return position; } set { position = value; } }

        /// <summary>
        /// タイマー ルーラーの幅を取得または設定します。
        /// </summary>
        public int Width { get; set; }

        #endregion

        #region Fields

#if TRACE

        /// <summary>
        /// マーカーの構造体。
        /// </summary>
        private struct Marker
        {
            public int MarkerId;
            public float BeginTime;
            public float EndTime;
            public Color Color;
        }

        /// <summary>
        /// マーカーのコレクション。
        /// </summary>
        private class MarkerCollection
        {
            // マーカー コレクション。
            public Marker[] Markers = new Marker[MaxSamples];
            public int MarkCount;

            // マーカーの入れ子情報。
            public int[] MarkerNests = new int[MaxNestCall];
            public int NestCount;
        }

        /// <summary>
        /// フレーム ログ情報。
        /// </summary>
        private class FrameLog
        {
            public MarkerCollection[] Bars;

            public FrameLog()
            {
                // マーカーを初期化します。
                Bars = new MarkerCollection[MaxBars];
                for (int i = 0; i < MaxBars; ++i)
                    Bars[i] = new MarkerCollection();
            }
        }

        /// <summary>
        /// マーカー情報。
        /// </summary>
        private class MarkerInfo
        {
            // マーカー名。
            public string Name;

            // マーカー ログ。
            public MarkerLog[] Logs = new MarkerLog[MaxBars];

            public MarkerInfo(string name)
            {
                Name = name;
            }
        }

        /// <summary>
        /// マーカー ログ情報。
        /// </summary>
        private struct MarkerLog
        {
            public float SnapMin;
            public float SnapMax;
            public float SnapAvg;

            public float Min;
            public float Max;
            public float Avg;

            public int Samples;

            public Color Color;

            public bool Initialized;
        }

        // デバッグ マネージャーの参照。
        DebugManager debugManager;

        // 各フレームのログ。
        FrameLog[] logs;

        // 前回のフレームのログ。
        FrameLog prevLog;

        // 現在のログ。
        FrameLog curLog;

        // 現在のフレーム数。
        int frameCount;

        // 時間測定のストップウォッチ。
        Stopwatch stopwatch = new Stopwatch();

        // マーカー情報配列。
        List<MarkerInfo> markers = new List<MarkerInfo>();

        // マーカー名からマーカー ID にマップする辞書。
        Dictionary<string, int> markerNameToIdMap = new Dictionary<string, int>();

        // 表示フレーム調整カウンター。
        int frameAdjust;

        // 現在の表示フレーム数。
        int sampleFrames;

        // マーカー ログ文字列。
        StringBuilder logString = new StringBuilder(512);

        // StartFrame を Game.Update メソッドの先頭で呼び出します。
        // ただし、ゲームが固定タイム ステップ モードで低速で実行されると、
        // Game.Update の呼び出しが複数回行われます。
        // この場合、StartFrame の呼び出しを無視する必要があります。
        // そのために、StartFrame の呼び出し回数を Draw が呼び出されるまで
        // 追跡します。
        int updateCount;

#endif
        // TimerRuler の描画位置。
        Vector2 position;

        #endregion

        #region Initialization

        public TimeRuler(Game game)
            : base(game)
        {
            // これをサービスとして追加します。
            Game.Services.AddService(typeof(TimeRuler), this);
        }

        public override void Initialize()
        {
#if TRACE
            debugManager =
                Game.Services.GetService(typeof(DebugManager)) as DebugManager;

            if (debugManager == null)
                throw new InvalidOperationException("DebugManager is not registered.");

            // DebugCommandHost が登録されている場合は、"tr" コマンドを
            // 追加します。
            IDebugCommandHost host =
                                Game.Services.GetService(typeof(IDebugCommandHost))
                                                                    as IDebugCommandHost;
            if (host != null)
            {
                host.RegisterCommand("tr", "TimeRuler", this.CommandExecute);
                this.Visible = true;
            }

            // パラメーターを初期化します。
            logs = new FrameLog[2];
            for (int i = 0; i < logs.Length; ++i)
                logs[i] = new FrameLog();

            sampleFrames = TargetSampleFrames = 1;

            // TimeRuler の更新メソッドを呼び出す必要はありません。
            this.Enabled = false;
#endif
            base.Initialize();
        }

        protected override void LoadContent()
        {
            Width = (int)(GraphicsDevice.Viewport.Width * 0.8f);

            Layout layout = new Layout(GraphicsDevice.Viewport);
            position = layout.Place(new Vector2(Width, BarHeight),
                                                    0, 0.01f, Alignment.BottomCenter);

            base.LoadContent();
        }

#if TRACE
        /// <summary>
        /// 'tr' コマンドの実行。
        /// </summary>
        void CommandExecute(IDebugCommandHost host, string command,
                                                                IList<string> arguments)
        {
            bool previousVisible = Visible;

            if (arguments.Count == 0)
                Visible = !Visible;

            char[] subArgSeparator = new[] { ':' };
            foreach (string orgArg in arguments)
            {
                string arg = orgArg.ToLower();
                string[] subargs = arg.Split(subArgSeparator);
                switch (subargs[0])
                {
                    case "on":
                        Visible = true;
                        break;
                    case "off":
                        Visible = false;
                        break;
                    case "reset":
                        ResetLog();
                        break;
                    case "log":
                        if (subargs.Length > 1)
                        {
                            if (String.Compare(subargs[1], "on") == 0)
                                ShowLog = true;
                            if (String.Compare(subargs[1], "off") == 0)
                                ShowLog = false;
                        }
                        else
                        {
                            ShowLog = !ShowLog;
                        }
                        break;
                    case "frame":
                        int a = Int32.Parse(subargs[1]);
                        a = Math.Max(a, 1);
                        a = Math.Min(a, MaxSampleFrames);
                        TargetSampleFrames = a;
                        break;
                    case "/?":
                    case "--help":
                        host.Echo("tr [log|on|off|reset|frame]");
                        host.Echo("Options:");
                        host.Echo("       on     Display TimeRuler.");
                        host.Echo("       off    Hide TimeRuler.");
                        host.Echo("       log    Show/Hide marker log.");
                        host.Echo("       reset  Reset marker log.");
                        host.Echo("       frame:sampleFrames");
                        host.Echo("              Change target sample frame count");
                        break;
                    default:
                        break;
                }
            }

            // Visible の状態が変更された場合は、更新数をリセットします。
            if (Visible != previousVisible)
            {
                Interlocked.Exchange(ref updateCount, 0);
            }
        }
#endif

        #endregion

        #region Measuring methods

        /// <summary>
        /// 新しいフレームを開始します。
        /// </summary>
        [Conditional("TRACE")]
        public void StartFrame()
        {
#if TRACE
            lock (this)
            {
                // このメソッドが複数回呼び出されると、フレームのリセットを
                // スキップします。
                int count = Interlocked.Increment(ref updateCount);
                if (Visible && (1 < count && count < MaxSampleFrames))
                    return;

                // 現在のフレーム ログを更新します。
                prevLog = logs[frameCount++ & 0x1];
                curLog = logs[frameCount & 0x1];

                float endFrameTime = (float)stopwatch.Elapsed.TotalMilliseconds;

                // マーカーを更新し、ログを作成します。
                for (int barIdx = 0; barIdx < prevLog.Bars.Length; ++barIdx)
                {
                    MarkerCollection prevBar = prevLog.Bars[barIdx];
                    MarkerCollection nextBar = curLog.Bars[barIdx];

                    // 前回のフレームで EndMark の呼び出しがなかったマーカーを
                    // 再度開きます。
                    for (int nest = 0; nest < prevBar.NestCount; ++nest)
                    {
                        int markerIdx = prevBar.MarkerNests[nest];

                        prevBar.Markers[markerIdx].EndTime = endFrameTime;

                        nextBar.MarkerNests[nest] = nest;
                        nextBar.Markers[nest].MarkerId =
                            prevBar.Markers[markerIdx].MarkerId;
                        nextBar.Markers[nest].BeginTime = 0;
                        nextBar.Markers[nest].EndTime = -1;
                        nextBar.Markers[nest].Color = prevBar.Markers[markerIdx].Color;
                    }

                    // マーカー ログを更新します。
                    for (int markerIdx = 0; markerIdx < prevBar.MarkCount; ++markerIdx)
                    {
                        float duration = prevBar.Markers[markerIdx].EndTime -
                                            prevBar.Markers[markerIdx].BeginTime;

                        int markerId = prevBar.Markers[markerIdx].MarkerId;
                        MarkerInfo m = markers[markerId];

                        m.Logs[barIdx].Color = prevBar.Markers[markerIdx].Color;

                        if (!m.Logs[barIdx].Initialized)
                        {
                            // 最初のフレーム処理。
                            m.Logs[barIdx].Min = duration;
                            m.Logs[barIdx].Max = duration;
                            m.Logs[barIdx].Avg = duration;

                            m.Logs[barIdx].Initialized = true;
                        }
                        else
                        {
                            // 最初のフレーム後の処理。
                            m.Logs[barIdx].Min = Math.Min(m.Logs[barIdx].Min, duration);
                            m.Logs[barIdx].Max = Math.Min(m.Logs[barIdx].Max, duration);
                            m.Logs[barIdx].Avg += duration;
                            m.Logs[barIdx].Avg *= 0.5f;

                            if (m.Logs[barIdx].Samples++ >= LogSnapDuration)
                            {
                                m.Logs[barIdx].SnapMin = m.Logs[barIdx].Min;
                                m.Logs[barIdx].SnapMax = m.Logs[barIdx].Max;
                                m.Logs[barIdx].SnapAvg = m.Logs[barIdx].Avg;
                                m.Logs[barIdx].Samples = 0;
                            }
                        }
                    }

                    nextBar.MarkCount = prevBar.NestCount;
                    nextBar.NestCount = prevBar.NestCount;
                }

                // 測定を開始します。
                stopwatch.Reset();
                stopwatch.Start();
            }
#endif
        }

        /// <summary>
        /// 時間測定を開始します。
        /// </summary>
        /// <param name="markerName">マーカー名。</param>
        /// <param name="color">カラー</param>
        [Conditional("TRACE")]
        public void BeginMark(string markerName, Color color)
        {
#if TRACE
            BeginMark(0, markerName, color);
#endif
        }

        /// <summary>
        /// 時間測定を開始します。
        /// </summary>
        /// <param name="barIndex">バーのインデックス。</param>
        /// <param name="markerName">マーカー名。</param>
        /// <param name="color">カラー</param>
        [Conditional("TRACE")]
        public void BeginMark(int barIndex, string markerName, Color color)
        {
#if TRACE
            lock (this)
            {
                if (barIndex < 0 || barIndex >= MaxBars)
                    throw new ArgumentOutOfRangeException("barIndex");

                MarkerCollection bar = curLog.Bars[barIndex];

                if (bar.MarkCount >= MaxSamples)
                {
                    throw new OverflowException(
                        "Exceeded sample count.\n" +
                        "Either set larger number to TimeRuler.MaxSmpale or" +
                        "lower sample count.");
                }

                if (bar.NestCount >= MaxNestCall)
                {
                    throw new OverflowException(
                        "Exceeded nest count.\n" +
                        "Either set larget number to TimeRuler.MaxNestCall or" +
                        "lower nest calls.");
                }

                // 登録済みマーカーを取得します。
                int markerId;
                if (!markerNameToIdMap.TryGetValue(markerName, out markerId))
                {
                    // このマーカーが登録されていない場合は、これを登録します。
                    markerId = markers.Count;
                    markerNameToIdMap.Add(markerName, markerId);
                    markers.Add(new MarkerInfo(markerName));
                }

                // 測定を開始します。
                bar.MarkerNests[bar.NestCount++] = bar.MarkCount;

                // マーカー パラメーターに値を設定します。
                bar.Markers[bar.MarkCount].MarkerId = markerId;
                bar.Markers[bar.MarkCount].Color = color;
                bar.Markers[bar.MarkCount].BeginTime =
                                        (float)stopwatch.Elapsed.TotalMilliseconds;

                bar.Markers[bar.MarkCount].EndTime = -1;

                bar.MarkCount++;
            }
#endif
        }

        /// <summary>
        /// 測定を終了します。
        /// </summary>
        /// <param name="markerName">マーカー名。</param>
        [Conditional("TRACE")]
        public void EndMark(string markerName)
        {
#if TRACE
            EndMark(0, markerName);
#endif
        }

        /// <summary>
        /// 測定を終了します。
        /// </summary>
        /// <param name="barIndex">バーのインデックス。</param>
        /// <param name="markerName">マーカー名。</param>
        [Conditional("TRACE")]
        public void EndMark(int barIndex, string markerName)
        {
#if TRACE
            lock (this)
            {
                if (barIndex < 0 || barIndex >= MaxBars)
                    throw new ArgumentOutOfRangeException("barIndex");

                MarkerCollection bar = curLog.Bars[barIndex];

                if (bar.NestCount <= 0)
                {
                    throw new InvalidOperationException(
                        "Call BeingMark method before call EndMark method.");
                }

                int markerId;
                if (!markerNameToIdMap.TryGetValue(markerName, out markerId))
                {
                    throw new InvalidOperationException(
                        String.Format("Maker '{0}' is not registered." +
                            "Make sure you specifed same name as you used for BeginMark" +
                            " method.",
                            markerName));
                }

                int markerIdx = bar.MarkerNests[--bar.NestCount];
                if (bar.Markers[markerIdx].MarkerId != markerId)
                {
                    throw new InvalidOperationException(
                    "Incorrect call order of BeginMark/EndMark method." +
                    "You call it like BeginMark(A), BeginMark(B), EndMark(B), EndMark(A)" +
                    " But you can't call it like " +
                    "BeginMark(A), BeginMark(B), EndMark(A), EndMark(B).");
                }

                bar.Markers[markerIdx].EndTime =
                    (float)stopwatch.Elapsed.TotalMilliseconds;
            }
#endif
        }

        /// <summary>
        /// 与えられたバー インデックスとマーカー名の平均時間を取得します。
        /// </summary>
        /// <param name="barIndex">バーのインデックス。</param>
        /// <param name="markerName">マーカー名。</param>
        /// <returns>平均経過時間 (ミリ秒単位)。</returns>
        public float GetAverageTime(int barIndex, string markerName)
        {
#if TRACE
            if (barIndex < 0 || barIndex >= MaxBars)
                throw new ArgumentOutOfRangeException("barIndex");

            float result = 0;
            int markerId;
            if (markerNameToIdMap.TryGetValue(markerName, out markerId))
                result = markers[markerId].Logs[barIndex].Avg;

            return result;
#else
            return 0f;
#endif
        }

        /// <summary>
        /// マーカー ログをリセットします。
        /// </summary>
        [Conditional("TRACE")]
        public void ResetLog()
        {
#if TRACE
            lock (this)
            {
                foreach (MarkerInfo markerInfo in markers)
                {
                    for (int i = 0; i < markerInfo.Logs.Length; ++i)
                    {
                        markerInfo.Logs[i].Initialized = false;
                        markerInfo.Logs[i].SnapMin = 0;
                        markerInfo.Logs[i].SnapMax = 0;
                        markerInfo.Logs[i].SnapAvg = 0;

                        markerInfo.Logs[i].Min = 0;
                        markerInfo.Logs[i].Max = 0;
                        markerInfo.Logs[i].Avg = 0;

                        markerInfo.Logs[i].Samples = 0;
                    }
                }
            }
#endif
        }

        #endregion

        #region Draw

        public override void Draw(GameTime gameTime)
        {

            Draw(position, Width);
            base.Draw(gameTime);
        }

        [Conditional("TRACE")]
        public void Draw(Vector2 position, int width)
        {
#if TRACE
            // 更新数をリセットします。
            Interlocked.Exchange(ref updateCount, 0);

            // SpriteBatch、SpriteFont、および WhiteTexture を DebugManager
            // から取得します。
            SpriteBatch spriteBatch = debugManager.SpriteBatch;
            SpriteFont font = debugManager.DebugFont;
            Texture2D texture = debugManager.WhiteTexture;

            // 描画するバーの数に基づいてサイズと位置を調整します。
            int height = 0;
            float maxTime = 0;
            foreach (MarkerCollection bar in prevLog.Bars)
            {
                if (bar.MarkCount > 0)
                {
                    height += BarHeight + BarPadding * 2;
                    maxTime = Math.Max(maxTime,
                                            bar.Markers[bar.MarkCount - 1].EndTime);
                }
            }

            // 表示フレームの自動調整。
            // たとえば、フレームの処理全体が 16.6 ミリ秒未満で終了しない場合は、
            // 表示フレームの継続時間を 33.3 ミリ秒として調整します。
            const float frameSpan = 1.0f / 60.0f * 1000f;
            float sampleSpan = (float)sampleFrames * frameSpan;

            if (maxTime > sampleSpan)
                frameAdjust = Math.Max(0, frameAdjust) + 1;
            else
                frameAdjust = Math.Min(0, frameAdjust) - 1;

            if (Math.Abs(frameAdjust) > AutoAdjustDelay)
            {
                sampleFrames = Math.Min(MaxSampleFrames, sampleFrames);
                sampleFrames =
                    Math.Max(TargetSampleFrames, (int)(maxTime / frameSpan) + 1);

                frameAdjust = 0;
            }

            // ミリ秒からピクセルに変換する係数を計算します。
            float msToPs = (float)width / sampleSpan;

            // 開始位置を描画します。
            int startY = (int)position.Y - (height - BarHeight);

            // 現在の y 位置。
            int y = startY;

            spriteBatch.Begin();

            // 透明度を持つ背景を描画します。
            Rectangle rc = new Rectangle((int)position.X, y, width, height);
            spriteBatch.Draw(texture, rc, new Color(0, 0, 0, 128));

            // 各バーのマーカーを描画します。
            rc.Height = BarHeight;
            foreach (MarkerCollection bar in prevLog.Bars)
            {
                rc.Y = y + BarPadding;
                if (bar.MarkCount > 0)
                {
                    for (int j = 0; j < bar.MarkCount; ++j)
                    {
                        float bt = bar.Markers[j].BeginTime;
                        float et = bar.Markers[j].EndTime;
                        int sx = (int)(position.X + bt * msToPs);
                        int ex = (int)(position.X + et * msToPs);
                        rc.X = sx;
                        rc.Width = Math.Max(ex - sx, 1);

                        spriteBatch.Draw(texture, rc, bar.Markers[j].Color);
                    }
                }

                y += BarHeight + BarPadding;
            }

            // グリッド ラインを描画します。
            // 各グリッドはミリ秒を表します。
            rc = new Rectangle((int)position.X, (int)startY, 1, height);
            for (float t = 1.0f; t < sampleSpan; t += 1.0f)
            {
                rc.X = (int)(position.X + t * msToPs);
                spriteBatch.Draw(texture, rc, Color.Gray);
            }

            // フレーム グリッドを描画します。
            for (int i = 0; i <= sampleFrames; ++i)
            {
                rc.X = (int)(position.X + frameSpan * (float)i * msToPs);
                spriteBatch.Draw(texture, rc, Color.White);
            }

            // ログを描画します。
            if (ShowLog)
            {
                // ログ文字列を生成します。
                y = startY - font.LineSpacing;
                logString.Length = 0;
                foreach (MarkerInfo markerInfo in markers)
                {
                    for (int i = 0; i < MaxBars; ++i)
                    {
                        if (markerInfo.Logs[i].Initialized)
                        {
                            if (logString.Length > 0)
                                logString.Append("\n");

                            logString.Append(" Bar ");
                            logString.AppendNumber(i);
                            logString.Append(" ");
                            logString.Append(markerInfo.Name);

                            logString.Append(" Avg.:");
                            logString.AppendNumber(markerInfo.Logs[i].SnapAvg);
                            logString.Append("ms ");

                            y -= font.LineSpacing;
                        }
                    }
                }

                // 背景のサイズを計算し、描画します。
                Vector2 size = font.MeasureString(logString);
                rc = new Rectangle((int)position.X, (int)y, (int)size.X + 12, (int)size.Y);
                spriteBatch.Draw(texture, rc, new Color(0, 0, 0, 128));

                // ログ文字列を描画します。
                spriteBatch.DrawString(font, logString,
                                        new Vector2(position.X + 12, y), Color.White);


                // ログ カラー ボックスを描画します。
                y += (int)((float)font.LineSpacing * 0.3f);
                rc = new Rectangle((int)position.X + 4, y, 10, 10);
                Rectangle rc2 = new Rectangle((int)position.X + 5, y + 1, 8, 8);
                foreach (MarkerInfo markerInfo in markers)
                {
                    for (int i = 0; i < MaxBars; ++i)
                    {
                        if (markerInfo.Logs[i].Initialized)
                        {
                            rc.Y = y;
                            rc2.Y = y + 1;
                            spriteBatch.Draw(texture, rc, Color.White);
                            spriteBatch.Draw(texture, rc2, markerInfo.Logs[i].Color);

                            y += font.LineSpacing;
                        }
                    }
                }


            }

            spriteBatch.End();
#endif
        }

        #endregion

    }
}
