#region File Description
//-----------------------------------------------------------------------------
// KeyboardUtils.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;

#endregion

namespace GameDebugTools
{
    /// <summary>
    /// キーボード入力のヘルパー クラス。
    /// </summary>
    public static class KeyboardUtils
    {
        #region Fields

        /// <summary>
        /// 文字ペアのクラス。通常の文字と、Shift キーを同時に押したときの文字を
        /// 保持します。
        /// </summary>
        class CharPair
        {
            public CharPair(char normalChar, Nullable<char> shiftChar)
            {
                this.NormalChar = normalChar;
                this.ShiftChar = shiftChar;
            }

            public char NormalChar;
            public Nullable<char> ShiftChar;
        }

        // キー: Keys、値: CharPair
        static private Dictionary<Keys, CharPair> keyMap =
                                                    new Dictionary<Keys, CharPair>();

        #endregion

        /// <summary>
        /// キー情報から文字を取得します。
        /// </summary>
        /// <param name="key">入力キー。</param>
        /// <param name="shitKeyPressed">Shift キーが押されているかどうか。
        /// </param>
        /// <param name="character">キー入力から変換された文字。</param>
        /// <returns>文字を取得したら true を返します。</returns>
        public static bool KeyToString(Keys key, bool shitKeyPressed,
                                                                    out char character)
        {
            bool result = false;
            character = ' ';
            CharPair charPair;

            if ((Keys.A <= key && key <= Keys.Z) || key == Keys.Space)
            {
                // A 〜 Z、または Space キーの場合は、そのまま使用します。
                character = (shitKeyPressed) ? (char)key : Char.ToLower((char)key);
                result = true;
            }
            else if (keyMap.TryGetValue(key, out charPair))
            {
                // それ以外の場合は、キー マップで変換します。
                if (!shitKeyPressed)
                {
                    character = charPair.NormalChar;
                    result = true;
                }
                else if (charPair.ShiftChar.HasValue)
                {
                    character = charPair.ShiftChar.Value;
                    result = true;
                }
            }

            return result;
        }

        #region Initialization

        static KeyboardUtils()
        {
            InitializeKeyMap();
        }

        /// <summary>
        /// 文字マップを初期化します。
        /// </summary>
        static void InitializeKeyMap()
        {
            // US キーボードの第 1 列。
            AddKeyMap(Keys.OemTilde, "`~");
            AddKeyMap(Keys.D1, "1!");
            AddKeyMap(Keys.D2, "2@");
            AddKeyMap(Keys.D3, "3#");
            AddKeyMap(Keys.D4, "4$");
            AddKeyMap(Keys.D5, "5%");
            AddKeyMap(Keys.D6, "6^");
            AddKeyMap(Keys.D7, "7&");
            AddKeyMap(Keys.D8, "8*");
            AddKeyMap(Keys.D9, "9(");
            AddKeyMap(Keys.D0, "0)");
            AddKeyMap(Keys.OemMinus, "-_");
            AddKeyMap(Keys.OemPlus, "=+");

            // US キーボードの第 2 列。
            AddKeyMap(Keys.OemOpenBrackets, "[{");
            AddKeyMap(Keys.OemCloseBrackets, "]}");
            AddKeyMap(Keys.OemPipe, "\\|");

            // US キーボードの第 3 列。
            AddKeyMap(Keys.OemSemicolon, ";:");
            AddKeyMap(Keys.OemQuotes, "'\"");
            AddKeyMap(Keys.OemComma, ",<");
            AddKeyMap(Keys.OemPeriod, ".>");
            AddKeyMap(Keys.OemQuestion, "/?");

            // US キーボードのキーパッド キー。
            AddKeyMap(Keys.NumPad1, "1");
            AddKeyMap(Keys.NumPad2, "2");
            AddKeyMap(Keys.NumPad3, "3");
            AddKeyMap(Keys.NumPad4, "4");
            AddKeyMap(Keys.NumPad5, "5");
            AddKeyMap(Keys.NumPad6, "6");
            AddKeyMap(Keys.NumPad7, "7");
            AddKeyMap(Keys.NumPad8, "8");
            AddKeyMap(Keys.NumPad9, "9");
            AddKeyMap(Keys.NumPad0, "0");
            AddKeyMap(Keys.Add, "+");
            AddKeyMap(Keys.Divide, "/");
            AddKeyMap(Keys.Multiply, "*");
            AddKeyMap(Keys.Subtract, "-");
            AddKeyMap(Keys.Decimal, ".");
        }

        /// <summary>
        ///　追加されたキーと文字マップ。
        /// </summary>
        /// <param name="key">キーボード キー</param>
        /// <param name="charPair">
        /// 文字。2 文字の場合、1 文字目は Shift キーを押していないときのもので、
        /// 2 文字目は Shift キーを押しているときのものです。</param>
        static void AddKeyMap(Keys key, string charPair)
        {
            char char1 = charPair[0];
            Nullable<char> char2 = null;
            if (charPair.Length > 1)
                char2 = charPair[1];

            keyMap.Add(key, new CharPair(char1, char2));
        }

        #endregion

    }

}
