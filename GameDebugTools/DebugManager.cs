#region File Description
//-----------------------------------------------------------------------------
// DebugManager.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

#endregion

namespace GameDebugTools
{
    /// <summary>
    /// デバッグ用のグラフィック リソースを保持する DebugManager クラス。
    /// </summary>
    public class DebugManager : DrawableGameComponent
    {
        // 読み込むフォントの名前。
        private string debugFont;

        #region Properties

        /// <summary>
        /// デバッグ用のスプライト バッチを取得します。
        /// </summary>
        public SpriteBatch SpriteBatch { get; private set; }

        /// <summary>
        /// 白のテクスチャーを取得します。
        /// </summary>
        public Texture2D WhiteTexture { get; private set; }

        /// <summary>
        /// デバッグ用の SpriteFont を取得します。
        /// </summary>
        public SpriteFont DebugFont { get; private set; }

        #endregion

        #region Initialize

        public DebugManager(Game game, string debugFont)
            : base(game)
        {
            // サービスとして追加します。
            Game.Services.AddService(typeof(DebugManager), this);
            this.debugFont = debugFont;

            // このコンポーネントは、更新と描画のどちらも呼び出す必要は
            // ありません。
            this.Enabled = false;
            this.Visible = false;
        }

        protected override void LoadContent()
        {
            // デバッグ コンテンツを読み込みます。
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            DebugFont = Game.Content.Load<SpriteFont>(debugFont);

            // 白のテクスチャーを作成します。
            WhiteTexture = new Texture2D(GraphicsDevice, 1, 1);
            Color[] whitePixels = new Color[] { Color.White };
            WhiteTexture.SetData<Color>(whitePixels);

            base.LoadContent();
        }

        #endregion
    }
}
