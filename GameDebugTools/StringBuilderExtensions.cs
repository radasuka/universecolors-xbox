#region File Description
//-----------------------------------------------------------------------------
// StringBuilderExtensions.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements

using System;
using System.Globalization;
using System.Text;

#endregion

namespace GameDebugTools
{
    /// <summary>
    /// StringBuilder 拡張メソッドのオプション。
    /// </summary>
    [Flags]
    public enum AppendNumberOptions
    {
        // 通常のフォーマット。
        None = 0,

        // 正の値に "+" 符号を追加します。
        PositiveSign = 1,

        // 番号グループの区切り文字を挿入します。
        // 使用する場合、3 桁ごとに "," を追加します。
        NumberGroup = 2,
    }

    /// <summary>
    /// 文字列ビルダー拡張メソッドの静的クラス。
    /// </summary>
    /// <remarks>
    /// XNA GS 3.0 から SpriteFont.DrawString に StringBuilder を指定できます。
    /// 不要なメモリー割り当てを節約できるようになります。
    /// 
    /// しかし、StringBuilder に数値を追加する際の問題は残ります。
    /// その 1 つは、StringBuilder.AppendFormat メソッドの使用時にボックス化が
    /// 発生することです。
    /// また、StringBuild.Append メソッドに int または float を指定すると、
    /// メモリー割り当てが発生するという問題もあります。
    /// 
    /// このクラスは、上記の問題の解決策を提供します。
    /// 
    /// すべてのメソッドは、StringBuilder の拡張メソッドとして定義されます。
    /// そのため、これらのメソッドを以下のように使用できます。
    /// 
    /// stringBuilder.AppendNumber(12345);
    /// 
    /// </remarks>
    public static class StringBuilderExtensions
    {
        #region Fields

        /// <summary>
        /// NumberFormat クラスの NumberGroupSizes のキャッシュ。
        /// </summary>
        static int[] numberGroupSizes =
            CultureInfo.CurrentCulture.NumberFormat.NumberGroupSizes;

        /// <summary>
        /// 文字列変換バッファー。
        /// </summary>
        static char[] numberString = new char[32];

        #endregion

        /// <summary>
        /// 整数値を文字列に変換し、文字列ビルダーに追加します。
        /// </summary>
        public static void AppendNumber(this StringBuilder builder, int number)
        {
            AppendNumbernternal(builder, number, 0, AppendNumberOptions.None);
        }

        /// <summary>
        /// 整数値を文字列に変換し、文字列ビルダーに追加します。
        /// </summary>
        /// <param name="number"></param>
        /// <param name="options">フォーマット オプション。</param>
        public static void AppendNumber(this StringBuilder builder, int number,
                                                            AppendNumberOptions options)
        {
            AppendNumbernternal(builder, number, 0, options);
        }

        /// <summary>
        /// float 値を文字列に変換し、文字列ビルダーに追加します。
        /// </summary>
        /// <remarks>小数点以下 2 桁を表示します。</remarks>
        public static void AppendNumber(this StringBuilder builder, float number)
        {
            AppendNumber(builder, number, 2, AppendNumberOptions.None);
        }

        /// <summary>
        /// float 値を文字列に変換し、文字列ビルダーに追加します。
        /// </summary>
        /// <remarks>小数点以下 2 桁を表示します。</remarks>
        public static void AppendNumber(this StringBuilder builder, float number,
                                                            AppendNumberOptions options)
        {
            AppendNumber(builder, number, 2, options);
        }

        /// <summary>
        /// float 値を文字列に変換し、文字列ビルダーに追加します。
        /// </summary>
        public static void AppendNumber(this StringBuilder builder, float number,
                                        int decimalCount, AppendNumberOptions options)
        {
            // NaN、Infinity の場合を処理します。
            if (float.IsNaN(number))
            {
                builder.Append("NaN");
            }
            else if (float.IsNegativeInfinity(number))
            {
                builder.Append("-Infinity");
            }
            else if (float.IsPositiveInfinity(number))
            {
                builder.Append("+Infinity");
            }
            else
            {
                int intNumber =
                        (int)(number * (float)Math.Pow(10, decimalCount) + 0.5f);

                AppendNumbernternal(builder, intNumber, decimalCount, options);
            }
        }


        static void AppendNumbernternal(StringBuilder builder, int number,
                                        int decimalCount, AppendNumberOptions options)
        {
            // 変換のための変数を初期化します。
            NumberFormatInfo nfi = CultureInfo.CurrentCulture.NumberFormat;

            int idx = numberString.Length;
            int decimalPos = idx - decimalCount;

            if (decimalPos == idx)
                decimalPos = idx + 1;

            int numberGroupIdx = 0;
            int numberGroupCount = numberGroupSizes[numberGroupIdx] + decimalCount;

            bool showNumberGroup = (options & AppendNumberOptions.NumberGroup) != 0;
            bool showPositiveSign = (options & AppendNumberOptions.PositiveSign) != 0;

            bool isNegative = number < 0;
            number = Math.Abs(number);

            // 最小の桁から変換します。
            do
            {
                // 小数点の記号を追加します (US では ".")。
                if (idx == decimalPos)
                {
                    numberString[--idx] = nfi.NumberDecimalSeparator[0];
                }

                // 番号グループの区切り文字を追加します (US では ",")。
                if (--numberGroupCount < 0 && showNumberGroup)
                {
                    numberString[--idx] = nfi.NumberGroupSeparator[0];

                    if (numberGroupIdx < numberGroupSizes.Length - 1)
                        numberGroupIdx++;

                    numberGroupCount = numberGroupSizes[numberGroupIdx] - 1;
                }

                // 現在の桁を文字に変換し、バッファーに追加します。
                numberString[--idx] = (char)('0' + (number % 10));
                number /= 10;

            } while (number > 0 || decimalPos <= idx);


            // 必要に応じて符号を追加します。
            if (isNegative)
            {
                numberString[--idx] = nfi.NegativeSign[0];
            }
            else if (showPositiveSign)
            {
                numberString[--idx] = nfi.PositiveSign[0];
            }

            // 変換された文字列を StringBuilder に追加します。
            builder.Append(numberString, idx, numberString.Length - idx);
        }

    }
}
