﻿using GameLibrary.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary
{
    /// <summary>
    /// バウンディングスフィアを描画(Debug用)
    /// </summary>
    public class RenderBoundingSphere : BasicItem
    {
        protected Matrix[] transforms;
        public Matrix[] Transforms
        {
            get { return transforms; }
        }

        public override Model Shape
        {
            get{ return base.Shape; }
            set
            {
                base.Shape = value;
                this.transforms = new Matrix[value.Bones.Count];
            }
        }

        public void Draw(GameTime gameTime, Matrix view, Matrix projection)
        {
            Shape.CopyAbsoluteBoneTransformsTo(transforms);
            foreach (ModelMesh mesh in Shape.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.World = transforms[mesh.ParentBone.Index] * World;
                    effect.DiffuseColor = Color.White.ToVector3();
                    // 追尾カメラの行列を使用します。
                    effect.View = view;
                    effect.Projection = projection;

                    effect.SpecularColor = new Vector3(0.4f, 0.4f, 0.4f);
                }
                mesh.Draw();
            }
        }
    }
}
