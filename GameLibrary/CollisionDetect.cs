﻿using System;
using Microsoft.Xna.Framework;

namespace GameLibrary
{
    public class CollisionDetect
    {
        /// <summary>
        /// パーティクル衝突判定・時刻・位置算出関数
        /// </summary>
        /// <param name="sourceRadius">パーティクルAの半径</param>
        /// <param name="targetRadius">パーティクルBの半径</param>
        /// <param name="prevSourcePos">パーティクルAの前の位置</param>
        /// <param name="sourcePos">パーティクルAの次の到達位置</param>
        /// <param name="prevTargetPos">パーティクルBの前位置</param>
        /// <param name="targetPos">パーティクルBの次の到達位置</param>
        /// <param name="collisionTime">衝突時間</param>
        /// <param name="collisionSourcePos">パーティクルAの衝突位置</param>
        /// <param name="collisionTargetPos">パーティクルAの衝突位置</param>
        /// <returns></returns>
        public static bool ParticleCollisionDetect(
            float sourceRadius, float targetRadius,
            ref Vector3 prevSourcePos, ref Vector3 sourcePos,
            ref Vector3 prevTargetPos, ref Vector3 targetPos,
            out float collisionTime,
            out Vector3 collisionSourcePos,
            out Vector3 collisionTargetPos)
        {
            collisionTime = 0;
            collisionSourcePos = Vector3.Zero;
            collisionTargetPos = Vector3.Zero;

            // 前位置及び到達位置におけるパーティクル間のベクトルを算出
            Vector3 C0 = prevTargetPos - prevSourcePos;
            Vector3 C1 = targetPos - sourcePos;
            Vector3 D = C1 - C0;

            // 衝突判定用の2次関数係数の算出
            float P = D.LengthSquared(); if (P == 0) return false; // 同じ方向に移動
            float Q = Vector3.Dot(C0, D);
            float R = C0.LengthSquared();

            // パーティクル距離
            float r = sourceRadius + targetRadius;

            // 衝突判定式
            float judge = Q * Q - P * (R - r * r);
            if (judge < 0)
            {
                // 衝突していない
                return false;
            }

            // 衝突時間の算出
            float t_plus = (-Q + (float)Math.Sqrt(judge)) / P;
            float t_minus = (-Q - (float)Math.Sqrt(judge)) / P;

            // 衝突時間が0未満1より大きい場合、衝突しない
            if ((t_plus < 0 || t_plus > 1) && (t_minus < 0 || t_minus > 1)) return false;

            // 衝突時間の決定（t_minus側が常に最初の衝突）
            collisionTime = t_minus;

            // 衝突位置の決定
            collisionSourcePos = prevSourcePos + t_minus * (sourcePos - prevSourcePos);
            collisionTargetPos = prevTargetPos + t_minus * (targetPos - prevTargetPos);

            return true; // 衝突報告
        }
    }
}
