﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Storage;

namespace GameLibrary
{
    /// <summary>
    /// セーブデータ
    /// </summary>
    public class SaveData
    {
        public List<int> Score;
    }


    public abstract class SaveDataHandler
    {
        #region 定数
        protected static readonly string EncryptKey = "1m0fLyvz6gidyX1NajMqPZUjxrBdrwPB";
        protected static readonly int EncryptPasswordCount = 16;
        protected static readonly string PasswordChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        protected static readonly int PasswordCharsLength = PasswordChars.Length;
        protected static readonly string SavePath = "";
        protected static readonly string SaveName = "SaveData.sav";
        #endregion

        protected StorageDevice storageDevice;
        protected IAsyncResult asyncResult;
        protected StorageContainer storageContainer;
        protected PlayerIndex controller;
        protected string filename;
        protected string diplayName;
        protected SaveData data;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public SaveDataHandler()
        {
            data = null;
            storageContainer = null;
            storageDevice = null;
            filename = "";
            diplayName = "";
        }

        public SaveData Data
        {
            get { return data; }
            set { data = value; }
        }

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="filename">ファイル名</param>
        /// <param name="displayName"></param>
        /// <param name="data">セーブデータ</param>
        /// <param name="controller"></param>
        public void Initialize(string displayName, SaveData data, PlayerIndex controller)
        {
            this.data = data;
            this.filename = SaveName;
            this.diplayName = displayName;
            this.controller = controller;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        public void Update()
        {
#if XBOX
            if(!Guide.IsVisible)
#endif
            {
                asyncResult = StorageDevice.BeginShowSelector(controller, null, null);
            }
            if (asyncResult.IsCompleted)
            {
                // デバイス選択を閉じる
                storageDevice = StorageDevice.EndShowSelector(asyncResult);
                if (storageDevice != null && storageDevice.IsConnected)
                {
                    // 指定されたゲームのコンテナを開く
                    asyncResult = storageDevice.BeginOpenContainer(diplayName, null, null);

                    asyncResult.AsyncWaitHandle.WaitOne();

                    // コンテナが開いたので、処理を終了
                    storageContainer = storageDevice.EndOpenContainer(asyncResult);

                    asyncResult.AsyncWaitHandle.Close();

                    try
                    {
                        // 継承されたクラスで行われる、ファイル処理
                        Process();
                    }
                    catch (IOException e)
                    {
                        OnError(e.Message);
                    }
                    finally
                    {
                        // コンテナを破棄
                        storageContainer.Dispose();
                        storageContainer = null;
                        filename = "";
                        diplayName = "";
                    }
                }
            }
        }

        /// <summary>
        /// AES暗号化
        /// </summary>
        /// <param name="src"></param>
        /// <param name="iv"></param>
        /// <param name="dst"></param>
        public static void EncryptAes(byte[] src, out string iv, out byte[] dst)
        {
            iv = CreatePassword(EncryptPasswordCount);
            dst = null;

            using (RijndaelManaged rijndael = new RijndaelManaged())
            {
                rijndael.Padding = PaddingMode.PKCS7;
                rijndael.Mode = CipherMode.CBC;
                rijndael.KeySize = 256;
                rijndael.BlockSize = 128;

                byte[] key = Encoding.UTF8.GetBytes(EncryptKey);
                byte[] vec = Encoding.UTF8.GetBytes(iv);

                using (ICryptoTransform encrypt = rijndael.CreateEncryptor(key, vec))
                {
                    using (MemoryStream msEncrypt = new MemoryStream())
                    {
                        using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encrypt, CryptoStreamMode.Write))
                        {
                            csEncrypt.Write(src, 0, src.Length);
                            csEncrypt.FlushFinalBlock();

                            dst = msEncrypt.ToArray();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// AES復号化
        /// </summary>
        /// <param name="src"></param>
        /// <param name="iv"></param>
        /// <param name="dst"></param>
        public static void DecryptAes(byte[] src, string iv, out byte[] dst)
        {
            dst = new byte[src.Length];

            using (RijndaelManaged rijndael = new RijndaelManaged())
            {
                rijndael.Padding = PaddingMode.PKCS7;
                rijndael.Mode = CipherMode.CBC;
                rijndael.KeySize = 256;
                rijndael.BlockSize = 128;

                byte[] key = Encoding.UTF8.GetBytes(EncryptKey);
                byte[] vec = Encoding.UTF8.GetBytes(iv);

                using (ICryptoTransform decryptor = rijndael.CreateDecryptor(key, vec))
                {
                    using (MemoryStream msDecrypt = new MemoryStream(src))
                    {
                        using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            csDecrypt.Read(dst, 0, dst.Length);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// セーブデータ削除
        /// </summary>
        /// <param name="filename"></param>
        protected void DeletExistingData(string filename)
        {
            // 指定されたファイルが存在した場合消去
            if (storageContainer.FileExists(filename))
                storageContainer.DeleteFile(filename);
        }

        /// <summary>
        /// パスワード生成
        /// </summary>
        /// <param name="count">文字列数</param>
        /// <returns>パスワード</returns>
        public static string CreatePassword(int count)
        {
            StringBuilder stringBuilder = new StringBuilder(count);
            Random rand = new Random();

            for (int index = count - 1; index >= 0; --index)
            {
                char c = PasswordChars[rand.Next(0, PasswordCharsLength)];
                stringBuilder.Append(c);
            }

            return stringBuilder.ToString();
        }

        protected virtual void OnError(string exceptionMessage)
        {
            Debug.WriteLine(exceptionMessage);
        }

        protected abstract void Process();
    }
}