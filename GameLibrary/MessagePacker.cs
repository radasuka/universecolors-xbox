﻿using System.IO;
using MsgPack.Serialization;

namespace GameLibrary
{
    public static class MessagePacker
    {
        
        /// <summary>
        /// object -> MessagePack(byte[])
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static byte[] Object2MessagePack<T>(this object obj)
        {
            var serializer = MessagePackSerializer.Get<T>();
            var memoryStream = new MemoryStream();

            serializer.Pack(memoryStream, (T)obj);

            var ret = memoryStream.ToArray();
            memoryStream.Close();

            return ret;
        }

        /// <summary>
        /// MessagePack(byte[]) -> object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static T MessagePack2Object<T>(this byte[] bytes)
        {
            var serializer = MessagePackSerializer.Get<T>();
            var memoryStream = new MemoryStream(bytes);
            var ret = serializer.Unpack(memoryStream);
            memoryStream.Close();

            return ret;
        }
    }
}
