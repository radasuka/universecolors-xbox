﻿using Microsoft.Xna.Framework;

namespace GameLibrary
{
    /// <summary>
    /// 状態を表すインターフェイス
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IState<T>
    {
        void Update(GameTime gameTime, T item);
    }
}
