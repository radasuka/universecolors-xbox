﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GameLibrary
{
    public class XYZAxisDraw
    {
        private VertexBuffer vertexBuffer;

        private BasicEffect basicEffect;

        public XYZAxisDraw(GraphicsDevice device)
        {
            basicEffect = new BasicEffect(device);
            basicEffect.VertexColorEnabled = true;

            // 頂点バッファを作成する。
            vertexBuffer = new VertexBuffer(device, typeof(VertexPositionColor), 6, BufferUsage.None);

            // 頂点データを作成する。
            VertexPositionColor[] vertices = new VertexPositionColor[6];

            vertices[0] = new VertexPositionColor(new Vector3(0.0f, 0.0f, 0.0f), Color.Red);
            vertices[1] = new VertexPositionColor(new Vector3(50000.0f, 0.0f, 0.0f), Color.Red);
            vertices[2] = new VertexPositionColor(new Vector3(0.0f, 0.0f, 0.0f), Color.Green);
            vertices[3] = new VertexPositionColor(new Vector3(0.0f, 50000.0f, 0.0f), Color.Green);
            vertices[4] = new VertexPositionColor(new Vector3(0.0f, 0.0f, 0.0f), Color.Blue);
            vertices[5] = new VertexPositionColor(new Vector3(0.0f, 0.0f, 50000.0f), Color.Blue);

            // 頂点データを書き込む
            this.vertexBuffer.SetData(vertices);
        }

        public void Draw(GraphicsDevice device, Matrix view, Matrix projection)
        {
            device.SetVertexBuffer(this.vertexBuffer);

            basicEffect.VertexColorEnabled = true;
            // 追尾カメラの行列を使用します。
            

            foreach (EffectPass pass in basicEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                basicEffect.View = view;
                basicEffect.Projection = projection;
                device.DrawPrimitives(PrimitiveType.LineList, 0, 3);
            }
        }
    }
}
