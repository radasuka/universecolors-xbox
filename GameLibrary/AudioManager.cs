﻿using Microsoft.Xna.Framework.Audio;

namespace GameLibrary
{
    public class AudioManager
    {
        private static AudioManager _instance;
        AudioEngine audioEngine;
        SoundBank soundBank;
        WaveBank waveBank;
        AudioCategory audioCategory;

        public static AudioManager Instance { get { return _instance ?? (_instance = new AudioManager()); } }

        private AudioManager()
        {
            audioEngine = new AudioEngine("Content\\Sounds\\UniverseColorsSound.xgs");
            soundBank = new SoundBank(audioEngine, "Content\\Sounds\\BGM.xsb");
            waveBank = new WaveBank(audioEngine, "Content\\Sounds\\Wave Bank.xwb");
        }

        public void SetCategory(string CategoryName, float Volume = 1.0f)
        {
            audioCategory = audioEngine.GetCategory(CategoryName);
            audioCategory.SetVolume(Volume);
        }

        public Cue GetCue(string CueName)
        {
            return soundBank.GetCue(CueName);
        }

        public void Stop(Cue cue)
        {
            if (cue.IsPlaying)
                cue.Stop(AudioStopOptions.Immediate);
        }

        public void Update()
        {
            audioEngine.Update();
        }
    }
}
