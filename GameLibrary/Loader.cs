﻿using System.IO;
using System.Text;

namespace GameLibrary
{
    public class Loader : SaveDataHandler
    {
        public Loader()
            : base() { }

        protected override void OnError(string exceptionMessage)
        {
            base.OnError(exceptionMessage);
        }

        /// <summary>
        /// 読み込み
        /// </summary>
        protected override void Process()
        {
            // ファイルが存在しない場合終了
            if (storageContainer.FileExists(filename))
            {
                // 読み込み
                byte[] ivBytes = null;
                byte[] data = null;
                using (Stream stream = storageContainer.OpenFile(filename, FileMode.Open))
                {
                    using (BinaryReader reader = new BinaryReader(stream))
                    {
                        int length = reader.ReadInt32();
                        ivBytes = reader.ReadBytes(length);

                        length = reader.ReadInt32();
                        data = reader.ReadBytes(length);
                    }
                }

                // 復号化
                string iv = Encoding.UTF8.GetString(ivBytes);
                byte[] pack;
                DecryptAes(data, iv, out pack);

                // セーブデータ復元
                var obj = MessagePacker.MessagePack2Object<SaveData>(pack);
                this.Data = obj;
            }
        }
    }
}