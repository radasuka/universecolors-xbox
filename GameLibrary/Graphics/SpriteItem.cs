﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.Graphics
{
    public class SpriteItem
    {
        #region プロパティ

        /// <summary>
        /// 形状を取得または設定します。
        /// </summary>
        public SpriteShape Shape { get; set; }
        /// <summary>
        /// 拡大・縮小を取得または設定します。
        /// </summary>
        public Vector2 Scale = Vector2.One;
        /// <summary>
        /// 回転を取得または設定します。
        /// </summary>
        public float Rotation { get; set; }
        /// <summary>
        /// 位置座標の加速度を取得または設定します。
        /// </summary>
        public Vector2 Accel;
        /// <summary>
        /// 位置座標の速度を取得または設定します。
        /// </summary>
        public Vector2 Velocity;
        /// <summary>
        /// 位置座標を取得または設定します。
        /// </summary>
        public Vector2 Position;
        /// <summary>
        /// スプライトを着色する色を取得または設定します。
        /// </summary>
        public Color Color = Color.White;

        SpriteEffects spriteEffects = SpriteEffects.None;
        /// <summary>
        /// 適用するエフェクトを取得または設定します。
        /// </summary>
        public SpriteEffects SpriteEffects { get { return spriteEffects; } set { spriteEffects = value; } }

        /// <summary>
        /// テクスチャの領域を取得します。
        /// </summary>
        public Rectangle HitBounds
        {
            get
            {
                // テクスチャーに基づいて矩形を作成します
                return new Rectangle(
                    (int)(Position.X - Shape.Origin.X),
                    (int)(Position.Y - Shape.Origin.Y),
                    (int)(Shape.Texture.Width),
                    (int)(Shape.Texture.Height));
            }
        }

        #endregion

        /// <summary>
        /// <see cref="SpriteItem"/>を更新する必要がある場合に呼び出されます。
        /// </summary>
        /// <param name="gameTime">前回<c>Update</c>が呼び出されてからの経過時間</param>
        public virtual void Update(GameTime gameTime)
        {
            float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            Velocity += Accel * elapsedTime;
            Position += Velocity * elapsedTime + Accel * elapsedTime * elapsedTime * 0.5f;
        }

        /// <summary>
        /// <see cref="SpriteItem"/>を描画する必要がある場合に呼び出されます。
        /// </summary>
        /// <param name="spriteBatch">スプライト描画に使用する<see cref="SpriteBatch"/></param>
        /// <param name="gameTime">前回<c>Update</c>が呼び出されてからの経過時間</param>
        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Draw(Shape.Texture,
                Position,
                Shape.Rect,
                Color,
                Rotation,
                Shape.Origin,
                Scale, SpriteEffects, 0);
        }
    }
}
