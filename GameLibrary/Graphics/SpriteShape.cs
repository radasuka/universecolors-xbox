﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.Graphics
{
    public class SpriteShape
    {
        /// <summary>
        /// テクスチャーを取得または設定します。
        /// </summary>
        public Texture2D Texture { get; set; }

        /// <summary>
        /// テクスチャーから元のテクセルをテクセル単位で指定する矩形を取得または設定します。
        /// </summary>
        public Rectangle? Rect { get; set; }

        /// <summary>
        /// スプライトの原点を取得または設定します。
        /// </summary>
        public Vector2 Origin = Vector2.Zero;
    }
}