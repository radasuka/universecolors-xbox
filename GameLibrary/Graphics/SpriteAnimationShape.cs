﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace GameLibrary.Graphics
{
    /// <summary>
    /// スプライト アニメーションに使用する形状データ。
    /// </summary>
    public class SpriteAnimationShape
    {
        #region フィールド

        // アニメーション クリップのリスト。
        [ContentSerializer]
        List<SpriteSheet> animationClips = null;

        // 名前でアニメーション クリップを検索できるように、元のスプライト シート
        // ファイル名を保存します。
        [ContentSerializer]
        Dictionary<string, int> clipNames = null;

        /// <summary>
        /// スプライトの原点。
        /// </summary>
        [ContentSerializerIgnore]
        public Vector2 Origin = Vector2.Zero;

        #endregion

        #region プロパティとメソッド

        /// <summary>
        /// この形状に含まれているアニメーション クリップの数を取得します。
        /// </summary>
        public int Count
        {
            get { return this.animationClips.Count; }
        }

        /// <summary>
        /// 指定された名前でアニメーション クリップを検索します。
        /// </summary>
        public SpriteSheet AnimationClip(string clipName)
        {
            int spriteIndex = GetIndex(clipName);

            return animationClips[spriteIndex];
        }

        /// <summary>
        /// 指定された位置のアニメーション クリップを検索します。
        /// </summary>
        /// <param name="clipIndex">アニメーション クリップのインデックス</param>
        /// <returns>検索した<see cref="SpriteSheet"/></returns>
        public SpriteSheet AnimationClip(int clipIndex)
        {
            if ((clipIndex < 0) || (clipIndex >= animationClips.Count))
                throw new ArgumentOutOfRangeException("clipIndex");

            return animationClips[clipIndex];
        }

        /// <summary>
        /// 指定されたアニメーション クリップの数値インデックスを検索します。
        /// </summary>
        /// <remarks>
        /// 名前より数値インデックスによる検索の方がパフォーマンス的に有利なため、
        /// 事前にインデックスを保存しておくと便利です。
        /// </remarks>
        public int GetIndex(string spriteName)
        {
            int index;

            if (!clipNames.TryGetValue(spriteName, out index))
            {
                string error = "'{0}'という名前のアニメーション クリップは存在しません。";

                throw new KeyNotFoundException(string.Format(error, spriteName));
            }

            return index;
        }

        #endregion
    }
}
