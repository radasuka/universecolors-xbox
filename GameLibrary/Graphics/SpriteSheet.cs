#region File Description
//-----------------------------------------------------------------------------
// SpriteSheet.cs
//
// Microsoft Game Technology Group
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace GameLibrary.Graphics
{
    /// <summary>
    /// スプライト シートには、単一の大きなテクスチャーの異なる領域に
    /// パックされた多数の個別のスプライト イメージが、テクスチャー内に
    /// 各スプライトが配置されている場所を示す情報と共に含まれています。
    /// スプライト シートにより、グラフィック ハードウェアが
    /// あるテクスチャーから別のテクスチャーに切り替えなければならない
    /// 回数が減少するのでより効率的にゲームを描画することができます。
    /// </summary>
    public class SpriteSheet
    {
        // 単一のテクスチャーには、多くの別個のスプライト イメージが
        // 含まれています。
        [ContentSerializer]
        Texture2D texture = null;

        // 各スプライトがテクスチャー内に配置された場所を忘れないでください。
        [ContentSerializer]
        List<Rectangle> spriteRectangles = null;

        // 名前でスプライトを検索できるように、元のスプライト ファイル名を
        // 保存します。
        [ContentSerializer]
        Dictionary<string, int> spriteNames = null;

        /// <summary>
        /// スプライトの原点。
        /// </summary>
        [ContentSerializerIgnore]
        public Vector2 Origin = Vector2.Zero;

        /// <summary>
        /// このスプライト シートによって使用される単一の大きなテクスチャーを
        /// 取得します。
        /// </summary>
        public Texture2D Texture
        {
            get { return texture; }
        }

        /// <summary>
        /// このスプライト シートに含まれるスプライトの数を取得します。
        /// </summary>
        public int Count
        {
            get { return this.spriteRectangles.Count; }
        }


        /// <summary>
        /// 大きなテクスチャー内の指定されたスプライトの位置を検索します。
        /// </summary>
        public Rectangle SourceRectangle(string spriteName)
        {
            int spriteIndex = GetIndex(spriteName);

            return spriteRectangles[spriteIndex];
        }


        /// <summary>
        /// 大きなテクスチャー内の指定されたスプライトの位置を検索します。
        /// </summary>
        public Rectangle SourceRectangle(int spriteIndex)
        {
            if ((spriteIndex < 0) || (spriteIndex >= spriteRectangles.Count))
                throw new ArgumentOutOfRangeException("spriteIndex");

            return spriteRectangles[spriteIndex];
        }


        /// <summary>
        /// 指定されたスプライトの数値インデックスを検索します。
        /// これは、一連の関連するスプライトを繰り返し処理することで、
        /// アニメーションを実装する場合に便利です。
        /// </summary>
        public int GetIndex(string spriteName)
        {
            int index;

            if (!spriteNames.TryGetValue(spriteName, out index))
            {
                string error = "SpriteSheet does not contain a sprite named '{0}'.";

                throw new KeyNotFoundException(string.Format(error, spriteName));
            }

            return index;
        }
    }
}
