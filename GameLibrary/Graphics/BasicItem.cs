﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.Graphics
{
    public class BasicItem
    {
        private const float ThrustForce = 24000.0f;

        #region フィールド

        /// <summary>
        /// 描画に使うモデル。
        /// </summary>
        public virtual Model Shape { get; set; }
        /// <summary>
        /// ｘ、y、およびz軸上でのスケーリングの程度を取得または設定します。
        /// </summary>
        public Vector3 Scales = Vector3.One;
        /// <summary>
        /// 回転クオータニオンを取得または設定します。
        /// </summary>
        public Quaternion Rotation = Quaternion.Identity;
        /// <summary>
        /// y 軸のヨーです (ラジアン単位)。
        /// </summary>
        public float Yaw = 0;
        /// <summary>
        /// x 軸のピッチです (ラジアン単位)。
        /// </summary>
        public float Pitch = 0;
        /// <summary>
        /// z 軸のロールです (ラジアン単位)。
        /// </summary>
        public float Roll = 0;
        /// <summary>
        /// ワールド空間における位置。
        /// </summary>
        public Vector3 Position = Vector3.Zero;
        /// <summary>
        /// 直前のフレームを位置座標を取得または設定します。
        /// </summary>
        public Vector3 PreviousPosition = Vector3.Zero;
        /// <summary>
        /// 加速度。
        /// </summary>
        public Vector3 Acceleration = Vector3.Zero;
        /// <summary>
        /// 現在の速度。
        /// </summary>
        public Vector3 Velocity = Vector3.Zero;
        ///// <summary>
        ///// 向いている方向
        ///// </summary>
        //public Vector3 Direction = -Vector3.Forward;
        ///// <summary>
        ///// 上方向ベクトル
        ///// </summary>
        //public Vector3 Up = Vector3.Up;

        //private Vector3 right = Vector3.Right;
        ///// <summary>
        ///// 右方向ベクトル
        ///// </summary>
        //public Vector3 Right
        //{
        //    get { return right; }
        //}

        private Matrix world = Matrix.Identity;
        /// <summary>
        /// ワールド トランスフォーム行列。
        /// </summary>
        public Matrix World
        {
            get { return world; }
            protected set { world = value; }
        }

        /// <summary>
        /// 衝突判定用の球体
        /// </summary>
        private BoundingSphere boundingSphere;
        public BoundingSphere BoundingSphere { get { return boundingSphere; } }

        #endregion

        //public virtual void Reset()
        //{
        //    Direction = Vector3.Up;
        //    Up = Vector3.Up;
        //    right = Vector3.Right;
        //    Velocity = Vector3.Zero;
        //    InitializeBoundingSphere();
        //}

        /// <summary>
        /// バウンディングスフィアの初期化
        /// </summary>
        public virtual void InitializeBoundingSphere()
        {
            boundingSphere = this.Shape.Meshes[0].BoundingSphere;
            boundingSphere.Center = this.Position;
        }

        /// <summary>
        /// 更新処理をします。
        /// </summary>
        /// <param name="gameTime">前回のフレームからの経過時間</param>
        public virtual void Update(GameTime gameTime)
        {
            float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            //// Yaw、Pitch、Rollから回転クォータニオンを作成します。
            //Quaternion.CreateFromYawPitchRoll(Yaw, Pitch, Roll, out Rotation);
            //// 回転クォータニオンから回転行列を作成します。
            //Matrix rotationMatrix = Matrix.CreateFromQuaternion(Rotation);

            //// 方向ベクトルを回転します。
            //Direction = Vector3.TransformNormal(Direction, rotationMatrix);
            //Up = Vector3.TransformNormal(Up, rotationMatrix);

            //// 方向ベクトルを再度正規化します。
            //Direction.Normalize();
            //Up.Normalize();

            //// 右方向ベクトルを再計算します。
            //right = Vector3.Cross(Direction, Up);
            //// 直行を確保するため Up ベクトルを再計算します。
            //Up = Vector3.Cross(Right, Direction);

            // 直前の位置座標
            PreviousPosition = Position;

            //// 推進量から力を計算します。
            //Vector3 force = Direction * 1.0f * ThrustForce;

            //// 加速を適用
            //Acceleration = force / Mass;
            Velocity += Acceleration * elapsedTime;
            // 速度を適用します。
            Position += Velocity * elapsedTime;

            boundingSphere.Center = this.Position;

            //World = Matrix.CreateScale(Scales) * Matrix.CreateTranslation(Position);

            UpdateDrawParamaters(gameTime);
        }

        /// <summary>
        /// 描画パラメータを更新します。
        /// </summary>
        /// <param name="gameTime">前回のフレームからの経過時間</param>
        public void UpdateDrawParamaters(GameTime gameTime)
        {
            // ワールド行列を再構築します。
            Quaternion.CreateFromYawPitchRoll(Yaw, Pitch, Roll, out Rotation);
            World = Matrix.CreateScale(Scales) * Matrix.CreateFromQuaternion(Rotation) * Matrix.CreateTranslation(Position);
        }
    }
}
