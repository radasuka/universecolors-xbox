﻿using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.Graphics
{
    /// <summary>
    /// スプライト アニメーション機能を提供するスクリーン アイテム。
    /// </summary>
    public class SpriteAnimationItem : SpriteItem
    {
        #region プロパティ

        SpriteAnimationShape shape;
        /// <summary>
        /// スプライト アニメーションに使用する形状データを取得または設定します。
        /// </summary>
        public new SpriteAnimationShape Shape
        {
            get { return shape; }
            set
            {
                // 現在のクリップを初期化しておく。
                StartClip(value.AnimationClip(0));
                shape = value;
            }
        }

        int animationFramesPerSecond = 10;
        /// <summary>
        /// 再生するFPSを取得または設定します。
        /// </summary>
        public int AnimationFramesPerSecond
        { get { return animationFramesPerSecond; } set { animationFramesPerSecond = value; } }

        /// <summary>
        /// 現在再生しているアニメーション クリップを取得します。
        /// </summary>
        public SpriteSheet CurrentClip { get; private set; }

        /// <summary>
        /// 現在の再生位置を取得します。
        /// </summary>
        public float CurrentTime { get; private set; }

        // 現在再生しているスプライト アニメーション内の位置
        int currentSprite;

        /// <summary>
        /// アニメーションの一時停止フラグ
        /// </summary>
        public bool IsPaused { get; set; }

        #endregion

        /// <summary>
        /// 指定したアニメーション クリップの再生を開始します。
        /// </summary>
        /// <param name="clip">アニメーション クリップ</param>
        public void StartClip(SpriteSheet clip)
        {
            Debug.Assert(clip != null, "clip");

            CurrentClip = clip;
            CurrentTime = 0;
            currentSprite = 0;
            IsPaused = false;
        }

        /// <summary>
        /// このスクリーン アイテムを更新する必要がある場合に呼び出されます。
        /// </summary>
        /// <param name="gameTime">前回<c>Update</c>が呼び出されてからの経過時間</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            // インデックスを変更し、アニメーションの現在のフレームを選択します。
            if (!IsPaused)
            {
                CurrentTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
                currentSprite = (int)(CurrentTime * AnimationFramesPerSecond) % CurrentClip.Count;
            }
        }

        /// <summary>
        /// このスクリーン アイテムを描画する必要がある場合に呼び出されます。
        /// </summary>
        /// <param name="spriteBatch">スプライト描画に使用する<see cref="SpriteBatch"/></param>
        /// <param name="gameTime">前回<c>Update</c>が呼び出されてからの経過時間</param>
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Draw(CurrentClip.Texture,
                             Position,
                             CurrentClip.SourceRectangle(currentSprite),
                             Color,
                             Rotation,
                             Shape.Origin,
                             Scale, SpriteEffects, 0);
        }
    }
}