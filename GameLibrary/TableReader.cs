using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework.Content;

namespace GameLibrary
{
    /// <summary>
    /// このクラスは XNA Framework コンテンツ パイプラインによってインスタンス化され、
    /// 指定された<see cref="Table"/>型を xnb 形式のバイナリ ファイルから読み込みます。
    /// </summary>
    public class TableReader : ContentTypeReader<Table>
    {
        /// <summary>
        /// 現在のストリームから、<see cref="Table"/>オブジェクトを読み込みます。
        /// </summary>
        /// <param name="input">オブジェクトの読み込みに使う<see cref="ContentReader"/>。</param>
        /// <param name="existingInstance">読み込み先として使う既存のオブジェクト。</param>
        /// <returns>オブジェクトのタイプ</returns>
        protected override Table Read(ContentReader input, Table existingInstance)
        {
            Table retVal = new Table();
            retVal.Name = Path.GetFileName(input.AssetName);
            retVal.Rows = input.ReadObject<List<List<string>>>();

            return retVal;
        }
    }
}
