﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using Microsoft.Xna.Framework.Content;

namespace GameLibrary
{
    /// <summary>
    /// 表データ。
    /// </summary>
    public class Table
    {
        /// <summary>
        /// リソースの名前を取得または設定します。
        /// </summary>
        [ContentSerializer]
        public string Name { get; set; }

        /// <summary>
        /// このテーブルに属する行のコレクションを取得します。
        /// </summary>
        [ContentSerializer]
        public List<List<string>> Rows { get; set; }

        /// <summary>
        /// 表の列数を取得します。
        /// </summary>
        public int ColumnCount { get { return Rows[0].Count; } }

        /// <summary>
        /// 表の行数を取得します。
        /// </summary>
        public int RowCount { get { return Rows.Count; } }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public void SaveToCsv(string filename)
        {
            using (StreamWriter writer = new StreamWriter(filename))
            {
                // 文字列の書き込み
                for (int y = 0; y < this.Rows.Count; y++)
                {
                    for (int x = 0; x < this.Rows[0].Count; x++)
                    {
                        if (x != 0)
                            writer.Write(",");
                        writer.Write(this.Rows[y][x].ToString());
                    }
                    writer.WriteLine();
                }

                // StreamWriter を閉じる
                writer.Close();
            }
        }
    }
}