﻿using System.IO;
using System.Text;

namespace GameLibrary
{
    public class Saver : SaveDataHandler
    {
        public Saver()
            : base() { }

        protected override void OnError(string exceptionMessage)
        {
            base.OnError(exceptionMessage);
        }

        /// <summary>
        /// 保存
        /// </summary>
        protected override void Process()
        {
            // 存在するセーブデータを削除
            DeletExistingData(filename);

            // 暗号化
            byte[] pack = MessagePacker.Object2MessagePack<SaveData>(this.Data);
            //byte[] pack = this.Data.Object2MessagePack<SaveData>();
            string iv;
            byte[] data;
            EncryptAes(pack, out iv, out data);

            // 保存
            byte[] ivBytes = Encoding.UTF8.GetBytes(iv);
            using (Stream stream = storageContainer.CreateFile(filename))
            {
                using (BinaryWriter bw = new BinaryWriter(stream))
                {
                    bw.Write(ivBytes.Length);
                    bw.Write(ivBytes);

                    bw.Write(data.Length);
                    bw.Write(data);
                }
            }

        }
    }
}
