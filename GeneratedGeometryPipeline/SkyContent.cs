#region File Description
//-----------------------------------------------------------------------------
// SkyContent.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Microsoft.Xna.Framework.Content;
#endregion

namespace GeneratedGeometryPipeline
{
    /// <summary>
    /// スカイドームを保持するためのデザインタイム クラス。このクラスは、
    /// SkyProcessor によって作成され、コンパイル済みの XNB ファイルに
    /// 書き出されます。実行時に、データがランタイム Sky クラスに
    /// 読み込まれます。
    /// </summary>
    [ContentSerializerRuntimeType("ChaseCameraSample.Sky, ChaseCameraSample")]
    public class SkyContent
    {
        public ModelContent Model;
        public TextureContent Texture;
    }
}
