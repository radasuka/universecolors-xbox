#region File Description
//-----------------------------------------------------------------------------
// TerrainProcessor.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
#endregion

namespace GeneratedGeometryPipeline
{
    /// <summary>
    /// 地形メッシュを作成するためのカスタム コンテンツ プロセッサ。入力高さ
    /// フィールド テクスチャーが与えられると、このプロセッサは MeshBuilder 
    /// クラスを使用して地形ジオメトリをプログラムによって生成します。
    /// </summary>
    [ContentProcessor]
    public class TerrainProcessor : ContentProcessor<Texture2DContent, ModelContent>
    {
        const float terrainScale = 16000;
        const float terrainBumpiness = 115000;
        const float texCoordScale = 1/128f;
        const string terrainTexture = "rocks.bmp";


        /// <summary>
        /// 入力高さフィールド テクスチャーから地形メッシュを生成します。
        /// </summary>
        public override ModelContent Process(Texture2DContent input,
                                             ContentProcessorContext context)
        {
            MeshBuilder builder = MeshBuilder.StartMesh("terrain");

            // 処理を簡単にするために、浮動小数点フォーマットに入力テクスチャーを変換します。
            input.ConvertBitmapType(typeof(PixelBitmapContent<float>));

            PixelBitmapContent<float> heightfield;
            heightfield = (PixelBitmapContent<float>)input.Mipmaps[0];

            // 地形の頂点を作成します。
            for (int y = 0; y < heightfield.Height; y++)
            {
                for (int x = 0; x < heightfield.Width; x++)
                {
                    Vector3 position;

                    position.X = (x - heightfield.Width / 2) * terrainScale;
                    position.Z = (y - heightfield.Height / 2) * terrainScale;

                    position.Y = (heightfield.GetPixel(x, y) - 1) * terrainBumpiness;

                    builder.CreatePosition(position);
                }
            }

            // マテリアルを作成し、それを地形テクスチャーに向けます。
            BasicMaterialContent material = new BasicMaterialContent();

            string directory = Path.GetDirectoryName(input.Identity.SourceFilename);
            string texture = Path.Combine(directory, terrainTexture);

            material.Texture = new ExternalReference<TextureContent>(texture);

            builder.SetMaterial(material);

            // テクスチャー座標を保持するための頂点チャンネルを作成します。
            int texCoordId = builder.CreateVertexChannel<Vector2>(
                                            VertexChannelNames.TextureCoordinate(0));

            // 地形を構成する個々のトライアングルを作成します。
            for (int y = 0; y < heightfield.Height - 1; y++)
            {
                for (int x = 0; x < heightfield.Width - 1; x++)
                {
                    AddVertex(builder, texCoordId, heightfield.Width, x, y);
                    AddVertex(builder, texCoordId, heightfield.Width, x + 1, y);
                    AddVertex(builder, texCoordId, heightfield.Width, x + 1, y + 1);

                    AddVertex(builder, texCoordId, heightfield.Width, x, y);
                    AddVertex(builder, texCoordId, heightfield.Width, x + 1, y + 1);
                    AddVertex(builder, texCoordId, heightfield.Width, x, y + 1);
                }
            }

            // 生成したばかりのメッシュを変換できるように、ModelProcessor にチェーンします。
            MeshContent terrainMesh = builder.FinishMesh();
            
            return context.Convert<MeshContent, ModelContent>(terrainMesh,
                                                              "ModelProcessor");
        }

        /// <summary>
        /// 関連付けられたテクスチャー座標値とともに新しいトライアングル
        /// 頂点を MeshBuilder に追加するためのヘルパー。
        /// </summary>
        static void AddVertex(MeshBuilder builder, int texCoordId, int w, int x, int y)
        {
            builder.SetVertexChannelData(texCoordId, new Vector2(x, y) * texCoordScale);

            builder.AddTriangleVertex(x + y * w);
        }
    }
}
