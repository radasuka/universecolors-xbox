#region File Description
//-----------------------------------------------------------------------------
// SkyProcessor.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
#endregion

namespace GeneratedGeometryPipeline
{
    /// <summary>
    /// スカイドーム メッシュを作成するためのカスタム コンテンツ プロセッサ。処理しようとする
    /// 空のテクスチャーが与えられると、このプロセッサは MeshBuilder クラスを使用して
    /// スカイドーム ジオメトリをプログラムによって生成します。円柱を作成し、
    /// テクスチャーが周囲に空のイメージをマッピングし、空をレンダリングするために
    /// 使用するカスタム エフェクトを割り当てます。
    /// </summary>
    [ContentProcessor]
    public class SkyProcessor : ContentProcessor<Texture2DContent, SkyContent>
    {
        const float cylinderSize = 100;

        const int cylinderSegments = 32;
        
        const float texCoordTop = 0.1f;
        const float texCoordBottom = 0.9f;

        
        /// <summary>
        /// 処理しようとする空のテクスチャーのスカイドーム ジオメトリを生成します。
        /// </summary>
        public override SkyContent Process(Texture2DContent input,
                                           ContentProcessorContext context)
        {
            MeshBuilder builder = MeshBuilder.StartMesh("sky");

            // 円柱の上面と底面の周囲に 2 つの頂点のリングを作成します。
            List<int> topVertices = new List<int>();
            List<int> bottomVertices = new List<int>();

            for (int i = 0; i < cylinderSegments; i++)
            {
                float angle = MathHelper.TwoPi * i / cylinderSegments;

                float x = (float)Math.Cos(angle) * cylinderSize;
                float z = (float)Math.Sin(angle) * cylinderSize;

                topVertices.Add(builder.CreatePosition(x, cylinderSize, z));
                bottomVertices.Add(builder.CreatePosition(x, -cylinderSize, z));
            }

            // 上面と底面を閉じるために使用する 2 つの中心頂点を作成します。
            int topCenterVertex = builder.CreatePosition(0, cylinderSize * 2, 0);
            int bottomCenterVertex = builder.CreatePosition(0, -cylinderSize * 2, 0);

            // テクスチャー座標を保持するための頂点チャンネルを作成します。
            int texCoordId = builder.CreateVertexChannel<Vector2>(
                                            VertexChannelNames.TextureCoordinate(0));

            builder.SetMaterial(new BasicMaterialContent());

            // スカイドームを構成する個々のトライアングルを作成します。
            for (int i = 0; i < cylinderSegments; i++)
            {
                int j = (i + 1) % cylinderSegments;

                // この円柱のセグメントのテクスチャー座標を計算します。
                float u1 = (float)i / (float)cylinderSegments;
                float u2 = (float)(i + 1) / (float)cylinderSegments;

                // 2 つのトライアングルが円柱の一つの側面にクワッドを形成します。
                AddVertex(builder, topVertices[i], texCoordId, u1, texCoordTop);
                AddVertex(builder, topVertices[j], texCoordId, u2, texCoordTop);
                AddVertex(builder, bottomVertices[i], texCoordId, u1, texCoordBottom);

                AddVertex(builder, topVertices[j], texCoordId, u2, texCoordTop);
                AddVertex(builder, bottomVertices[j], texCoordId, u2, texCoordBottom);
                AddVertex(builder, bottomVertices[i], texCoordId, u1, texCoordBottom);

                // このセグメントの上にある上面を満たすために内側に広がるトライアングル。
                AddVertex(builder, topCenterVertex, texCoordId, u1, 0);
                AddVertex(builder, topVertices[j], texCoordId, u2, texCoordTop);
                AddVertex(builder, topVertices[i], texCoordId, u1, texCoordTop);

                // このセグメントの下にある底面を満たすために内側に広がるトライアングル。
                AddVertex(builder, bottomCenterVertex, texCoordId, u1, 1);
                AddVertex(builder, bottomVertices[i], texCoordId, u1, texCoordBottom);
                AddVertex(builder, bottomVertices[j], texCoordId, u2, texCoordBottom);
            }

            // 出力オブジェクトを作成します。
            SkyContent sky = new SkyContent();

            // 生成したばかりのメッシュを変換できるように、ModelProcessor にチェーンします。
            MeshContent skyMesh = builder.FinishMesh();

            sky.Model = context.Convert<MeshContent, ModelContent>(skyMesh,
                                                                   "ModelProcessor");

            // 空のテクスチャーを変換できるように、TextureProcessor に
            // チェーンします。通常、空のイメージでは見栄えが
            // あまりよくない DXT 圧縮が適用されるので、ここでは、
            // 既定の ModelTextureProcessor を使用しません。

            // 注意: これは、既定の TextureProcessor によって、そのテクスチャーを
            // 処理するカスタム ModelProcessor を作成し、空のテクスチャーを
            // マテリアルの Textures 辞書に追加することで、実現することもできます。
            // 簡潔にするため、代わりに次の方法を使用します。
            sky.Texture = context.Convert<TextureContent, TextureContent>(input,
                                                                    "TextureProcessor");

            return sky;
        }


        /// <summary>
        /// 関連付けられたテクスチャー座標値とともに新しいトライアングル
        /// 頂点を MeshBuilder に追加するためのヘルパー。
        /// </summary>
        static void AddVertex(MeshBuilder builder, int vertex,
                              int texCoordId, float u, float v)
        {
            builder.SetVertexChannelData(texCoordId, new Vector2(u, v));
            
            builder.AddTriangleVertex(vertex);
        }
    }
}
