#region File Description
//-----------------------------------------------------------------------------
// ParticleSystemSettings.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation.All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace ParticlesSettings
{
    /// <summary>
    /// パーティクル システムの加速方法の指定に使用します。
    /// </summary>
    public enum AccelerationMode
    {
        /// <summary>
        /// パーティクル システムは加速を使用しません。
        /// </summary>
        None,

        /// <summary>
        /// パーティクル システムは加速を計算します。その際、
        /// MinAccelerationScale 値と MaxAccelerationScale 値を使用して
        /// ランダムなスカラー値を計算します。
        /// この値には、パーティクルの方向が乗算されます。
        /// </summary>
        Scalar,

        /// <summary>
        /// パーティクル システムは加速を計算します。その際、EndVelocity 値を
        /// 使用し、方程式 vt = v0 + (a0 * t) を a0 について解きます。
        /// 詳細については、ParticleSystem.cs を参照してください。
        /// </summary>
        EndVelocity,

        /// <summary>
        /// パーティクル システムは加速を計算します。その際、
        /// MinAccelerationVector 値と MaxAccelerationVector 値を使用して
        /// ランダムなベクトル値を計算します。
        /// これは、パーティクルの加速度として使用されます。
        /// </summary>
        Vector
    }

    /// <summary>
    /// Settings クラスには、パーティクル システムの外観の制御に使用する
    /// 調整可能なオプションがすべて定義されています。多くの
    /// 設定は、オプションとして指定する属性でマークされます。それによって、
    /// 既定値を使用する場合に、XML ファイルを単純化することが
    /// できます。
    /// </summary>
    public class ParticleSystemSettings
    {
        // パーティクル システムの使用時に各 "エフェクト" に使用する
        // パーティクルの範囲を設定します。
        public int MinNumParticles;
        public int MaxNumParticles;

        // このパーティクル システムが使用するテクスチャーの名前。  
        public string TextureFilename = null;

        // MinDirectionAngle と MaxDirectionAngle を使用して、パーティクルの
        // 有効な移動方向を制御します。ここではラジアン単位ではなく
        // 360 度の単位を設定に使用して、XML の構築を容易にします。
        // ParticleSystem は、必要に応じて、これらをラジアン単位に変換します。
        [ContentSerializer(Optional = true)]
        public float MinDirectionAngle = 0;
        [ContentSerializer(Optional = true)]
        public float MaxDirectionAngle = 360;

        // MinInitialSpeed と MaxInitialSpeed を使用して、パーティクルの
        // 初期速度を制御します。 
        public float MinInitialSpeed;
        public float MaxInitialSpeed;

        // パーティクルの加速度を計算するモードを設定します。
        public AccelerationMode AccelerationMode = AccelerationMode.None;

        // 存続期間中にパーティクルの速度がどのように変化するかを制御します。
        // 1 に設定すると、パーティクルは作成時と同じ速度を維持します。
        // 0 に設定すると、パーティクルは存続期間の終了時に完全に停止します。
        // 1 よりも大きい値では、パーティクルの速度は時間経過とともに上昇します。
        // このフィールドは、AccelerationMode.EndVelocity モードの使用時に
        // 使用されます。
        [ContentSerializer(Optional = true)]
        public float EndVelocity = 1f;

        // パーティクルの加速度の最小値と最大値を制御します。
        // AccelerationMode.Scalar モード使用時。
        [ContentSerializer(Optional = true)]
        public float MinAccelerationScale = 0;
        [ContentSerializer(Optional = true)]
        public float MaxAccelerationScale = 0;

        // パーティクルの加速度の最小値と最大値を制御します。
        // AccelerationMode.Vector モード使用時。
        [ContentSerializer(Optional = true)]
        public Vector2 MinAccelerationVector = Vector2.Zero;
        [ContentSerializer(Optional = true)]
        public Vector2 MaxAccelerationVector = Vector2.Zero;

        // パーティクルが作成元のオブジェクトの速度に影響を受ける度合いを
        // 制御します。AddParticles は、作成されるパーティクルの基本速度を表わす
        // Vector2 をとります。この速度は、最初にこの
        // EmitterVelocitySensitivity で乗算されて、実際にパーティクルが
        // どれだけ速度の影響を受けるかが決定されます。
        [ContentSerializer(Optional = true)]
        public float EmitterVelocitySensitivity = 0;

        // パーティクルの回転速度を制御する値の範囲。これらの値は
        // 度単位なので、XML のオーサリングが容易です。
        [ContentSerializer(Optional = true)]
        public float MinRotationSpeed = 0;
        [ContentSerializer(Optional = true)]
        public float MaxRotationSpeed = 0;

        // パーティクルの存続期間を制御する値の範囲。
        public float MinLifetime;
        public float MaxLifetime;

        // パーティクルの大きさを制御する値の範囲。
        [ContentSerializer(Optional = true)]
        public float MinSize = 1;
        [ContentSerializer(Optional = true)]
        public float MaxSize = 1;

        // パーティクルに適用する重力を制御します。これによって、
        // パーティクルを落下させて重力をシミュレートしたり、
        // 煙などのエフェクトでは浮き上がらせたり、その他すべての方向に
        // 動かしたりすることができます。        
        [ContentSerializer(Optional = true)]
        public Vector2 Gravity = Vector2.Zero;

        // アルファ ブレンディングのステート。ここでの既定値では、BlendState は
        // BlendState.AlphaBlend と等しくなるため、多くのパーティクル エフェクト
        // に適しています。
        [ContentSerializer(Optional = true)]
        public Blend SourceBlend = Blend.One;
        [ContentSerializer(Optional = true)]
        public Blend DestinationBlend = Blend.InverseSourceAlpha;
    }
}
