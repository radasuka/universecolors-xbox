//-----------------------------------------------------------------------------
// AssemblyInfo.cs
//
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// アセンブリに関する全般的な情報は、以下の属性セットによって制御されます。
// アセンブリに関連付けられている情報を変更するには、これらの属性値を変更します。
[assembly: AssemblyTitle("ParticleSettings")]
[assembly: AssemblyProduct("ParticleSettings")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyCompany("Microsoft")]
[assembly: AssemblyCopyright("Copyright c Microsoft 2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// ComVisible を false に設定すると、このアセンブリ内の型を COM コンポーネントが
// 検知できなくなります。このアセンブリ内の型に COM からアクセスする必要がある場合は、
// その型について ComVisible 属性を true に設定します。
[assembly: ComVisible(false)]

// 次の GUID は、このプロジェクトが COM に公開されている場合の typelib の ID です。
[assembly: Guid("e272f71d-aac8-49e3-8238-61a4c9d7584e")]

// アセンブリのバージョン情報は、次の 4 つの値で構成されています。
//
//      Major Version (メジャー バージョン)
//
//      Minor Version  (マイナー バージョン)
//
//      Build Number (ビルド番号)
//
//      Revision (リビジョン番号)
//
[assembly: AssemblyVersion("1.0.0.0")]
